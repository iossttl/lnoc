//
//  TablePickerViewController.swift
//  CIC1
//
//  Created by Vikram Jagad on 30/01/18.
//  Copyright © 2018 Vikram Jagad. All rights reserved.
//

import UIKit

typealias TablePickerDoneAction1 = (_ selectedArray: [DropDownModel]) -> Void
enum TablePickerSelectionType: Int {
    case single = 0
    case multiple
}

class TablePickerViewController: UIViewController {

    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var roundRectShadowView: UIView!
    @IBOutlet weak var roundRectView: UIView!
    @IBOutlet weak var headerLblView: UIView!
    @IBOutlet weak var headerLbl: UILabel!
    @IBOutlet weak var pickerTableView: UITableView!
    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var doneBtn: UIButton!
    
    @IBOutlet weak var roundRectViewHeightConstraint: NSLayoutConstraint!
    let kCellIdentifier = "TableViewPickerCell"
    var arraySelection: [DropDownModel] = []
    var arrayData: [DropDownModel] = []
    var strHeaderTitle: String = ""
    var tablePickerDoneAction1: TablePickerDoneAction1?
    var selectionType: TablePickerSelectionType = .single
    var selectToDeselectAllRow : DropDownModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        setUpTableView()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        let tblViewHeight = pickerTableView.contentSize.height
        var maxHeight = SCREEN_HEIGHT - 100 - bottomMarginFromSafeArea - STATUS_BAR_HEIGHT
        if let navHeight = navigationController?.navigationBar.frame.size.height {
            maxHeight -= navHeight
        }
        if (tblViewHeight > maxHeight) {
            roundRectViewHeightConstraint.constant = maxHeight - 20
        } else {
            roundRectViewHeightConstraint.constant = pickerTableView.contentSize.height + 100
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewWillLayoutSubviews()
    }
    
    func setUpView() {
        //headerLblView.backgroundColor = .custom_themBlueClr//kColor_HeaderCenter
        
        headerLblView.backgroundColor = .black
        headerLbl.text = strHeaderTitle
        headerLbl.textColor = UIColor.white
        headerLbl.font = UIFont.boldMediumFont
        
//        roundRectView.viewWith(radius: 10, borderColor: .clear, borderWidth: 0)
//        roundRectShadowView.viewWith(radius: 10, borderColor: .clear, borderWidth: 0)
        
        cancelBtn.backgroundColor = .customRedBtnClr
        cancelBtn.setTitle(CommonButtons.cancel.capitalized, for: .normal)
        cancelBtn.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        cancelBtn.setTitleColor(UIColor.white, for: .normal)
//            if #available(iOS 13, *) {
//                cancelBtn.viewWith(radius: 8, borderColor: .systemGray, borderWidth: 1)
//                cancelBtn.setTitleColor(.systemGray, for: .normal)
//            } else {
//                cancelBtn.viewWith(radius: 8, borderColor: .gray, borderWidth: 1)
//                cancelBtn.setTitleColor(.gray, for: .normal)
//            }
        
        doneBtn.setTitle(CommonButtons.ok.capitalized, for: .normal)
        doneBtn.setTitleColor(UIColor.white, for: .normal)
        doneBtn.backgroundColor = .customRedBtnClr
//        doneBtn.viewWith(radius: 8, borderColor: .clear, borderWidth: 0)
        doneBtn.titleLabel?.font = UIFont.systemFont(ofSize: 18)
        if #available(iOS 13.0, *) {
            view.addVibrancyEffect(withEffect: .systemUltraThinMaterial, bringSubViewToFrontView: viewMain)
        } else {
            view.addVibrancyEffect(bringSubViewToFrontView: viewMain)
        }
        self.view.backgroundColor = .clear
        self.viewMain.backgroundColor = .clear
    }
    
    func setTablePickerDoneAction(tablePickerDoneAction: @escaping TablePickerDoneAction1){
        self.tablePickerDoneAction1 = tablePickerDoneAction
    }
    
    @IBAction func cancelBtn(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func doneBtn(_ sender: Any) {
        tablePickerDoneAction1?(arraySelection)
        dismiss(animated: false, completion: nil)
        if arraySelection.count > 0 {
            /*if  let vc = mainStoryboard.instantiateViewController(withIdentifier: K_VC_LrsDetail) as? LrsDetailVC {
                vc.strProjectId = getString(anything: arraySelection[0][key_value])
                appDelegate.rootNavigationVC.pushViewController(vc, animated: true)
            }*/
        }
    }
}

extension TablePickerViewController: UITableViewDelegate, UITableViewDataSource{
    
    func setUpTableView() {
        pickerTableView.register(UINib(nibName: "popupTblViewCell", bundle: nil), forCellReuseIdentifier: "popupTblViewCell")
        pickerTableView.dataSource = self
        pickerTableView.delegate = self
        pickerTableView.register(UITableViewCell.self, forCellReuseIdentifier: kCellIdentifier)
        pickerTableView.estimatedRowHeight = 50
        pickerTableView.rowHeight = UITableView.automaticDimension
        pickerTableView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "popupTblViewCell", for: indexPath) as? popupTblViewCell
        
        cell?.lblTitle?.text = getString(anything: self.arrayData[indexPath.row].title)
        cell?.lblTitle.textColor = .customBlack
        
        if arraySelection.count > 0 {
            if arraySelection.contains(where: { (dict) -> Bool in
                getString(anything: dict.title) == getString(anything: arrayData[indexPath.row].title)
            }) {
                cell?.imgSelection.image = .radio_button_on
                cell?.lblTitle.textColor = .customRedBtnClr
            } else {
                cell?.imgSelection.image = .radio_button_off
                if let deselectId = selectToDeselectAllRow?.id,
                   arraySelection.contains(where: { $0.id == deselectId }) {
                    cell?.lblTitle.textColor = .customSubtitle
                }
            }
        } else {
            cell?.imgSelection.image = .radio_button_off
            if let deselectId = selectToDeselectAllRow?.id,
               arraySelection.contains(where: { $0.id == deselectId }) {
                cell?.lblTitle.textColor = .customSubtitle
            }
        }
        cell?.imgSelection.image = cell?.imgSelection.image?.withRenderingMode(.alwaysTemplate)
        cell?.imgSelection.tintColor = .customBlack
        
    
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch selectionType {
        case .single:
            if (arraySelection.count == 0) {
                arraySelection.append(arrayData[indexPath.row])
            } else {
                let tempArr = arraySelection.filter { (dict) -> Bool in
                    (getString(anything: dict.title) != getString(anything: arrayData[indexPath.row].title))
                }
                if tempArr.count > 0 {
                    arraySelection.removeAll(where: {(getString(anything: $0.title) == getString(anything: tempArr[0].title))})
                }
                arraySelection.append(arrayData[indexPath.row])
            }
        case .multiple:
            let tempArr = arraySelection.filter { (dict) -> Bool in
                (getString(anything: dict.title) != getString(anything: arrayData[indexPath.row].title))
            }
            if (tempArr.count == arraySelection.count) {
                if let deselectId = selectToDeselectAllRow?.id,
                   arraySelection.contains(where: { $0.id == deselectId }) {
                    pickerTableView.reloadData()
                    return
                }
                if arrayData[indexPath.row].id == selectToDeselectAllRow?.id {
                    arraySelection = [arrayData[indexPath.row]]
                } else {
                    arraySelection.append(arrayData[indexPath.row])
                }
            } else {
                arraySelection = tempArr
            }
        }
        pickerTableView.reloadData()
    }
}
