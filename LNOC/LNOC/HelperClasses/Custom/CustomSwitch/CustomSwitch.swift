//
//  CustomSwitch.swift
//  LSAttendance
//
//  Created by Vikram Jagad on 30/06/20.
//  Copyright © 2020 Hitesh. All rights reserved.
//

import UIKit

public class CustomSwitch: UIControl {
    
    // MARK: Public properties
    public var animationDelay: Double = 0
    public var animationSpriteWithDamping = CGFloat(0.7)
    public var initialSpringVelocity = CGFloat(0.5)
    public var animationOptions: UIView.AnimationOptions = [UIView.AnimationOptions.curveEaseOut, .beginFromCurrentState, .allowUserInteraction]
    
    public var isOn:Bool = true
    
    public var animationDuration: Double = 0.5
    
    public var padding: CGFloat = 1 {
        didSet {
            self.layoutSubviews()
        }
    }
    
    public var onTintColor: UIColor = UIColor.customOrange {
        didSet {
            self.setupUI()
        }
    }
    
    public var offTintColor: UIColor = UIColor.lightGray.withAlphaComponent(0.4) {
        didSet {
            self.setupUI()
        }
    }
    
    public var cornerRadius: CGFloat {
        
        get {
            return self.privateCornerRadius
        }
        set {
            if newValue > 0.5 || newValue < 0.0 {
                privateCornerRadius = 0.5
            } else {
                privateCornerRadius = newValue
            }
        }
        
    }
    
    private var privateCornerRadius: CGFloat = 0.5 {
        didSet {
            self.layoutSubviews()
        }
    }
    
    // thumb properties
    public var thumbTintColor: UIColor = UIColor.white {
        didSet {
            self.thumbView.backgroundColor = self.thumbTintColor
        }
    }
    
    public var thumbCornerRadius: CGFloat {
        get {
            return self.privateThumbCornerRadius
        }
        set {
            if newValue > 0.5 || newValue < 0.0 {
                privateThumbCornerRadius = 0.5
            } else {
                privateThumbCornerRadius = newValue
            }
        }
        
    }
    
    private var privateThumbCornerRadius: CGFloat = 0.5 {
        didSet {
            self.layoutSubviews()
            
        }
    }
    
    public var thumbSize: CGSize = CGSize.zero {
        didSet {
            self.layoutSubviews()
        }
    }
    
    public var thumbImage:UIImage? = nil {
        didSet {
            guard let image = thumbImage else {
                return
            }
            thumbView.thumbImageView.image = image
        }
    }
    
    public var onImage:UIImage? {
        didSet {
            self.onImageView.image = onImage
            self.layoutSubviews()
            
        }
        
    }
    
    public var offImage:UIImage? {
        didSet {
            self.offImageView.image = offImage
            self.layoutSubviews()
        }
        
    }
    
    
    // dodati kasnije
    public var thumbShadowColor: UIColor = UIColor.black {
        didSet {
            self.thumbView.layer.shadowColor = self.thumbShadowColor.cgColor
        }
    }
    
    public var thumbShadowOffset: CGSize = CGSize(width: 0.75, height: 2) {
        didSet {
            self.thumbView.layer.shadowOffset = self.thumbShadowOffset
        }
    }
    
    public var thumbShaddowRadius: CGFloat = 1.5 {
        didSet {
            self.thumbView.layer.shadowRadius = self.thumbShaddowRadius
        }
    }
    
    public var thumbShaddowOppacity: Float = 0.4 {
        didSet {
            self.thumbView.layer.shadowOpacity = self.thumbShaddowOppacity
        }
    }
    
    // labels
    
    public var labelOff:UILabel = UILabel()
    public var labelOn:UILabel = UILabel()
    
    public var areLabelsShown: Bool = false {
        didSet {
            self.setupUI()
        }
    }
    
    public var thumbView = CustomThumbView(frame: CGRect.zero)
    public var onImageView = UIImageView(frame: CGRect.zero)
    public var offImageView = UIImageView(frame: CGRect.zero)
    public var onPoint = CGPoint.zero
    public var offPoint = CGPoint.zero
    public var isAnimating = false
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupUI()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupUI()
    }
}

// MARK: Private methods
extension CustomSwitch {
    fileprivate func setupUI() {
        // clear self before configuration
        self.clear()
        
        self.clipsToBounds = false
        
        // configure thumb view
        self.thumbView.backgroundColor = self.thumbTintColor
        self.thumbView.isUserInteractionEnabled = false
        
        // dodati kasnije
        self.thumbView.layer.shadowColor = self.thumbShadowColor.cgColor
        self.thumbView.layer.shadowRadius = self.thumbShaddowRadius
        self.thumbView.layer.shadowOpacity = self.thumbShaddowOppacity
        self.thumbView.layer.shadowOffset = self.thumbShadowOffset
        
        self.backgroundColor = self.isOn ? self.onTintColor : self.offTintColor
        
        self.addSubview(self.thumbView)
        self.addSubview(self.onImageView)
        self.addSubview(self.offImageView)
        
        self.setupLabels()
    }
    
    
    private func clear() {
        for view in self.subviews {
            view.removeFromSuperview()
        }
    }
    
    override open func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        super.beginTracking(touch, with: event)
        
        self.animate()
        return true
    }
    
    func setOn(on:Bool, animated:Bool) {
        
        switch animated {
        case true:
            self.animate(on: on)
        case false:
            self.isOn = on
            self.setupViewsOnAction()
            self.completeAction()
        }
    }
    
    fileprivate func animate(on:Bool? = nil) {
        self.isOn = on ?? !self.isOn
        
        self.isAnimating = true
        
        UIView.animate(withDuration: self.animationDuration, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.5, options: [UIView.AnimationOptions.curveEaseOut, UIView.AnimationOptions.beginFromCurrentState, UIView.AnimationOptions.allowUserInteraction], animations: {
            self.setupViewsOnAction()
            
        }, completion: { _ in
            self.completeAction()
        })
    }
    
    private func setupViewsOnAction() {
        self.thumbView.frame.origin.x = self.isOn ? self.onPoint.x : self.offPoint.x
        self.backgroundColor = self.isOn ? self.onTintColor : self.offTintColor
        self.setOnOffImageFrame()
    }
    
    private func completeAction() {
        self.isAnimating = false
        self.sendActions(for: UIControl.Event.valueChanged)
    }
    
}

// Mark: Public methods
extension CustomSwitch {
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        
        if !self.isAnimating {
            self.layer.cornerRadius = self.bounds.size.height * self.cornerRadius
            self.backgroundColor = self.isOn ? self.onTintColor : self.offTintColor
            
            // thumb managment
            // get thumb size, if none set, use one from bounds
            let thumbSize = self.thumbSize != CGSize.zero ? self.thumbSize : CGSize(width: self.bounds.size.height - 2, height: self.bounds.height - 2)
            let yPostition = (self.bounds.size.height - thumbSize.height) / 2
            
            self.onPoint = CGPoint(x: self.bounds.size.width - thumbSize.width - self.padding, y: yPostition)
            self.offPoint = CGPoint(x: self.padding, y: yPostition)
            
            self.thumbView.frame = CGRect(origin: self.isOn ? self.onPoint : self.offPoint, size: thumbSize)
            self.thumbView.layer.cornerRadius = thumbSize.height * self.thumbCornerRadius
            
            
            //label frame
            if self.areLabelsShown {
                let labelWidth = self.bounds.width / 2 - self.padding * 2
                self.labelOn.frame = CGRect(x: 0, y: 0, width: labelWidth, height: self.frame.height)
                self.labelOff.frame = CGRect(x: self.frame.width - labelWidth, y: 0, width: labelWidth, height: self.frame.height)
            }
            
            // on/off images
            //set to preserve aspect ratio of image in thumbView
            
            guard onImage != nil && offImage != nil else {
                return
            }
            
            let frameSize = thumbSize.width > thumbSize.height ? thumbSize.height * 0.7 : thumbSize.width * 0.7
            
            let onOffImageSize = CGSize(width: frameSize, height: frameSize)
            
            
            self.onImageView.frame.size = onOffImageSize
            self.offImageView.frame.size = onOffImageSize
            
            self.onImageView.center = CGPoint(x: self.onPoint.x + self.thumbView.frame.size.width / 2, y: self.thumbView.center.y)
            self.offImageView.center = CGPoint(x: self.offPoint.x + self.thumbView.frame.size.width / 2, y: self.thumbView.center.y)
            
            
            self.onImageView.alpha = self.isOn ? 1.0 : 0.0
            self.offImageView.alpha = self.isOn ? 0.0 : 1.0
            
        }
    }
}

//Mark: Labels frame
extension CustomSwitch {
    
    fileprivate func setupLabels() {
        guard self.areLabelsShown else {
            self.labelOff.alpha = 0
            self.labelOn.alpha = 0
            return
            
        }
        
        self.labelOff.alpha = 1
        self.labelOn.alpha = 1
        
        let labelWidth = self.bounds.width / 2 - self.padding * 2
        self.labelOn.frame = CGRect(x: 0, y: 0, width: labelWidth, height: self.frame.height)
        self.labelOff.frame = CGRect(x: self.frame.width - labelWidth, y: 0, width: labelWidth, height: self.frame.height)
        self.labelOn.font = UIFont.boldSystemFont(ofSize: 12)
        self.labelOff.font = UIFont.boldSystemFont(ofSize: 12)
        self.labelOn.textColor = UIColor.white
        self.labelOff.textColor = UIColor.white
        
        self.labelOff.sizeToFit()
        self.labelOff.text = "Off"
        self.labelOn.text = "On"
        self.labelOff.textAlignment = .center
        self.labelOn.textAlignment = .center
        
        self.insertSubview(self.labelOff, belowSubview: self.thumbView)
        self.insertSubview(self.labelOn, belowSubview: self.thumbView)
        
    }
    
}

//Mark: Animating on/off images
extension CustomSwitch {
    
    fileprivate func setOnOffImageFrame() {
        guard onImage != nil && offImage != nil else {
            return
        }
        
        self.onImageView.center.x = self.isOn ? self.onPoint.x + self.thumbView.frame.size.width / 2 : self.frame.width
        self.offImageView.center.x = !self.isOn ? self.offPoint.x + self.thumbView.frame.size.width / 2 : 0
        self.onImageView.alpha = self.isOn ? 1.0 : 0.0
        self.offImageView.alpha = self.isOn ? 0.0 : 1.0
    }
}



@IBDesignable
class CustomSwitchNew : UIControl {
    
    @IBInspectable
    var isOn: Bool = false {
        didSet{
            self.setNeedsDisplay()
        }
    }
    
    @IBInspectable
    var thumbOnColor: UIColor = UIColor.customOrange {
        didSet{
            
        }
    }
    
    @IBInspectable
    var thumbOffColor: UIColor = UIColor.lightGray {
        didSet{
            
        }
    }
    
    @IBInspectable
    var trackOnColor: UIColor = UIColor.customOrange.withAlphaComponent(0.3) {
        didSet{
            
        }
    }
    
    @IBInspectable
    var trackOffColor: UIColor = UIColor.lightGray.withAlphaComponent(0.3) {
        didSet{
            
        }
    }
    
    //Required radius to render the track for the control.
    private lazy var trackCornerRadius: CGFloat = bounds.height * 0.65
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: self.bounds.width, height: self.bounds.height)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        backgroundColor = .clear
    }
    
    //Used for shadows on the track and thumb
    private let shadowRadius: CGFloat = 0.5
    
    override func draw(_ rect: CGRect) {
        //Calculate correct radius to render track
        trackCornerRadius = rect.height * 0.65
        
        //While giving corner radius for the track, the corners will go out of bounds, so inset the required edges with bounds to make it render properly.
        let newRect = rect.inset(by: .init(top: trackCornerRadius/2, left: trackCornerRadius/2, bottom: trackCornerRadius/2, right: trackCornerRadius/2))
        
        //by using the above rect, define the path for track.
        let trackPath = UIBezierPath(roundedRect: newRect, cornerRadius: newRect.height / 2)
        
        //Begin the contect, and configure as it required.
        let context = UIGraphicsGetCurrentContext()!
        context.setLineWidth(1.0)
        context.setShadow(offset: .zero, blur: 1.25, color: UIColor.lightGray.cgColor)
        let trackColor = self.isOn ? trackOnColor.cgColor : trackOffColor.cgColor
        context.setFillColor(trackColor)
        context.setStrokeColor(UIColor.lightText.cgColor)
        
        //Fill the track path, and that to the contexct to show up.
        trackPath.fill()
        context.addPath(trackPath.cgPath)
        context.strokePath()
        context.fillPath()
        context.saveGState()
        
        //Save the canvas, before changing the context configurations
        let offset: CGSize = isOn ? .zero : .init(width: shadowRadius, height: 0)
        let shadowColor = self.isOn ? UIColor.red.cgColor : UIColor.clear.cgColor
        context.setShadow(offset: offset, blur: shadowRadius, color: shadowColor)
        let thumbColor = self.isOn ? thumbOnColor.cgColor : thumbOffColor.cgColor
        context.setFillColor(thumbColor)
        //Calculate the thumbpath depending on control state, and then add that to the context.
        let thumpPath = thumbPath(for: isOn, with: newRect)
        context.addPath(thumpPath)
        //Restore the previous saved canvas, so it appears on screen.
        context.restoreGState()
    }
    
    // Calculates the thumb frame, by usning control's state
    // according to the toggle status, it moves its x-co ordinate
    func thumbPath(for on: Bool, with rect: CGRect) -> CGPath {
        
        let newRect: CGRect
        let delta = trackCornerRadius / 2
        if on {
            newRect = CGRect(x: rect.maxX - delta - (shadowRadius * 1.2), y: rect.midY - delta, width: trackCornerRadius, height: trackCornerRadius)
        } else {
            newRect = CGRect(x: rect.minX - delta, y: rect.midY - delta, width: trackCornerRadius, height: trackCornerRadius)
        }
        
        let thumbPath =  UIBezierPath(ovalIn: newRect)
        thumbPath.fill()
        return thumbPath.cgPath
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.isOn = !isOn
        self.sendActions(for: .valueChanged)
    }
    
    // Extending the touch area, if the control is too small
    // Apple's guide lines suggest min 44x44 points
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let minHitDiemension: CGFloat = 44.0
        let minimumHitRect: CGRect = CGRect(x: self.bounds.size.width / 2 - minHitDiemension / 2,
                                            y: self.bounds.size.height / 2 - minHitDiemension / 2,
                                            width: minHitDiemension,
                                            height: minHitDiemension)
        
        //  Hit rectangle = our bounds or the minimum rect if larger
        let hitRect = bounds.union(minimumHitRect)
        
        if hitRect.contains(point) {
            return self
        }
        return super.hitTest(point, with: event)
    }
}
