//
//  DateConverter.swift
//  CIC
//
//  Created by Jinal Patel on 9/21/17.
//  Copyright © 2017 minesh doshi. All rights reserved.
//

import UIKit

class DateConverter: NSObject
{
    //MARK:- Properties
    //Public Properties
    static let dateFormatter = DateFormatter()

    class func getDateString(aStrDate : String?, inputFormat : String, outputFormat : String) -> String
    {
        guard let stringDate = aStrDate?.is_trimming_WS_NL_to_String else {
            return ""
        }
       
        dateFormatter.dateFormat = inputFormat
        if let languageCode = LanguageCodes(rawValue: UserPreferences.string(forKey: UserPreferencesKeys.General.languageCode)) {
            switch languageCode {
            case .english:
                print("")
             dateFormatter.locale = Locale(identifier: "en_US")
          
            case .marathi:
            dateFormatter.locale = Locale(identifier: "mr_IN")
            
            }
            
            
        } else {
          dateFormatter.locale = Locale(identifier: "en_US")
        }
        if let convertedDate: Date = dateFormatter.date(from: stringDate) {
            dateFormatter.dateFormat = outputFormat
            let dateFormated = dateFormatter.string(from: convertedDate)
            return dateFormated
        } else {
            return ""
        }
    }
    
    class func getDateStringFromDate(aDate: Date?, outputFormat: String) -> String {
        if (aDate == nil) {
            return ""
        }
        dateFormatter.dateFormat = outputFormat
        if let languageCode = LanguageCodes(rawValue: UserPreferences.string(forKey: UserPreferencesKeys.General.languageCode)) {
            switch languageCode {
            case .english:
                dateFormatter.locale = Locale(identifier: "en_US")
            case .marathi:
                dateFormatter.locale = Locale(identifier: "mr_IN")
            
            }
        } else {
            dateFormatter.locale = Locale(identifier: "en_US")
        }
        let dateFormated = dateFormatter.string(from: aDate!)
        return dateFormated
    }
    
    class func getDateFromDateString(aDateString : String?, inputFormat : String) -> Date? {
        guard let stringDate = aDateString?.is_trimming_WS_NL_to_String else {
            return Date()
        }
        
        dateFormatter.dateFormat = inputFormat
        if let languageCode = LanguageCodes(rawValue: UserPreferences.string(forKey: UserPreferencesKeys.General.languageCode)) {
            switch languageCode {
            case .english:
                dateFormatter.locale = Locale(identifier: "en_US")
            case .marathi:
                dateFormatter.locale = Locale(identifier: "mr_IN")
            
            }
        } else {
            dateFormatter.locale = Locale(identifier: "en_US")
        }
        let dateFormated = dateFormatter.date(from: stringDate)
        return dateFormated
    }
    
    class func getDate(fromTimeStamp timeStamp: String) -> Date? {
        //let str = timeStamp as NSString
        var timeStampInt: Double = 0
        if let timeStampDouble = Double(timeStamp) {
            timeStampInt = timeStampDouble//1000
        }
        let date = Date(timeIntervalSince1970: TimeInterval(timeStampInt))
        return date
    }
    
    //MARK:- Get TimeStamp
    class func getTimeStamp() -> String {
        let date = Date()
        let timestamp = String(format:"%.0f",date.timeIntervalSince1970 * 1000)
        return timestamp
    }//End
}
