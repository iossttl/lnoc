//
//  ParkingNumbers.swift
//  LNOC
//
//  Created by Pradip on 04/01/22.
//

import UIKit

private var key_name_ParkingNumbers: String { return "ParkingNumbers" }

class ParkingNumbersTextField : UIView, UITextFieldDelegate {
    
    @IBOutlet weak var txtParkingCarpetAreaFeet: UITextField!
    @IBOutlet weak var txtParkingCarpetAreaMt: UITextField!
    @IBOutlet weak var txtParkingBuiltUpAreafeet: UITextField!
    @IBOutlet weak var txtParkingBuiltUpAreaMt: UITextField!
    @IBOutlet var txtParkingNumber: UITextField!
    @IBOutlet var btnCross: UIButton!
    @IBOutlet var viewSeperator: UIView!
    @IBOutlet weak var viewBgClose: UIView!
    
    @IBOutlet weak var lblParkigType: UILabel!
    
    @IBOutlet weak var btnDropDown: UIButton!
    
    @IBOutlet weak var img_Dropdoen: UIImageView!
    
    
    @IBOutlet var lblTitle: [LocalizedLabel]!
    
    @IBOutlet var lblSeperator: [UILabel]!
    
    @IBOutlet var txtField: [UITextField]!
    
    @IBOutlet var lblAsterisks: [LocalizedLabel]!

    
    override class func loadNib() -> Self {
        return Bundle.main.loadNibNamed(key_name_ParkingNumbers, owner: nil, options: nil)![0] as! Self
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        changeHeightForiPad(view: txtParkingNumber)
        setImageColor(color: nil)
        viewSeperator.backgroundColor = .customSeparator
        txtParkingNumber.delegate = self
        self.txtParkingCarpetAreaFeet.delegate = self
        self.txtParkingBuiltUpAreafeet.delegate = self
        txtParkingCarpetAreaMt.isUserInteractionEnabled = false
        txtParkingBuiltUpAreaMt.isUserInteractionEnabled = false
        setUpView()
    }
    
    func setUpView() {
       
        self.lblTitle.forEach({
            $0.font = .regularTitleFont
            $0.textColor = .customBlackTitle
        })
        
        self.txtField.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
        })
        
        self.lblSeperator.forEach { (lbl) in
            lbl.backgroundColor = .customSeparator
        }
        
        self.img_Dropdoen.image = .ic_down_arrow
        
        for lbl in lblAsterisks {
            lbl.textColor = .customRed
            lbl.font = .regularTitleFont
            lbl.text = "*"
        }
    }
    
    public func setTextField(font: UIFont?, textColor: UIColor) {
        txtParkingNumber.font = font
        txtParkingNumber.textColor = textColor
//        txtParkingNumber.placeholder = PropertyDetailLabels.parkingNumber
    }
    
    
    public func setImageColor(color: UIColor?) {
        var img = UIImage.ic_plus
        if let getColor = color {
            btnCross.tintColor = getColor
            img = img.alwaysTemplate
        }
        btnCross.setImage(img, for: .normal)
        let padding: CGFloat = 7.5
        btnCross.contentEdgeInsets = .init(top: padding, left: padding, bottom: padding, right: padding)
        btnCross.transform = .init(rotationAngle: 45 * .pi / 180)
    }
    
    public func setSeperator(isHidden setHidden: Bool, color: UIColor = .lightGray) {
        viewSeperator.isHidden = setHidden
        viewSeperator.backgroundColor = color
    }
    
    public func setCloseButton(isHidden setHidden: Bool, target: Any? = nil, action: Selector? = nil) {
        btnCross.superview?.isHidden = setHidden
        if let action = action {
            btnCross.addTarget(target, action: action, for: .touchUpInside)
        }
    }
    
    public func setDropDownButton(isHidden setHidden: Bool, target: Any? = nil, action: Selector? = nil) {
        lblParkigType.text = CommonLabels.select
        btnDropDown.superview?.isHidden = setHidden
        if let action = action {
            btnDropDown.addTarget(target, action: action, for: .touchUpInside)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtParkingNumber{
            let fullString = NSString(string: textField.text ?? "").replacingCharacters(in: range, with: string)
            return (fullString.count < 7)

           /* let numberSet = CharacterSet(charactersIn: NUMERIC).inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return (string == numberFiltered) && (fullString.count < 7)*/
        }
        
        return true
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
       if textField == txtParkingCarpetAreaFeet {
            txtParkingCarpetAreaMt.text = getAreaFromFeetToMT(Feet: getInteger(anything: txtParkingCarpetAreaFeet.text))
        } else if textField == txtParkingBuiltUpAreafeet {
            txtParkingBuiltUpAreaMt.text = getAreaFromFeetToMT(Feet: getInteger(anything: txtParkingBuiltUpAreafeet.text))
        }
    }
    
    func getAreaFromFeetToMT(Feet:Int) -> String{
        let areaInMT = String(format: "%.2f", (Double(Feet)*(0.0929)))
        //getString(anything: (Double(Feet)/10.764))
        return areaInMT
    }
}


class ParkingNumbersLabel : UIView {
    
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet var lblTitles: [LocalizedLabel]!
    @IBOutlet var lblValues: [UILabel]!
    
    @IBOutlet weak var lblParking: UILabel!
    @IBOutlet weak var lblPASqFTCarpet: UILabel!
    @IBOutlet weak var lblPASqMTCarpet: UILabel!
    @IBOutlet weak var lblPASqFTBuiltUp: UILabel!
    @IBOutlet weak var lblPASqMTBuiltUp: UILabel!
    @IBOutlet var lblParkingNumber: UILabel!
    
    override class func loadNib() -> Self {
        return Bundle.main.loadNibNamed(key_name_ParkingNumbers, owner: nil, options: nil)![1] as! Self
    }
    
//    public func setLabel(font: UIFont?, textColor: UIColor, text: String) {
//        lblParkingNumber.font = font
//        lblParkingNumber.textColor = textColor
//        lblParkingNumber.text = text
//    }
    
//    public func setSeperator(isHidden setHidden: Bool, color: UIColor = .lightGray) {
//        viewSeperator.isHidden = setHidden
//        viewSeperator.backgroundColor = color
//    }
    
}
