//
//  LocalizationInteractor.swift
//  CIC
//
//  Created by STTL on 03/05/18.
//  Copyright © 2018 minesh doshi. All rights reserved.
//

import Foundation
import UIKit


enum LanguageCodes : String{
    case english = "en"
    case marathi = "mr"
    
    var plistName : String {
        switch self {
        case .english: return "EnglishLocalization"
        case .marathi : return "MarathiLocalization"
        }
    }
    
    var DateConverterCode : String {
            switch self {
            case .english : return "en_US"
            case .marathi : return "mr_IN" //gu
           }
        }
    
}

class LocalizationParam{
    static var getInstance = LocalizationParam()
    
    private var english_localizationDict : NSMutableDictionary = [:]
    var localizationDict : NSMutableDictionary = [:]
    var localizationPlistName : String = ""
    var localizationCode : LanguageCodes = LanguageCodes.english
    {
        didSet {
            self.localizationPlistName = localizationCode.plistName
            let path: String = Bundle.main.path(forResource: self.localizationPlistName, ofType: "plist")!
            self.localizationDict = NSMutableDictionary(contentsOfFile: path)!
            
            self.english_localizationDict = NSMutableDictionary(contentsOfFile: Bundle.main.path(forResource: LanguageCodes.english.plistName, ofType: "plist")!)!
        }
    }
    
    static func getLocalizedStringFor(key : String) -> String{
        if let value = LocalizationParam.getInstance.localizationDict[key] as? String, value.trimming_WS_NL.count > 0 {
            return value
        }
        if let value = LocalizationParam.getInstance.english_localizationDict[key] as? String {
            return value
        }
        return ""
        
//        if let value = LocalizationParam.getInstance.localizationDict[key] as? String{
//            return value
//        }
//        return ""
    }
    
    static func getLocalizedArrayFor(key : String) -> [String]{
        if let value = LocalizationParam.getInstance.localizationDict[key] as? [String]{
            return value
        }
        return [""]
    }
}

class LoocalizedStrings{
    
    static func getLocalizedStringFor(key : String) -> String{
        if let value = LocalizationParam.getInstance.localizationDict[key] as? String{
            return value
        }
        return ""
    }
}


func setLocalization(language : LanguageCodes) {
    
//    isRightToLeftLocalization = NSLocale.characterDirection(forLanguage: language.rawValue) == .rightToLeft
//
//    let attribute : UISemanticContentAttribute = isRightToLeftLocalization ? .forceRightToLeft : .forceLeftToRight
//    UIView.appearance().semanticContentAttribute = attribute
//    UITextView.appearance().semanticContentAttribute = attribute
//    UITextField.appearance().semanticContentAttribute = attribute
    selectedLanguage = language
    
}
