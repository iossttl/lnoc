//
//  FileInfoViewController.swift
//  CIC
//
//  Created by Jignesh Ashara on 03/10/17.
//  Copyright © 2017 minesh doshi. All rights reserved.
//

import UIKit

class FileInfoViewController: UIViewController
{
    //MARK:- Outlets
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var txtViewInfo: UITextView!
    
    //MARK:- Variables
    var appName = ""
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        viewMain.layer.borderWidth = 2
        viewMain.layer.borderColor = UIColor.lightGray.cgColor
        viewMain.layer.cornerRadius = 12
        
        self.txtViewInfo.backgroundColor = UIColor.clear
        self.txtViewInfo.isUserInteractionEnabled = false
        
        guard let appName = Bundle.main.object(forInfoDictionaryKey: "CFBundleName")
        else
        {
            return
        }
        
        if IS_IPAD
        {
            self.txtViewInfo.text = "\("1. Connect your iPad with a Computer by USB") \n\n\("2. Open iTunes in the computer") \n\n\("3. iTunes -> iPad -> Apps -> File Sharing") -> \(appName) \n\n\("4. Add files to") \(appName) \("App")."
        }
        else
        {
            self.txtViewInfo.text = "\("1. Connect your iPhone with a Computer by USB") \n\n\("2. Open iTunes in the computer") \n\n\("3. iTunes -> iPhone -> Apps -> File Sharing") -> \(appName) \n\n\("4. Add files to") \(appName) \("App")."
        }
    }
}
