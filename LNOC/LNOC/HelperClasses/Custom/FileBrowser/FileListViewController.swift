//
//  FileListViewController.swift
//  FileBrowser
//
//  Created by Roy Marmelstein on 12/02/2016.
//  Copyright © 2016 Roy Marmelstein. All rights reserved.
//

import UIKit

class FileListViewController: UIViewController,UIPopoverPresentationControllerDelegate {
    
    // TableView
    @IBOutlet weak var tableView: UITableView!
    let collation = UILocalizedIndexedCollation.current()
    
    /// Data
    var didSelectFile: ((FBFile) -> ())?
    var files = [FBFile]()
    var initialPath: URL?
    let parser = FileParser.sharedInstance
    let previewManager = PreviewManager()
    var sections: [[FBFile]] = []
    var allowEditing: Bool = false

    //=============>>>>>>>Custom Addition<<<<<<<<=============
    var cancelButtonTitle : String = ""
    var strNoFileFound : String = ""
    var navigationBarColor: UIColor = .white
    //=============>>>>>>>Custom Addition<<<<<<<<=============

    // Search controller
    var filteredFiles = [FBFile]()
    /*let searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.searchBarStyle = .minimal
        searchController.searchBar.backgroundColor = UIColor.white
        searchController.dimsBackgroundDuringPresentation = false
        return searchController
    }()*/
    
    
    //MARK: Lifecycle
    convenience init (initialPath: URL) {
        self.init(initialPath: initialPath, showCancelButton: true)
    }
    
    convenience init (initialPath: URL, showCancelButton: Bool, screenTitle : String? = "") {
        self.init(nibName: "FileBrowser", bundle: Bundle(for: FileListViewController.self))
        self.edgesForExtendedLayout = UIRectEdge()
        
        // Set initial path
        self.initialPath = initialPath
        
        if(screenTitle != ""){
            self.title = screenTitle
        }
        else{
            self.title = initialPath.lastPathComponent
        }
        
        // Set search controller delegates
        /*searchController.searchResultsUpdater = self
         searchController.searchBar.delegate = self
         searchController.delegate = self*/
        
        if showCancelButton {
            // Add dismiss button
            let dismissButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(FileListViewController.dismiss(button:)))
            self.navigationItem.rightBarButtonItem = dismissButton
        }
        
        //=============>>>>>>>Custom Addition<<<<<<<<=============
        //let leftBarButton = UIBarButtonItem.init(image: UIImage(named: "info_white")?.withRenderingMode(.alwaysTemplate), style: UIBarButtonItem.Style.plain, target: self, action: #selector(showInfo))
        //leftBarButton.imageInsets = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 20)
        //self.navigationItem.leftBarButtonItem = leftBarButton
        self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(image: UIImage(named: "info_white"), style: UIBarButtonItem.Style.plain, target: self, action: #selector(showInfo))
        //UIBarButtonItem.init(barButtonSystemItem: UIBarButtonSystemItem.organize, target: self, action:#selector(showInfo));
        
        self.navigationItem.leftBarButtonItem?.setTitleTextAttributes([NSAttributedString.Key.font : UIFont.systemFont(ofSize: 15),NSAttributedString.Key.foregroundColor:UIColor.black], for: .normal)
        
        //=============>>>>>>>Custom Addition<<<<<<<<=============
        
    }
    
    //=============>>>>>>>Custom Addition<<<<<<<<=============
    @objc func showInfo(sender:UIBarButtonItem)
    {
        let storyboard = UIStoryboard(name: "FileInfo", bundle: nil)
        let popoverViewController = storyboard.instantiateViewController(withIdentifier: "FileInfoViewController")
        popoverViewController.modalPresentationStyle = .popover
        popoverViewController.preferredContentSize   = CGSize(width: 200, height: 250)
        let popoverPresentationViewController = popoverViewController.popoverPresentationController
        
        popoverPresentationViewController?.permittedArrowDirections = .any
        popoverPresentationViewController?.delegate = self
        popoverPresentationViewController?.barButtonItem = sender //as UIBarButtonItem
        popoverPresentationViewController?.backgroundColor = UIColor.lightGray
        
        present(popoverViewController, animated: true, completion: nil)
    }
    
    //MARK:- UIPopoverPresentationControllerDelegate
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return UIModalPresentationStyle.none
    }
    //=============>>>>>>>Custom Addition<<<<<<<<=============
    
    deinit{
        if #available(iOS 9.0, *) {
          //  searchController.loadViewIfNeeded()
        } else {
          //  searchController.loadView()
        }
    }
    
    func prepareData() {
        // Prepare data
        if let initialPath = initialPath {
            files = parser.filesForDirectory(initialPath)
            indexFiles()
        }
    }
    
    //MARK: UIViewController
    
    override func viewDidLoad() {
        
        prepareData()
        
        // Set search bar
        //tableView.tableHeaderView = searchController.searchBar
        
        // Register for 3D touch
        self.registerFor3DTouch()
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.tintColor = UIColor.white
        
        //=============>>>>>>>Custom Addition<<<<<<<<=============
        navigationController?.navigationBar.barTintColor = navigationBarColor
        if(cancelButtonTitle != ""){
            let aboutButton = UIBarButtonItem(title: cancelButtonTitle, style: .plain, target: self, action: #selector(FileListViewController.dismiss(button:)))
            self.navigationItem.setRightBarButton(aboutButton, animated: false)
        }
        //=============>>>>>>>Custom Addition<<<<<<<<=============
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Scroll to hide search bar
       // self.tableView.contentOffset = CGPoint(x: 0, y: searchController.searchBar.frame.size.height)
        
        // Make sure navigation bar is visible
        self.navigationController?.isNavigationBarHidden = false
        
        if files.count > 0
        {
            
        }
        else
        {
            self.showInfo(sender: self.navigationItem.leftBarButtonItem!)
        }
    }
    
    @objc func dismiss(button: UIBarButtonItem = UIBarButtonItem()) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Data
    
    func indexFiles() {
        let selector: Selector = #selector(getter: FBFile.displayName)
        sections = Array(repeating: [], count: collation.sectionTitles.count)
        if let sortedObjects = collation.sortedArray(from: files, collationStringSelector: selector) as? [FBFile]{
            for object in sortedObjects {
                let sectionNumber = collation.section(for: object, collationStringSelector: selector)
                sections[sectionNumber].append(object)
            }
        }
        
        //=============>>>>>>>Custom Addition<<<<<<<<=============
        if files.count > 0
        {
            self.tableView.isHidden = false
        }
        else
        {
            self.addCustomNoFileLabel()
            self.tableView.isHidden = true
        }
        //=============>>>>>>>Custom Addition<<<<<<<<=============
    }
    
    //=============>>>>>>>Custom Addition<<<<<<<<=============
    func addCustomNoFileLabel()
    {
        let lbl = UILabel()
        DispatchQueue.main.async{
            lbl.frame = CGRect(x: 0, y: self.view.frame.size.height/2-20, width: self.view.frame.size.width, height: 40)
        }
        lbl.backgroundColor = UIColor.clear
        lbl.textAlignment = .center
        lbl.text = self.strNoFileFound
        self.view.addSubview(lbl)
    }
    //=============>>>>>>>Custom Addition<<<<<<<<=============
    
    func fileForIndexPath(_ indexPath: IndexPath) -> FBFile {
        var file: FBFile
        /*if searchController.isActive {
            file = filteredFiles[(indexPath as NSIndexPath).row]
        }
        else {*/
            file = sections[(indexPath as NSIndexPath).section][(indexPath as NSIndexPath).row]
        //}
        return file
    }
    
    func filterContentForSearchText(_ searchText: String) {
        filteredFiles = files.filter({ (file: FBFile) -> Bool in
            return file.displayName.lowercased().contains(searchText.lowercased())
        })
        tableView.reloadData()
    }

}

