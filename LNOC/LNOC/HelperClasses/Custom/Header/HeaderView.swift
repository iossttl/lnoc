//
//  HeaderView.swift
//  PSIS
//
//  Created by Vikram Jagad on 02/05/19.
//  Copyright © 2019 Vikram Jagad. All rights reserved.
//

import UIKit

class HeaderView: UIView {
    //MARK:- Interface Builder
    //UIView
    @IBOutlet weak var viewHeaderBg: UIView!
    
    //UIButton
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var btnSync: UIButton!
    @IBOutlet weak var btnNotification: UIButton!
    @IBOutlet weak var btnFavourite: UIButton!
   
    //UILabel
    @IBOutlet weak var lblHeaderSubTitle: UILabel!
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    //NSLayoutConstraint
    @IBOutlet weak var constTopStatus: NSLayoutConstraint!
    @IBOutlet weak var constBtnBackWidth: NSLayoutConstraint!
    
    //UITextField
    @IBOutlet weak var tfSearch: UITextField!
    
    //UIImageView
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var ImgLogo: UIImageView!
    @IBOutlet weak var viewImgLogo: UIView!
    
    @IBOutlet weak var lblNotificationCount: UILabel!
    
    @IBOutlet weak var constImgLogoHeight: NSLayoutConstraint!

    override var intrinsicContentSize: CGSize {
        return UIView.layoutFittingExpandedSize
    }
}
