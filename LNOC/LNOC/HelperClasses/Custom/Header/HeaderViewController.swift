//
//  HeaderViewController.swift
//  PSIS
//
//  Created by Vikram Jagad on 02/05/19.
//  Copyright © 2019 Vikram Jagad. All rights reserved.
//

import UIKit
import SideMenu

class HeaderViewController: UIViewController {
    
    //MARK:- Variables
    //Public
    var viewHeader: HeaderView = HeaderView.loadNib()
    var btnBack: UIButton! { return viewHeader.btnBack }
    
    weak var tfDelegate: HeaderViewTextFieldDelegate?
    
    var sideMenuViewObj : UIViewController? {
        return SideMenuManager.default.leftMenuNavigationController
    }
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    
    //MARK:- Private Methods
    //View Set Up
    fileprivate func setUpView() {
        setUpHeader()
        setUpBtns()
        setUpLbls()
        setUpTxtField()
    }
    
    //Navigation titleView Set Up
    fileprivate func setUpHeader() {
        self.viewHeader.viewHeaderBg.backgroundColor = .clear
        
        let height = self.navigationController?.navigationBar.frame.size.height ?? 44
        
        self.viewHeader.clipsToBounds = false
        self.viewHeader.frame = CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: height)
        navigationController?.edgesForExtendedLayout = .all
        navigationItem.setHidesBackButton(true, animated: false)
        navigationItem.titleView = self.viewHeader
        
        self.viewHeader.constTopStatus.constant = -STATUS_BAR_HEIGHT
        
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationController?.navigationBar.shadowImage = UIImage()
        
        navigationController?.navigationBar.backIndicatorImage = UIImage()
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage()
        hideSearch()
        hideFilter()
        hideNotification()
        hideNotificationCount()
        hideFavourite()
        self.viewHeader.imgSearch.image = .ic_header_search
        self.viewHeader.imgSearch.tintColor = .white
        self.viewHeader.tfSearch.superview?.setCustomCornerRadius(radius: 5.0)
        
        self.viewHeader.tfSearch.superview?.backgroundColor = .getColorAccordingToAppearance(darkModeClr: .darkGray, lightModeClr: UIColor.white.withAlphaComponent(0.15), defaultClr: UIColor.white.withAlphaComponent(0.15))
    }
    
    //Buttons Set Up
    fileprivate func setUpBtns() {
        self.viewHeader.btnBack.setImage(.ic_header_menu, for: .normal)
        self.viewHeader.btnBack.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        self.viewHeader.btnBack.tintColor = .white
        self.viewHeader.btnBack.addTarget(self, action: #selector(btnBackTapped), for: .touchUpInside)
        
        self.viewHeader.btnSearch.setImage(.ic_header_search, for: .normal)
        self.viewHeader.btnSearch.tintColor = .white
        self.viewHeader.btnSearch.isSelected = true
        self.viewHeader.btnSearch.addTarget(self, action: #selector(btnSearchTapped), for: .touchUpInside)
        
        self.viewHeader.btnSync.setImage(.ic_header_filter, for: .normal)
        self.viewHeader.btnSync.tintColor = .white
        self.viewHeader.btnSync.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        self.viewHeader.btnSync.addTarget(self, action: #selector(btnSyncTapped), for: .touchUpInside)
        
        self.viewHeader.btnNotification.setImage(.ic_header_notification, for: .normal)
        self.viewHeader.btnNotification.tintColor = .customWhite
        self.viewHeader.btnNotification.addTarget(self, action: #selector(btnNotificationTapped(_:)), for: .touchUpInside)
        
        self.viewHeader.btnFavourite.setImage(.favorite_empty, for: .normal)
        self.viewHeader.btnFavourite.tintColor = .customWhite
        self.viewHeader.btnFavourite.addTarget(self, action: #selector(btnFavourite(_:)), for: .touchUpInside)
    }
    
    //Labels Set Up
    fileprivate func setUpLbls() {
        self.viewHeader.lblHeaderTitle.textColor = .white
        self.viewHeader.lblHeaderTitle.font = .boldMediumFont
        
        self.viewHeader.lblHeaderSubTitle.textColor = .white
        self.viewHeader.lblHeaderSubTitle.font = .regularValueFont
        
        self.viewHeader.lblNotificationCount.font = .regularSmallestFont
        self.viewHeader.lblNotificationCount.textColor = .customWhite
        self.viewHeader.lblNotificationCount.viewWith(radius: self.viewHeader.lblNotificationCount.frame.height/2)
        self.viewHeader.lblNotificationCount.backgroundColor = .customRed
        
    }
    
    //TextField Set Up
    fileprivate func setUpTxtField() {
        self.viewHeader.tfSearch.superview?.isHidden = true
        self.viewHeader.tfSearch.font = .regularMediumFont
        self.viewHeader.tfSearch.attributedPlaceholder = NSAttributedString(string: CommonLabels.search, attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        self.viewHeader.tfSearch.textColor = .white
        self.viewHeader.tfSearch.returnKeyType = .search
        self.viewHeader.tfSearch.delegate = self
    }
    
    //Title Set Up
    func setUpHeaderTitle(strHeaderTitle: String) {
        self.viewHeader.lblHeaderTitle.text = strHeaderTitle
        self.viewHeader.lblHeaderTitle.textAlignment = .left
    }
    
    func setupSubHeaderTitle(strHeaderTitle: String) {
        self.viewHeader.lblHeaderSubTitle.text = strHeaderTitle
        self.viewHeader.lblHeaderTitle.textAlignment = .left
    }
    //MARK:- Public Methods
    func setHeaderView_MenuImage() {
        self.viewHeader.btnBack.setImage(.ic_header_menu, for: .normal)
        self.viewHeader.btnBack.imageEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
    }

    func setHeaderView_BackImage() {
        self.viewHeader.btnBack.setImage(.ic_header_back, for: .normal)
        self.viewHeader.btnBack.imageEdgeInsets = UIEdgeInsets(top: 12, left: 12, bottom: 12, right: 12)
    }
    
    func hideBack() {
        self.viewHeader.constBtnBackWidth.constant = 0
        self.viewHeader.btnBack.isHidden = true
    }
    
    func hideSearch() {
        if let viewBtnSearch = self.viewHeader.btnSearch.superview {
            viewBtnSearch.isHidden = true
        }
    }
    
    func hideFilter() {
        if let viewBtnSearch = self.viewHeader.btnSync.superview {
            viewBtnSearch.isHidden = true
        }
    }
    
    func hideNotificationCount() {
        self.viewHeader.lblNotificationCount.isHidden = true
    }
    
    func hideNotification() {
        if let viewBtnNotification = self.viewHeader.btnNotification.superview {
            viewBtnNotification.isHidden = true
        }
    }
    
    func hideFavourite() {
        if let viewBtnFav = self.viewHeader.btnFavourite.superview {
            viewBtnFav.isHidden = true
        }
    }
    
    func showLogo() {
        self.viewHeader.viewImgLogo.isHidden = false
        self.viewHeader.constImgLogoHeight.constant = IS_IPAD ? 35 : 20
        self.viewHeader.ImgLogo.image = .ic_top_logo
    }
    
    func hideLogo() {
        self.viewHeader.viewImgLogo.isHidden = true
    }
    
    func showBack() {
        self.viewHeader.constBtnBackWidth.constant = 44
        self.viewHeader.btnBack.isHidden = false
    }
    
    func showSearch() {
        if let viewBtnSearch = self.viewHeader.btnSearch.superview {
            viewBtnSearch.isHidden = false
        }
    }
    
    func showFilter() {
        if let viewBtnFilter = self.viewHeader.btnSync.superview {
            viewBtnFilter.isHidden = false
        }
    }
    
    func showDownload() {
        if let viewBtnNotification = self.viewHeader.btnSearch.superview {
            viewBtnNotification.isHidden = false
            self.viewHeader.btnSearch.setImage(.ic_header_download, for: .normal)
        }
    }
    
    func showFavourite() {
        if let viewBtnFav = self.viewHeader.btnFavourite.superview {
            viewBtnFav.isHidden = false
        }
    }
    
    func showNotification() {
        if let viewBtnNotification = self.viewHeader.btnNotification.superview {
            viewBtnNotification.isHidden = false
            self.viewHeader.btnNotification.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            //self.viewHeader.lblNotificationCount.isHidden = false
            //self.viewHeader.lblNotificationCount.text = "2"
        }
    }
    
    @discardableResult func showSideMenu() -> UIViewController? {
            UIView.hideKeyBoard()
            if let menuLeftNavigationController = SideMenuManager.default.leftMenuNavigationController {
                if menuLeftNavigationController.presentingViewController != nil {
                    menuLeftNavigationController.dismiss(animated: false) {
                        self.present(menuLeftNavigationController, animated: true, completion: nil)
                    }
                } else {
                    self.present(menuLeftNavigationController, animated: true, completion: nil)
                }
                return menuLeftNavigationController
            }
            return nil
        }
    
    //MARK:- Selector Methods
    @objc func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func btnViewGuidelinesTapped(_ sender: UIButton) {
        
    }
    
    @objc func btnNotificationTapped(_ sender: UIButton) {
       
    }
    
    @objc func btnFavourite(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            sender.setImage(.favorite_fill, for: .normal)
        } else {
            sender.setImage(.favorite_empty, for: .normal)
        }
    }
    
    @objc func btnSyncTapped(_ sender: UIButton) {
        
    }
    
    @objc func btnSearchTapped(_ sender: UIButton) {
        if sender.isSelected {
            sender.setImage(.ic_header_close, for: .normal)
            //sender.imageEdgeInsets = UIEdgeInsets(top: 16, left: 24, bottom: 16, right: 8)
            sender.imageEdgeInsets = UIEdgeInsets(top: 16, left: 16, bottom: 16, right: 16)
            self.viewHeader.tfSearch.becomeFirstResponder()
        } else {
            sender.setImage(.ic_header_search, for: .normal)
            sender.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
            self.viewHeader.tfSearch.resignFirstResponder()
            self.viewHeader.tfSearch.text = ""
            tfDelegate?.search(text: "")
        }
        self.viewHeader.lblHeaderTitle.isHidden = sender.isSelected
        self.viewHeader.tfSearch.superview?.isHidden = !sender.isSelected
        sender.isSelected = !sender.isSelected
    }
}

//MARK:- UITextFieldDelegate
extension HeaderViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.viewHeader.tfSearch {
            tfDelegate?.search(text: getString(anything: textField.text))
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        return tfDelegate?.textFieldShouldClear?(textField: textField) ?? true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.viewHeader.tfSearch {
            tfDelegate?.searchShouldChangeCharactersIn(textField, shouldChangeCharactersIn: range, replacementString: string)
        }
        return true
    }
}
