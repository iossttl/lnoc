//
//  AppDelegate.swift
//  Khel Mahakumbh
//
//  Created by Dhruv on 20/12/21.
//

import UIKit
import IQKeyboardManagerSwift
import SideMenu
import IOSSecuritySuite

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        /*IOSSecuritySuite.denyDebugger()
        
        if IOSSecuritySuite.amIJailbroken() {
            let dvc = JailbrokenVC.instantiate(appStoryboard: .jailbroken)
            window?.rootViewController = dvc
            window?.makeKeyAndVisible()
            return true
        }
        
        if IOSSecuritySuite.amIRunInEmulator() {
            let dvc = JailbrokenVC.instantiate(appStoryboard: .jailbroken)
            window?.rootViewController = dvc
            window?.makeKeyAndVisible()
            return true
        }*/
        
        setUpInitialVC()
        setupLanguage()
        setupIQKeyboard()
        setUpLeftMenu()
        return true
    }
    
    func setupLanguage() {
        print(UserPreferences.string(forKey: UserPreferencesKeys.General.languageCode))
        if  UserPreferences.string(forKey: UserPreferencesKeys.General.languageCode) != ""
        {
            LocalizationParam.getInstance.localizationCode = LanguageCodes(rawValue: UserPreferences.string(forKey: UserPreferencesKeys.General.languageCode)) ?? .english
        }
        else{
            LocalizationParam.getInstance.localizationCode = .english
            UserPreferences.set(value: LanguageCodes.english.rawValue, forKey: UserPreferencesKeys.General.languageCode)
        }
        setLocalization(language : LocalizationParam.getInstance.localizationCode)
    }
    
    private func setupIQKeyboard() {
        
        IQKeyboardManager.shared.toolbarDoneBarButtonItemText = CommonButtons.done
        IQKeyboardManager.shared.enable = true
        
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.toolbarManageBehaviour = .byPosition
        IQKeyboardManager.shared.previousNextDisplayMode = .default
        IQKeyboardManager.shared.toolbarPreviousNextAllowedClasses.append(contentsOf: [UIStackView.self, UIView.self, UIScrollView.self, UITextField.self, RaisePlaceholder.self])
        IQKeyboardManager.shared.toolbarTintColor = .customBlack
    }
    
    //Left Menu set up
    private func setUpLeftMenu() {
        let aSideMenuViewController = SideMenuViewController.instantiate(appStoryboard: .sideMenu)
       
        let sideMenuViewController = SideMenuNavigationController(rootViewController: aSideMenuViewController)
        sideMenuViewController.leftSide = true
        sideMenuViewController.isNavigationBarHidden = true
        SideMenuManager.default.leftMenuNavigationController = sideMenuViewController
        SideMenuManager.default.rightMenuNavigationController = sideMenuViewController
        sideMenuViewController.pushStyle = .preserve
        sideMenuViewController.presentationStyle = .menuSlideIn
        sideMenuViewController.presentationStyle.backgroundColor = .black
        sideMenuViewController.settings.statusBarEndAlpha = 0
        sideMenuViewController.presentationStyle.presentingEndAlpha = 0.6
        if IS_IPAD {
            sideMenuViewController.menuWidth = SCREEN_WIDTH * 0.6
        } else {
            sideMenuViewController.menuWidth = SCREEN_WIDTH * 0.80
        }
    }
    
    func setUpInitialVC() {
//        let dVC = PropertyDetailViewController.instantiate(appStoryboard: .propertyDetail)
//        let navCon = UICustomNavigationController(rootViewController: dVC)
//        window?.rootViewController = navCon
//        window?.makeKeyAndVisible()
        
//        let homeVC = SelectLanguageViewController.instantiate(appStoryboard: .selectLanguage)
//        let navCon = UICustomNavigationController(rootViewController: homeVC)
//        window?.rootViewController = navCon
//        window?.makeKeyAndVisible()
        
       if UserPreferences.bool(forKey: UserPreferencesKeys.General.isLanguageSelected) {
            
            /*let dVC = DashboardViewController.instantiate(appStoryboard: .dashboard)
            let navCon = UICustomNavigationController(rootViewController: dVC)
            window?.rootViewController = navCon
            window?.makeKeyAndVisible()*/
             if (UserPreferences.bool(forKey: UserPreferencesKeys.General.isUserLoggedIn) || UserPreferences.bool(forKey: UserPreferencesKeys.General.isSkip)) {
                
                let dVC = DashboardViewController.instantiate(appStoryboard: .dashboard)
                let navCon = UICustomNavigationController(rootViewController: dVC)
                window?.rootViewController = navCon
                window?.makeKeyAndVisible()
            } else {
                
                let homeVC = LoginViewController.instantiate(appStoryboard: .Login)
                let navCon = UICustomNavigationController(rootViewController: homeVC)
                window?.rootViewController = navCon
                window?.makeKeyAndVisible()
            }
        } else {
            
            let homeVC = SelectLanguageViewController.instantiate(appStoryboard: .selectLanguage)
            let navCon = UICustomNavigationController(rootViewController: homeVC)
            window?.rootViewController = navCon
            window?.makeKeyAndVisible()
        }
    }

    // MARK: UISceneSession Lifecycle

    /*func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }*/


}

