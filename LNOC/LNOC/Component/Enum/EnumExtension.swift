//
//  EnumExtension.swift
//  SharkID
//
//  Created by stadmin on 25/07/17.
//  Copyright © 2017 sttl. All rights reserved.
//

import UIKit

//MARK:- Image Type
enum ImageType : String {
    case jpeg
    case JPEG
    case jpg
    case JPG
    case png
    case gif
}

//MARK:- Document Type
enum DocumentType: String {
    case pdf
    case PDF
    case jpg
    case JPG
    case jpeg
    case JPEG
    case png
    case PNG
    case doc
    case DOC
    case docx
    case DOCX
    case xls
    case XLS
    case mp4
}

enum CustomFont: String {
    case regular = "regular"
    case bold = "bold"
    case semiBold = "semibold"
    case italic = "italic"
    
    init(font: String) {
        switch font.lowercased() {
        case "regular": self = .regular
        case "bold": self = .bold
        case "semibold": self = .semiBold
        case "italic": self = .italic
        default: self = .regular
        }
    }
}

enum DashboardColViewType: String {
    case banner
    case detail
    
}

//MARK:- Gender Type
enum GenderTypeInt: Int {
    case male = 1
    case female = 2
    case Transgender = 3
}

enum EnumShareType : String {
    case shareText = "text"
    case shareImage = "image"
}

enum SideMenuSelectionType: String {
    case User_Dashboard = "User Dashboard"
    case Notifications = "Notifications"
    case Search_Lands = "Search Lands"
    case Land_Favourites = "Land Favourites"
    case Land_Bank = "Land Bank"
    case NOC_Applications = "NOC Applications"
    case Add_New_Application = "Add New Application"
    case Change_Password = "Change Password"
    case settings = "Settings"
    case addLandDetail = "Add Land Detail"
    case feedback = "Feedback"
    case aboutUs = "About Us"
    case FAQ = "FAQ's"
    case contatUs = "Contact Us"
}
enum SideMenuGrid: String {
    case latest = "Latest"
    case featured = "Featured"
    case inDepth = "In-Depth"
    case bookmarks = "Bookmarks"
}

enum DashboardSelectionType: String {
    case latest = "Latest News"
    case featured = "Featured"
    case inDepth = "In-Depth"
    case bookmarks = "Bookmarks"
    case newIndiaShow = "The New Indian Show"
    case topBanner = "Top Banner"
    
}

enum Fonts_Size : String {
    case Small = "Small"
    case Regular = "Regular"
    case Large = "Large"
    case Extra_Large = "Extra Large"
    case Maximum = "Maximum"
}

enum SettingType:String {
    case password = "password"
    case language = "language"
}

enum Profile:String {
    case player = "player"
    case school = "school"
    case coach = "coach"
}

enum Gender: String {
    case male = "Male"
    case female = "Female"
    case transgender = "Other"
    
    var displayString : String {
        switch self {
        case .male: return "Male".localizedString
        case .female: return "Female".localizedString
        case .transgender: return "Other".localizedString
        }
    }
    
    var id : String {
        switch self {
        case .male: return "Male"
        case .female: return "Female"
        case .transgender: return "Other"
        }
    }
    
    var arrayTitle : [String] {
        return ["Male".localizedString, "Female".localizedString, "Other".localizedString]
    }
}



// LNOC New

enum NOCType {
    case sale
    case gift
    case mortgage
    case releaseDeed
    case willProbate
    case QC
    case QA
    
    struct NocDocumentsKeyModel {
        var title: String
        var isOptional: Bool = false
        var uploadingKey: String
        var receiveKey: String
    }
    
    static var list : [NOCType] { return [.sale, .gift, .mortgage, .releaseDeed, .willProbate, .QC, .QA] }
    
    var displayString : String {
        switch self {
        case .sale: return "Sale"
        case .gift: return "Gift"
        case .mortgage: return "MortGage"
        case .releaseDeed: return "Release Dead"
        case .willProbate: return "Will Probate"
        case .QC: return "QC"
        case .QA: return "QA"
        }
    }
    
    var uplopadDocuments: [NocDocumentsKeyModel] {
        switch self {
        case .sale:
            return [.init(title: "Share Certificate (Both Front & Back Side)".localizedString,
                          uploadingKey: "share_certificatefile",
                          receiveKey: ""),
                    
                        .init(title: "NOC From Society ( Detail of Flat for example Area, floor, Parking area & Building completion Year Must be Mentioned in Society NOC)".localizedString,
                              uploadingKey: "noc_from_societyfile",
                              receiveKey: ""),
                    
                        .init(title: "Occupation Certificate of Society or B.M.C. Assessment Bill Copy",
                              uploadingKey: "occupation_certificate_of_societyfile".localizedString,
                              receiveKey: ""),
                    
                        .init(title: "Draft Copy of Sale Deed / Agreement For Sale".localizedString,
                              uploadingKey: "draft_copyfile",
                              receiveKey: ""),
                    
                        .init(title: "Details of Previous Transfer of the Property with Documents".localizedString,
                              uploadingKey: "previous_transfer_property_docfile",
                              receiveKey: ""),
                    
                        .init(title: "Previous N.O.C. of this Office for Previous Transfer of The Property".localizedString,
                              isOptional: true,
                              uploadingKey: "previous_nocfile",
                              receiveKey: ""),
                    
                        .init(title: "I.D. Proof of Buyer i.e. Aadhar card & Pan Card".localizedString,
                              uploadingKey: "id_proof_buyerfile",
                              receiveKey: ""),
                    
                        .init(title: "I.D. Proof of Seller i.e. Aadhar card & Pan Card".localizedString,
                              uploadingKey: "id_proof_sellerfile",
                              receiveKey: ""),
                    
                        .init(title: "Other Doc".localizedString,
                              isOptional: true,
                              uploadingKey: "other_docfile",
                              receiveKey: "")]
            
        case .gift :
            return [.init(title: "Share Certificate (Both Front & Back Side)".localizedString,
                          uploadingKey: "share_certificatefile",
                          receiveKey: ""),
                    
                        .init(title: "NOC From Society ( Detail of Flat for example Area, floor, Parking area & Building completion Year Must be Mentioned in Society NOC)".localizedString,
                              uploadingKey: "noc_from_societyfile",
                              receiveKey: ""),
                    
                        .init(title: "Occupation Certificate of Society or B.M.C. Assessment Bill Copy".localizedString,
                              uploadingKey: "occupation_certificate_of_societyfile",
                              receiveKey: ""),
                    
                        .init(title: "Draft Copy of Sale Deed / Agreement For Sale".localizedString,
                              uploadingKey: "draft_copyfile",
                              receiveKey: "")]
            
        case .mortgage:
            return [.init(title: "Occupation Certificate of Society or B.M.C. Assessment Bill Copy".localizedString,
                          uploadingKey: "occupation_certificate_of_societyfile",
                          receiveKey: ""),
                    
                        .init(title: "Draft Copy of Sale Deed / Agreement For Sale".localizedString,
                              uploadingKey: "draft_copyfile",
                              receiveKey: ""),
                    
                        .init(title: "Details of Previous Transfer of the Property with Documents".localizedString,
                              uploadingKey: "previous_transfer_property_docfile",
                              receiveKey: ""),
                    
                    
                        .init(title: "Previous N.O.C. of this Office for Previous Transfer of The Property".localizedString,
                              isOptional: true,
                              uploadingKey: "previous_nocfile",
                              receiveKey: ""),
                    
                        .init(title: "I.D. Proof of Buyer i.e. Aadhar card & Pan Card".localizedString,
                              uploadingKey: "id_proof_buyerfile",
                              receiveKey: ""),
                    
                        .init(title: "I.D. Proof of Seller i.e. Aadhar card & Pan Card".localizedString,
                              uploadingKey: "id_proof_sellerfile",
                              receiveKey: ""),
                    
                        .init(title: "Other Doc".localizedString,
                              isOptional: true,
                              uploadingKey: "other_docfile",
                              receiveKey: "")]
            
        case .releaseDeed:
            return [.init(title: "Share Certificate (Both Front & Back Side)".localizedString,
                          uploadingKey: "share_certificatefile",
                          receiveKey: ""),
                    
                        .init(title: "NOC From Society ( Detail of Flat for example Area, floor, Parking area & Building completion Year Must be Mentioned in Society NOC)".localizedString,
                              uploadingKey: "noc_from_societyfile",
                              receiveKey: ""),
                    
                        .init(title: "I.D. Proof of Buyer i.e. Aadhar card & Pan Card".localizedString,
                              uploadingKey: "id_proof_buyerfile",
                              receiveKey: ""),
                    
                        .init(title: "I.D. Proof of Seller i.e. Aadhar card & Pan Card".localizedString,
                              uploadingKey: "id_proof_sellerfile",
                              receiveKey: ""),
                    
                        .init(title: "Other Doc".localizedString,
                              isOptional: true,
                              uploadingKey: "other_docfile",
                              receiveKey: "")]
            
        case .willProbate:
            return [.init(title: "Share Certificate (Both Front & Back Side)".localizedString,
                          uploadingKey: "share_certificatefile",
                          receiveKey: ""),
                    
                        .init(title: "NOC From Society ( Detail of Flat for example Area, floor, Parking area & Building completion Year Must be Mentioned in Society NOC)".localizedString,
                              uploadingKey: "noc_from_societyfile",
                              receiveKey: ""),
                    
                        .init(title: "I.D. Proof of Buyer i.e. Aadhar card & Pan Card".localizedString,
                              uploadingKey: "id_proof_buyerfile",
                              receiveKey: ""),
                    
                        .init(title: "I.D. Proof of Seller i.e. Aadhar card & Pan Card".localizedString,
                              uploadingKey: "id_proof_sellerfile",
                              receiveKey: ""),
                    
                        .init(title: "Other Doc".localizedString,
                              isOptional: true,
                              uploadingKey: "other_docfile",
                              receiveKey: ""),
                    
                        .init(title: "Will Probate Document".localizedString,
                              uploadingKey: "will_probate_docfile",
                              receiveKey: "")]
            
        case .QC:
            return []
            
        case .QA:
            return []
        }
    }
}


enum webViewType: String {
    case aboutUS = "About Us"
    case FAQ = "FAQ's"
    case contactUs = "Contact Us"

    
    var displayStr: String {
        switch self {
        case .aboutUS:
            return "About Us".localizedString
        case .FAQ:
            return "FAQ's".localizedString
        case .contactUs:
            return "Contact Us".localizedString
        }
    }
    
    var apiValue: String {
        
        let languageCode = "&language=" + (LanguageCodes(rawValue: UserPreferences.string(forKey: UserPreferencesKeys.General.languageCode)) ?? .english).rawValue
//        let id = getString(anything: UserPreferences.string(forKey: UserPreferencesKeys.UserInfo.userID))
        switch self {
        case .aboutUS:
            return WebViewAPI.aboutUs + languageCode
        case .FAQ:
            return WebViewAPI.FAQ + languageCode
        case .contactUs:
            return WebUrlLink.contactUs + languageCode
        }
    }
}
