//
//  RaisePlaceholder.swift
//  RaisePlaceholder
//
//  Created by Lee Jiho on 2017. 6. 11..
//  Copyright © 2017년 Lee Jiho. All rights reserved.
//

import UIKit

@objc protocol RaisePlaceholderDelegate
{
    @objc optional func textFieldDidBeginEditing(_ textField: RaisePlaceholder)
    @objc optional func textFieldDidEndEditing(_ textField: RaisePlaceholder)
    @objc optional func textFieldShouldReturn(_ textField: RaisePlaceholder) -> Bool
    @objc optional func textFieldShouldBeginEditing(_ textField: RaisePlaceholder) -> Bool
    @objc optional func textField1(_ textField: RaisePlaceholder, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
}

public class RaisePlaceholder: UITextField, UITextFieldDelegate
{
    weak var raisePlaceholderDelegate : RaisePlaceholderDelegate?

    public var animationDuration: Double = 0.5
    public var subjectColor: UIColor = .gray
    public var underLineColor: UIColor = .lightGray
    public var placeHolderColor: UIColor = .lightGray
    
    fileprivate let placeholderLabelFontSize: CGFloat = 10.0
    fileprivate var placeholderLabel: UILabel?
    fileprivate var placeholderTitle: String?
    fileprivate var underLineView : UIView?
    fileprivate var containAttributedPlaceholder : Bool = false
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        delegate = self
    }
    
    required public init?(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)
        delegate = self
    }
    
    override public func awakeFromNib()
    {
        drawUnderLine()
        createPlaceholderLabel()
        self.clipsToBounds = false
        self.borderStyle = .none
        
        if let placeHolderLocal = self.placeholder
        {
            if self.responds(to: #selector(getter: self.attributedPlaceholder))
            {
                
                if(containAttributedPlaceholder)
                {
                    self.attributedPlaceholder = NSAttributedString(string: placeHolderLocal, attributes: [NSAttributedString.Key.foregroundColor: self.placeHolderColor])
                }
            }
        }
    }
    
    override public var placeholder: String?
    {
        didSet
        {
            if let placeholderString = placeholder
            {
                self.containAttributedPlaceholder = true
                
                if(placeholderString != "")
                {
                    self.placeholderTitle = placeholderString
                }
                let attStr = NSMutableAttributedString(string: placeholderString as String, attributes: [NSAttributedString.Key.foregroundColor: self.placeHolderColor, NSAttributedString.Key.font : self.font!])
                if (getString(anything: placeholder).contains("*")) {
                    let range = (getString(anything: placeholder) as NSString).range(of: "*")
                    attStr.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.customRed], range: range)
                }
                self.attributedPlaceholder = attStr
            }
        }
    }
    
    override public var text : String?
    {
        willSet
        {
            
        }
        
        didSet
        {
            if(self.placeholder != "" && self.text != "")
            {
                DispatchQueue.main.async {
                    self.showFloatingLabel()
                }
            }
            else if(self.placeholder != nil && self.text == "")
            {
                if(self.placeholderTitle != nil && self.placeholderTitle != "")
                {
                    self.placeholder = self.placeholderTitle
                }
                else
                {
                    self.placeholderTitle = self.placeholder
                }
                self.hideFloatingLabel()
            }
        }
    }
    
    override public func layoutSubviews()
    {
        super.layoutSubviews()
        if(underLineView != nil && (underLineView?.isDescendant(of: self))!)
        {
            underLineView?.frame =  CGRect(x: 0, y: frame.size.height - 1, width: frame.size.width, height: 1)
            underLineView?.backgroundColor = underLineColor
            
            if let _ = self.placeholderLabel
            {
                
                if(self.placeholderLabel?.alpha == 1)
                {
                    //self.placeholderLabel?.frame = CGRect(x: 0, y: -5, width: (self.placeholderLabel?.frame.size.width)!, height: (self.placeholderLabel?.frame.size.height)!)
                }
            }
        }
    }
    
    func setAttributedPlaceholder(placeholderString : String)
    {
        self.attributedPlaceholder = NSAttributedString(string: placeholderString, attributes: [NSAttributedString.Key.foregroundColor: self.placeHolderColor])
        containAttributedPlaceholder = true
    }
    
    fileprivate func drawUnderLine()
    {
        
        underLineView = UIView(frame: CGRect(x: 0, y: frame.size.height - 1, width: frame.size.width, height: 1))
        underLineView?.backgroundColor = underLineColor
        self.addSubview(underLineView!)
    }
    
    fileprivate func createPlaceholderLabel()
    {
        
        let origin = self.frame.origin
        let label = UILabel(frame: CGRect(x: origin.x, y: origin.y, width: self.frame.size.width, height: 15.0))
        label.center = self.center
        label.text = ""
        label.font = self.font
        label.textColor = subjectColor
        
        self.addSubview(label)
        self.placeholderLabel = label
        self.placeholderTitle = self.placeholder

        self.placeholderLabel?.translatesAutoresizingMaskIntoConstraints = false
        self.addConstraint(NSLayoutConstraint.init(item: label, attribute: NSLayoutConstraint.Attribute.left, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.left, multiplier: 1.0, constant: 0))
        //self.addConstraint(NSLayoutConstraint.init(item: label, attribute: NSLayoutAttribute.height, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.height, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint.init(item: label, attribute: NSLayoutConstraint.Attribute.height, relatedBy: NSLayoutConstraint.Relation.equal, toItem: nil, attribute: NSLayoutConstraint.Attribute.notAnAttribute, multiplier: 1.0, constant: 35.0))
        self.addConstraint(NSLayoutConstraint.init(item: label, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.width, multiplier: 1.0, constant: 0))
        //self.addConstraint(NSLayoutConstraint.init(item: label, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: self, attribute: NSLayoutAttribute.centerY, multiplier: 1.0, constant: 0))
        self.addConstraint(NSLayoutConstraint.init(item: label, attribute: NSLayoutConstraint.Attribute.top, relatedBy: NSLayoutConstraint.Relation.equal, toItem: self, attribute: NSLayoutConstraint.Attribute.top, multiplier: 1.0, constant: 0))
    }
    
    
    override open func becomeFirstResponder() -> Bool
    {
        let result = super.becomeFirstResponder()
        if let _ = self.placeholderLabel,self.text == ""
        {
            self.showFloatingLabel()
        }
        
        //DispatchQueue.main.asyncAfter(deadline: .now() + 0.0, execute: {
            if let _ = self.raisePlaceholderDelegate?.textFieldDidBeginEditing?(self)
            {
                
            }
        //})
        return result
    }
    
    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool
    {
        if let returnValue = self.raisePlaceholderDelegate?.textFieldShouldBeginEditing?(self)
        {
            return returnValue
        }
        return true;
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
    }
    
    public func showFloatingLabel()
    {
        
        if let placeholderLabel = self.placeholderLabel
        {

            if placeholderLabel.alpha == 0
            {
                placeholderLabel.alpha = 1
            }
            
            if self.placeholder == ""
            {
                self.placeholderTitle = placeholderLabel.text
            }
            else
            {
                self.placeholderTitle = self.placeholder
            }
            self.placeholder = ""
            
            let attStr = NSMutableAttributedString(string: getString(anything: placeholderTitle))
            var range = (getString(anything: placeholderTitle) as NSString).range(of: getString(anything: placeholderTitle))
            attStr.addAttributes([NSAttributedString.Key.foregroundColor: subjectColor], range: range)
            if (getString(anything: placeholderTitle).contains("*")) {
                range = (getString(anything: placeholderTitle) as NSString).range(of: "*")
                attStr.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.customRed], range: range)
            }
            placeholderLabel.attributedText = attStr
            self.placeholderLabel?.font = self.font
            
            //self.placeholderLabel?.textColor = self.subjectColor
            CATransaction.begin()
            CATransaction.setCompletionBlock({
                //self.placeholderLabel?.textColor = self.subjectColor
            })
            
            let anim2 = CABasicAnimation(keyPath: "transform")
            let fromTransform = CATransform3DMakeScale(CGFloat(1.0), CGFloat(1.0), CGFloat(1))
            var toTransform = CATransform3DMakeScale(CGFloat(0.8), CGFloat(0.8), CGFloat(1))
            toTransform = CATransform3DTranslate(toTransform, -placeholderLabel.frame.width/8, -placeholderLabel.frame.height + 10, 0)
            anim2.fromValue = NSValue(caTransform3D: fromTransform)
            anim2.toValue = NSValue(caTransform3D: toTransform)
            anim2.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
            let animGroup = CAAnimationGroup()
            animGroup.animations = [anim2]
            animGroup.duration = 0.3
            animGroup.fillMode = CAMediaTimingFillMode.forwards;
            animGroup.isRemovedOnCompletion = false;
            placeholderLabel.layer.add(animGroup, forKey: "_floatingLabel")
            self.clipsToBounds = false
            CATransaction.commit()
        }
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField)
    {
        
        if let _ = self.placeholderLabel, self.text == ""
        {
            //let frame = placeholderLabel.frame
            self.hideFloatingLabel()
        }
        
        if let _ = self.raisePlaceholderDelegate?.textFieldDidEndEditing?(self)
        {
        }
        
    }
    
    public func hideFloatingLabel(){
        if let placeholderLabel = self.placeholderLabel, self.text == ""
        {
            
            //self.placeholderLabel?.textColor = placeHolderColor
            
            CATransaction.begin()
            CATransaction.setCompletionBlock({
                
                self.placeholder = self.placeholderTitle
                
                if(self.containAttributedPlaceholder)
                {
                    if(self.placeholderTitle != nil)
                    {
                        let attStr = NSMutableAttributedString(string: getString(anything: self.placeholderTitle))
                        var range = (getString(anything: self.placeholderTitle) as NSString).range(of: getString(anything: self.placeholderTitle))
                        attStr.addAttributes([NSAttributedString.Key.foregroundColor: self.placeHolderColor], range: range)
                        if (getString(anything: self.placeholderTitle).contains("*")) {
                            range = (getString(anything: self.placeholderTitle) as NSString).range(of: "*")
                            attStr.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.customRed], range: range)
                        }
                        self.attributedPlaceholder = attStr
                        //self.attributedPlaceholder = NSMutableAttributedString(string: self.placeholderTitle!, attributes: [NSAttributedString.Key.foregroundColor: self.placeHolderColor, NSAttributedString.Key.font : self.font!])
                    }
                }
                placeholderLabel.alpha = 0
            })
            
            let anim2 = CABasicAnimation(keyPath: "transform")
            var fromTransform = CATransform3DMakeScale(0.8, 0.8, 1)
            fromTransform = CATransform3DTranslate(fromTransform, -placeholderLabel.frame.width/8, -placeholderLabel.frame.height + 10, 0);
            let toTransform = CATransform3DMakeScale(1.0, 1.0, 1)
            anim2.fromValue = NSValue(caTransform3D: fromTransform)
            anim2.toValue = NSValue(caTransform3D: toTransform)
            anim2.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
            
            let animGroup = CAAnimationGroup()
            animGroup.animations = [anim2]
            animGroup.duration = 0.3
            animGroup.fillMode = CAMediaTimingFillMode.forwards;
            animGroup.isRemovedOnCompletion = false;
            
            placeholderLabel.layer.add(animGroup, forKey: "_animateLabelBack")
            CATransaction.commit()
        }
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        if let returnValue = self.raisePlaceholderDelegate?.textField1?(self, shouldChangeCharactersIn: range, replacementString: string)
        {
            return returnValue
        }
        return true
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool
    {
        if let returnvalue = self.raisePlaceholderDelegate?.textFieldShouldReturn?(self)
        {
            return returnvalue
        }
        return true
    }
}
