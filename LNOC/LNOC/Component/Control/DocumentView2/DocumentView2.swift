//
//  DocumentView.swift
//  StartupHaryana
//
//  Created by Vikram Jagad on 26/09/19.
//  Copyright © 2019 Gunjan Patel. All rights reserved.
//

import UIKit

open class DocumentView2 : UIView {
    //MARK:- Interface Builder
    //UIView
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewSeperator: UIView!
    
    //UILabel
    @IBOutlet weak var lblDocumentType: UILabel!
    @IBOutlet weak var lblViewDocumentStatic: UILabel!
    @IBOutlet weak var lblFileName: UILabel!
    @IBOutlet weak var lblMaxSize: UILabel!
    @IBOutlet weak var lblStar: UILabel!
    
    //UIImageView
    @IBOutlet weak var imgViewDocument: UIImageView!
    @IBOutlet weak var imgViewAttachment: UIImageView!
    @IBOutlet weak var imgViewPDF: UIImageView!
    
    //UIButton
    @IBOutlet weak var btnDocument: UIButton!
    @IBOutlet weak var btnViewDoc: UIButton!
    @IBOutlet weak var btnUpload: UIButton!
    
    @IBOutlet weak var anchorImgViewPdfHeight: NSLayoutConstraint!
    @IBOutlet weak var anchorImgViewPdfTrailing: NSLayoutConstraint!
    
    //MARK:- Lifecycle Methods
    override open func awakeFromNib() {
        super.awakeFromNib()
        setUpLbls()
        setupView()
        setupImage()
    }
    
    
    //set up lbls
    fileprivate func setUpLbls() {
        lblDocumentType.font = UIFont.regularTitleFont
        lblDocumentType.textColor = .customDescription
        lblStar.font = UIFont.regularTitleFont
        lblStar.textColor = .red
        lblMaxSize.text = "Max. \(documentSizeLimit)MB (Allowed pdf, doc, docx formats)"
        lblMaxSize.textColor = .customDescription
        lblMaxSize.font = UIFont.regularSystemFont(withSize: 12)
        lblFileName.font = UIFont.regularSystemFont(withSize: 14)
        lblViewDocumentStatic.font = UIFont.regularValueFont
        lblFileName.textColor = .red
        lblFileName.text = ""
        btnViewDoc.setTitle("", for: .normal)
        btnViewDoc.superview?.isHidden = true
        //btnViewDoc.setTitleColor(.pottersClay, for: .normal)
        //btnViewDoc.titleLabel?.font = UIFont.semiboldSmallFont
        
        btnUpload.setTitleColor(.customgreenTheamClr, for: .normal)
        btnUpload.setTitle("Upload", for: .normal)
        btnUpload.titleLabel?.font = UIFont.boldSystemFont(withSize: 14)
        btnUpload.superview?.isHidden = true
    }
    
    //set up view
    fileprivate func setupView() {
        viewSeperator.backgroundColor = .appSeparatorColor
        
    }
    
    fileprivate func setupImage() {
        imgViewAttachment.image = UIImage(named: k_ic_attach)
        imgViewPDF.image = UIImage(named: k_ic_viewpdf)?.withRenderingMode(.alwaysTemplate)
        imgViewPDF.tintColor = .customLightBrownGradientColor
    }
    
    //MARK:- Public Methods
    func configureTitle(str: String, btnTag: Int, showFormatBtn: Bool = false, hideAsterisk: Bool = false, hideMaxSize:Bool = false, isFileNameShow : Bool = false) {
        
        self.lblStar.text = ""
        
        if !hideAsterisk {
            self.lblStar.text = " *"
        }
        
        lblDocumentType.text = str
        
        lblFileName.superview?.isHidden = !isFileNameShow
        setFileName(fileName: nil)
        
        if (showFormatBtn) {
            //btnFormatHeightConstraint.constant = 30
            //btnViewDoc.setTitle("View Document", for: .normal)
        }
        if (hideMaxSize) {
            lblMaxSize.text = ""
            lblMaxSize.isHidden = true
        }
        btnDocument.tag = btnTag
        btnUpload.tag = btnTag
        btnViewDoc.tag = btnTag
    }
    
    func setFileName(fileName : String?, noSetelectedFileText: String = CommonLabels.noFileSelectedString, showViewDoc: Bool = false, showBtnUpload: Bool = false) {
        if (fileName ?? "").trimmingCharacters(in: .whitespacesAndNewlines).count > 0 {
            lblFileName.textColor = .customLightBrownGradientColor
            lblFileName.text = fileName
            if (showViewDoc) {
                btnViewDoc.superview?.isHidden = false
                lblViewDocumentStatic.text = "View Document"
                //btnFormatHeightConstraint.constant = 30
                //btnViewDoc.setTitle("View Document", for: .normal)
            }
            
            btnUpload.superview?.isHidden = !showBtnUpload
            btnUpload.setTitle("Upload", for: .normal)

            
        } else {
            lblFileName.textColor = .red
            lblFileName.text = noSetelectedFileText
            btnUpload.superview?.isHidden = true
        }
    }
    
    
    func setEditMode(fileName: String, hideAsterisk: Bool = false) {
        self.lblStar.isHidden = false
        
        self.lblViewDocumentStatic.text = fileName.is_trimming_WS_NL_to_String ?? ""
        self.lblViewDocumentStatic.textColor = .customBlack
        
        self.btnViewDoc.superview?.isHidden = fileName.is_trimming_WS_NL_to_String == nil
        
        self.imgViewDocument.superview?.isHidden = false
        self.lblMaxSize.superview?.isHidden = false
        self.btnUpload.superview?.isHidden = true
        self.viewSeperator.isHidden = false
        
    }
    
    func setViewMode(fileName: String) {
        self.lblStar.isHidden = true
        
        self.lblViewDocumentStatic.text = fileName.is_trimming_WS_NL_to_String ?? CommonLabels.noFileSelectedString
        self.lblViewDocumentStatic.textColor = .customBlack
        
        self.btnViewDoc.superview?.isHidden = false
        self.imgViewDocument.superview?.isHidden = true
        self.lblMaxSize.superview?.isHidden = true
        self.btnUpload.superview?.isHidden = true
        self.viewSeperator.isHidden = true
        
        
        self.imgViewPDF.image = .ic_viewpdf
        let isFileAvailable = fileName.is_trimming_WS_NL_to_String == nil
        
        self.imgViewPDF.isHidden = isFileAvailable
        self.anchorImgViewPdfHeight.constant = isFileAvailable ? 0 : (IS_IPAD ? 30 : 20)
        self.anchorImgViewPdfTrailing.constant = isFileAvailable ? 0 : 4
    }
}
