//
//  DocumentView.swift
//  StartupHaryana
//
//  Created by Vikram Jagad on 26/09/19.
//  Copyright © 2019 Gunjan Patel. All rights reserved.
//

import UIKit

open class DocumentView: UIView {
    //MARK:- Interface Builder
    //UIView
    @IBOutlet var contentView: UIView!
    @IBOutlet var viewSeparator: UIView!
    
    //UIStackView
    @IBOutlet var mainStackView: UIStackView!
    
    //UILabel
    @IBOutlet weak var lblDocumentName: UILabel!
    @IBOutlet weak var lblDocumentInfo: UILabel!
    @IBOutlet weak var lblDocumentSize: UILabel!
    
    //UIImageView
    @IBOutlet weak var imgViewClose: UIImageView!
    
    //UIButton
    @IBOutlet weak var btnDocument: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    
    //NSLayoutConstraint
    @IBOutlet weak var imgViewCloseHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgViewCloseWidthConstraint: NSLayoutConstraint!
    
    var isShowFileSize = false
    
    //MARK:- Lifecycle Methods
    override open func awakeFromNib() {
        super.awakeFromNib()
        setUpLbls()
        showDocumentViewChangeImage(isEdit: true)
    }
    
    //MARK:- Private Methods
    //set up lbls
    fileprivate func setUpLbls() {
        lblDocumentName.font = .boldValueFont
        lblDocumentName.textColor = .customBlue
        lblDocumentInfo.font = .regularValueFont
        lblDocumentInfo.textColor = .customSubtitle
        lblDocumentSize.font = .regularTitleFont
        lblDocumentSize.textColor = .customSubtitle
        lblDocumentName.numberOfLines = 0
        
        viewSeparator.backgroundColor = .customSeparator
    }
    
    func showDocumentViewChangeImage(isEdit: Bool) {
        imgViewClose.image = isEdit ? .ic_header_close : .ic_pdf
        imgViewClose.tintColor = .customRed
        imgViewCloseHeightConstraint.constant = isEdit ? (IS_IPAD ? 25 : 15) : (IS_IPAD ? 50 : 40)
        imgViewCloseWidthConstraint.constant = isEdit ? (IS_IPAD ? 25 : 15) : (IS_IPAD ? 40 : 30)
    }
    
    //MARK:- Public Methods
    func configureViewForShowing(model:DocumentModel, btnTag: Int) {
        self.lblDocumentName.text = [model.title.is_trimming_WS_NL_to_String, model.type.is_trimming_WS_NL_to_String].compactMap({ $0 }).joined(separator: ".")
        self.lblDocumentInfo.text = DateConverter.getDateStringFromDate(aDate: model.date, outputFormat: DateFormats.dd_MM_yyyy)
        self.btnDocument.tag = btnTag
        self.btnClose.tag = btnTag
        
        self.lblDocumentSize.text = ""
        
        
        if isShowFileSize {
            if let docImage = model.document as? UIImage {
                DispatchQueue.main.async {
                    let imageDataCount = docImage.jpegData(compressionQuality: 1)?.count ?? docImage.pngData()?.count ?? -1
                    
                    if imageDataCount <= 0 {
                        self.lblDocumentSize.text = ""
                    } else {
                        let imageSize = Double(imageDataCount) / 1024
                        
                        if imageSize < 1024 {
                            self.lblDocumentSize.text = String(format: "%.2f", imageSize) + " KB"
                        } else {
                            let imageSize = imageSize / 1024
                            self.lblDocumentSize.text = String(format: "%.2f", imageSize) + " MB"
                        }
                    }
                }
                
            } else if let url = model.url {
                if let fileAttributes = try? FileManager.default.attributesOfItem(atPath: url.path) {
                    let fileSizeNumber = fileAttributes[FileAttributeKey.size] as! NSNumber
                    var fileSize : Double = Double(fileSizeNumber.int64Value)
                    fileSize = fileSize / 1024
                
                    if fileSize <= 0 {
                        self.lblDocumentSize.text = ""
                    } else {
                        fileSize = Double(fileSize) / 1024
                        
                        if fileSize < 1024 {
                            self.lblDocumentSize.text = String(format: "%.2f", fileSize) + " KB"
                        } else {
                            let fileSize = fileSize / 1024
                            self.lblDocumentSize.text = String(format: "%.2f", fileSize) + " MB"
                        }
                    }
                    
                } else {
                    DispatchQueue.main.async {
                        if let fileSizeNumber = try? Data(contentsOf: url).count as NSNumber {
                            
                            var fileSize : Double = Double(fileSizeNumber.int64Value)
                            fileSize = fileSize / 1024
                        
                            if fileSize <= 0 {
                                self.lblDocumentSize.text = ""
                            } else {
                                
                                if fileSize < 1024 {
                                    self.lblDocumentSize.text = String(format: "%.2f", fileSize) + " KB"
                                } else {
                                    let fileSize = fileSize / 1024
                                    self.lblDocumentSize.text = String(format: "%.2f", fileSize) + " MB"
                                }
                            }
                        } else {
                            self.lblDocumentSize.text = ""
                        }
                    }
                }
            } else {
                self.lblDocumentSize.text = ""
            }
        }
        
        
    }
    
    func reArrange(btnTag: Int) {
        btnDocument.tag = btnTag
        btnClose.tag = btnTag
    }
}
