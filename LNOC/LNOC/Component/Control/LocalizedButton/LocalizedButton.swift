//
//  LocalizedButton.swift
//  Vidyanjali
//
//  Created by Vikram Jagad on 06/11/20.
//  Copyright © 2020 Vikram Jagad. All rights reserved.
//

import UIKit

class LocalizedButton: UIButton {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        setTitle(LocalizationParam.getLocalizedStringFor(key: getString(anything: title(for: .normal))), for: .normal)
    }
    
    var customFont = CustomFont.regular
    
    @IBInspectable var customFontString: String? = "regular" {
        willSet {
            if let font = CustomFont(rawValue: getString(anything: newValue?.lowercased())) {
                customFont = font
            }
        }
    }
    
    @IBInspectable var fontSize: CGFloat = 16 {
        willSet {
            switch customFont {
            case .regular:
                titleLabel?.font = UIFont.regularSystemFont(withSize: newValue)
            case .bold:
                titleLabel?.font = UIFont.boldSystemFont(withSize: newValue)
            case .semiBold:
                titleLabel?.font = UIFont.semiBoldSystemFont(withSize: newValue)
            case .italic:
                titleLabel?.font = UIFont.italicSystemFont(withSize: newValue)
            }
        }
    }
}
