//
//  LocalizedLabel.swift
//  Vidyanjali
//
//  Created by Vikram Jagad on 06/11/20.
//  Copyright © 2020 Vikram Jagad. All rights reserved.
//

import UIKit

class LocalizedLabel: UILabel {
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    func commonInit() {
        text = LocalizationParam.getLocalizedStringFor(key: getString(anything: text))
    }
    
    var customFont = CustomFont.regular
    
    @IBInspectable var customFontString: String? = "regular" {
        willSet {
            if let font = CustomFont(rawValue: getString(anything: newValue?.lowercased())) {
                customFont = font
            }
        }
    }
    
    @IBInspectable var fontSize: CGFloat = 16 {
        willSet {
            switch customFont {
            case .regular:
                font = UIFont.regularSystemFont(withSize: newValue)
            case .bold:
                font = UIFont.boldSystemFont(withSize: newValue)
            case .semiBold:
                font = UIFont.semiBoldSystemFont(withSize: newValue)
            case .italic:
                font = UIFont.italicSystemFont(withSize: newValue)
            }
        }
    }
}
