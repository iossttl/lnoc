//
//  ColViewDelegate.swift
//  Vidyanjali
//
//  Created by Vikram Jagad on 22/11/20.
//  Copyright © 2020 Vikram Jagad. All rights reserved.
//

import UIKit

protocol ColViewDelegate: class {
//    new
   func didSelect(colView: UICollectionView, indexPath: IndexPath)
    
}
 protocol ColViewDashboardDelegate {
    func didSelect(colView: UICollectionView, indexPath: IndexPath, type: DashboardColViewType)
    func changeCurrentPage(index: Double)
}

@objc protocol ColViewDelegateForPhotosVideos {
    @objc optional func didSelect(colView: UICollectionView, indexPath: IndexPath)
    @objc optional func willDisplay(colView: UICollectionView, cell: UICollectionViewCell, indexPath: IndexPath)
    @objc optional func btnRemoveClickHandler(_ sender: UIButton)
}

@objc protocol ColViewDelegatePageController {
    @objc optional func changeCurrentPage(index: Double)
    @objc optional func didSelect(colView: UICollectionView, indexPath: IndexPath)
}

@objc protocol ColViewShareDelegate {
    @objc optional func shareBtnTapped(index: Int)
}
