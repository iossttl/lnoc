//
//  TblViewDelegate.swift
//  Vidyanjali
//
//  Created by Pradip Patel on 20/11/20.
//  Copyright © 2020 Vikram Jagad. All rights reserved.
//

import UIKit

@objc protocol TblViewDelegate : class {
    @objc optional func didSelect(tbl: UITableView, indexPath: IndexPath)
    @objc optional func willDisplay(tbl: UITableView, indexPath: IndexPath)
    @objc optional func searchGrievanceId(str: String)
    @objc optional func tbldidScroll(scrollView:UIScrollView)
    @objc optional func tblViewDidEndDecelerating(scrollView:UIScrollView)
    @objc optional func didShare(tbl: UITableView, indexPath: IndexPath)
}
