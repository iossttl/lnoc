//
//  DocumentPickerViewControllerDelegate.swift
//  PSIS
//
//  Created by Vikram Jagad on 08/05/19.
//  Copyright © 2019 Vikram Jagad. All rights reserved.
//

import UIKit

class DocumentPickerViewControllerDelegate: NSObject
{
    //MARK:- Variables
    //Local
    fileprivate let controller: UIDocumentPickerViewController
    fileprivate weak var delegate: DocumentPickerDelegate?
    fileprivate let sender: UIView
    fileprivate var fileSize : Double = documentSizeLimit

    init(VC: UIDocumentPickerViewController, delegate: DocumentPickerDelegate, sender: UIView, file_Size : Double = documentSizeLimit) {
        controller = VC
        self.delegate = delegate
        self.sender = sender
        self.fileSize = file_Size
        super.init()
        setUp()
    }
    
    //MARK:- Private Methods
    //Set Up
    fileprivate func setUp() {
        controller.delegate = self
    }
}

//MARK:- UIDocumentPickerDelegate Methods
extension DocumentPickerViewControllerDelegate: UIDocumentPickerDelegate {
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        do {
            let fileAttributes = try FileManager.default.attributesOfItem(atPath: url.path)
            let fileSizeNumber = fileAttributes[FileAttributeKey.size] as! NSNumber
            let fileSize = fileSizeNumber.int64Value
            var sizeMB = Double(fileSize / 1024)
            sizeMB = Double(sizeMB / 1024)
            if (sizeMB > self.fileSize) {
                delegate?.DocumentPickerData(url: nil, errorMsg: CommonMessages.msg_document_should_be_less_than.replacingOccurrences(of: "abc", with: "\(self.fileSize)"), sender: sender)
            } else {
                delegate?.DocumentPickerData(url: url, errorMsg: "", sender: sender)
            }
        } catch let error {
            delegate?.DocumentPickerData(url: nil, errorMsg: error.localizedDescription, sender: sender)
        }
        controller.dismiss(animated: true, completion: nil)
    }
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let url = urls.first else {
            delegate?.DocumentPickerData(url: nil, errorMsg: CommonMessages.msg_unexpected_problem, sender: sender)
            controller.dismiss(animated: true, completion: nil)
            return
        }
        
        do {
            let fileAttributes = try FileManager.default.attributesOfItem(atPath: url.path)
            let fileSizeNumber = fileAttributes[FileAttributeKey.size] as! NSNumber
            let fileSize = fileSizeNumber.int64Value
            var sizeMB = Double(fileSize / 1024)
            sizeMB = Double(sizeMB / 1024)
            if (sizeMB > self.fileSize) {
                delegate?.DocumentPickerData(url: nil, errorMsg: CommonMessages.msg_document_should_be_less_than.replacingOccurrences(of: "abc", with: "\(self.fileSize)"), sender: sender)
            } else {
                delegate?.DocumentPickerData(url: url, errorMsg: "", sender: sender)
            }
        } catch let error {
            delegate?.DocumentPickerData(url: nil, errorMsg: error.localizedDescription, sender: sender)
        }
        controller.dismiss(animated: true, completion: nil)
    }
}
