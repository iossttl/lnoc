//
//  DocumentPickerUrldelegate.swift
//  MyGovMaharashtra
//
//  Created by Gunjan Patel on 28/06/19.
//  Copyright © 2019 Vikram Jagad. All rights reserved.
//

import UIKit

protocol DocumentPickerDelegate : class
{
    func DocumentPickerData(url: URL?,errorMsg: String, sender: UIView)
}
