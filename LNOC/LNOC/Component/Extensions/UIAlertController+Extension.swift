//
//  UIAlertController+Extension.swift
//  DemoPickerView
//
//  Created by Vikram Jagad on 25/04/19.
//  Copyright © 2019 Vikram Jagad. All rights reserved.
//

import UIKit

extension UIAlertController
{
    func addAction(withTitle title: String, style: UIAlertAction.Style, textColor: UIColor? = nil, image: UIImage? = nil, handler: ((UIAlertAction) -> Void)?)
    {
        let action = UIAlertAction(title: title, style: style, handler: handler)
        if let image = image
        {
            action.setValue(image, forKey: "image")
        }
        if let textColor = textColor
        {
            action.setValue(textColor, forKey: "titleTextColor")
        }
        self.addAction(action)
    }
    
    func set(vc: UIViewController?, width: CGFloat? = nil, height: CGFloat? = nil)
    {
        guard let vc = vc else {
            return
        }
        self.setValue(vc, forKey: "contentViewController")
        if let height = height
        {
            vc.preferredContentSize.height = height
            self.preferredContentSize.height = height
        }
    }
    
    class func showWith(title: String? = nil, msg: String? = nil, style: UIAlertController.Style = .alert, onVC: UIViewController? = nil, actionTitles: [String]?, actionStyles: [UIAlertAction.Style], actionHandlers: [((UIAlertAction?) -> Swift.Void)?] = []) {
        let alertVC = UIAlertController(title: title, message: msg, preferredStyle: style)
        if let actionTitles = actionTitles {
            for i in 0..<actionTitles.count {
                alertVC.addAction(withTitle: actionTitles[i], style: actionStyles.indices.contains(i) ? actionStyles[i] : .default, handler: actionHandlers.indices.contains(i) ? actionHandlers[i] : nil)
            }
        }
        if let vc = onVC {
            vc.present(alertVC, animated: true, completion: nil)
        } else {
            (Global.shared.delegateRootVC)?.present(alertVC, animated: true, completion: nil)
        }
    }
    
    class func invalidToken(msg: String? = nil, onVC: UIViewController, signInHandlers : @escaping () -> ()) {
        
        self.showWith(title: "", msg: msg, style: .alert, onVC: onVC, actionTitles: [CommonButtons.cancel,CommonButtons.btn_signin], actionStyles: [.cancel,.destructive], actionHandlers: [{ (cancelAction) in
            
            //Cancel Action
            UserPreferences.remove(forKey: UserPreferencesKeys.General.isSkip)
            UserModel.signedIn = false
            UserModel.remove()
            K_NC.post(name: .loginLogoutDone, object: nil)
            onVC.navigationController?.popViewController(animated: true)

        }, { (yesAction) in
            
            //Sign In Action
            let loginVC = LoginViewController.instantiate(appStoryboard: .Login)
//            loginVC.isPopedUp = true
//            loginVC.isFromInvalidTokenPopUp = true
//            loginVC.loginDoneBlock = {
//                onVC.dismiss(animated: true, completion: nil)
//                signInHandlers()
//            }
            UserPreferences.remove(forKey: UserPreferencesKeys.General.isSkip)
            let navCon = UICustomNavigationController(rootViewController: loginVC)
            navCon.modalPresentationStyle = .fullScreen
            onVC.present(navCon, animated: false, completion: nil)
            
        }])
    }
}
