//
//  UIImage+Extension.swift
//  SharkID
//
//  Created by Vikram Jagad on 25/02/19.
//  Copyright © 2019 sttl. All rights reserved.
//

import UIKit
import CoreImage
import SDWebImage

extension UIImage
{
    var alwaysTemplate : UIImage {
        return withRenderingMode(.alwaysTemplate)
    }
    
    func fixOrientation() -> UIImage
    {
        
        // No-op if the orientation is already correct
        if ( self.imageOrientation == UIImage.Orientation.up )
        {
            return self;
        }
        
        // We need to calculate the proper transformation to make the image upright.
        // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
        var transform: CGAffineTransform = CGAffineTransform.identity
        
        if ( self.imageOrientation == UIImage.Orientation.down || self.imageOrientation == UIImage.Orientation.downMirrored )
        {
            transform = transform.translatedBy(x: self.size.width, y: self.size.height)
            transform = transform.rotated(by: CGFloat(Double.pi))
        }
        
        if ( self.imageOrientation == UIImage.Orientation.left || self.imageOrientation == UIImage.Orientation.leftMirrored )
        {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.rotated(by: CGFloat(Double.pi / 2.0))
        }
        
        if ( self.imageOrientation == UIImage.Orientation.right || self.imageOrientation == UIImage.Orientation.rightMirrored )
        {
            transform = transform.translatedBy(x: 0, y: self.size.height);
            transform = transform.rotated(by: CGFloat(-Double.pi / 2.0));
        }
        
        if ( self.imageOrientation == UIImage.Orientation.upMirrored || self.imageOrientation == UIImage.Orientation.downMirrored )
        {
            transform = transform.translatedBy(x: self.size.width, y: 0)
            transform = transform.scaledBy(x: -1, y: 1)
        }
        
        if ( self.imageOrientation == UIImage.Orientation.leftMirrored || self.imageOrientation == UIImage.Orientation.rightMirrored )
        {
            transform = transform.translatedBy(x: self.size.height, y: 0);
            transform = transform.scaledBy(x: -1, y: 1);
        }
        
        // Now we draw the underlying CGImage into a new context, applying the transform
        // calculated above.
        let ctx: CGContext = CGContext(data: nil, width: Int(self.size.width), height: Int(self.size.height),
                                       bitsPerComponent: self.cgImage!.bitsPerComponent, bytesPerRow: 0,
                                       space: self.cgImage!.colorSpace!,
                                       bitmapInfo: self.cgImage!.bitmapInfo.rawValue)!;
        
        ctx.concatenate(transform)
        
        if ( self.imageOrientation == UIImage.Orientation.left ||
            self.imageOrientation == UIImage.Orientation.leftMirrored ||
            self.imageOrientation == UIImage.Orientation.right ||
            self.imageOrientation == UIImage.Orientation.rightMirrored )
        {
            ctx.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.height,height: self.size.width))
        }
        else
        {
            ctx.draw(self.cgImage!, in: CGRect(x: 0,y: 0,width: self.size.width,height: self.size.height))
        }
        
        // And now we just create a new UIImage from the drawing context and return it
        return UIImage(cgImage: ctx.makeImage()!)
    }
    
    func rotate(degrees: Float) -> UIImage?
    {
        let radians = degrees * (Float.pi / 180.0)
        var newSize = CGRect(origin: CGPoint.zero, size: self.size).applying(CGAffineTransform(rotationAngle: CGFloat(radians))).size
        // Trim off the extremely small float value to prevent core graphics from rounding it up
        newSize.width = floor(newSize.width)
        newSize.height = floor(newSize.height)
        
        UIGraphicsBeginImageContextWithOptions(newSize, true, self.scale)
        let context = UIGraphicsGetCurrentContext()!
        
        // Move origin to middle
        context.translateBy(x: newSize.width/2, y: newSize.height/2)
        // Rotate around middle
        context.rotate(by: CGFloat(radians))
        // Draw the image at its center
        self.draw(in: CGRect(x: -self.size.width/2, y: -self.size.height/2, width: self.size.width, height: self.size.height))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func hasFace() -> Bool
    {
        guard let ciImage = CIImage(image: self) else{
            return false
        }
        let accuracy = [CIDetectorAccuracy:CIDetectorAccuracyHigh]
        if let detector = CIDetector(ofType: CIDetectorTypeFace, context: nil, options: accuracy){
            let faces = detector.features(in: ciImage)
            if faces.count > 0{
                return true
            }
        }
        return false
    }
    
    //MARK: - Image Compression Function
    func compressImage() -> UIImage
    {
        let originalImageSize = self.size
        var compressedImage = self
        
        if(originalImageSize.width > 400.0 || originalImageSize.height  > 400.0)
        {
            let newsize = CGSize(width: 400.0, height: 400.0)
            
            // Scale the original image to match the new size.
            UIGraphicsBeginImageContext(newsize)
            self.draw(in: CGRect(x: 0, y: 0, width: newsize.width, height: newsize.height))
            compressedImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
        }
        
        return compressedImage
    }
    
    func compressImageToLogoSize() -> UIImage
    {
        let originalImageSize = self.size
        var compressedImage = self
        
        if(originalImageSize.width > 40 || originalImageSize.height  > 40)
        {
            var ratio: CGFloat = 0
            var newsize = CGSize(width: 0, height: 0)
            if (originalImageSize.width > originalImageSize.height)
            {
                ratio = originalImageSize.height/originalImageSize.width
                newsize = CGSize(width: 100, height: 100*ratio)
            }
            else
            {
                ratio = originalImageSize.width/originalImageSize.height
                newsize = CGSize(width: 100*ratio, height: 100)
            }
            
            // Scale the original image to match the new size.
            UIGraphicsBeginImageContext(newsize)
            self.draw(in: CGRect(x: 0, y: 0, width: newsize.width, height: newsize.height))
            compressedImage = UIGraphicsGetImageFromCurrentImageContext()!
            UIGraphicsEndImageContext()
        }
        
        return compressedImage
    }
    
    func compressImageToImageSizeLimit(scale: CGFloat) -> UIImage
    {
        // Calculate new size given scale factor.
        let originalImageSize = self.size
        let newsize = CGSize(width: scale * originalImageSize.width, height: scale * originalImageSize.height)
        
        // Scale the original image to match the new size.
        UIGraphicsBeginImageContext(newsize)
        self.draw(in: CGRect(x: 0, y: 0, width: newsize.width, height: newsize.height))
        let compressedImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        
        return compressedImage
    }
    
    //MARK:- Resize Percentage
    func resized(withPercentage percentage: CGFloat) -> UIImage? {
        let canvasSize = CGSize(width: size.width * percentage, height: size.height * percentage)
        UIGraphicsBeginImageContextWithOptions(canvasSize, false, scale)
        defer { UIGraphicsEndImageContext() }
        draw(in: CGRect(origin: .zero, size: canvasSize))
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    //MARK:- Resize To MB
    func resizedToMB(sizeInMB: Double) -> (UIImage?, Data?) {
        guard let imageData = self.pngData() else { return (nil, nil) }
        
        var resizingImage = self
        var imageSizeKB = Double(imageData.count) / (1024 * sizeInMB)
        
        while imageSizeKB > (1024 * sizeInMB) {
            guard let resizedImage = resizingImage.resized(withPercentage: 0.9),
                let imageData = resizedImage.pngData()
                else { return (nil, nil) }
            
            resizingImage = resizedImage
            imageSizeKB = Double(imageData.count) / (1024 * sizeInMB)
        }
        
        return (resizingImage, imageData)
    }
    
    func grayscaleImage() -> UIImage? {
        let filter = CIFilter(name: "CIPhotoEffectMono")
        let ciInput = CIImage(image: self)
        filter?.setValue(ciInput, forKey: "inputImage")
        let ciOutput = filter?.outputImage
        let ciContext = CIContext()
        let cgImage = ciContext.createCGImage(ciOutput!, from: (ciOutput?.extent)!)
        return UIImage(cgImage: cgImage!)
    }
}
