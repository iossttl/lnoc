//
//  UITextfield+CutomizedTextfield.swift
//  SharkID
//
//  Created by Jinal Patel on 5/31/17.
//  Copyright © 2017 sttl. All rights reserved.
//

import Foundation
import UIKit

//MARK:- For PlaceHolderColor
extension UITextField
{
    func setPaddingForTextField()
    {
        let paddingView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 10, height: 30))
        self.leftView = paddingView;
        self.leftViewMode = UITextField.ViewMode.always
    }
    
    func setAttributtedPlaceHolder(text: String, ofColor: UIColor, font: UIFont? = nil)
    {
        if let getFont = font {
            self.attributedPlaceholder = NSAttributedString(string: text, attributes: [.foregroundColor : ofColor, .font : getFont])
        } else {
            self.attributedPlaceholder = NSAttributedString(string: text, attributes: [.foregroundColor : ofColor])
        }
    }
    
    func setPlaceholder(_ placeholderString : String , color: UIColor, setFont: UIFont?) {
        let aSetFont = setFont ?? self.font
        let attributeString : [NSAttributedString.Key : Any]
        
        if let getFont = aSetFont {
            attributeString = [.foregroundColor: color,
                               .font: getFont]
        } else {
            attributeString = [.foregroundColor: color]
        }
        
        self.attributedPlaceholder = NSAttributedString(string: placeholderString, attributes: attributeString)
    }
}

//MARK:- For Border
extension UITextField
{
    //MARK:- Set Border to TextFields
    func addBottomBorder(withColor: UIColor)
    {
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = withColor.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
}
