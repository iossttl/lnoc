//
//  DGGlobal.swift
//  DigitalGujarat
//
//  Created by Xamarin PC on 15/09/16.
//  Copyright © 2016 Digital Gujarat. All rights reserved.
//

import UIKit
import CoreText

extension UIFont {
    
    class func regularSystemFont(withSize: CGFloat) -> UIFont {
       
        if IS_IPAD {
            return UIFont(name: FontConst.Roboto_Regular, size: withSize+8) ?? UIFont.systemFont(ofSize: withSize+8)
        } else if IS_IPHONE_6P {
            return UIFont(name: FontConst.Proxi_Regular, size: withSize+2) ?? UIFont.systemFont(ofSize: withSize+2)
        } else if IS_IPHONE_6 || IS_IPHONE_X {
            return UIFont(name: FontConst.Roboto_Regular, size: withSize+1) ?? UIFont.systemFont(ofSize: withSize+1)
        } else {
            return UIFont(name: FontConst.Roboto_Regular, size: withSize) ?? UIFont.systemFont(ofSize: withSize)
        }
    }
    
    class func boldSystemFont(withSize: CGFloat) -> UIFont {
        if (IS_IPAD) {
            return UIFont(name: FontConst.Roboto_Bold, size: withSize+8) ?? UIFont.boldSystemFont(ofSize: withSize+8)
        } else if (IS_IPHONE_6P) {
            return UIFont(name: FontConst.Roboto_Bold, size: withSize+2) ?? UIFont.boldSystemFont(ofSize: withSize+2)
        } else if (IS_IPHONE_6 || IS_IPHONE_X) {
            return UIFont(name: FontConst.Roboto_Bold, size: withSize+1) ?? UIFont.boldSystemFont(ofSize: withSize+1)
        } else {
            return UIFont(name: FontConst.Roboto_Bold, size: withSize) ?? UIFont.boldSystemFont(ofSize: withSize)
        }
    }
    
    class func heavySystemFont(withSize: CGFloat) -> UIFont {
        if (IS_IPAD) {
            return UIFont.systemFont(ofSize: withSize + 8, weight: .heavy)
        } else if (IS_IPHONE_6P) {
            return UIFont.systemFont(ofSize: withSize + 2, weight: .heavy)
        } else if (IS_IPHONE_6 || IS_IPHONE_X) {
            return UIFont.systemFont(ofSize: withSize + 1, weight: .heavy)
        } else {
            return UIFont.systemFont(ofSize: withSize, weight: .heavy)
        }
    }
    
    class func mediumSystemFont(withSize: CGFloat) -> UIFont {
//        if (IS_IPAD) {
//            return UIFont.systemFont(ofSize: withSize+8, weight: .medium)
//        } else if (IS_IPHONE_6P) {
//            return UIFont.systemFont(ofSize: withSize+2, weight: .medium)
//        } else if (IS_IPHONE_6 || IS_IPHONE_X) {
//            return UIFont.systemFont(ofSize: withSize+1, weight: .medium)
//        } else {
//            return UIFont.systemFont(ofSize: withSize, weight: .medium)
//        }
   
        if (IS_IPAD) {
            return UIFont(name: FontConst.Roboto_Medium, size: withSize+8) ?? UIFont.systemFont(ofSize: withSize+8, weight: .semibold)
        } else if (IS_IPHONE_6P) {
            return UIFont(name: FontConst.Roboto_Medium, size: withSize+2) ?? UIFont.systemFont(ofSize: withSize+2, weight: .semibold)
        } else if (IS_IPHONE_6 || IS_IPHONE_X) {
            return UIFont(name: FontConst.Roboto_Medium, size: withSize+1) ?? UIFont.systemFont(ofSize: withSize+1, weight: .semibold)
        } else {
            return UIFont(name: FontConst.Roboto_Medium, size: withSize) ?? UIFont.systemFont(ofSize: withSize, weight: .semibold)
        }
    
    
    
    
    }
    
    class func semiBoldSystemFont(withSize: CGFloat) -> UIFont {
        if (IS_IPAD) {
            return UIFont(name: FontConst.Roboto_Medium, size: withSize+8) ?? UIFont.systemFont(ofSize: withSize+8, weight: .semibold)
        } else if (IS_IPHONE_6P) {
            return UIFont(name: FontConst.Roboto_Medium, size: withSize+2) ?? UIFont.systemFont(ofSize: withSize+2, weight: .semibold)
        } else if (IS_IPHONE_6 || IS_IPHONE_X) {
            return UIFont(name: FontConst.Roboto_Medium, size: withSize+1) ?? UIFont.systemFont(ofSize: withSize+1, weight: .semibold)
        } else {
            return UIFont(name: FontConst.Roboto_Medium, size: withSize) ?? UIFont.systemFont(ofSize: withSize, weight: .semibold)
        }
    }
    
    class func italicSystemFont(withSize: CGFloat) -> UIFont {
        if (IS_IPAD) {
            return UIFont.italicSystemFont(ofSize: withSize+8)
        } else if (IS_IPHONE_6P) {
            return UIFont.italicSystemFont(ofSize: withSize+2)
        } else if (IS_IPHONE_6 || IS_IPHONE_X) {
            return UIFont.italicSystemFont(ofSize: withSize+1)
        } else {
            return UIFont.italicSystemFont(ofSize: withSize)
        }
    }
    
    //MARK:- Proxima-Nova
    class func proximaNovaRegular(withSize: CGFloat) -> UIFont? {
        
        if let device = UIDevice.current.deviceType{
            switch device {
            case .iPad:
                return UIFont(name: FontConst.Proxi_Regular, size: withSize+8)
            case .iPhone6Plus:
                return UIFont(name: FontConst.Proxi_Regular, size: withSize+2)
            case .iPhone6,.iPhoneX:
                 return UIFont(name: FontConst.Proxi_Regular, size: withSize+1)
            default:
                return UIFont(name: FontConst.Proxi_Regular, size: withSize)
            }
        }else{
            return UIFont(name: FontConst.Proxi_Regular, size: withSize)
        }
        
    }
    
    class func proximaNovaBold(withSize: CGFloat) -> UIFont? {
        
        
        if let device = UIDevice.current.deviceType{
            switch device {
            case .iPad:
                return UIFont(name: "ProximaNova-Bold", size: withSize+8)
            case .iPhone6Plus:
                return UIFont(name: "ProximaNova-Bold", size: withSize+2)
            case .iPhone6,.iPhoneX:
                return UIFont(name: "ProximaNova-Bold", size: withSize+1)
            default:
                return UIFont(name: "ProximaNova-Bold", size: withSize)
            }
        }else{
            return UIFont(name: FontConst.Proxi_Bold, size: withSize)
        }
        
    }
    
    class func proximaNovaSemiBold(withSize: CGFloat) -> UIFont? {
        
        
        if let device = UIDevice.current.deviceType{
            switch device {
            case .iPad:
                return UIFont(name: FontConst.Proxi_Semibold, size: withSize+8)
            case .iPhone6Plus:
                return UIFont(name: FontConst.Proxi_Semibold, size: withSize+2)
            case .iPhone6,.iPhoneX:
                return UIFont(name: FontConst.Proxi_Semibold, size: withSize+1)
            default:
                return UIFont(name: FontConst.Proxi_Semibold, size: withSize)
            }
        }else{
            return UIFont(name: FontConst.Proxi_Semibold, size: withSize)
        }
        
    }
    
    //MARK:- Roboto
    class func RobotoRegular(withSize: CGFloat) -> UIFont? {
        
        if let device = UIDevice.current.deviceType{
            switch device {
            case .iPad:
                return UIFont(name: FontConst.Roboto_Regular, size: withSize+8)
            case .iPhone6Plus:
                return UIFont(name: FontConst.Roboto_Regular, size: withSize+2)
            case .iPhone6,.iPhoneX:
                 return UIFont(name: FontConst.Roboto_Regular, size: withSize+1)
            default:
                return UIFont(name: FontConst.Roboto_Regular, size: withSize)
            }
        }else{
            return UIFont(name: FontConst.Roboto_Regular, size: withSize)
        }
        
    }
    
    class func RobotoBold(withSize: CGFloat) -> UIFont? {
        
        
        if let device = UIDevice.current.deviceType{
            switch device {
            case .iPad:
                return UIFont(name: FontConst.Roboto_Bold, size: withSize+8)
            case .iPhone6Plus:
                return UIFont(name: FontConst.Roboto_Bold, size: withSize+2)
            case .iPhone6,.iPhoneX:
                return UIFont(name: FontConst.Roboto_Bold, size: withSize+1)
            default:
                return UIFont(name: FontConst.Roboto_Bold, size: withSize)
            }
        }else{
            return UIFont(name: FontConst.Roboto_Bold, size: withSize)
        }
        
    }
    
    class func RobotoSemiBold(withSize: CGFloat) -> UIFont? {
        
        
        if let device = UIDevice.current.deviceType{
            switch device {
            case .iPad:
                return UIFont(name: FontConst.Roboto_Medium, size: withSize+8)
            case .iPhone6Plus:
                return UIFont(name: FontConst.Roboto_Medium, size: withSize+2)
            case .iPhone6,.iPhoneX:
                return UIFont(name: FontConst.Roboto_Medium, size: withSize+1)
            default:
                return UIFont(name: FontConst.Roboto_Medium, size: withSize)
            }
        }else{
            return UIFont(name: FontConst.Roboto_Medium, size: withSize)
        }
        
    }
    
    
    //regular
    @objc class var regularExtraSmallestFont : UIFont {
        return UIFont.regularSystemFont(withSize: 7)
    }
    @objc class var regularVerySmallestFont : UIFont {
        return UIFont.regularSystemFont(withSize: 8)
    }
    @objc class var regularVerySmallFont : UIFont {
        return UIFont.regularSystemFont(withSize: 9)
    }
    
    @objc class var regularSmallestFont : UIFont {
        return UIFont.regularSystemFont(withSize: 10)
    }
    
    @objc class var regularBadgeCountFont : UIFont {
        return UIFont(name: FontConst.Roboto_Regular, size: 10) ?? UIFont.systemFont(ofSize: 10)
    }
    
    @objc class var regularSmallFont: UIFont {
        return UIFont.regularSystemFont(withSize: 12)
    }
    
    @objc class var regularTitleFont: UIFont {
        return UIFont.regularSystemFont(withSize: 14)
    }
    
    @objc class var regularSmallValueFont: UIFont {
        return UIFont.regularSystemFont(withSize: 15)
    }
    
    @objc class var regularValueFont: UIFont {
        return UIFont.regularSystemFont(withSize: 16)
    }
    
    @objc class var regularMediumFont: UIFont {
        return UIFont.regularSystemFont(withSize: 18)
    }
    
    @objc class var regularLargeFont: UIFont {
        return UIFont.regularSystemFont(withSize: 20)
    }

    @objc class var regularExtraLargeFont: UIFont {
        return UIFont.regularSystemFont(withSize: 24)
    }
    
    //medium
    
    @objc class var mediumSmallestFont : UIFont {
        return UIFont.mediumSystemFont(withSize: 10)
    }
    
    @objc class var mediumSmallFont: UIFont {
        return UIFont.mediumSystemFont(withSize: 12)
    }
    
    @objc class var mediumTitleFont: UIFont {
        return UIFont.mediumSystemFont(withSize: 14)
    }
    
    @objc class var mediumValueFont: UIFont {
        return UIFont.mediumSystemFont(withSize: 16)
    }
    
    @objc class var mediumMediumFont: UIFont {
        return UIFont.mediumSystemFont(withSize: 18)
    }
    
    @objc class var mediumLargeFont: UIFont {
        return UIFont.mediumSystemFont(withSize: 20)
    }
    
    //semibold
    
    @objc class var semiboldSmallestFont : UIFont {
        return UIFont.semiBoldSystemFont(withSize: 10)
    }
    
    @objc class var semiboldSmallFont: UIFont {
        return UIFont.semiBoldSystemFont(withSize: 12)
    }
    
    @objc class var semiboldSmallTitleFont: UIFont {
        return UIFont.semiBoldSystemFont(withSize: 13)
    }
    
    @objc class var semiboldTitleFont: UIFont {
        return UIFont.semiBoldSystemFont(withSize: 14)
    }
    
    @objc class var semiboldValueFont: UIFont {
        return UIFont.semiBoldSystemFont(withSize: 16)
    }
    
    @objc class var semiboldMediumFont: UIFont {
        return UIFont.semiBoldSystemFont(withSize: 18)
    }
    
    @objc class var semiboldLargeFont: UIFont {
        return UIFont.semiBoldSystemFont(withSize: 20)
    }
    
    @objc class var semiboldLargePlusFont: UIFont {
        return UIFont.semiBoldSystemFont(withSize: 22)
    }
    
    @objc class var semiboldExtraLargeFont: UIFont {
        return UIFont.semiBoldSystemFont(withSize: 24)
    }
    
    //bold
    
    @objc class var boldVerySmallestFont : UIFont {
        return UIFont.boldSystemFont(withSize: 10)
    }
    
    @objc class var boldSmallFont: UIFont {
        return UIFont.boldSystemFont(withSize: 12)
    }
    
    @objc class var boldSmallTitleFont: UIFont {
        return UIFont.boldSystemFont(withSize: 13)
    }
    
    @objc class var boldTitleFont: UIFont {
        return UIFont.boldSystemFont(withSize: 14)
    }
    
    @objc class var boldTitleMediumFont: UIFont {
        return UIFont.boldSystemFont(withSize: 15)
    }
    
    @objc class var boldValueFont: UIFont {
        return UIFont.boldSystemFont(withSize: 16)
    }
    
    @objc class var boldLargeValueFont: UIFont {
        return UIFont.boldSystemFont(withSize: 17)
    }
    
    
    @objc class var boldMediumFont: UIFont {
        return UIFont.boldSystemFont(withSize: 18)
    }
    
    @objc class var boldLargeFont: UIFont {
        return UIFont.boldSystemFont(withSize: 20)
    }
    
    @objc class var boldLargePlusFont: UIFont {
        return UIFont.boldSystemFont(withSize: 22)
    }
    
    @objc class var boldExtraLargeFont: UIFont {
        return UIFont.boldSystemFont(withSize: 24)
    }
    
    @objc class var heavyExtraLargeFont: UIFont {
        return UIFont.heavySystemFont(withSize: 24)
    }
    
    //italic
    
    @objc class var italicVerySmallestFont : UIFont {
        return UIFont.italicSystemFont(withSize: 10)
    }
    
    @objc class var italicSmallFont: UIFont {
        return UIFont.italicSystemFont(withSize: 12)
    }
    
    @objc class var italicTitleFont: UIFont {
        return UIFont.italicSystemFont(withSize: 14)
    }
    
    @objc class var italicValueFont: UIFont {
        return UIFont.italicSystemFont(withSize: 16)
    }
    
    @objc class var italicMediumFont: UIFont {
        return UIFont.italicSystemFont(withSize: 18)
    }
    
    @objc class var italicLargeFont: UIFont {
        return UIFont.italicSystemFont(withSize: 20)
    }
    
   
    
    @objc class func printAllFontNames() {
        for family in UIFont.familyNames {
            
            print("Family name - " + family)
            
            let fontNames = UIFont.fontNames(forFamilyName: family)
            for font in fontNames {
                print("\tFont name: " + font)
            }
        }
    }
    
    @objc class var webViewFont : UIFont {
       
            
            if IS_IPAD {
                return UIFont.systemFont(ofSize: 200)
            } else {
                return UIFont.regularSystemFont(withSize: 100)
            }
    //        } else if IS_IPHONE_6P {
    //            return UIFont.systemFont(ofSize: 40 + 2)
    //        } else if IS_IPHONE_6 || IS_IPHONE_X {
    //            return UIFont.systemFont(ofSize: 40 + 1)
    //        } else {
    //            return UIFont.systemFont(ofSize: 40)
    //        }
        //}
        
//            if IS_IPAD {
//            return UIFont(name: FontConst.Roboto_Regular, size: 24) ?? UIFont.systemFont(ofSize: 24)
//
//            } else if IS_IPHONE_6P {
//            return UIFont(name: FontConst.Roboto_Regular, size:46) ?? UIFont.systemFont(ofSize: 46)
//            } else if IS_IPHONE_6 || IS_IPHONE_X {
//            return UIFont(name: FontConst.Roboto_Regular, size: 40) ?? UIFont.systemFont(ofSize: 40)
//            } else {
//            return UIFont(name: FontConst.Roboto_Regular, size:36) ?? UIFont.systemFont(ofSize: 36)
//            }
        
    }
}
