//
//  UILabel+Extension.swift
//  SharkID
//
//  Created by Vikram Jagad on 25/02/19.
//  Copyright © 2019 sttl. All rights reserved.
//

import UIKit

extension UILabel
{
    //MARK:- Set Attribuated Title with Color
    func setAttributedTitleAfterCharWithColor(text: String, ofColor: UIColor)
    {
        let title : String = self.text!
        
        let attrString = NSMutableAttributedString(string: title)
        let myAttribute = [NSAttributedString.Key.foregroundColor :ofColor]
        
        let range = (title as NSString).range(of: text)
        attrString.addAttributes(myAttribute, range: range)
        self.attributedText = attrString
    }
    
    //MARK:- Set Border to TextFields
    func addBottomBorder(ofColor: UIColor) {
        let border = CALayer()
        let borderWidth = CGFloat(1.0)
        border.borderColor = ofColor.cgColor
        border.frame = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = borderWidth
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
    }
    
    func setUpNoRecordFoundLabel(){
        self.font = UIFont.regularValueFont
        self.isHidden = true
        self.textColor = .customSubtitle
        self.text = CommonLabels.no_record_found
    }
    
    func addAsteriskMark() {
        let text = getString(anything: self.text) + " *"
        let atrStr = NSMutableAttributedString(string: text, attributes: [NSAttributedString.Key.font : UIFont.regularTitleFont , NSAttributedString.Key.foregroundColor: UIColor.customSubtitle])
        let range = (text as NSString).range(of: "*")
        atrStr.addAttributes([NSAttributedString.Key.foregroundColor: UIColor.red], range: range)
        self.attributedText = atrStr
    }
}
