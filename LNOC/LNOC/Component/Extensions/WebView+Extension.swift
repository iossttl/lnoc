//
//  WebView+Extension.swift
//  SharkID
//
//  Created by Nimitt Nemade on 19/03/19.
//  Copyright © 2019 sttl. All rights reserved.
//

import UIKit
import WebKit

extension WKWebView {
    func loadCustom(_ urlString: String, completion: ((Bool) -> Void), failure: ((Bool) -> Void)) {
        guard let url = URL(string: urlString)
            else {
            failure(true)
            return
        }
        let req = URLRequest(url: url)
        self.load(req)
        self.scrollView.scrollsToTop = true
        completion(true)
    }
}
