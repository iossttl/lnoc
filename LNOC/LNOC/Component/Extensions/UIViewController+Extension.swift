//
//  UIViewController+Extension.swift

//
//  Created by Vikram Jagad on 06/11/20.
//  Copyright © 2020 Vikram Jagad. All rights reserved.
//

import UIKit

enum AppStoryboard: String {
    
    case main = "Main"
    case NOCAppList = "NOCAppList"
    case selectLanguage = "SelectLanguage"
    case dashboard = "Dashboard"
    case sideMenu = "SideMenu"
    case settings = "Settings"
    case changePassword = "ChangePassword"
    case register = "Register"
    case AddDetails = "AddDetails"
    case Filter = "Filter"
    case Profile = "Profile"
    case NotificationsList = "NotificationsList"
    case RemarksList = "RemarksList"
    case RemarksTabContainer = "RemarksTabContainer"
    
    case RegisterContainer = "RegisterContainer"
    case Login = "Login"
    case ForgotpasswordScreen = "ForgotpasswordScreen"
    case LandList = "LandList"
    case LandDetails = "LandDetails"
    case applicationDetails = "ApplicationDetails"
    case propertyDetail = "PropertyDetail"
    case pdfDocumentViewer = "PDFDocumentViewer"
    case remarksListPopup = "RemarksListPopup"
    case nocUploadDocument = "NOCUploadDocument"
    case previousPropertyDetails = "PreviousPropertyDetails"
    case addClarificationDocument = "AddClarificationDocument"
    case addLandDetail = "AddLandDetail"
    case feedback = "Feedback"
    case webView = "WebView"
    case LandDetailsMultiple = "LandDetailsMultiple"
    
    case noticeWarrant = "NoticeWarrant"
    case noticeFilter = "NoticeFilter"
    case warrantFilter = "WarrantFilter"
    case jailbroken = "Jailbroken"
    
    case challanList = "ChallanList"
}

extension UIViewController {
    class func instantiate(appStoryboard: AppStoryboard) -> Self {
        let storyboard = UIStoryboard(name: appStoryboard.rawValue, bundle: nil)
        let identifier = String(describing: Self.self)
        return storyboard.instantiateViewController(withIdentifier: identifier) as! Self
    }
}

extension UIViewController {
    func popupAlert(title: String?, message: String?, actionTitles:[String?], actions:[((UIAlertAction) -> Void)?]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        for (index, title) in actionTitles.enumerated() {
            let action = UIAlertAction(title: title, style: .default, handler: actions[index])
            alert.addAction(action)
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    var isUIViewController_State_To_Deinit: Bool {
        return self.isBeingDismissed || self.isMovingFromParent || (self.navigationController?.isBeingDismissed ?? false)
    }
}

//self.popupAlert(title: "Title", message: " Oops, xxxx ", actionTitles: ["Option1","Option2","Option3"], actions:[{action1 in
//
//},{action2 in
//
//}, nil])
