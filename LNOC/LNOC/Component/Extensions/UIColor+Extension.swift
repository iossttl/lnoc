//
//  DGGlobal.swift
//  DigitalGujarat
//
//  Created by Xamarin PC on 15/09/16.
//  Copyright © 2016 Digital Gujarat. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(_ rValue: Int, _ gValue: Int, _ bValue: Int, alpha: CGFloat = 1.0) {
//        assert(rValue >= 0 && rValue <= 255, "Invalid red component")
//        assert(gValue >= 0 && gValue <= 255, "Invalid green component")
//        assert(bValue >= 0 && bValue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(rValue) / 255.0, green: CGFloat(gValue) / 255.0, blue: CGFloat(bValue) / 255.0, alpha: alpha)
    }
    
    //MARK:- Hex String to UIColor
    convenience init(hexStringToUIColor hex: String, alpha:CGFloat = 1.0) {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if (cString.length) != 6 {
            self.init(red: 66, green: 66, blue: 66, alpha: alpha)
        }
        else {
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            self.init(Int((rgbValue & 0xFF0000) >> 16), Int((rgbValue & 0x00FF00) >> 8), Int(rgbValue & 0x0000FF), alpha: alpha)
        }
    }
    
    var hexString: String {
        let colorRef = cgColor.components
        let r = colorRef?[0] ?? 0
        let g = colorRef?[1] ?? 0
        let b = ((colorRef?.count ?? 0) > 2 ? colorRef?[2] : g) ?? 0
        let a = cgColor.alpha

        var color = String(
            format: "#%02lX%02lX%02lX",
            lroundf(Float(r * 255)),
            lroundf(Float(g * 255)),
            lroundf(Float(b * 255))
        )

        if a < 1 {
            color += String(format: "%02lX", lroundf(Float(a)))
        }

        return color
    }
}
