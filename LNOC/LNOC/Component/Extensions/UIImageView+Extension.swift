//
//  UIImageView+Extension.swift
//  Vidyanjali
//
//  Created by Vikram Jagad on 11/11/20.
//  Copyright © 2020 Vikram Jagad. All rights reserved.
//

import UIKit
import SDWebImage
import SDWebImageWebPCoder

extension UIImageView {
    /*
    func webImage(with: URL?, placeholderImage: UIImage? = nil, options: SDWebImageOptions = .continueInBackground, context: [SDWebImageContextOption: Any]? = nil, progess: SDImageLoaderProgressBlock? = nil, completed: SDExternalCompletionBlock? = nil) {
        if (getString(anything: with?.pathExtension).contains("webp")) {
            let webPCoder = SDImageWebPCoder.shared
            SDImageCodersManager.shared.addCoder(webPCoder)
        }
        self.sd_setImage(with: with, placeholderImage: placeholderImage, options: options, context: context, progress: progess, completed: completed)
    }*/
    
    func downloadImage(with string: String?, placeholderImage: UIImage? = nil, options: SDWebImageOptions = .continueInBackground, progess: SDImageLoaderProgressBlock? = nil,completed:SDExternalCompletionBlock? = nil) {
        
        self.image = nil
        
        guard let getImageString = string?.trimmingCharacters(in: .whitespacesAndNewlines),
              getImageString.count > 0 else {
            self.image = placeholderImage
            return
        }
        
        let url = URL(string: getImageString) ?? URL(string: getImageString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        self.sd_imageIndicator = SDWebImageActivityIndicator.gray
        
        self.sd_setImage(with: url, placeholderImage: placeholderImage, options: options, progress: progess, completed: completed)

//        self.sd_setImage(with: url, placeholderImage: placeholderImage, options: options, progress: progess) { [weak self] (getImage, getError, sdImageType, getUrl) in
//            if getImage == nil {
//                self?.image = placeholderImage
//            }
//        }
    }
    
    func downloadImageWithoutProgress(with string: String?, placeholderImage: UIImage? = nil, options: SDWebImageOptions = .continueInBackground, progess: SDImageLoaderProgressBlock? = nil,completed:SDExternalCompletionBlock? = nil) {
        
        self.image = nil
        
        guard let getImageString = string?.trimmingCharacters(in: .whitespacesAndNewlines),
              getImageString.count > 0 else {
            self.image = placeholderImage
            return
        }
        
        let url = URL(string: getImageString) ?? URL(string: getImageString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)
        
        //self.sd_imageIndicator = SDWebImageActivityIndicator.
        
        self.sd_setImage(with: url, placeholderImage: placeholderImage, options: options, progress: progess, completed: completed)

//        self.sd_setImage(with: url, placeholderImage: placeholderImage, options: options, progress: progess) { [weak self] (getImage, getError, sdImageType, getUrl) in
//            if getImage == nil {
//                self?.image = placeholderImage
//            }
//        }
    }
    
    
    func webImage(with: URL?, placeholderImage: UIImage? = nil, options: SDWebImageOptions = .continueInBackground, context: [SDWebImageContextOption: Any]? = nil, progess: SDImageLoaderProgressBlock? = nil, completed: SDExternalCompletionBlock? = nil) {
        if (getString(anything: with?.pathExtension).contains("webp")) {
            let webPCoder = SDImageWebPCoder.shared
            SDImageCodersManager.shared.addCoder(webPCoder)
        }
        self.sd_setImage(with: with, placeholderImage: placeholderImage, options: options, context: context, progress: progess, completed: completed)
    }
}
