//
//  UIView+CustomizedView.swift
//  Tradesmen
//
//  Created by Jinal Patel on 7/26/16.
//  Copyright © 2016 minesh doshi. All rights reserved.
//

import UIKit

extension UIView
{
    //MARK:- View With Border
    func viewWith(radius: CGFloat, borderColor color: UIColor = .clear, borderWidth: CGFloat = 0)
    {
        self.layer.cornerRadius = radius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = color.cgColor
        self.clipsToBounds = true
        self.layer.masksToBounds = true
    }
    
    //MARK:- Add Shadow to View
    func addShadow(shadowColor: UIColor = .customShadow, shadowOpacity: Float = 0.3, shadowRadius: CGFloat? = 2, shadowOffset: CGSize = CGSize(width: 0, height : 0))
    {
        self.layer.shadowColor = shadowColor.cgColor
        self.layer.masksToBounds = false
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowOpacity = shadowOpacity
        if let getShadowRadius = shadowRadius {
            self.layer.shadowRadius = getShadowRadius
        }
    }
    func addShadowRound(shadowColor: UIColor = .customShadow, shadowOpacity: Float = 0.3, shadowRadius: CGFloat? = 2, shadowOffset: CGSize = CGSize(width: 0, height : 0))
    {
    self.layer.masksToBounds = false
    self.layer.cornerRadius = 5
    self.layer.shadowColor = UIColor.red.cgColor
    self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
    self.layer.shadowOffset = CGSize(width:10.0, height: 10.0)
    self.layer.shadowOpacity = 0.1
    self.layer.shadowRadius = 2.0
        
       
        
        
    }
    func setCustomCornerRadius(radius : CGFloat, borderColor : UIColor = .clear, borderWidth : CGFloat = 0) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = radius
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        self.clipsToBounds = true
    }
    
    //MARK:- Round Corners Specified
    func roundCorners(corners: UIRectCorner, radius: CGFloat)
    {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    //MARK:- Load Nib / Xib File
    @objc public class func loadNib() -> Self {
        return Bundle.main.loadNibNamed(className, owner: nil, options: nil)![0] as! Self
    }
    
    //Hide Keyboard
    func hideKeyBoard() {
        UIApplication.shared.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
    class func hideKeyBoard() {
//        if #available(iOS 13, *) {
//            ((UIApplication.shared.connectedScenes.first)?.delegate as? SceneDelegate)?.window?.hideKeyBoard()
//        } else {
            (UIApplication.shared.delegate as? AppDelegate)?.window?.hideKeyBoard()
//        }
    }
    
    class func makeNoRecordFoundViewWithPullToRefresh(frame: CGRect, msg: String) -> UIView
    {
        let viewRefresh = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        viewRefresh.translatesAutoresizingMaskIntoConstraints = true
        viewRefresh.backgroundColor = .clear
        
        let imgPullToRefresh = UIImageView(image: UIImage(named: k_ic_pull_to_refresh))
        imgPullToRefresh.translatesAutoresizingMaskIntoConstraints = true
        imgPullToRefresh.frame = CGRect(x: 0, y: 0, width: 37, height: 37)
        imgPullToRefresh.center = viewRefresh.center
        viewRefresh.addSubview(imgPullToRefresh)
        
        let lblNoRecord = UILabel()
        lblNoRecord.tag = 100
        lblNoRecord.translatesAutoresizingMaskIntoConstraints = true
        viewRefresh.addSubview(lblNoRecord)
        //Condition to check internet connection left.
        lblNoRecord.text = msg
        
        lblNoRecord.textColor = .black
        lblNoRecord.textAlignment = .center
        lblNoRecord.numberOfLines = 0
        lblNoRecord.font = .regularValueFont
        lblNoRecord.frame = CGRect(x: 10, y: imgPullToRefresh.frame.origin.y - lblNoRecord.frame.size.height - 10, width: SCREEN_WIDTH - 20, height: lblNoRecord.frame.size.height)
        lblNoRecord.sizeToFit()
        lblNoRecord.frame = CGRect(x: 20, y: imgPullToRefresh.frame.origin.y - lblNoRecord.frame.size.height - 10, width: SCREEN_WIDTH - 40, height: lblNoRecord.frame.size.height)
        lblNoRecord.center = CGPoint(x: viewRefresh.center.x, y: lblNoRecord.center.y)
        
        let lblPullDown = UILabel()
        lblPullDown.translatesAutoresizingMaskIntoConstraints = true
        viewRefresh.addSubview(lblPullDown)
        lblPullDown.text = CommonLabels.pull_to_refresh
        
        lblPullDown.textColor = .black
        lblPullDown.textAlignment = .center
        lblPullDown.numberOfLines = 0
        lblPullDown.font = .regularValueFont
        lblPullDown.sizeToFit()
        lblPullDown.frame = CGRect(x: 10, y: imgPullToRefresh.frame.origin.y + imgPullToRefresh.frame.size.height + 10, width: SCREEN_WIDTH - 20, height: 30)
        
        lblPullDown.center = CGPoint(x: viewRefresh.center.x, y: lblPullDown.center.y)
        
        return viewRefresh
    }
    
    class func makeNoRecordFoundView(frame: CGRect, msg: String) -> UIView
    {
        let viewRefresh = UIView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: frame.size.height))
        viewRefresh.translatesAutoresizingMaskIntoConstraints = true
        viewRefresh.backgroundColor = .clear
        
        let lblNoRecord = UILabel()
        lblNoRecord.tag = 100
        lblNoRecord.translatesAutoresizingMaskIntoConstraints = true
        viewRefresh.addSubview(lblNoRecord)
        //Condition to check internet connection left.
        lblNoRecord.text = msg
        
        lblNoRecord.textColor = .customBlack
        lblNoRecord.textAlignment = .center
        lblNoRecord.numberOfLines = 0
        lblNoRecord.font = .regularValueFont
        lblNoRecord.frame = CGRect(x: 10, y: 10, width: SCREEN_WIDTH - 20, height: viewRefresh.frame.size.height - 20)
        lblNoRecord.sizeToFit()
        lblNoRecord.frame = CGRect(x: 20, y: 20, width: SCREEN_WIDTH - 40, height: viewRefresh.frame.size.height - 40)
        lblNoRecord.center = CGPoint(x: viewRefresh.center.x, y: lblNoRecord.center.y)
        
        return viewRefresh
    }
    
    func addVibrancyEffect(withEffect: UIBlurEffect.Style = .light, bringSubViewToFrontView view: UIView)
    {
        let viewBlurr = UIView(frame: self.frame)
        self.backgroundColor = .clear
        let blurEffect = UIBlurEffect(style: withEffect)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.translatesAutoresizingMaskIntoConstraints = false
        self.insertSubview(blurView, at: 0)
        NSLayoutConstraint.activate([
            blurView.heightAnchor.constraint(equalTo: self.heightAnchor),
            blurView.widthAnchor.constraint(equalTo: self.widthAnchor),
            ])
        let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
        let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
        vibrancyView.translatesAutoresizingMaskIntoConstraints = false
        vibrancyView.contentView.addSubview(viewBlurr)
        blurView.contentView.addSubview(vibrancyView)
        NSLayoutConstraint.activate([
            vibrancyView.heightAnchor.constraint(equalTo: blurView.contentView.heightAnchor),
            vibrancyView.widthAnchor.constraint(equalTo: blurView.contentView.widthAnchor),
            vibrancyView.centerXAnchor.constraint(equalTo: blurView.contentView.centerXAnchor),
            vibrancyView.centerYAnchor.constraint(equalTo: blurView.contentView.centerYAnchor)
            ])
        NSLayoutConstraint.activate([
            self.centerXAnchor.constraint(equalTo: vibrancyView.contentView.centerXAnchor),
            self.centerYAnchor.constraint(equalTo: vibrancyView.contentView.centerYAnchor),
            ])
        self.addSubview(viewBlurr)
        
        NSLayoutConstraint.activate([
            self.centerXAnchor.constraint(equalTo: viewBlurr.centerXAnchor),
            self.centerYAnchor.constraint(equalTo: viewBlurr.centerYAnchor)
            ])
        self.bringSubviewToFront(view)
    }
    func addGradient(ofColors: [CGColor] = [UIColor.customgreenTheamClr.cgColor, UIColor.customgreenTheamClr.cgColor], direction: GradientPoint) {
        gradientLayer.colors = ofColors
        gradientLayer.gradient = direction.draw()
    }
    
    public enum CornerRadiusType {
        case byHeight
        case byWidth
        case automatic
    }
    
    func setFullCornerRadius(type : CornerRadiusType = .byHeight, borderColor : UIColor = .clear, borderWidth : CGFloat = 0) {
        self.layoutIfNeeded()
        self.layer.masksToBounds = true
        
        var getSize : CGFloat {
            switch type{
            case .byHeight: return self.frame.height
            case .byWidth: return self.frame.width
            case .automatic: return min(self.frame.height, self.frame.width)
            }
        }
        
        self.layer.cornerRadius = getSize / 2
        self.layer.borderWidth = borderWidth
        self.layer.borderColor = borderColor.cgColor
        self.clipsToBounds = true
    }
    
    func removeDashedBorder() {
        if let _layer = self.layer.sublayers {
            for layer in _layer {
                if layer.name == "DashedLine" {
                     layer.removeFromSuperlayer()
                }
            }
        }
    }
    
    func addDashedBorder(withColor: UIColor = .red, lineWidth: CGFloat = 2, dashPattern: [NSNumber]? = [6, 3], cornerRadius: CGFloat = 8) {
        removeDashedBorder()
        let color = withColor.cgColor
        let shapeLayer: CAShapeLayer = CAShapeLayer()
        shapeLayer.name = "DashedLine"
        let frameSize = bounds.size
        let shapeRect = CGRect(x: 0, y: 0, width: frameSize.width, height: frameSize.height)

        shapeLayer.bounds = shapeRect
        shapeLayer.position = CGPoint(x: frameSize.width/2, y: frameSize.height/2)
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = color
        shapeLayer.lineWidth = lineWidth
        shapeLayer.lineJoin = CAShapeLayerLineJoin.round
        shapeLayer.lineDashPattern = dashPattern
        shapeLayer.path = UIBezierPath(roundedRect: shapeRect, cornerRadius: cornerRadius).cgPath

        self.layer.addSublayer(shapeLayer)
    }
    
    func createDottedLine(width: CGFloat, color: UIColor) {
        self.layoutIfNeeded()
        let shapeLayer = CAShapeLayer()
        shapeLayer.strokeColor = color.cgColor
        shapeLayer.lineWidth = width
        // passing an array with the values [2,3] sets a dash pattern that alternates between a 2-user-space-unit-long painted segment and a 3-user-space-unit-long unpainted segment
        shapeLayer.lineDashPattern = [5,5]
        
        let path = CGMutablePath()
        
        path.addLines(between: [CGPoint(x: 0, y: self.frame.height / 2),
                                CGPoint(x: self.frame.width, y: self.frame.height / 2)])
        shapeLayer.path = path
        
        layer.addSublayer(shapeLayer)

    }

}

extension UIView {
    
    func fullFillOnSuperView() {
        self.translatesAutoresizingMaskIntoConstraints = false
        if let getSuperView = self.superview {
            [self.topAnchor.constraint(equalTo: getSuperView.topAnchor, constant: 0),
             self.leftAnchor.constraint(equalTo: getSuperView.leftAnchor, constant: 0),
             self.rightAnchor.constraint(equalTo: getSuperView.rightAnchor, constant: 0),
             self.bottomAnchor.constraint(equalTo: getSuperView.bottomAnchor, constant: 0)].forEach({ $0.isActive = true })
        }
    }
    
    @discardableResult
    func addConstraint(topAnchr: NSLayoutYAxisAnchor?, leftAnchr: NSLayoutXAxisAnchor?, rightAnchr: NSLayoutXAxisAnchor?, bottomAnchr: NSLayoutYAxisAnchor?, padding: UIEdgeInsets = .zero) -> [NSLayoutConstraint] {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        var anchors : [NSLayoutConstraint] = []
        
        if let getAnchor = topAnchr {
            anchors.append(self.topAnchor.constraint(equalTo: getAnchor, constant: padding.top))
        }
        if let getAnchor = leftAnchr {
            anchors.append(self.leftAnchor.constraint(equalTo: getAnchor, constant: padding.left))
        }
        if let getAnchor = rightAnchr {
            anchors.append(self.rightAnchor.constraint(equalTo: getAnchor, constant: padding.right))
        }
        if let getAnchor = bottomAnchr {
            anchors.append(self.bottomAnchor.constraint(equalTo: getAnchor, constant: padding.bottom))
        }
        
        anchors.forEach({ $0.isActive = true })
        
        return anchors
    }
    
    @discardableResult
    func setHeightWidth(setHeight: CGFloat?, setWidth: CGFloat?) -> [NSLayoutConstraint] {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        var anchors : [NSLayoutConstraint] = []
        
        if let getValue = setHeight {
            anchors.append(self.heightAnchor.constraint(equalToConstant: getValue))
        }
        
        if let getValue = setWidth {
            anchors.append(self.widthAnchor.constraint(equalToConstant: getValue))
        }
        
        anchors.forEach({ $0.isActive = true })
        
        return anchors
    }
    
}

extension UIView {
    
    // This is the function to get subViews of a view of a particular type
    func subViews<T : UIView>(type : T.Type) -> [T] {
        var all = [T]()
        for view in self.subviews {
            if let aView = view as? T {
                all.append(aView)
            }
        }
        return all
    }
    
    
    /// This is a function to get subViews of a particular type from view recursively. It would look recursively in all subviews and return back the subviews of the type T
    func allSubViewsOf<T : UIView>(type : T.Type) -> [T]{
        var all = [T]()
        func getSubview(view: UIView) {
            if let aView = view as? T {
                all.append(aView)
            }
            guard view.subviews.count > 0 else { return }
            view.subviews.forEach{ getSubview(view: $0) }
        }
        getSubview(view: self)
        return all
    }
}

extension UIView {
    @discardableResult
    func addLineDashedStroke(pattern: [NSNumber]?, radius: CGFloat, color: UIColor) -> CALayer {
        let borderLayer = CAShapeLayer()

        borderLayer.strokeColor = color.cgColor
        borderLayer.lineDashPattern = pattern
        borderLayer.frame = bounds
        borderLayer.fillColor = nil
        borderLayer.path = UIBezierPath(roundedRect: bounds, byRoundingCorners: .allCorners, cornerRadii: CGSize(width: radius, height: radius)).cgPath

        layer.addSublayer(borderLayer)
        return borderLayer
    }
}
extension UIView {

    func takeScreenshot() -> UIImage {        
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
}
