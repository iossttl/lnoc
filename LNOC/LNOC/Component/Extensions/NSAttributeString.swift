//
//  NSAttributeString.swift

//
//  Created by Savan Ankola on 07/06/21.
//

import UIKit

extension NSAttributedString {
    func changeFontSize(factor: CGFloat) -> NSAttributedString {
        
        guard let output = self.mutableCopy() as? NSMutableAttributedString else {
            return self
        }

        output.beginEditing()
        output.enumerateAttribute(NSAttributedString.Key.font,
                                  in: NSRange(location: 0, length: self.length),
                                  options: []) { (value, range, stop) -> Void in
            guard let oldFont = value as? UIFont else {
                return
            }
             
            guard let newfont = UIFont(name: oldFont.fontName, size: oldFont.pointSize * factor) else {
                return
            }
                        
//            if oldFont.fontName == "TimesNewRomanPSMT" {
                output.removeAttribute(NSAttributedString.Key.font, range: range)
                output.addAttribute(NSAttributedString.Key.font, value: newfont, range: range)
//                print("Font Chnaged")
                
//            } else {
//                print("Without Font Chnaged")
//            }
        }
        output.endEditing()

        return output
    }
    
    private func applyTraitsFromFont(_ f1: UIFont, to f2: UIFont) -> UIFont? {
        let t = f1.fontDescriptor.symbolicTraits
        if t == UIFontDescriptor.SymbolicTraits(rawValue: 268435456) {
            return UIFont(name: "Avenir-Medium", size: f2.pointSize)
            
        } else if let fd = f2.fontDescriptor.withSymbolicTraits(t) {
            return UIFont.init(descriptor: fd, size: f2.pointSize)
        }
        
        return UIFont(name: "Avenir-Medium", size: f2.pointSize)
    }
}
