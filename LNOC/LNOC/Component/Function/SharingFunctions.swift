//
//  SharingFunctions.swift
//  SharkID
//
//  Created by Vikram Jagad on 06/03/19.
//  Copyright © 2019 sttl. All rights reserved.
//

import UIKit

enum ShareType : String
{
    case text
    case image
}

typealias CompletionBlock = ()->()
typealias CompletionBlock1 = (_ act: Any?, _ result: Bool) -> Void

struct SharingKeys
{
    static let key_title = "title"
    static let key_subject = "subject"
    static let key_picurl = "picurl"
}

//MARK:- Sharing
func shareAccordtingToType(type: ShareType, dict:[String : Any], image:UIImage?, VC:UIViewController?, sender: UIView, completionBlock:@escaping CompletionBlock)
{
    var vc: UIViewController?
    if VC == nil {
        vc = Global.shared.delegateRootVC
    } else {
        vc = VC
    }
    switch type {
    case .text:
        let strDetail = dict[SharingKeys.key_title] as? String
        let strSubject = dict[SharingKeys.key_subject] as? String
        let picURLString = dict[SharingKeys.key_picurl] as? String
        
        var stringSub = ""
        if let strSubject = strSubject
        {
            stringSub = strSubject
        }
        
        var stringDetail = ""
        if stringSub.isEmptyString {
            
        }
        if let strDetail = strDetail
        {
            if stringSub.isEmptyString {
                stringDetail += strDetail
            } else {
                stringDetail += "\n\n" + strDetail + "\n"
            }
        }
        
        var items: [Any] = [stringSub, stringDetail]
        if let picURLString = picURLString
        {
            if let picURL = URL(string: picURLString)
            {
                items.append(picURL)
            }
        }
        print(items)
        shareWithCompletion(arrayItem: items, subject: stringSub, VC: vc, sender: sender, completion: { (act, result) in
            //Web Service Call
            completionBlock()//
        })
    case .image:
        let strDetail = dict[SharingKeys.key_title] as? String
        let strSubject = dict[SharingKeys.key_subject] as? String
        let picURLString = dict[SharingKeys.key_picurl] as? String
        
        var stringSub = ""
        if let strSubject = strSubject
        {
            stringSub = strSubject
        }
        
        var stringDetail = ""
        if let strDetail = strDetail
        {
            stringDetail += "\n\n" + strDetail + "\n"
        }
        
        var items: [Any] = [stringSub, stringDetail]
        
        if let picURLString = picURLString
        {
            if let picURL = URL(string: picURLString)
            {
                items.append(picURL)
            }
        }
        
        if (image == nil)
        {
            
        }
        else
        {
            items.append(image!)
        }
        print(items)
        shareWithCompletion(arrayItem: items, subject: stringSub, VC: vc, sender: sender, completion: { (act, result) in
            //Web Service Call
            completionBlock()
        })
    }
}

func shareWithCompletion(arrayItem: [Any], subject:String?, VC:UIViewController?, sender: UIView, completion: @escaping CompletionBlock1)
{
    let activityViewController = UIActivityViewController(activityItems: arrayItem as [Any], applicationActivities: nil)
    activityViewController.setValue(subject, forKey: SharingKeys.key_subject)
    
    if (IS_IPAD)
    {
        activityViewController.popoverPresentationController?.sourceView = VC?.view
        let point = sender.superview?.convert(sender.frame.origin, to: nil)
        activityViewController.popoverPresentationController?.sourceRect = CGRect(x: (point?.x)!, y: (point?.y)!, width: sender.frame.size.width, height: sender.frame.size.height)
    }
    else
    {
        
    }
    VC?.present(activityViewController, animated: true, completion: nil)
    activityViewController.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
        if !completed {
            // User canceled
            return
        }
        // User completed activity
        completion(activityType, completed)
    }
}
