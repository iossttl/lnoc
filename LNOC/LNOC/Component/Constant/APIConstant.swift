//
//  APIConstant.swift
//  SharkID
//
//  Created by Vikram Jagad on 25/02/19.
//  Copyright © 2019 sttl. All rights reserved.
//

import UIKit

//MARK:- User Information
struct CommonAPIConstant {
    static let key_errormsg = "errormsg"
    static let key_message = "message"
    static let key_languageCode = "language"
    static let key__resultFlag = "_resultflag"
    static let key_offset = "Offset"
    static let key_limit = "Limit"
    static let key_totalCount = "totalCount"
    static let key_total_Count = "totalCount"
    static let key_totalcount = "totalcount"
    static let key_document = "document"
    static let key_Coltype = "Coltype"
    static let key_total_record = "total_record"
    static let key_totalRecords = "totalRecords"
    static let key_list = "list"
    static let key_data = "data"
    static let key_title = "title"
    static let key_subject = "subject"
    static let key_PicURL = "picURL"
    static let key_deviceID = "device_id"
    static let key_resultFlag = "_resultflag"
    static let key_authurpost =  "authorPost"
    static let key_bookmarkList = "bookmarkData"
    static let key_latestList = "latestData"
    static let key_id = "id"
    static let key_deviceid = "deviceid"
    static let key_token = "token"
    static let key_darkGradient = "darkGradient"
    static let key_lightGradient = "lightGradient"
    static let key_thumbnail = "thumbnail"
    static let key_count = "count"
    static let key_language = "language"
    static let key_userDetails = "userDetails"    
    
    static let key_InvalidToken_Flag = 2
}
