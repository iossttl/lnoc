//
//  NumericConstant.swift
//  SharkID
//
//  Created by Vikram Jagad on 25/02/19.
//  Copyright © 2019 sttl. All rights reserved.
//

import UIKit

//MARK:- Counts Per pageIndex
let kNumberOfCountsPerPage: Int = 10
