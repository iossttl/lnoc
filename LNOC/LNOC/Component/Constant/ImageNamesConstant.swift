//
//  ImageNamesConstant.swift
//  SharkID
//
//  Created by Vikram Jagad on 25/02/19.
//  Copyright © 2019 sttl. All rights reserved.
//

import UIKit



//MARK:- Common
let k_ic_pull_to_refresh = "ic_pull_to_refresh"
let k_ic_attach = "ic_attach"
let k_ic_viewpdf = "ic_viewpdf"

extension UIImage {
    
    class var ic_attach: UIImage? {
        return UIImage(named: "ic_attach")?.withRenderingMode(.alwaysTemplate)
    }

    class var ic_camera: UIImage? {
        return UIImage(named: "ic_camera")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ic_header_menu: UIImage? {
        return UIImage(named: "ic_header_menu")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ic_header_back: UIImage? {
        return UIImage(named: "ic_header_back")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var minus: UIImage? {
        return UIImage(named: "minus")
    }
    
    class var add: UIImage? {
        return UIImage(named: "add")
    }
    
    class var ic_header_notification: UIImage? {
        return UIImage(named: "ic_header_notification")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ic_header_search: UIImage? {
        return UIImage(named: "ic_header_search")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ic_header_sync: UIImage? {
        return UIImage(named: "ic_header_sync")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ic_header_filter: UIImage? {
        return UIImage(named: "ic_header_filter")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ic_header_close: UIImage? {
        return UIImage(named: "ic_header_close")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ic_header_share: UIImage? {
        return UIImage(named: "ic_header_share")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ic_header_download: UIImage? {
        return UIImage(named: "ic_header_download")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ico_share: UIImage? {
        return UIImage(named: "ico_share")?.withRenderingMode(.alwaysTemplate)
    }

    class var radio_button_on: UIImage? {
        return UIImage(named: "ic_radio_on")
    }
    
    class var radio_button_off: UIImage? {
        return UIImage(named: "ic_radio_off")
    }
    
    class var featured_placeholder: UIImage? {
        return UIImage(named: "featured_placeholder")
    }
    
    class var img_Text: UIImage? {
        return UIImage(named: "ic_text")
    }
    
    class var mid_banner_placeholder: UIImage? {
        return UIImage(named: "mid_banner_placeholder")
    }
    
    class var side_listing_placeholder: UIImage? {
        return UIImage(named: "side_listing_placeholder")
    }
    class var imgPostComment: UIImage? {
        return UIImage(named: "postcomment")
    }
    
    class var tnishow_placeholder: UIImage? {
        return UIImage(named: "tni-show_placeholder")
    }
    
    class var top_banner_placeholder: UIImage? {
        return UIImage(named: "top_banner_placeholder")
    }
    
    class var comment_placeholder: UIImage? {
        return UIImage(named: "comment_placeholder")
    }
    class var bookmark: UIImage? {
        return UIImage(named: "ico_bookmark")
    }
    class var authorimg: UIImage? {
        return UIImage(named: "icn_author")
    }
    class var bookmarkFill: UIImage? {
        return UIImage(named: "ic_bookmarkfill")
    }
    
    class var ic_play: UIImage? {
        return UIImage(named: "ic_play")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var dislike: UIImage? {
        return UIImage(named: "dislike")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var like: UIImage? {
        return UIImage(named: "like")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var calender: UIImage? {
        return UIImage(named: "calender")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ic_user: UIImage? {
        return UIImage(named: "ic_user")
    }
    
    class var ic_play_top: UIImage? {
        return UIImage(named: "ic_play_top")
    }
    
    class var user_palcehold_round: UIImage? {
        return UIImage(named: "user-palcehold_round")
    }
    
    class var dislike_fill: UIImage? {
        return UIImage(named: "dislike_fill")
    }
    
    class var like_fill: UIImage? {
        return UIImage(named: "like_fill")
    }
    
    class var reply: UIImage? {
        return UIImage(named: "reply")
    }
    
    class var replyIconGray: UIImage? {
        return UIImage(named: "replyIconGray")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ico_add_comment: UIImage? {
        return UIImage(named: "ico_add-comment")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ico_play_banner: UIImage? {
        return UIImage(named: "play_banner")
    }
    
    @objc class var ic_password_show: UIImage? {
        return UIImage(named: "ico_password_show")?.withRenderingMode(.alwaysTemplate)
    }
    
    @objc class var ic_password_not_show: UIImage? {
        return UIImage(named: "ico_password_not_show")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var checkboxSelected : UIImage? {
        return UIImage(named: "checkboxTSelected")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var checkbox : UIImage? {
        return UIImage(named: "checkboxT")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var radio_selected: UIImage? {
        return UIImage(named: "radio_selected")
    }
    
    class var radio_UnSelected: UIImage? {
        return UIImage(named: "radio_UnSelected")
    }
    
    class var ic_correct_white_bg: UIImage? {
        return UIImage(named: "ic_correct_white_bg")
    }
    
    class var ic_login: UIImage? {
        return UIImage(named: "ic_login")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ic_logout: UIImage? {
        return UIImage(named: "ic_logout")?.withRenderingMode(.alwaysTemplate)
    }
    
    @objc class var ico_language_globe: UIImage? {
        return UIImage(named: "ico_language_globe")
    }
    
    class var ic_down_arrow: UIImage? {
        return UIImage(named: "ic_down_arrow")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ic_top_logo: UIImage? {
        return UIImage(named: "ic_top_logo")
    }
    
    @objc class var ico_language_globewithrenderingmode: UIImage? {
        return UIImage(named: "ico_language")?.withRenderingMode(.alwaysTemplate)
    }
    class var img_rightArrow: UIImage? {
        return UIImage(named: "ic_rightArrow")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var ico_password : UIImage? {
        return UIImage(named: "ico_password")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var videoGalleryIcon: UIImage? {
        return UIImage(named: "videoGalleryIcon")
    }
    
    class var ic_banner_placeholder: UIImage? {
        return UIImage(named: "banner_placeholder")
    }
    
    class var gallery_album_placeholder: UIImage?{
        return UIImage(named: "gallery_album_placeholder")
    }
    
    class var gallerydetails_album_placeholder: UIImage?{
        return UIImage(named: "gallerydetails_album_placeholder")
    }
    
    class var ic_user_placeholder: UIImage? {
        return UIImage(named: "ic_user_placeholder")
    }
    
    class var ic_sign_up_check_deselected: UIImage? {
        return UIImage(named: "ic_sign_up_check_deselected")
    }
    
    class var ic_sign_up_check_selected: UIImage? {
        return UIImage(named: "ic_sign_up_check_selected")
    }
    

    class var whatsNew_placeholder: UIImage? {
        return UIImage(named: "whatsNew_placeholder")
    }
    
    class var upcommingEvents_placeholder: UIImage? {
        return UIImage(named: "upcommingEvents_placeholder")
    }
    
    class var topBanner_placeholder: UIImage? {
        return UIImage(named: "topBanner_placeholder")
    }

    class var ic_user_placeholder2: UIImage? {
        return UIImage(named: "ic_user_placeholder2")
    }
    
    class var arrow_left_icon: UIImage? {
        return UIImage(named: "arrow_left_icon")?.withRenderingMode(.alwaysTemplate)
    }
    
    class var arrow_right_icon: UIImage? {
        return UIImage(named: "arrow_right_icon")?.withRenderingMode(.alwaysTemplate)
    }
    // new images
    
    class var ic_correct_border: UIImage {
        return UIImage(named: "ic_correct_border") ?? UIImage()
    }
    
    class var ic_plus: UIImage {
        return UIImage(named: "ic_plus") ?? UIImage()
    }
    
    class var ic_minus: UIImage {
        return UIImage(named: "ic_minus") ?? UIImage()
    }
    
    class var ic_pdf: UIImage {
        return UIImage(named: "ic_pdf") ?? UIImage()
    }
    
    class var ic_viewpdf: UIImage {
        return UIImage(named: k_ic_viewpdf) ?? UIImage()
    }
    
    class var ic_edit: UIImage? {
        return UIImage(named: "ic_edit")?.withRenderingMode(.alwaysTemplate)
    }
    
    
    class var ico_header_like: UIImage? {
        return UIImage(named: "ico_bookmark_filled")
    }
    
    class var ico_header_unLike: UIImage? {
        return UIImage(named: "ico_bookmark")
    }
    
    class var ic_header_delete: UIImage? {
        return UIImage(named: "ic_header_delete")?.withRenderingMode(.alwaysTemplate)
    }

    class var correct: UIImage? {
        return UIImage(named: "correct")
    }
    

    class var favorite_empty: UIImage? {
        return UIImage(named: "favorite_empty")
    }
    
    class var favorite_fill: UIImage? {
        return UIImage(named: "favorite_fill")
    }
    
    class var dots: UIImage? {
        return UIImage(named: "dots")
    }
    
    class var ic_list: UIImage? {
        return UIImage(named: "ic_list")?.withRenderingMode(.alwaysTemplate)
    }
    
}



struct ImageNameConstants {
    static var ic_camera : String { return "ic_camera" }
    static var ic_photo_library : String { return "ic_photo_library" }
    static var ic_document : String { return "ic_document" }
}
