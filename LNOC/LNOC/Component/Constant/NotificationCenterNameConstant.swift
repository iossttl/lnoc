//
//  NotificationCenterNameConstant.swift
//  SharkID
//
//  Created by Vikram Jagad on 25/02/19.
//  Copyright © 2019 sttl. All rights reserved.
//

import UIKit

//MARK:- Notification name

struct NotificationObserverKeys {
    static let reloadList = "reloadList"
    static let loginLogoutDone = "loginLogoutDone"
    static let syncSocialMedia = "syncSocialMedia"

    //New
    static let addLand = "addland"
    static let addNoc = "addNoc"
}
