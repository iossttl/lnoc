//
//  IdentifierConstant.swift
//  SharkID
//
//  Created by Vikram Jagad on 25/02/19.
//  Copyright © 2019 sttl. All rights reserved.
//

import UIKit

//MARK:- Common Constants
struct Constants {
    
    static let pageLimit = 10
    static var eventid = ""
    static var newsid = ""
   static var isLoginPopedUp = false
    static let CommentCharCount = 3000
 struct SideMenu {
        static let imgViewProfileHeightMultiplier: CGFloat = 0.12
    }
    
    struct Notification {
        static let topSpacing: CGFloat = 8
    }
    
    struct CompletedActivities {
        static let topSpacing: CGFloat = 8
    }
    
    
    
    
 struct Dashboard {
     static let bannerTopBottomInsets: CGFloat = 16
     static let bannerLeadingTrailingInsets: CGFloat = 16
     static let noOfCellsInBanner: CGFloat = 3
     static let bannerLineSpacing: CGFloat = 8
     static let bannerImgMultiplier: CGFloat = 0.4
     static let detailTopBottomInsets: CGFloat = 16
     static let detailLeadingTrailingInsets: CGFloat = 16
     static let noOfCellsInDetail: CGFloat = IS_IPAD ? 4 : 3
     static let detailLineSpacing: CGFloat = 8
     static let detailImgMultiplier: CGFloat = 0.4
 }
    struct RegisterGrievance {
        
        struct Container {
            static let leadingSpacing: CGFloat = 8
            static let topSpacing: CGFloat = 0
            static let colViewHeight: CGFloat = (IS_IPAD ? 80 : 35) + (UIFont.regularSmallFont.lineHeight * 2) + 13
        }
    }
}
