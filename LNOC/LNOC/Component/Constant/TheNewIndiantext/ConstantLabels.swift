//
//  ConstantLable.swift
//  SharkID
//
//  Created by stadmin on 09/08/17.
//  Copyright © 2017 sttl. All rights reserved.
//

import UIKit
import PDFKit

//MARK:- Common
struct CommonLabels {
    static var select: String { return "Select".localizedString }
    static var no_record_found : String { return "No Record Found".localizedString }
    static var pull_to_refresh : String { return "Pull to Refresh".localizedString }
    static var search : String { return "Search".localizedString }
    static var alert : String { return "Alert".localizedString }
    static var login : String { return "Login".localizedString }
    static var logout : String { return "Logout".localizedString }
    static var go_to_settings: String{ return "Go to settins".localizedString}
    static var yes: String { return "Yes".localizedString }
    static var cancel: String { return "Cancel".localizedString }
    static var viewAttachment: String { return "View Attachment".localizedString }
    static var settings: String{ return "Settings".localizedString}
    static var OK: String{ return "OK".localizedString}
    static var selctstartdate: String { return "Select Start Date".localizedString }
    static var selectenddate: String { return "Select End Date".localizedString }
    static var noFileSelectedString: String { return "No file selected".localizedString }
    static var character: String { return "Character".localizedString }
    static var workDone: String { return "Work Done".localizedString }
    static var maxPdfMBAllowed: String { return "Max. abcMB (Allowed pdf)".localizedString }
    static var maxDocMBAllowed: String { return "Max. abcMB (Allowed pdf,jpg,png,jpeg)".localizedString }
    static var maxImgMBAllowed: String { return "Max abc MB (jpg, png or jpeg)".localizedString }
    static var Assessment_Amount_Rent_Fee: String { return "Assessment Amount/Rent/Fee".localizedString }
    static var isRequired: String { return "is required".localizedString }
    static var Receipt: String { return "Receipt".localizedString }
    static var PayNow: String { return "Pay Now".localizedString }
}


//MARK:- Login
struct Login {
    static var Password : String { return "Password".localizedString }
    static var dontHaveAccount : String { return "Don't have an account?".localizedString }
    static var forgotPassword : String { return "Forgot Password?".localizedString }
    static var Email: String { "Email".localizedString }
    static var Remember_Me:String {return "Remember Me".localizedString }
    static var Register : String { return "Register".localizedString }
}

struct SideMenuLabels {
    static var guest: String { return "Guest".localizedString }
    static var login: String { return "Login".localizedString }
    static var logout: String { return "Logout".localizedString }
    static var welcome:String{return "Welcome".localizedString}
}

struct SignUp_EditProfileLabels {
    static var get_Otp : String { return "Get OTP".localizedString }
    static var resend_Otp : String { return "Resend OTP".localizedString }
    static var otp_has_been_sent_on_registered_mobile_number: String { return "One Time Password (OTP) has been sent to your Mobile No.".localizedString }
    static var otp_has_been_sent_on_your_email: String { return "One Time Password (OTP) has been sent to your Email".localizedString }

    static var Send_OTP : String { return "Send OTP".localizedString }
    static var verifyOTP : String { return "Verify OTP".localizedString }
}

struct HomeLabels {
    static var selectLanguage : String { return "Select Language".localizedString }
}

struct AddDeatilsLabels {
    static var Affidavit : String { return "Affidavit".localizedString }
    static var Challan : String { return "Challan".localizedString }
    static var OtherDoc : String { return "OtherDoc".localizedString }
}

struct ProfileLabels {
    static var AttachPan : String { return "Pan".localizedString }
    static var AttachAdharCard : String { return "Aadhaar Card".localizedString }
    static var Camera_unavailable : String { return "Camera Unavailable" }
}

struct RemarksLabels {
    static var Remarks : String { return "Remarks".localizedString }
    static var QueryRejection : String { return "Query Rejection".localizedString }
    static var Remark : String { return "Remark".localizedString }
}

struct PropertyDetailLabels {
    static var parkingNumber : String { return "Parking Number".localizedString }
    static var selectNOCType : String { return "Select NOC Type".localizedString}
    static var Ownershipof :String {return "Select Ownership of".localizedString}
    static var Salutation :String {return "Select Salutation".localizedString}
}

struct RegistrtionLabels {
    static var selectState : String { return "Select State".localizedString }
    static var selectDistrict : String { return "Select District".localizedString }
    static var selectCity : String { return "Select City".localizedString }
}

struct AddClarificationDocumentLabels {
    static var clarificationDocument : String { return "Clarification Document".localizedString }
    static var enterRemark: String { return "Enter Remark".localizedString }
}

struct NoticeListLabels {
    static var active : String { return "active".localizedString }
    static var inactive: String { return "inactive".localizedString }
}
