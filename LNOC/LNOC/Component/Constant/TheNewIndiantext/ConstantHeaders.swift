//
//  ConstantHeader.swift
//  SharkID
//
//  Created by stadmin on 09/08/17.
//  Copyright © 2017 sttl. All rights reserved.
//

import UIKit

struct Headers {
   
    static var applicationDetails : String { return "Application Details" }
    static var document : String { return "Document" }
    static var Filter : String { return "Filter".localizedString }
    static var AddChallanDetails : String { return "Add Challan Details".localizedString }
    static var addClarificationDocument : String { return "Add Clarification Document".localizedString }
    static var NotificationsList : String { return "Notifications List".localizedString }
    static var Remarks : String { return "Remarks".localizedString }
    static var NOCCertificateRemarks : String { return "NOC Certificate Remarks".localizedString }
    static var QueryRejectionRemarks : String { return "Query Rejection Remarks".localizedString }
    
    static var searchLands : String { return "Search Lands".localizedString }
    static var favouriteLands : String { return "Favourite Lands".localizedString }
    static var landBank : String { return "Land Bank".localizedString }
    static var linkLand : String { return "Link Land".localizedString }
    static var Feedback : String { return "Feedback".localizedString }

    // olds remove
    static var dashboard : String { "Dashboard".localizedString }
    static var SignUp : String { "Sign Up".localizedString }
    static var SAG : String { "Sports Authority of Gujarat".localizedString }
    static var whatsNew : String { "What's New".localizedString }
    static var Setting : String { "Settings".localizedString }
    static var upcomingEvents : String {"Upcoming Events".localizedString}
    static var photoGallery : String { "Photo Gallery".localizedString }
    static var videoGallery : String { "Video Gallery".localizedString }
    static var mediaCenter : String { "Media Center".localizedString }
    static var galleryDetails : String { "Gallery Details".localizedString }
    static var profile : String { "Profile".localizedString }
    static var playerList : String { "Players List".localizedString }
    static var addPlayer : String { "ADD PLAYER".localizedString }
    static var TeamRegistration : String { "Team Registration".localizedString }
    static var IndividualRegistration : String { "Individual Registration".localizedString }
    static var NOCApp : String { "NOC Applications".localizedString }
    static var edit_profile: String{ return "Edit Profile".localizedString }
    static var NOCApplicationForm:String{"NOC Application Form".localizedString }
    static var LandDetails:String{"Land Details".localizedString }
    static var addLand:String{"Add Land".localizedString }
    static var NoticeWarrantLands:String {"Notice/Warrant Lands".localizedString}
    static var ChallanList:String {"Challan List".localizedString}

}
