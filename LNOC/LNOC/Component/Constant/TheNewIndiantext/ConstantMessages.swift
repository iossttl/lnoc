//
//  ConstantMessages.swift
//  SharkID
//
//  Created by stadmin on 09/08/17.
//  Copyright © 2017 sttl. All rights reserved.
//

import UIKit

struct CommonMessages {
    private init() { }
    static var msg_Internet_Issue : String { return "Internet connection is not available. Please turn on cellular data or correct Wi-Fi settings.".localizedString }
    static var msg_connection_error : String { return "Connection Error".localizedString }
    static var msg_Camera_Unavailable : String { return "Camera Permission".localizedString }
    static var msg_This_Device_Has_No_Camera : String { return "This device has no camera".localizedString }
    static var msg_unexpected_problem : String { return "Something went wrong. Please try after some time.".localizedString }
    static var msg_the_request_timed_out: String { return "Connection time out. Please check your internet connection and try again.".localizedString }
    static var msg_document_should_be_less_than:String {return "Document should be less than".localizedString + " abc MB." }
    static var msg_no_image_selected:String{return "No Image selected".localizedString }
    static var msg_Video_size_must_be_up_to_4_MB : String { "Video size must be up to 4 MB.".localizedString }
    
    static var wantToQuit : String{ "Are your sure you want to quit?".localizedString }
}

struct LoginMessages {
    static var msg_enter_email: String { return "Please enter email Id".localizedString }
    static var msg_enter_password :String { return "Please enter password".localizedString }
    static var msg_please_enter_valid_email : String { return "Please enter a valid email address.".localizedString }
}


struct RegisterMessages {
    static var enterFirstname: String { return "Please enter first name.".localizedString }
    static var enterMiddlename: String { return "Please enter middle name.".localizedString }
    static var enterLastname: String { return "Please enter last name.".localizedString }
    static var msg_please_enter_email : String { "Please enter email id.".localizedString }
    static var msg_please_enter_OTP : String { "Please enter OTP.".localizedString }
    static var msg_please_enter_valid_OTP : String { "Please enter valid OTP.".localizedString }
    static var msg_please_enter_valid_email : String { return "Please enter valid email id.".localizedString }
    static var enterMobileNo: String { return "Please enter mobile number.".localizedString }
    static var enterValidMobileNo: String { return "Please enter valid mobile number.".localizedString }
    static var selectGender: String { return "Please select gender.".localizedString }
    static var selectState: String { return "Please select state.".localizedString }
    static var selectDistrict: String { return "Please select district.".localizedString }
    static var msg_please_enter_password :String { return "Please enter password.".localizedString }
    static var msg_password_not_match :String { return "Password & confirm password do not match.".localizedString }
    static var msg_password :String { "The password should contain at least one uppercase letter, at least one numeric, and at least one special character and password length should be minimum 8 characters".localizedString }
    
    static var msg_PrivacyPolicy :String { "Please accept the Privacy Policy".localizedString }
    
    static var firstNameIsRequired: String { return "First Name is required".localizedString }
    static var middleNameIsRequired: String { return "Middle Name is required".localizedString }
    static var lastNameIsRequired: String { return "Last Name is required".localizedString }
    static var dateOfBirthIsRequired: String { return "Date Of Birth is required".localizedString }
    static var emailIsRequired: String { return "Email is required".localizedString }
    static var mobileNumberIsRequired: String { return "Mobile Number is required".localizedString }
    static var pleaseSelectYourState: String { return "Please select your state".localizedString }
    static var pleaseSelectYourDistrict: String { return "Please select your district".localizedString }
    static var passwordIsRequired: String { return "Password is required".localizedString }
    static var confirmPasswordIsRequired: String { return "Confirm Password is required".localizedString }
    static var thePasswordAndConfirmPasswordShouldBeSame: String { return "The password and confirm password should be same".localizedString}
    static var pleaseEnteraValidEmailAddress: String { return "Please enter a valid email address".localizedString}
    static var mobileNumberIsNotValidMinimum10NumberRequired: String { return "Mobile number is not validMinimum 10 number required".localizedString}
    static var otpIsRequired: String { return "OTP is required".localizedString }
    static var minimum6Required: String { return "Minimum 6 required".localizedString }
    static var mobileNumberIsNotVerify: String { return "Mobile Number is not verify".localizedString }
    static var emailIsNotVerify: String { return "Email is not verify".localizedString }
    static var password_not_in_valid_formate : String {return "Password (UpperCase, LowerCase, Number, SpecialChar and min 8 Chars)".localizedString}
    
    
}

struct SettingsMessages {
    static var msg_please_enter_current_password: String { return "Please enter current password.".localizedString }
    static var msg_please_new_password: String { return "Please enter new password.".localizedString }
    static var msg_please_repeat_new_password: String { return "Please Enter Confirm Password".localizedString }
    static var msg_please_enter_same_password: String { return "Password & confirm password do not match.".localizedString }
    static var msg_password :String { "The password should contain at least one uppercase letter, at least one numeric, and at least one special character and password length should be minimum 8 characters".localizedString }
    static var password_not_in_valid_formate : String {return "Password (UpperCase, LowerCase, Number, SpecialChar and min 8 Chars)".localizedString}

}

struct SideMenuMessages {
    static var suretoLogout :String { "Are you sure you want to logout?".localizedString }
    static var logoutSuccessful :String { "You have successfully Logout".localizedString }
}

struct ProfileMessages {    
    static var firstNameIsRequired: String { return "First Name is required".localizedString }
    static var middleNameIsRequired: String { return "Middle Name is required".localizedString }
    static var lastNameIsRequired: String { return "Last Name is required".localizedString }
    static var dateOfBirthIsRequired: String { return "Date Of Birth is required".localizedString }
    static var emailIsRequired: String { return "Email is required".localizedString }
    static var pleaseEnteraValidEmailAddress: String { return "Please enter a valid email address".localizedString }
    static var mobileNumberIsRequired: String { return "Mobile Number is required".localizedString }
    static var pleaseSelectYourState: String { return "Please select your state".localizedString }
    static var pleaseSelectYourDistrict: String { return "Please select your district".localizedString }
    static var mobileNumberIsNotValid: String { return "Mobile number is not valid".localizedString}
    static var addressIsRequired : String { return "Address is required".localizedString }
    static var pleaseSelectGender: String { return "Please select Gender".localizedString }
    static var aadhaarNoIsRequired: String { return "Aadhaar No is required".localizedString }
    static var panNoIsRequired: String{ return "Pan No is required".localizedString }
    static var bankNameIsRequired: String { return "Bank Name is required".localizedString }
    static var bankAccountNumberIsRequired: String {return "Bank Account Number is required".localizedString}
    static var valueBtw1220chrs: String {return "Please enter value between 12 and 20 characters long".localizedString}
    static var pleaseEnterPANNumberInValidFormat : String{return "Please Enter PAN Number in Valid Format".localizedString}
}

struct AddDetailsMessages {
    static var enterChallanNumber: String { return "Please enter Challan Number.".localizedString }
    static var enterAmount: String { return "Please enter Amount.".localizedString }
    static var enter_AccountHead : String { "Please select Account Head.".localizedString }
    static var enter_BankName : String { return "Please enter Bank Name.".localizedString }
    static var selectChallanDate: String { return "Please select Challan Date.".localizedString }
    static var attachAffidavit: String { return "Please attach Affidavit Document.".localizedString }
    static var attachChallan: String { return "Please attach Challan Document.".localizedString }
}


struct PropertyDetailsMessages {
    static var pleaseSelectOwnership: String { return "Ownership of is required".localizedString}
    static var pleaseSelectSalutation: String { return "salutation of is required".localizedString}
    static var pleaseSelectFirstName: String { return "First Name is required".localizedString}
    static var pleaseSelectMiddleName: String { return "Middle Name is required".localizedString}
    static var pleaseSelectLastname: String { return "Last Name is required".localizedString}
    static var pleaseSelectCompanyName: String { return "Company Name is required".localizedString}
    
    static var pleaseSelectNocType: String { return "Please select NOC Type".localizedString }
    static var pleaseSelectDivision: String { return "Please select Division".localizedString }
    static var pleaseselectTypeOfPremises : String { return "Please select Type Of Premises".localizedString }
    static var NameOfTheTransfereeisrequired: String { return "Name Of The Transferee is required".localizedString }
    static var ApplicantAddressisrequired: String { return "Applicant Address is required".localizedString }
    static var NameOfTheTransferorisrequired: String { return "Name Of The Transferor is required".localizedString }
    static var CSNoPlotNoisrequired: String { return "CS No / Plot No is required".localizedString }
    static var NameofSocietyisrequired: String { return "Name of Society is required".localizedString }
    static var NameofBuildingPremisesisrequired: String { return "Name of Building / Premises is required".localizedString }
    static var Totalnumberoffloorsofbuildingisrequired: String { return "Total number of floors of building is required".localizedString }
    static var pleaseEnteraValidEmailAddress: String { return "Please enter a valid email address".localizedString }

    static var NoofLiftisrequired: String { return "No. of Lift is required".localizedString }
    static var completionofbuildingisrequired: String { return "Not valid year".localizedString }
    static var societychairmannameisrequired: String { return "society chairman name is required".localizedString }
    static var chairmancontactnumberisrequired: String { return RegisterMessages.enterValidMobileNo.localizedString }
    static var chairmancontactnumberValidMobileNo: String { return "chairman contact number is required".localizedString }
    static var societysecretarynameisrequired: String { return "society secretary name is required".localizedString }
    static var secretarycontactnumberisrequired: String { return "secretary contact number is required".localizedString }
    static var NoOfMembersisrequired: String { return "No Of Members is required".localizedString }
    static var FlatNumberisrequired: String { return "Flat Number / Office Number / Shop Number/ Apartment / Condominium is required".localizedString }
    static var FloorOfFlatisrequired: String { return "Floor Of Flat / Office / Shop / Apartment / Condominium is required".localizedString }
    static var AreaOfFlatSqFtisrequired: String { return "Area Of Flat Sq Ft is required".localizedString }
    static var Pleaseenteravalue: String { return "Please enter a value".localizedString }
    static var pleaseEnterAValueMatchingThePattern : String { return "Please enter a value matching the pattern".localizedString}
    static var ParkingNumberisrequired: String { return "Parking Number is required".localizedString}
    static var ParkingDetailsDontMatch: String { return "Number of Under transfer and Parking Details Dont match".localizedString}
}

struct PreviousPropertyDetailsMessages {
    
    static var PleaseEnterNameofTransferrer: String { return "Please Enter Name of Transferrer".localizedString }
    static var PleaseEnterNameofTransferee: String { return "Please Enter Name of Transferee".localizedString }
    static var PleaseEnterYearofTransfer: String { return "Please Enter Year of Transfer".localizedString }
    static var YearNotlessthan4digits: String { return "Year Should not be less than 4 digits".localizedString }
    static var PleaseSelectTypeofTransfer: String { return "Please Select Type of Transfer".localizedString }
    static var PleaseSelectNOCObtained: String { return "Please Select NOC Obtained".localizedString }
    static var PleaseUploadNOCObtainedDoc: String { return "Please Upload NOC Obtained Document".localizedString }
    static var PleaseEnterPreviousDetails: String { return "Please enter previous transfer details".localizedString }
}

struct AddLandDetailsMessages {
    
    static var CRRNumberisrequired: String { return "CRR Number is required".localizedString }
    static var CSNumberisrequired: String { return "CS Number is required".localizedString }
    static var Divisionisrequired: String { return "Division is required".localizedString }
    static var LandHolderisrequired: String { return "Land Holder is required".localizedString }
    static var AddressOfOwnerisrequired: String { return "Address Of Owner is required".localizedString }
    static var LocationOfpropertyisrequired: String { return "Location Of property is required".localizedString }
    static var UseofLandisrequired: String { return "Use of Land is required".localizedString }
    static var InterestRateisrequired: String { return "Interest Rate is required".localizedString }
    static var Areaisrequired: String { return "Area is required".localizedString }
    static var PendingTaxisrequired: String { return "Pending Tax is required".localizedString }
    static var Interestisrequired: String { return "Interest is required".localizedString }
    static var Rentisrequired: String { return "Rent is required".localizedString }
    static var DueDateisrequired: String { return "Due Date is required".localizedString }
    static var DeedNoisrequired: String { return "Deed No is required".localizedString }
    static var DueDate1isrequired: String { return "Due Date 1 is required".localizedString }
    static var DueDate2isrequired: String { return "Due Date 2 is required".localizedString }
    static var DueDate3isrequired: String { return "Due Date 3 is required".localizedString }
    static var DueDate4isrequired: String { return "Due Date 4 is required".localizedString }
    static var CommDateisrequired: String { return "Comm Date is required".localizedString }
    static var ExpDateisrequired: String { return "Exp Date is required".localizedString }
    static var PRYearisrequired: String { return "PR Year is required".localizedString }
    static var LeaseStartDateisrequired: String { return "Lease Start Date is required".localizedString }
    static var LeaseEndDateisrequired: String { return "Lease End Date is required".localizedString }
    static var LeasePeriodisrequired: String { return "Lease Period is required".localizedString }
    static var LandTenureisrequired: String { return "Land Tenure is required".localizedString }
    static var AssessmentAmountisrequired: String{return "Assessment Amount is required".localizedString}
    static var Feeisrequired: String { return "Fee is required".localizedString }
    static var DeedDateisrequired: String { return "Deed Date is required".localizedString }
    
}

struct FeedbackMessages {
    
    static var msg_enter_name: String { return "Please enter name".localizedString }
    static var msg_enter_email: String { return "Please enter email id.".localizedString }
    static var msg_please_enter_valid_email : String { return "Please enter valid email id.".localizedString }
    static var mobileNumberIsRequired: String { return "Mobile Number is required".localizedString }
    static var mobileNumberIsNotValidMinimum10NumberRequired: String { return "Mobile number is not validMinimum 10 number required".localizedString}
    static var msg_enter_desc: String { return "Description is required".localizedString }
}

struct LandDetailsMessages {
    static var msg_Removed_from_Land_Favourites : String { return "Removed from Land Favourites".localizedString }
    static var msg_Added_to_Land_Favourites: String { return "Added to Land Favourites".localizedString }
}

struct LandListMessages {
    static var msg_delete : String { return "Are you sure you want to delete this record?".localizedString }
    static var msg_deleted : String { return "Record has been deleted".localizedString }
    static var msg_Land_linked_successfully : String { return "Land linked successfully".localizedString }
}

struct AddClarificationDocumentMessages {
    static var clarificationDocumentIsRequired : String { return "Clarification Document is required".localizedString }
    static var clarificationRemarksIsRequired : String { return "Clarification Remarks is required".localizedString }
}
