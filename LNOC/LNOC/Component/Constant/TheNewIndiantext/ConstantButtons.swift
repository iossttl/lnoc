//
//  ConstantButton.swift
//  SharkID
//
//  Created by stadmin on 09/08/17.
//  Copyright © 2017 sttl. All rights reserved.
//

import UIKit

struct CommonButtons {
    static var camera: String { return "Camera".localizedString }
    static var photo_library: String { return "Photo Library".localizedString }
    static var cancel: String { return "Cancel".localizedString }
    static var done: String { return "Done".localizedString }
    static var yes: String { return "Yes".localizedString }
    static var no: String { return "No".localizedString }
    static var save: String { return "Save".localizedString }
    static var send: String { return "Send".localizedString }
    static var ok: String { return "OK".localizedString }
    static var document: String { return "Document".localizedString }
    static var btn_submit: String  {"Submit".localizedString}
    static var btn_skip: String {"Skip".localizedString}
    static var select: String { return "Select".localizedString }
    static var btn_signin : String { return "Sign In".localizedString }
    static var btn_finish : String { return "Finish".localizedString }
    
    static var btn_AddLinkLand : String { return "Add / Link Land".localizedString }
    
    static var btn_update : String { return "Update".localizedString }
    static var btn_Add_Previous_Transfer_Details : String { return "Add Previous Transfer Details".localizedString }
}


struct LoginButtons {
    static var btn_forgot_password : String { return "Forgot Password?".localizedString }
    static var btn_signin : String { return "Sign In".localizedString }
    static var btn_register : String { return "Register".localizedString }
    static var btn_login : String { return "LOGIN".localizedString }
}

struct HomeButtons {
    static var continueStr: String { return "Continue" }//.localizedString }
}



