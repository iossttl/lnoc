//
//  ConstantPlaceHolder.swift
//  SharkID
//
//  Created by stadmin on 09/08/17.
//  Copyright © 2017 sttl. All rights reserved.
//

import UIKit

struct CommonPlaceholders {
    static var search: String { "Search".localizedString }
}

struct LoginPlaceHolders {
    static var email_address : String { return "Enter Email Address".localizedString }
    static var password : String { return "Enter Password".localizedString }
}

struct FilterPlaceHolders {
    static var enter_CRR_number : String { return "Enter CRR number".localizedString }
    static var enter_Cs_number : String { return "Enter Cs number".localizedString }
    static var enter_Land_Holder : String { return "Enter Land Holder".localizedString }
    static var enter_Location_Of_Property : String { return "Enter Location Of Property".localizedString }
    static var enter_Area : String { return "Enter Area".localizedString }
    static var enter_CS_Number : String { return "Enter CS Number".localizedString }
}

struct NoticeFilterPlaceHolders {
    static var enter_Notice_Type : String { return "Enter Notice Type".localizedString }
    static var enter_Template_Type : String { return "Enter Template Type".localizedString }
  
}

