//
//  ConfigurationConstant.swift
//  SharkID
//
//  Created by Vikram Jagad on 25/02/19.
//  Copyright © 2019 sttl. All rights reserved.
//

//http://192.168.0.72:5000/api/app/

import UIKit
import MobileCoreServices

//MARK:- Common Use for all
let DeviceID = UIDevice.current.identifierForVendor!.uuidString
//MARK:- Devices Type
let IS_IPAD = (UIDevice.current.userInterfaceIdiom == .pad)
let IS_IPHONE = (UIDevice.current.userInterfaceIdiom == .phone)
let osVersion = UIDevice.current.systemVersion

//MARK:- Get Devices Width and Height
let SCREEN_WIDTH = CGFloat(UIScreen.main.bounds.size.width)
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let SCREEN_MAX_LENGTH = max(SCREEN_WIDTH, SCREEN_HEIGHT)
let SCREEN_MIN_LENGTH = min(SCREEN_WIDTH, SCREEN_HEIGHT)
let IS_IPHONE_4_OR_LESS = (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
let IS_IPHONE_5 = (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
let IS_IPHONE_6 = (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
let IS_IPHONE_6P = (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
let IS_IPHONE_X = (IS_IPHONE && SCREEN_HEIGHT >= 812.0)
public typealias JSONObject = [String : Any]
var bottomMarginFromSafeArea: CGFloat {
    if #available(iOS 11.0, *)
    {
        return UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
    }
    return 0
}

let IS_IPHONE_WITH_NOTCH = (IS_IPHONE && STATUS_BAR_HEIGHT > 20)
let IPAD_MARGIN: CGFloat = 70

var STATUS_BAR_HEIGHT : CGFloat {
    return UIApplication.shared.statusBarFrame.height
}


//MARK:- DeviceID
//let DeviceID = UIDevice.current.identifierForVendor!.uuidString //"75a30533aaaa87b"

//MARK:- StoryBoard
let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)

//MARK:- NotificationCenter
let K_NC = NotificationCenter.default

//MARK:- AppDelegates instances
var appDelegate : AppDelegate! { return UIApplication.shared.delegate as? AppDelegate }

var selectedLanguage : LanguageCodes = LanguageCodes.english
//MARK:- DeviceID
//let DeviceID = Global.uniQueID()

//MARK:- TableView
let groupTablePadding: CGFloat = 35

let ALPHABETS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
let ALPHABETS_UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
let NUMERIC = "1234567890"
let ALPHABETS_NUMERIC = "\(ALPHABETS)\(NUMERIC)"
let ALPHABETS_NUMERIC_UPPERCASE = "\(ALPHABETS_UPPERCASE)\(NUMERIC)"
let NUMERIC_SPACE = "\(NUMERIC) "
let ALLOWED_SPECIAL_CHARACTER = "&@._-'/"
let ALPHABETS_NUMERIC_SPACE = "\(ALPHABETS)\(NUMERIC) "
let ALPHABETS_SPACE = "\(ALPHABETS) "
let MOBILE_LENGTH_VALIDATION = 10
let BANK_ACC_VALIDATION_LENGTH = 20
let YEAR_VALIDATION = 4
let OTP_LENGTH = 6
let CIN_NO_LENGTH = 21
let PAN_CARD_LENGTH = 10
let AADHAR_CARD_LENGTH = 12
let GST_NO_LENGTH = 25
let MAX_QUANTITY: Int = 999999
let k_address_Length = 300


var ONLY_PDF_Types: [String] { return [kUTTypePDF as String] }
var AllDoc_Types: [String] { return ["public.content"] }

var isPresent = false



//MARK:- Document Types
let arrdocTypes:[String] = ["com.microsoft.word.doc","org.openxmlformats.wordprocessingml.document", kUTTypePDF as String]
let arrexcludeFile:[String] = ["com.microsoft.word.doc","org.openxmlformats.wordprocessingml.document", kUTTypePDF as String]
let arrdocTypesForProfile : [String] = ["public.image", kUTTypePDF as String]

//    ["sqlite","com.apple.iwork.pages.pages", "com.apple.iwork.numbers.numbers", "com.apple.iwork.keynote.key","public.image", "com.apple.application", "public.item","public.data", "public.content", "public.audiovisual-content", "public.movie", "public.audiovisual-content", "public.video", "public.audio", "public.text", "public.data", "public.zip-archive", "com.pkware.zip-archive", "public.composite-content", "public.text","com.microsoft.word.doc","org.openxmlformats.wordprocessingml.document"]

