//
//  FontConstants.swift
//  Inspection Module
//
//  Created by Mobile Team on 20/01/20.
//  Copyright © 2020 Mobile Team. All rights reserved.
//

import UIKit

struct FontConst {
    public static let Proxi_Bold = "ProximaNova-Bold"
    public static let Proxi_Regular = "ProximaNovaCond-Regular"
    public static let Proxi_Semibold = "ProximaNova-Semibold"
    public static let Roboto_Bold = "Roboto-Bold"
    public static let Roboto_Medium = "Roboto-Medium"
    public static let Roboto_Regular = "Roboto-Regular"
}



