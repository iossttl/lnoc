//
//  ColorConstant.swift
//  SharkID
//
//  Created by Vikram Jagad on 25/02/19.
//  Copyright © 2019 sttl. All rights reserved.
//

import UIKit

extension UIColor {
    @objc class var customBlack: UIColor {
        if #available(iOS 13, *) {
            return .label
        } else {
            return .black
        }
    }
    
    @objc class var customWhite: UIColor {
        if #available(iOS 13, *) {
            return .systemBackground
        } else {
            return .white
        }
    }
    
    @objc class var customTertiary: UIColor {
        
        return UIColor(named: "customTertinary") ?? UIColor(hexStringToUIColor: "#8F8884")
//
//        if #available(iOS 13, *) {
//            return .tertiarySystemBackground
//        } else {
//            return .white
//        }
    }
    
    @objc class var customSecondary: UIColor {
        if #available(iOS 13, *) {
            return .secondarySystemBackground
        } else {
            return .groupTableViewBackground
        }
    }
    
    @objc class var customSubtitle: UIColor {
        return UIColor(named: "description") ?? UIColor(hexStringToUIColor: "#8F8884")
    }
    
    @objc class var customShadow: UIColor {
        if #available(iOS 13, *) {
            return .secondaryLabel
        } else {
            return .lightGray
        }
    }
    
    @objc class var customSeparator: UIColor {
        return UIColor(named: "seperatorclr") ?? UIColor(186, 186, 186)
    }
    
    @objc class var customBlackTitle: UIColor {
        return UIColor(named: "BlackTitle") ?? UIColor(hexStringToUIColor: "5d5d5d")
    }
    
    @objc class var customGray: UIColor {
        return UIColor(named: "customGray") ?? UIColor(hexStringToUIColor: "#8F908F")
        
        //return UIColor.gray
    }
    
    @objc class var customLightGray: UIColor {
        return UIColor.lightGray
    }
    
    @objc class var customRed: UIColor {
        if #available(iOS 13, *) {
            return .systemRed
        } else {
            return .red
        }
    }
    
    @objc class var customGreen: UIColor {
        if #available(iOS 13, *) {
            return .systemGreen
        } else {
            return .green
        }
    }
    static var customgreenTheamClr: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "greenTheamclr") ?? UIColor(hexStringToUIColor: "#369749")
        } else {
            return UIColor(hexStringToUIColor: "#369749")
        }
    }

    //theam clr
    static var customblueTheamClr: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "bluetheamclr") ?? UIColor(hexStringToUIColor: "#1E6C92")
        } else {
            return UIColor(hexStringToUIColor: "#1E6C92")
        }
    }

    
    static var customlightblueTheamClr: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "lightBluetheamclr") ?? UIColor(hexStringToUIColor: "#3E9199")
        } else {
            return UIColor(hexStringToUIColor: "#3E9199")
        }
    }

    static var customPinkTheamClr: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "pinkThemclr") ?? UIColor(hexStringToUIColor: "#FE4180")
        } else {
            return UIColor(hexStringToUIColor: "#FE4180")
        }
    }
    
    static var customgreenUploadedClr: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "greenuploaded") ?? UIColor(hexStringToUIColor: "#1F6922")
        } else {
            return UIColor(hexStringToUIColor: "#1F6922")
        }
    }
    
    //
    static var customDarkBrownGradientColor: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "customDarkBrownGradientColor") ?? UIColor(hexStringToUIColor: "#7A4425")
        } else {
            return UIColor(hexStringToUIColor: "#7A4425")
        }
    }
    
    
    static var customCellSecondary: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "customCellSecondary") ?? UIColor(hexStringToUIColor: "#000000")
        } else {
            return UIColor(hexStringToUIColor: "#000000")
        }
    }

    static var customTitleBlue: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "customTitleBlue") ?? UIColor(hexStringToUIColor: "#3449C0")
        } else {
            return UIColor(hexStringToUIColor: "#3449C0")
        }
    }
    
    
    @objc class var customOrange: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "customOrange") ?? UIColor(hexStringToUIColor: "#FF5A5F")
        } else {
            return UIColor(hexStringToUIColor: "#FF5A5F")
        }
    }
    
    @objc class var halfSpanishWhite: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "halfSpanishWhite") ?? UIColor(hexStringToUIColor: "#FEF0DD")
        } else {
            return UIColor(hexStringToUIColor: "#FEF0DD")
        }
    }

    
    @objc class var customLightSkyBlue: UIColor {
        return UIColor(named: "customLightSkyBlue") ?? UIColor(hexStringToUIColor: "#E2EEF9")
    }
    
    @objc class var customBGLightSkyBlue: UIColor {
        return UIColor(named: "bgSkyBlueClr") ?? UIColor(hexStringToUIColor: "#EDF0F5")
    }
    
    @objc class var customHonourOptionsBackground: UIColor {
        return UIColor(named: "customHonourOptionsBackground") ?? UIColor(hexStringToUIColor: "#F4F4F4")
    }
    
    @objc class var customLightBlueGradient: UIColor {
        return UIColor(named: "darkLightBlueGradient") ?? UIColor(hexStringToUIColor: "#4021A4")
    }
    
    @objc class var customVirtualTourCurrentselected: UIColor {
        return UIColor(named: "customVirtualTourCurrentselected") ?? UIColor(hexStringToUIColor: "#ffa846")
    }
    
    @objc class var customSearchDropDownBg: UIColor {
        return UIColor(named: "customSearchDropDownBg") ?? UIColor(hexStringToUIColor: "DADBEA")
    }
    
    @objc class var darkBlue: UIColor {
        return UIColor(named: "darkBlue") ?? UIColor(hexStringToUIColor: "091B4F")
    }
    
    static var customHonorDetailsRed: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "customHonorDetailsRed") ?? UIColor(hexStringToUIColor: "#841400")
        } else {
            return UIColor(hexStringToUIColor: "#841400")
        }
    }
    
    @objc class var customDarkBlueGradient: UIColor {
        return UIColor(named: "customDarkBlueGradient") ?? UIColor(8, 30, 75)
    }

    @objc static let brownWall: UIColor? = UIColor(named: "brownWall")
    
    @objc static let homageMartyrName: UIColor? = UIColor(named: "homageMartyrName")
    
    @objc class var customSideMenuIconBlue: UIColor {
        return UIColor(named: "customSideMenuIconBlue") ?? UIColor(hexStringToUIColor: "#131F8E")
    }
    
    
    @objc class var customCancel: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "customCancel") ?? UIColor(hexStringToUIColor: "#A1A2A7")
        } else {
            return UIColor(hexStringToUIColor: "#A1A2A7")
        }
    }
    
    @objc class var customTextGrayClr: UIColor {
        return UIColor(named: "textGrayClr") ?? .gray
    }
    
    @objc class var customRedBtnClr: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "btnRedclr") ?? UIColor(hexStringToUIColor: "#f44336")
        } else {
            return UIColor(hexStringToUIColor: "#f44336")
        }
    }
    
    @objc class var customBlueTextclr: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "blueTextclr") ?? UIColor(hexStringToUIColor: "#182368")
        } else {
            return UIColor(hexStringToUIColor: "#182368")
        }
    }
    
    @objc class var customContainerHeader: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "customContainerHeader") ?? UIColor(hexStringToUIColor: "#5F6295")
        } else {
            return UIColor(hexStringToUIColor: "#5F6295")
        }
    }
    
    @objc class var customContainerHeader2 : UIColor {
        return UIColor(named: "customContainerHeader2") ?? UIColor(hexStringToUIColor: "#AF4F4C")
    }
    
    @objc class var customTagClr : UIColor {
        return UIColor(named: "clrTag") ?? UIColor(hexStringToUIColor: "#AF4F4C")
    }
    
    @objc class var customViewAllclr : UIColor {
        return UIColor(named: "clrViewAll") ?? UIColor(hexStringToUIColor: "#AF4F4C")
    }
    
    @objc class var clrFeaturedViewAll : UIColor {
        return UIColor(named: "clrFeaturedViewAll") ?? UIColor(hexStringToUIColor: "#AF4F4C")
    }
    
    
    @objc class var custom_lightGray : UIColor {
        return UIColor(named: "lightGray") ?? .lightGray
    }
    
    @objc class var custom_themBlueClr : UIColor {
        return UIColor(named: "themBlueClr") ?? .blue
    }
    
    @objc class var customBlue: UIColor {
        return UIColor(named: "blueClr") ?? UIColor(hexStringToUIColor: "4191F3")
    }
    
    @objc class var customTertinaryBlue : UIColor {
        return UIColor(named: "customTertinaryBlue") ?? .blue
    }
    
    @objc class var customBannerPurple: UIColor {
        return UIColor(named: "customBannerPurple") ?? UIColor(hexStringToUIColor: "#876CEC")
    }
    
    @objc class var customSharingGray: UIColor {
        return UIColor.gray
    }
    
    @objc class var customblackTagColor: UIColor {
        return UIColor.black.withAlphaComponent(0.6)
    }
    
    
    
    
    @objc class var leftMenuBg: UIColor {
        return UIColor(named: "leftMenuBg") ?? UIColor(hexStringToUIColor: "#FFFFFF")
    }
    
    @objc class var leftMenuTopGrid: UIColor {
        return UIColor(named: "leftMenuTopGrid") ?? UIColor(hexStringToUIColor: "#000000")
    }
    
    @objc class var leftMenuIcons: UIColor {
        return UIColor(named: "leftMenuIcons") ?? UIColor(hexStringToUIColor: "#F44336")
    }
    
    @objc class var lnocBackgroundColor: UIColor {
        return UIColor(named: "bgClr2") ?? UIColor(hexStringToUIColor: "#E0E0E6")
    }
    
    
    static func getColorAccordingToAppearance(darkModeClr : UIColor, lightModeClr : UIColor, defaultClr : UIColor?) -> UIColor {
        if #available(iOS 13.0, *) {
            return UIColor { (traits) -> UIColor in
                // Return one of two colors depending on light or dark mode
                return traits.userInterfaceStyle == .dark ? darkModeClr : lightModeClr
            }
        } else {
            // Same old color used for iOS 12 and earlier
            return defaultClr ?? lightModeClr
        }
    }
    
    @objc class var themeDarkBlueClr: UIColor {
        return UIColor(named: "themeDarkBlueClr") ?? UIColor(hexStringToUIColor: "#1E378E")
    }
    
    @objc class var themeLightBlueClr: UIColor {
        return UIColor(named: "themeLightBlueClr") ?? UIColor(hexStringToUIColor: "#2E62AE")
    }
    
    @objc class var placeholderTextClr: UIColor {
        return UIColor(named: "placeholderTextClr") ?? UIColor(hexStringToUIColor: "#8A8989")
    }
    
    @objc class var themeDarkOrangeClr: UIColor {
        return UIColor(named: "themeDarkOrangeClr") ?? UIColor(hexStringToUIColor: "#F7880A")
    }
    
    @objc class var themeLightOrangeClr: UIColor {
        return UIColor(named: "themeLightOrangeClr") ?? UIColor(hexStringToUIColor: "#F3B113")
    }
    
    @objc class var customlinkBlue: UIColor {
        return UIColor(named: "customlinkBlue") ?? UIColor(hexStringToUIColor: "#F3B113")
    }
    
    @objc class var themeColor: UIColor {
        return UIColor(named: "themeColor") ?? UIColor(hexStringToUIColor: "#2C783D")
    }
    
    static var customDescription: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "description") ?? UIColor(hexStringToUIColor: "#7b7b7b")
        } else {
            return UIColor(hexStringToUIColor: "#7b7b7b")
        }
    }
    
    static var customLightBrownGradientColor: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "customLightBrownGradientColor") ?? UIColor(hexStringToUIColor: "#996141")
        } else {
            return UIColor(hexStringToUIColor: "#996141")
        }
    }
    
    @objc class var flamingoApproxColor: UIColor {
        return UIColor(hexStringToUIColor: "#F55427")
    }
    
    @objc class var appSeparatorColor: UIColor {
        return UIColor.lightGray.withAlphaComponent(0.5)
    }
    
    static var customPercentWorkDone: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "customPercentWorkDone") ?? UIColor(hexStringToUIColor: "#757575")
        } else {
            return UIColor(hexStringToUIColor: "#757575")
        }
    }
    
    static var progressColor: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "progressColor") ?? UIColor(hexStringToUIColor: "#afd139")
        } else {
            return UIColor(hexStringToUIColor: "#afd139")
        }
    }
    
    static var unProgressColor: UIColor {
        if #available(iOS 11, *) {
            return UIColor(named: "unProgressColor") ?? UIColor(hexStringToUIColor: "#e4ddd6")
        } else {
            return UIColor(hexStringToUIColor: "#e4ddd6")
        }
    }
    
    
    
}

