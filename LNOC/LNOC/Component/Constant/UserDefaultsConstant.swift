//
//  UserDefaultsConstant.swift
//  SharkID
//
//  Created by Vikram Jagad on 25/02/19.
//  Copyright © 2019 sttl. All rights reserved.
//

import Foundation

struct UserPreferencesKeys {
    struct API {
        static let apiToken = "apiToken"
    }
    struct Alamofire {
        static let apiRequestTimeOut = "Api_Request_TimeOut"
    }
    struct General {
        static let isUserLoggedIn = "isUserLoggedIn"
        static let skippedLogin = "skippedLogin"
        static let languageCode = "languageCode"
        static let isSkip = "isSkip"
        static let email = "email"
        static let name = "name"
        static let first_name = "first_name"
        static let last_name = "last_name"
        static let isLanguageSelected = "isLanguageSelected"
     
        static let isintro = "itro_skip"
        static let isRemember = "isRemember"
        static let rememberEmail = "rememberEmail"
        static let rememberFormType = "rememberFormType"
        
        
        static let rememberPass = "rememberPass"
        static let dashBoardCategory = "dashBoardCategory"
        static let token = "token"
        static let user_roles_id = "user_roles_id"
        static let userImg = "userImg"
    }
    
    struct DeviceInfo {
        static let deviceID = "deviceid"
        static let deviceNotificationTokenSend = "deviceNotificationTokenSend"
        static let deviceNotificationToken = "deviceNotificationToken"
    }
    struct UserInfo {
        static let userID = "userID"
        static let userDetail = "userDetail"
        static let mobile = "mobile"
    }
    struct Notification {
        static let lmd = "lmd"
        static let notificationCount = "notificationCount"
    }
    
    struct ApplicationKeys {
        static let key_UD_ivEnc = "ivEnc"
    }
}


