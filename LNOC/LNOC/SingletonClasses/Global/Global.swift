//
//  Global.swift
//  Vidyanjali
//
//  Created by Vikram Jagad on 06/11/20.
//  Copyright © 2020 Vikram Jagad. All rights reserved.
//

import UIKit
import BRYXBanner

class Global: NSObject {
    static let shared = Global()
    
//    func showBanner(title: String? = nil, message: String, img: UIImage? = nil, bgColor: UIColor = .themeLightBlueClr) {
//    var window: UIWindow? {
////        if #available(iOS 13, *) {
////            return sceneDelegate?.window
////        } else {
//            return appDelegate.window
////        }
//    }
    
    var window: UIWindow? {
//        if #available(iOS 13, *) {
//            return sceneDelegate?.window
//        } else {
            return appDelegate.window
//        }
    }


    func getIVKey() -> String {
        var returnValue = UserPreferences.string(forKey: UserPreferencesKeys.ApplicationKeys.key_UD_ivEnc)
        if returnValue.isEmptyString {
            returnValue = EncryptionOtherKeys.decryptWithKeyIV(str: STATIC_IV_VALUE, key: staticKey, newiv: staticIv)
            UserPreferences.set(value: UserPreferencesKeys.ApplicationKeys.key_UD_ivEnc, forKey: returnValue)
        }
        return returnValue
    }
    
    func showBanner(title: String? = nil, message: String, img: UIImage? = nil, bgColor: UIColor = .white) {
        let banner = Banner(title: title, subtitle: message, image: img, backgroundColor: bgColor, didTapBlock: nil)
        banner.titleLabel.textColor = .red
        banner.titleLabel.font = .boldValueFont
        banner.detailLabel.textColor = .red
        banner.detailLabel.font = .regularValueFont
        banner.dismissesOnTap = true
        banner.dismissesOnSwipe = true
        banner.show(nil, duration: 1.5)
    }
    
    func changeInitialVC(animated:Bool = false, direction: UIWindow.TransitionOptions.Direction = .toRight) {
//        if #available(iOS 13, *) {
//            sceneDelegate?.setUpInitialVC()
//        } else {
            appDelegate.setUpInitialVC()
//        }
    }
    
    func getwindow() -> UIWindow{
    var window:UIWindow!
//    if #available(iOS 13, *) {
//        window = ((UIApplication.shared.connectedScenes.first)?.delegate as? SceneDelegate)?.window
//    } else {
        window = (UIApplication.shared.delegate as? AppDelegate)?.window
//    }
    return window
    }
    
    func readPropertyList(ofName: String) -> Any? {
        if let path = Bundle.main.path(forResource: ofName, ofType: "plist") {
            do {
                let fileUrl = URL(fileURLWithPath: path)
                let data = try Data(contentsOf: fileUrl, options: .init(rawValue: 0))
                let plistData = try PropertyListSerialization.propertyList(from: data, options: .mutableContainers, format: nil)
                return plistData
            } catch let error {
                DebugLog(error.localizedDescription)
            }
        }
        return nil
    }
    
    //Read Json file
    func readJsonFile(ofName: String) -> [String : Any] {
        guard let strPath = Bundle.main.path(forResource: ofName, ofType: ".json") else {
            return [:]
        }
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: strPath), options: .alwaysMapped)
            let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
            if let dictJson = jsonResult as? [String : Any] {
                return dictJson
            }
        } catch let getError {
            print("Error!! Unable to parse", getError.localizedDescription)
        }
        return [:]
    }
    
    var delegateRootVC: UIViewController? {
//        if #available(iOS 13, *) {
//            return sceneDelegate?.window?.rootViewController
//        } else {
            return appDelegate.window?.rootViewController
//        }
    }
    
    var uuidString: String {
        return UUID().uuidString
    }
    
    func widthOfLabel(text: String, height: CGFloat, font: UIFont) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: CGFloat.greatestFiniteMagnitude, height: height))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = text
        label.font = font
        label.sizeToFit()
        return label.frame.width
    }
    
    func heightOfLabel(text: String, width: CGFloat, font: UIFont) -> CGFloat {
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.text = text
        label.font = font
        label.sizeToFit()
        return label.frame.height
    }
    
    func heightOfAttributedTextView(attrStr: NSAttributedString?, width: CGFloat) -> CGFloat {
        let tv = UITextView(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        tv.attributedText = attrStr
        tv.sizeToFit()
        return tv.frame.height + 24
    }
    
    
    func setupImgHeight(img:UIImage,spacing:CGFloat = IS_IPAD ? (IPAD_MARGIN * 2) : 32)->CGFloat{
        let height = (((screenWidth - spacing) / img.size.width) * img.size.height).rounded(.up)
        return height
        
    }
    
    //MARK:- CSS
    func htmlWithCSS(strContent: String) -> String {
        let strCSS: String = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no'></header><html><body style=\"text-align:justify;\">\(strContent)</body></html>"
        return String(strCSS)
    }
    
    static func stringify(json: Any, prettyPrinted: Bool = false) -> String {
        var options: JSONSerialization.WritingOptions = []
        if prettyPrinted {
          options = JSONSerialization.WritingOptions.prettyPrinted
        }

        do {
          let data = try JSONSerialization.data(withJSONObject: json, options: options)
          if let string = String(data: data, encoding: String.Encoding.utf8) {
            return string
          }
        } catch {
          print(error)
        }

        return ""
    }
    
    static func convertToDictionary(text: String) -> Any? {

         if let data = text.data(using: .utf8) {
             do {
                 return try JSONSerialization.jsonObject(with: data, options: [])
             } catch {
                 print(error.localizedDescription)
             }
         }

         return nil

    }
    
    //Sharing:-
    func shareAccordtingToType(type:String, dict:[String : Any], image:UIImage?, VC:UIViewController?, sender:UIButton, completionBlock:@escaping CompletionBlock)
    {
        var vc: UIViewController?
        if VC == nil {
            vc = appDelegate.window?.rootViewController
        }
        else {
            vc = VC
        }
        if (type == EnumShareType.shareText.rawValue) {
            
            let strDetail =  dict[CommonAPIConstant.key_title] as? String
            let strSubject = dict[CommonAPIConstant.key_subject] as? String
            let picURLString = dict[CommonAPIConstant.key_PicURL] as? String
            
            var stringSub = ""
            if let strSubject = strSubject{
                stringSub = strSubject
            }
            
            var stringDetail = ""
            if let strDetail = strDetail{
                stringDetail += "\n\n" + strDetail + "\n"
            }
            
            var items: [Any] = [stringSub, stringDetail]
            if let picURLString = picURLString {
                if let picURL = URL(string: picURLString){
                    items.append(picURL)
                }
            }
            print(items)
            Global.shareTextWithCompletion(arrayItem: items, subject: stringSub, VC: vc, sender: sender, completion: { (act, result) in
                //Web Service Call
                completionBlock()//
            })
            
        }
        if (type == EnumShareType.shareImage.rawValue) {
            
            let strDetail = dict[CommonAPIConstant.key_title] as? String
            let strSubject = dict[CommonAPIConstant.key_subject] as? String
            let picURLString = dict[CommonAPIConstant.key_PicURL] as? String
            
            var stringSub = ""
            if let strSubject = strSubject{
                stringSub = strSubject
            }
            
            var stringDetail = ""
            if let strDetail = strDetail{
                stringDetail += "\n\n" + strDetail + "\n"
            }
            
            var items: [Any] = [stringSub, stringDetail]
            
            if let picURLString = picURLString {
                if let picURL = URL(string: picURLString){
                    items.append(picURL)
                }
            }
            
            if (image?.isEqual(UIImage(named: "ic_login_logo")))! || image == nil{
                
            }
            else{
                items.append(image!)
            }
            print(items)
            Global.shareTextWithCompletion(arrayItem: items, subject: stringSub, VC: vc, sender: sender, completion: { (act, result) in
                //Web Service Call
                completionBlock()
            })
        }
    }

    class func shareTextWithCompletion(arrayItem:[Any], subject:String?, VC:UIViewController?, sender:UIButton, completion: @escaping CompletionBlock1) {
        
        let activityViewController = UIActivityViewController(activityItems: arrayItem, applicationActivities: nil)
        if let subject = subject {
            activityViewController.setValue(subject, forKey: CommonAPIConstant.key_subject)
        }
        
        if IS_IPAD {
            activityViewController.popoverPresentationController?.sourceView = VC?.view

            let point = sender.superview?.convert(sender.frame.origin, to: nil)
            if let xPosition = point?.x, let yPostion = point?.y {
                activityViewController.popoverPresentationController?.sourceRect = CGRect(x: xPosition, y: yPostion, width: sender.frame.size.width, height: sender.frame.size.height)
            } else {
                activityViewController.popoverPresentationController?.sourceRect = VC?.view.bounds ?? CGRect(x: 0, y: 0, width: 0, height: 0)
            }
        }
        VC?.present(activityViewController, animated: true, completion: nil)
        activityViewController.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
            if !completed {
                // User canceled
                return
            }
            // User completed activity
            completion(activityType, completed)
        }
    }
    
    class func shareTextWithImage(arrayItems: [Any], subject: String?, VC:UIViewController?, sender:UIButton, complition:@escaping CompletionBlock1){
        
        let activityViewController = UIActivityViewController(activityItems: arrayItems as [Any], applicationActivities: nil)
        activityViewController.setValue(subject, forKey: CommonAPIConstant.key_subject)
        
        
        if(IS_IPAD){
            activityViewController.popoverPresentationController?.sourceView = VC?.view
            activityViewController.popoverPresentationController?.sourceRect = sender.frame
        }
        else{
            
        }
        VC?.present(activityViewController, animated: true, completion: nil)
        
        activityViewController.completionWithItemsHandler = {(activityType: UIActivity.ActivityType?, completed: Bool, returnedItems: [Any]?, error: Error?) in
            if !completed {
                // User canceled
                return
            }
            // User completed activity
            complition(activityType, completed)
        }
        
    }
}
