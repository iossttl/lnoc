//
//  RemarksListPopupViewController.swift
//  LNOC
//
//  Created by Pradip on 31/12/21.
//

import UIKit

class RemarksListPopupViewController : HeaderViewController {
    
    // MARK: Outlets
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var roundRectShadowView: UIView!
    @IBOutlet weak var roundRectView: UIView!
    @IBOutlet weak var tblView: UITableView!
    
    var arrayData: [RemarksListModel] = []
    var strHeaderTitle: String = ""
    var type = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        changeLeadingTrailingForiPad(view: self.viewMain)
//        setUpView()
        setUpTableView()
        self.setUpHeader()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.viewWillLayoutSubviews()
    }
    
    fileprivate func setUpHeader() {
        setHeaderView_BackImage()
        if type == 0 {
            setUpHeaderTitle(strHeaderTitle: Headers.Remarks)
        } else if type == 1 {
            setUpHeaderTitle(strHeaderTitle: Headers.QueryRejectionRemarks)
        } else if type == 2 {
            setUpHeaderTitle(strHeaderTitle: Headers.NOCCertificateRemarks)
        }
    }
    
    private func setUpView() {
        
        self.roundRectView.viewWith(radius: 10, borderColor: .clear, borderWidth: 0)
        self.roundRectShadowView.viewWith(radius: 10, borderColor: .clear, borderWidth: 0)
        
        if #available(iOS 13.0, *) {
            view.addVibrancyEffect(withEffect: .systemUltraThinMaterial, bringSubViewToFrontView: self.viewMain)
        } else {
            view.addVibrancyEffect(bringSubViewToFrontView: self.viewMain)
        }
    }
}

extension RemarksListPopupViewController: UITableViewDelegate, UITableViewDataSource{
    
    fileprivate func setUpTableView() {
        tblView.register(cellType: RemarksListPopupTableViewCell.self)
        tblView.dataSource = self
        tblView.delegate = self
        tblView.estimatedRowHeight = 50
        tblView.rowHeight = UITableView.automaticDimension
        tblView.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(with: RemarksListPopupTableViewCell.self, for: indexPath)
        
        let item = arrayData[indexPath.row]
        
        cell.lblCurrentlyAssignedTo.text = item.currently_presented_to
        cell.lblRemarks.text = item.remarks.isEmptyString ? "-" : item.remarks
        cell.lblDate.text = item.date
        
        if self.type == 1 {
            cell.lblFrom.superview?.isHidden = false
            cell.lblFrom.text = item.from
            
        } else {
            cell.lblFrom.superview?.isHidden = true
            cell.lblFrom.text = ""
        }
        
//        cell.viewSeparator.isHidden = false
//        if (indexPath.row == 0) && (indexPath.row == arrayData.count - 1) {
//            cell.viewSeparator.isHidden = true
//        } else if indexPath.row == (arrayData.count - 1) {
//            cell.viewSeparator.isHidden = true
//        }
        
        return cell
    }
    
}
