//
//  RemarksListPopupTableViewCell.swift
//  LNOC
//
//  Created by Pradip on 31/12/21.
//

import UIKit

class RemarksListPopupTableViewCell: UITableViewCell {

    //MARK:- Outlets
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet var lblTitleCurrentlyAssignedTo: UILabel!
    @IBOutlet var lblTitleRemarks: UILabel!
    @IBOutlet var lblTitleDate: UILabel!
    
    @IBOutlet var lblCurrentlyAssignedTo: UILabel!
    @IBOutlet var lblRemarks: UILabel!
    @IBOutlet var lblDate: UILabel!
    
    @IBOutlet var lblTitles: [UILabel]!
    @IBOutlet var lblValues: [UILabel]!
    
    @IBOutlet var lblTitleFrom: UILabel!
    @IBOutlet weak var lblFrom: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        
        self.viewMain.setCustomCornerRadius(radius: 12)
        self.viewShadow.setCustomCornerRadius(radius: 12)
        self.viewShadow.addShadow()
        
        lblTitles.forEach({
            $0.textColor = .lightGray
            $0.font = .regularSmallValueFont
            $0.numberOfLines = 0
        })
        
        lblValues.forEach({
            $0.textColor = .customBlack
            $0.font = .boldTitleFont
            $0.numberOfLines = 0
        })
        
        lblTitleCurrentlyAssignedTo.text = "Currently Presented To"
        lblTitleRemarks.text = "Remarks"
        lblTitleDate.text = "Date"
        lblTitleFrom.text = "From"
    }
    
}
