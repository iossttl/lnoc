//
//  DashboardTabViewController.swift
//  Social Justice
//
//  Created by Vikram Jagad on 26/02/21.
//

import UIKit

class RemarksTabViewController : HeaderViewController {
    
    //MARK:- Variables
    //Public
    var tabVC: CustomTabBarController!
    var selectedIndex = 0
    //MARK:- Lifecycle Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpVC()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    private func setUpVC() {
        setUpHeader()
        setUpTab()
    }
    
    private func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.Remarks)
        setHeaderView_BackImage()
    }
    
    private func setUpTab() {
        
        var customParam = TabBarCustomParam()
        let VC1 = RemarksListPopupViewController.instantiate(appStoryboard: .remarksListPopup)
        let VC2 = RemarksListPopupViewController.instantiate(appStoryboard: .remarksListPopup)
        
//        VC2.isFromRemarks = false
        
        customParam.viewControllers = [VC1, VC2]
        customParam.tabData = [TabBarModel(title: RemarksLabels.Remarks, img: "", selectedImg: "", badgeCount: ""),
                               TabBarModel(title: RemarksLabels.QueryRejection, img: "", selectedImg: "", badgeCount: "")]
        customParam.type = .title
        customParam.showSelectionView = true
        customParam.tabColor = .customLightSkyBlue
        customParam.titleColor = .customBlack
        customParam.addShadow = true
        //customParam.imgTintColor = .customSubtitle
        customParam.selectedTitleColor = .customRed
        customParam.selectedImgTintColor = .customRed
        customParam.selectedViewColor = .white
        customParam.imgRenderingMode = .alwaysTemplate
        customParam.viewMainSpacing = 0
        customParam.shadowOffset = CGSize(width: 0, height: -1)
        customParam.tabHeight = IS_IPAD ? 80 : 60
        customParam.titleFont = .semiboldValueFont
        customParam.place = .top
        customParam.edgeInsets = UIEdgeInsets(top: 8, left: 16, bottom: 8, right: 16)
        customParam.viewSelectionCornerRadius = 10
        customParam.viewMainSpacing = 4
        customParam.cornerTo = [.allCorners]
        customParam.cornerRadius = 10
        customParam.selectionViewTopBottomSpacing = 4
        customParam.topSafeAreaSpacing = 0
        tabVC = CustomTabBarController(param: customParam, parentVC: self, toView: view)
        tabVC.changeTabIndex(to: selectedIndex)
    }
}
