//
//  PDFDocumentViewerViewController.swift
//  LNOC
//
//  Created by Pradip Patel on 30/12/21.
//
//

import UIKit
import WebKit

class PDFDocumentViewerViewController: HeaderViewController {
    
    //MARK:- Interface Builder
    //UIView
    @IBOutlet weak var viewMain: UIView!
    
    //MARK:- Variables
    //Private
    fileprivate var webView: WKWebView!
    //Public
    var documentURLStr = ""
    var localURL : URL!
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViewController()
    }
    
    //MARK:- Private Methods
    //ViewController set up
    fileprivate func setUpViewController() {
        setUpHeader()
        setUpWebView()
    }
    
    //HeaderView set up
    fileprivate func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.document)
        setHeaderView_BackImage()
        showDownload()
    }
    
    fileprivate func setUpWebView() {
        let javaScript = "var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);"
        let userScript = WKUserScript(source: javaScript, injectionTime: .atDocumentEnd, forMainFrameOnly: true)
        let userContentController = WKUserContentController()
        userContentController.addUserScript(userScript)
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.userContentController = userContentController
        if localURL != nil {
            webView = WKWebView()
        } else {
            webView = WKWebView(frame: .zero, configuration: webConfiguration)
        }
        //webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.navigationDelegate = self
        viewMain.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.leadingAnchor.constraint(equalTo: viewMain.leadingAnchor, constant: IS_IPAD ? IPAD_MARGIN : 0).isActive = true
        webView.trailingAnchor.constraint(equalTo: viewMain.trailingAnchor, constant: IS_IPAD ? -IPAD_MARGIN : 0).isActive = true
        webView.topAnchor.constraint(equalTo: viewMain.topAnchor, constant: 0).isActive = true
        webView.bottomAnchor.constraint(equalTo: viewMain.bottomAnchor, constant: 0).isActive = true
        
        if let getLocalURL = localURL {
            CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
            
            if let dataOfFile = try? Data(contentsOf: getLocalURL) {
                webView.load(dataOfFile, mimeType: "application/pdf", characterEncodingName: "", baseURL: getLocalURL)
            } else {
                CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            }
            
        } else if let url = URL(string: documentURLStr) ?? URL(string: documentURLStr.urlQueryAllowed) {
            webView.load(URLRequest(url: url))
            CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        }
    }
    
    override func btnSearchTapped(_ sender: UIButton) {
        UIView.hideKeyBoard()
        
        guard let url = webView.url else { return }
        
        sender.isUserInteractionEnabled = false
        
        DispatchQueue.main.async {
            do {
                let data = try Data(contentsOf: url)
                Global.shareTextWithCompletion(arrayItem: [data], subject: nil, VC: self, sender: sender) { act, result in
                    
                }
            } catch let error {
                print("Error converting to data or error writing :- \(error.localizedDescription)")
            }
            
            sender.isUserInteractionEnabled = true
        }
        
        
    }
}

//MARK:- WKNavigationDelegate Methods
extension PDFDocumentViewerViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        CustomActivityIndicator.sharedInstance.hideIndicator(view: view)
    }
    
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error) {
        print("Error loading url :- \(error.localizedDescription)")
        print(error)
        CustomActivityIndicator.sharedInstance.hideIndicator(view: view)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        print("Error loading url :- \(error.localizedDescription)")
        print(error)
        CustomActivityIndicator.sharedInstance.hideIndicator(view: view)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if (navigationAction.navigationType == .linkActivated) {
            decisionHandler(.allow)
        }
        decisionHandler(.allow)
    }
}
