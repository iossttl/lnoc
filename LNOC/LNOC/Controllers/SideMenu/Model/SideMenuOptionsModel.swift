//
//  SideMenuOptionsModel.swift
//
//  Created by Vikram Jagad on 25/12/20.
//

import UIKit

class SideMenuOptionsModel: NSObject {
    let title: String
    let menuImgView: String
    var moreOptions: [SideMenuOptionsModel] = []
    let id: String
    
    enum Keys: String {
        case title
        case image
        case id
        case moreOptions
    }
    
    init(dict: Dictionary) {
        title = getString(anything: dict[Keys.title.rawValue])
        menuImgView = getString(anything: dict[Keys.image.rawValue])
        id = getString(anything: dict[Keys.id.rawValue])
        moreOptions = (dict[Keys.moreOptions.rawValue] as? [Dictionary])?.map({ SideMenuOptionsModel(dict: $0) }) ?? []
    }
}
