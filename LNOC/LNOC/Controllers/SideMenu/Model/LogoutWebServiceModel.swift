//
//  LoginServiceModel.swift
//  ASIDEMO
//
//  Created by Dhruv Patel on 13/01/21.
//

import UIKit

class LogoutServiceModel: NSObject {
    
    fileprivate var getLogout:String {
         return BASEURL + API.logout
     }
    
    enum keys:String {
        case token = "token"
    }

    fileprivate var LogoutDict: [String : Any] {
        let dict: Dictionary = ["token": UserPreferences.string(forKey: UserPreferencesKeys.General.token)]
        return dict
    }
    
    //MARK:- Initializer
    override init() {
        super.init()
    }
     
     func getLogout(block:@escaping ((Dictionary?) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            var param = WShandler.commonDict()
//            let dict = LogoutDict
//            for key in dict.keys {
//                param[key] = dict[key]
//            }
            
            WShandler.shared.postWebRequest(urlStr: getLogout, param: param) { (json, flag) in
                var responseModel:Dictionary?
                if (flag == 200) {
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        responseModel = json
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel)
            }
        } else {
            block(nil)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
}
