//
//  SideMenuViewController.swift
//
//  Created by Vikram Jagad on 25/12/20.
//

import UIKit
//import UnityFramework

enum SideMenuIds: String {
    case registerGrievance
    case trackGrievance
    case aboutUs
    case contactUs
    case loginLogout
}

class SideMenuViewController: UIViewController {
    
    //MARK:- Interface Builder
    //UIView
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var viewLbls: UIView!
    @IBOutlet weak var viewTop: GradientView!
    @IBOutlet weak var bottomSettingsGradientView: GradientView!
    @IBOutlet weak var viewBottomBg: UIView!
    @IBOutlet weak var viewSeparator: UIView!
    
    @IBOutlet weak var viewImg: UIView!
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var imgLoginLogOut: UIImageView!
    
    //UILabel
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var lblLogInLogOut: UILabel!
//    @IBOutlet weak var lblMyDashboard: UILabel!
    
    @IBOutlet weak var lblSettings: UILabel!
    
    //UIButton
    @IBOutlet weak var btnProfile: UIButton!
//    @IBOutlet weak var btnMyDashboard: UIButton!
    @IBOutlet weak var btnClose: UIButton!
    
    //IBOutlet Tableview
    @IBOutlet weak var sideMenuTblView: UITableView!
    
    @IBOutlet var labelTop: [UILabel]!
    
    
    //MARK:- Variables
    //Local
    fileprivate var selectedSection: IndexPath?
    fileprivate var sideMenuTblViewDelegateDataSource: SideMenuTblViewDelegateDataSource!
    //Public
    var menuDataSourceArr : [SideMenuOptionsModel] = []
    var logoutWebServicesModel = LogoutServiceModel()
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpVC()
        //setupData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupData()
        setUpTblView()
    }
    
    //MARK:- Private Methods
    //ViewController set up
    fileprivate func setUpVC() {
        //selectedSection = IndexPath(row: 0, section: 0)
        setUpViews()
        setUpIPadConstraints()
        setUpLbls()
        setUpTblView()
    }
    
    fileprivate func setUpViews() {
        viewTop.addGradient(direction: .leftRight)
        bottomSettingsGradientView.addGradient(direction: .leftRight)
    }
    
    fileprivate func setUpIPadConstraints() {
        changeHeightForiPad(view: viewBottomBg)
        
        self.viewImg.layer.cornerRadius = IS_iPad ? 75 : 45
        self.imgProfile.layer.cornerRadius = IS_iPad ? 70 : 40
    }
    
    fileprivate func setUpLbls() {
        lblName.font = .boldMediumFont
        lblName.textColor = .customWhite
        
        lblEmail.font = .regularSmallFont
        lblEmail.textColor = .customWhite
        
        lblVersion.font = .regularSmallFont
        lblVersion.textColor = .customWhite
        
        lblLogInLogOut.font = UIFont.regularTitleFont
        lblLogInLogOut.textColor = .customWhite
        
        lblSettings.font = UIFont.regularTitleFont
        lblSettings.textColor = .customWhite
        
        
//        lblMyDashboard.text = "My Dashboard"
//        lblMyDashboard.textColor = .white
//        lblMyDashboard.font = .semiboldSmallFont
//
//        lblMyDashboard.superview?.backgroundColor = .clear
//        lblMyDashboard.superview?.setFullCornerRadius(type: .automatic, borderColor: .white, borderWidth: 1.0)
    }
    
    
    
    fileprivate func setupData() {
        if getBoolean(anything: UserPreferences.bool(forKey: UserPreferencesKeys.General.isUserLoggedIn)) {
            self.viewProfile.isHidden = false
            self.lblLogInLogOut.text = CommonLabels.logout
            self.setUpTopView()
            
        } else {
            self.viewProfile.isHidden = true
            self.lblLogInLogOut.text = CommonLabels.login
//            lblName.text = SideMenuLabels.guest
//            self.lblEmail.text = ""
        }
        
        self.lblSettings.text = CommonLabels.settings
    }
    
    fileprivate func setUpTopView() {
        lblVersion.text = SideMenuLabels.welcome
        lblName.text = UserPreferences.string(forKey: UserPreferencesKeys.General.first_name).decryptStr + " " + UserPreferences.string(forKey: UserPreferencesKeys.General.last_name).decryptStr
        lblEmail.text = UserPreferences.string(forKey: UserPreferencesKeys.General.email).decryptStr
        self.imgProfile.downloadImage(with: UserModel.currentUser.imageUrl.decryptStr, placeholderImage: .ic_user_placeholder, options: .continueInBackground, progess: nil, completed: nil)
        lblEmail.isHidden = false
        lblName.isHidden = false
        lblVersion.isHidden = false
        viewProfile.isHidden = false
        viewSeparator.isHidden = false
        

       /* if !(UserModel.signedIn) {
//            lblName.text = SideMenuLabels.guest
//            lblEmail.isHidden = true
            viewProfile.isHidden = true
            viewSeparator.isHidden = true
        } else {
            viewProfile.isHidden = false
            viewSeparator.isHidden = false
//            let currentUser = UserModel.currentUser
//            lblName.text = currentUser.first_name.capitalized + " " + currentUser.last_name.capitalized
//            lblEmail.text = currentUser.email
            lblEmail.isHidden = false
        }
        
        self.labelTop.forEach({
            $0.textAlignment = isRightToLeftLocalization ? .right : .left
        })*/
    }
    
    fileprivate func setUpTblView() {
        if let arr = Global.shared.readPropertyList(ofName: "SideMenuData") as? [Dictionary] {
            self.menuDataSourceArr = arr.map({ SideMenuOptionsModel(dict: $0) })
        #if LNOC
            self.menuDataSourceArr = self.menuDataSourceArr.filter({ $0.id != "Search_Lands" })
            self.menuDataSourceArr = self.menuDataSourceArr.filter({ $0.id != "Land_Favourites" })
            self.menuDataSourceArr = self.menuDataSourceArr.filter({ $0.id != "Land_Bank" })
        #endif
        }
        
//        if !Constants.isDisplayMenu{
//            self.menuDataSourceArr = self.menuDataSourceArr.filter({$0.title != "Donate Now"})
//        }
        if (sideMenuTblViewDelegateDataSource == nil) {
            sideMenuTblViewDelegateDataSource = SideMenuTblViewDelegateDataSource(arrData: menuDataSourceArr, delegate: self, scrl: sideMenuTblView)
        } else {
            sideMenuTblViewDelegateDataSource.reloadTblView(arrData: menuDataSourceArr, selectedSection: selectedSection)
        }
    }
    
    //MARK:- IBActions Method
    @IBAction func btnCloseClickHandler(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func btnProfileTapped(_ sender: UIButton) {
        let dvc = ProfileViewController.instantiate(appStoryboard: .Profile)
        self.navigationController?.pushViewController(dvc, animated: true)
       /* if getBoolean(anything: UserPreferences.bool(forKey: UserPreferencesKeys.General.isUserLoggedIn)) {
            
            let dvc = ProfileViewController.instantiate(appStoryboard: .Profile)
            self.navigationController?.pushViewController(dvc, animated: true)
            
        } else {
            dismiss(animated: true, completion: nil)
        }*/
    }
    
    
    @IBAction func btnSettingsTapped(_ sender: UIButton) {
        let dvc = SettingViewController.instantiate(appStoryboard: .settings)
        self.navigationController?.pushViewController(dvc, animated: true)

    }
    
    @IBAction func btnMyDashboardTapped(_ sender: UIButton) {
        print("tapped")
    }
    
    @IBAction func btnLoginLogoutTapped(_ sender: UIButton) {
        /*UserPreferences.set(value: false, forKey: UserPreferencesKeys.General.isUserLoggedIn)
        UserPreferences.set(value: "", forKey: UserPreferencesKeys.General.name)
        self.navigationController?.popViewController(animated: false)
        let dvc = LoginViewController.instantiate(appStoryboard: .Login)
        self.navigationController?.pushViewController(dvc, animated: true)*/
        
        if getBoolean(anything: UserPreferences.bool(forKey: UserPreferencesKeys.General.isUserLoggedIn)) {
            self.dismiss(animated: false)
            UIAlertController.showWith(title: CommonLabels.alert, msg: SideMenuMessages.suretoLogout, onVC: Global.shared.delegateRootVC, actionTitles: [CommonButtons.no, CommonButtons.yes], actionStyles: [.destructive, .default], actionHandlers: [nil, { (yesAction) in
                self.getLogout()
            }])
        } else {
            self.dismiss(animated: false) {
                UserPreferences.remove(forKey: UserPreferencesKeys.General.isSkip)
                Global.shared.changeInitialVC(animated: true, direction: .toLeft)
            }
        }
    }
}

//MARK:- TblViewDelegate Methods
extension SideMenuViewController: TblViewDelegate {
    func didSelect(tbl: UITableView, indexPath: IndexPath) {
        let item = self.menuDataSourceArr[indexPath.section]
        
        if indexPath.row == 0 {
            if item.moreOptions.count > 0 {
                if let aSelectedIndex = selectedSection, aSelectedIndex == indexPath {
                    selectedSection = nil
                } else {
                    selectedSection = indexPath
                }
                
                setUpTblView()
                return
            }
        }
        
        var itemType = item.title
        
        if item.moreOptions.count > 0 {
            itemType = getString(anything: item.moreOptions[indexPath.row - 1].title)
        }
        
        switch SideMenuSelectionType(rawValue: itemType) {
        case .User_Dashboard:
            let dvc = DashboardViewController.instantiate(appStoryboard: .dashboard)
            self.navigationController?.pushViewController(dvc, animated: true)
    
        case .Notifications:
            let vc = NotificationsListViewController.instantiate(appStoryboard: .NotificationsList)
            self.navigationController?.pushViewController(vc, animated: true)
        
        case .Search_Lands:
            let dvc = LandListViewController.instantiate(appStoryboard: .LandList)
            dvc.landListType = .landSearch
            self.navigationController?.pushViewController(dvc, animated: true)
            
        case .Land_Favourites:
            let dvc = LandListViewController.instantiate(appStoryboard: .LandList)
            dvc.landListType = .landFavourite
            self.navigationController?.pushViewController(dvc, animated: true)
            
        case .Land_Bank:
            let dvc = LandListViewController.instantiate(appStoryboard: .LandList)
            dvc.landListType = .landBank
            self.navigationController?.pushViewController(dvc, animated: true)
            
//            let dvc = NoticeFilterViewController.instantiate(appStoryboard: .noticeFilter)
//            self.navigationController?.pushViewController(dvc, animated: true)
//            
        case .NOC_Applications:
            let dvc = NOCAppListViewController.instantiate(appStoryboard: .NOCAppList)
            self.navigationController?.pushViewController(dvc, animated: true)
            
        case .Add_New_Application:
            let dvc = RegisterContainerViewController.instantiate(appStoryboard: .RegisterContainer)
            self.navigationController?.pushViewController(dvc, animated: true)
            
        case .Change_Password:
            let dvc = DashboardViewController.instantiate(appStoryboard: .dashboard)
            self.navigationController?.pushViewController(dvc, animated: true)
            
        case .addLandDetail:
            let dvc = AddLandDetailViewController.instantiate(appStoryboard: .addLandDetail)
            self.navigationController?.pushViewController(dvc, animated: true)
            
        case .feedback:
            let dvc = FeedbackViewController.instantiate(appStoryboard: .feedback)
            self.navigationController?.pushViewController(dvc, animated: true)
         
        case .aboutUs:
            let dvc = WebViewViewController.instantiate(appStoryboard: .webView)
            dvc.typeOfWebView = .aboutUS
            self.navigationController?.pushViewController(dvc, animated: true)
            
        case .FAQ:
            let dvc = WebViewViewController.instantiate(appStoryboard: .webView)
            dvc.typeOfWebView = .FAQ
            self.navigationController?.pushViewController(dvc, animated: true)
            
        case .contatUs:
            let dvc = WebViewViewController.instantiate(appStoryboard: .webView)
            dvc.typeOfWebView = .contactUs
            self.navigationController?.pushViewController(dvc, animated: true)
            
        default:
            break
        }
    }
}

//MARK:- Webservice Functions
extension SideMenuViewController {
    func getLogout() {
        let vc = (Global.shared.delegateRootVC) ?? self
        CustomActivityIndicator.sharedInstance.showIndicator(view: vc.view)
        logoutWebServicesModel.getLogout { (json) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: vc.view)
            if let response = json {
                Global.shared.showBanner(message: getString(anything:response[CommonAPIConstant.key_message]).decryptStr)
                
                UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                Global.shared.changeInitialVC(animated: true, direction: .toLeft)
            }
        }
    }
}





