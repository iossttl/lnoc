//
//  SideMenuTblViewDelegateDataSource.swift
//
//  Created by Vikram Jagad on 25/12/20.
//

import UIKit

class SideMenuTblViewDelegateDataSource: NSObject, CustomProtocolForDelegateDataSource {
    //MARK:- Types
    typealias T = SideMenuOptionsModel
    typealias ScrlView = UITableView
    typealias Delegate = TblViewDelegate
    
    //MARK:- Variables
    internal var arrSource: [SideMenuOptionsModel]
    internal var scrlView: UITableView
    internal var delegate: TblViewDelegate
    private var selectedSection: IndexPath?
    private let topBottomInsets: CGFloat = 8
    
    //MARK:- Initializers
    required init(arrData: [SideMenuOptionsModel], delegate: TblViewDelegate, scrl: UITableView) {
        arrSource = arrData
        scrlView = scrl
        self.delegate = delegate
        super.init()
        setUpScrlView()
    }
    
    //MARK:- Private Methods
    //Object set up
    internal func setUpScrlView() {
        scrlView.delegate = self
        scrlView.dataSource = self
        registerCell()
    }
    
    //Register cells
    internal func registerCell() {
        let cellTypes: [UITableViewCell.Type] = [SideMenuOptionsCell.self, SideMenuSubOptionsCell.self]
        scrlView.register(cellTypes: cellTypes)
        scrlView.contentInset = UIEdgeInsets(top: topBottomInsets, left: 0, bottom: topBottomInsets, right: 0)
    }
    
    //MARK:- Public Methods
    func reloadScrlView(arr: [SideMenuOptionsModel]) {
        arrSource = arr
        scrlView.reloadData()
    }
    
    func reloadTblView(arrData: [SideMenuOptionsModel], selectedSection: IndexPath?) {
        arrSource = arrData
        self.selectedSection = selectedSection
        scrlView.reloadData()
    }
}

//MARK:- UITableViewDelegate Methods
extension SideMenuTblViewDelegateDataSource: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate.didSelect?(tbl: tableView, indexPath: indexPath)
    }
}

//MARK:- UITableViewDataSource Methods
extension SideMenuTblViewDelegateDataSource: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrSource.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let aMenuItems = arrSource[section]
        if let expandableArrCount = selectedSection,expandableArrCount.section == section {
            if aMenuItems.moreOptions.count > 0 {
                return aMenuItems.moreOptions.count + 1
            }
        }
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let aMenuOptionData = arrSource[indexPath.section]
        if (indexPath.row == 0) {
            let aMenuItenCell = tableView.dequeueReusableCell(with: SideMenuOptionsCell.self, for: indexPath)
            if let aSelectedIndex = selectedSection,indexPath == aSelectedIndex {
                aMenuItenCell.configureCell(menuOptionData: aMenuOptionData, isSelectedCell: true)
            } else {
                aMenuItenCell.configureCell(menuOptionData: aMenuOptionData, isSelectedCell: false)
            }
            return aMenuItenCell
        } else {
            let aSubMenuCell = tableView.dequeueReusableCell(with: SideMenuSubOptionsCell.self, for: indexPath)
            aSubMenuCell.configureCell(aMenuOptionData: aMenuOptionData, indexPath: indexPath)
            return aSubMenuCell
        }
    }
}
