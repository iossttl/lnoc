//
//  SideMenuSubOptionsTableViewController.swift
//
//  Created by Vikram Jagad on 25/12/20.
//

import UIKit

class SideMenuSubOptionsCell: UITableViewCell {
    //MARK:- Interface Outlets
    //UILabel
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK:- Awake From Nib
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCell()
    }
    
    fileprivate func setUpCell() {
        setUpLbl()
    }
    
    fileprivate func setUpLbl() {
        lblTitle.font = .regularValueFont
        lblTitle.textColor = .customBlack
    }

    //MARK:- Configure Cell
    func configureCell(aMenuOptionData : SideMenuOptionsModel, indexPath : IndexPath) {
        if (aMenuOptionData.moreOptions.count > 0) {
            lblTitle.text = getString(anything: aMenuOptionData.moreOptions[indexPath.row-1].title).localizedString
            let title = getString(anything: aMenuOptionData.moreOptions[indexPath.row-1].title)
            print(title.lowercased(),"====>>>>",title)
        }
    }
}
