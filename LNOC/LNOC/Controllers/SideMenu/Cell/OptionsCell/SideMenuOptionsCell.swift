//
//  SideMenuOptionsTableViewCell.swift
//
//  Created by Vikram Jagad on 25/12/20.
//

import UIKit

class SideMenuOptionsCell: UITableViewCell
{
    //MARK:- Interface Builder
    //UIImageView
    @IBOutlet weak var menuImgView: UIImageView!
    @IBOutlet weak var imgViewPlusMinus: UIImageView!
    
    //UILabel
    @IBOutlet weak var lblMenuTitle: UILabel!
    
    //UIView
    @IBOutlet weak var viewSeperator: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCell()
    }
    
    //MARK:- Private Methods
    fileprivate func setUpCell() {
        viewSeperator.backgroundColor = .customSeparator
        setUpIPadConstraints()
        setUpLbl()
    }
    
    fileprivate func setUpIPadConstraints() {
        changeHeightForiPad(view: menuImgView)
    }
    
    fileprivate func setUpLbl() {
        lblMenuTitle.font = .regularValueFont
        lblMenuTitle.textColor = .customBlack
    }
    
    //MARK:- Public Methods
    func configureCell(menuOptionData: SideMenuOptionsModel, isSelectedCell : Bool) {
        lblMenuTitle.text = menuOptionData.title.localizedString
        if (menuOptionData.id == SideMenuIds.loginLogout.rawValue) {
//            if (UserModel.signedIn) {
//                lblMenuTitle.text = SideMenuLabels.logout
//            } else {
                lblMenuTitle.text = SideMenuLabels.login
//            }
        }
        menuImgView.image = UIImage(named: menuOptionData.menuImgView)?.withRenderingMode(.alwaysTemplate)
        menuImgView.tintColor = .customgreenTheamClr
        if (menuOptionData.id == SideMenuIds.loginLogout.rawValue) {
//            if (UserModel.signedIn) {
//                menuImgView.image = .ic_logout
//            } else {
                menuImgView.image = .ic_login
//            }
        }
        if menuOptionData.moreOptions.count > 0 {
            imgViewPlusMinus.isHidden = false
            imgViewPlusMinus.image = .ic_plus
            imgViewPlusMinus.tintColor = .customBlack
            if !isSelectedCell {
                imgViewPlusMinus.image = .ic_plus
            } else {
                imgViewPlusMinus.image = .ic_minus
            }
        } else {
            imgViewPlusMinus.isHidden = true
            if !isSelectedCell {
                contentView.backgroundColor = .customWhite
            } else {
                contentView.backgroundColor = .customLightSkyBlue
            }
        }
    }
}
