//
//  NotificationsTableViewCell.swift
//  LNOC
//
//  Created by STTL Mac on 03/01/22.
//

import UIKit

class NotificationsTableViewCell: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewShadow: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.lblTitle.font = .regularValueFont
        self.lblDate.font = .regularTitleFont
        self.lblDate.textColor = .themeDarkOrangeClr
        
        self.viewMain.setCustomCornerRadius(radius: 12)
        self.viewShadow.setCustomCornerRadius(radius: 12)
        self.viewShadow.addShadow()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
