//
//  NotificationsListViewController.swift
//  LNOC
//
//  Created by STTL Mac on 03/01/22.
//

import UIKit

class NotificationsListViewController: HeaderViewController {

    @IBOutlet weak var lblNoDataFound: LocalizedLabel!
    @IBOutlet weak var tblList: UITableView!
    
    var responseList = [NotificationModel]()
    let webServiceModel = NotificationListWebServiceModel()
    var totalCount = 0
    var pageNo = 1
    var isDataLoadingOn = false
    
    //Local
    fileprivate var refreshControl = UIRefreshControl()
    fileprivate var isPullToRefresh = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        changeLeadingTrailingForiPad(view: self.view)
        self.lblNoDataFound.font = .regularTitleFont
        self.tblList.delegate = self
        self.tblList.dataSource = self
        self.setHeaderView_BackImage()
        self.setUpHeaderTitle(strHeaderTitle: Headers.NotificationsList)
        self.setUpRefreshControl()
        self.getNotifications()
    }
    
    override func btnBackTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    fileprivate func setUpRefreshControl() {
        self.tblList.addRefresh(control: refreshControl, target: self, action: #selector(handleRefreshControl))
        refreshControl.attributedTitle = NSAttributedString(string: CommonLabels.pull_to_refresh, attributes: [NSAttributedString.Key.font : UIFont.boldValueFont, NSAttributedString.Key.foregroundColor : UIColor.customSubtitle])
    }
    
    @objc fileprivate func handleRefreshControl(_ control: UIRefreshControl) {
        self.pageNo = 1
        totalCount = 0
        self.isPullToRefresh = true
        self.getNotifications()
    }
    
    func getNotifications() {
        
        self.isDataLoadingOn = true
        self.webServiceModel.page = self.pageNo

        if !(isPullToRefresh) {
            CustomActivityIndicator.sharedInstance.showIndicator(view: view)
        }

        self.webServiceModel.GetNotificationsListApi(block: { list, isSucess, totalRecords in

            if (self.isPullToRefresh) {
                self.isPullToRefresh = false
                self.refreshControl.endRefreshing()
            } else {
                CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            }
            
            self.isDataLoadingOn = false

            if isSucess {
                self.totalCount = totalRecords
                if (self.pageNo == 1) {
                    self.responseList = list
                } else {
                    self.responseList += list
                }
                self.tblList.reloadData()

                if self.responseList.count == 0 {
                    self.lblNoDataFound.isHidden = false
                    self.tblList.isHidden = true
                } else {
                    self.tblList.isHidden = false
                    self.lblNoDataFound.isHidden = true
                }
            }
            
            if (self.isPullToRefresh) {
                self.isPullToRefresh = false
                self.refreshControl.endRefreshing()
            } else {
                CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            }
            
        })
    }
}

//MARK:- ------ UITableView DataSource, Delegate Methods -----------
extension NotificationsListViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.responseList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell : NotificationsTableViewCell?
        if cell == nil {
            cell = Bundle.main.loadNibNamed("NotificationsTableViewCell", owner: nil, options: nil)?.first as? NotificationsTableViewCell
            cell?.lblTitle.text = self.responseList[indexPath.row].notify_text
            cell?.lblDate.text = self.responseList[indexPath.row].date
        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.isDataLoadingOn == false && (indexPath.row == responseList.count - 5) && (responseList.count < totalCount) {
            self.pageNo += 1
            self.getNotifications()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.responseList[indexPath.row].notify_type == "2" {
            let dvc = ApplicationDetailsViewController.instantiate(appStoryboard: .applicationDetails)
            dvc.noc_id = self.responseList[indexPath.row].noc_id
    //        dvc.app_no = self.responseList[indexPath.row].app_no
            self.navigationController?.pushViewController(dvc, animated: true)

        } else {
            let dvc = LandDetailsViewControllerMultiple.instantiate(appStoryboard: .LandDetailsMultiple)
            dvc.land_id = self.responseList[indexPath.row].noc_id
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
}
