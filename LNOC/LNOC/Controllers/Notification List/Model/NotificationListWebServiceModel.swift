//
//  NotificationListWebServiceModel.swift
//  LNOC
//
//  Created by STTL Mac on 03/01/22.
//

import Foundation

class NotificationListWebServiceModel : NSObject {
    
    var page = 1
    
    fileprivate var getNotificationUrl :String {
         return BASEURL + API.notifications
     }
    
    //MARK:- Initializer
    override init() {
        super.init()
    }
    
    func GetNotificationsListApi(block:@escaping (([NotificationModel], _ isSucess : Bool, _ totalRecords : Int) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {

            var param = WShandler.commonDict()
            param["page"] = self.page
            param["user_id"] = UserPreferences.string(forKey: UserPreferencesKeys.UserInfo.userID)

            print(param)
            
            WShandler.shared.postWebRequest(urlStr: self.getNotificationUrl, param: param) { (json, flag) in
                if flag == 200 {

                    //Invalid Token
                    if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                        
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                        UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                        Global.shared.changeInitialVC(animated: true, direction: .toLeft)
                        block([], false, 0)
                        
                    } else if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                        let responseModel = (json["notifications"] as? [Dictionary] ?? []).map({ NotificationModel(dict: $0) })
                        let totallist = getInteger(anything: getString(anything: json[CommonAPIConstant.key_totalRecords]).decryptStr)
                        block(responseModel, true, totallist)

                    } else {
                        block([], false, 0)
                        Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                    }
                } else {
                    block([], false, 0)
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
            }
        } else {
            block([], false, 0)
           Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
}
