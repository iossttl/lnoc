//
//  NofificationModel.swift
//  LNOC
//
//  Created by STTL Mac on 03/01/22.
//

import Foundation

class NotificationModel : NSObject {
    
    let date: String
    let id: String
    let notify_text: String
    let showview : String
    let noc_id: String
    let notify_type : String
    
    enum Keys : String {
        case date = "date"
        case id = "id"
        case notify_text = "notify_text"
        case showview = "showview"
        case noc_id = "noc_id"
        case notify_type = "notify_type"
    }
    
    init(dict: JSONObject) {
        date = getString(anything: dict[Keys.date.rawValue]).decryptStr
        id = getString(anything: dict[Keys.id.rawValue]).decryptStr
        notify_text = getString(anything: dict[Keys.notify_text.rawValue]).decryptStr.trimming_WS
        showview = getString(anything: dict[Keys.showview.rawValue]).decryptStr
        noc_id = getString(anything: dict[Keys.noc_id.rawValue]).decryptStr
        notify_type = getString(anything: dict[Keys.notify_type.rawValue]).decryptStr
    }
}


