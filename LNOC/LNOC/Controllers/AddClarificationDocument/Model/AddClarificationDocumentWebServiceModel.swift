//
//  AddClarificationDocumentWebServiceModel.swift
//  LNOC
//
//  Created by Pradip on 03/01/22.
//

import UIKit

struct ClarificationDocuments {
    var documentModel: DocumentModel
    var remarkString: String
}

class AddClarificationDocumentWebServiceModel : NSObject {
    
    var application_id : String = ""
    var clarificationDocuments: [ClarificationDocuments] = []
    
    enum Keys : String {
        case application_id = "application_id"
        case docs = "docs"
        case remarks = "remarks"
        case details = "details"
    }
    
    var addClarificationUrl : String {
        return BASEURL + API.add_clearification
    }
    
    private func getParameters() -> Dictionary {
        
        let dicArray = clarificationDocuments.map({ [Keys.remarks.rawValue : $0.remarkString.encryptStr] })
        
        return [Keys.application_id.rawValue : self.application_id.encryptStr,
                Keys.details.rawValue : dicArray]
    }
    
    func addClarificationDocumentApiCalling(invalidTokenMsg : @escaping ((String) -> Swift.Void), block:@escaping ((Bool?) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            
            var param = WShandler.commonDict()
            let dict = getParameters()
            for key in dict.keys {
                param[key] = dict[key]
            }
            
            clarificationDocuments.enumerated().forEach({
                $0.element.documentModel.key = Keys.docs.rawValue + "[\($0.offset)]"
            })
            
            let documentModels = clarificationDocuments.map({ $0.documentModel })
            
           WShandler.shared.multipartWebRequest(urlStr: addClarificationUrl, dictParams: param, documents: documentModels) { (json, flag) in
               var responseModel:Bool?
               if flag == 200 {
                  
                  //Invalid Token
                  if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                      invalidTokenMsg(getString(anything:json[CommonAPIConstant.key_message]).decryptStr)

                  } else if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                      Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                      responseModel = true
                      
                   } else {
                       Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                   }
               } else {
                   Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
               }
               block(responseModel)
           }
       } else {
           block(nil)
           Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
       }
       
       
    }
    
}
