//
//  AddClarificationDocumentTableViewCell.swift
//  LNOC
//
//  Created by Pradip on 03/01/22.
//

import UIKit

class AddClarificationDocumentTableViewCell: UITableViewCell {

    //MARK: - Outlets
    
    @IBOutlet var viewShadow: UIView!
    @IBOutlet var viewCoverDocument: UIView!
    
    @IBOutlet var lblRemarkTitle: LocalizedLabel!
    @IBOutlet var lblAsterisk: UILabel!
    @IBOutlet var lblPlaceHolder: UILabel!
    
    @IBOutlet var txtViewRemark: UITextView!
    
    @IBOutlet var btnDelete: UIButton!
    
    var docmentView : DocumentView2!
    var btnTextViewTextChanged : ((String, Int) -> Void)?
    
    //MARK: - Override Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        setupUp()
    }
    
    private func setupUp() {
        
        changeHeightForiPad(view: self.btnDelete, constant: 10)
        
        self.lblRemarkTitle.font = .boldTitleFont
        self.lblRemarkTitle.textColor = .customBlue
        self.lblRemarkTitle.numberOfLines = 0
        
        self.lblAsterisk.font = .boldTitleFont
        self.lblAsterisk.textColor = .customRed
        
        self.txtViewRemark.textContainerInset = .zero
        self.txtViewRemark.textContainer.lineFragmentPadding = 0
        
        self.txtViewRemark.textColor = .customBlack
        self.txtViewRemark.delegate = self
        self.txtViewRemark.font = .regularValueFont
        
        self.lblPlaceHolder.font = .regularValueFont
        self.lblPlaceHolder.textColor = .customGray
        self.lblPlaceHolder.text = AddClarificationDocumentLabels.enterRemark

        self.btnDelete.setImage(.ic_header_delete, for: .normal)
        self.btnDelete.tintColor = .customRed
        self.btnDelete.imageEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        self.btnDelete.contentEdgeInsets = .init(top: 5, left: IS_IPAD ? 45 : 35, bottom: 5, right: 5)
     
        self.viewShadow.addShadow(shadowRadius: 6)
        self.viewShadow.subviews.first?.viewWith(radius: 12)
        self.viewShadow.backgroundColor = .clear
        self.viewShadow.subviews.first?.backgroundColor = .white
        
    }
    
    func configurationData(item: ClarificationDocuments, tag: Int) {
        viewCoverDocument.subviews.forEach({ $0.removeFromSuperview() })
        self.docmentView = DocumentView2.loadNib()
        viewCoverDocument.addSubview(self.docmentView)
        self.docmentView.fullFillOnSuperView()
        
        self.docmentView.lblDocumentType.font = .boldTitleFont
        self.docmentView.lblDocumentType.textColor = .customBlue
        self.docmentView.lblDocumentType.font = .boldTitleFont
        self.docmentView.lblStar.font = .boldTitleFont
        
        self.docmentView.imgViewAttachment.tintColor = .customBlue
        self.docmentView.imgViewAttachment.image = UIImage(named: k_ic_attach)?.withRenderingMode(.alwaysTemplate)
        
        self.docmentView.configureTitle(str: AddClarificationDocumentLabels.clarificationDocument,
                                        btnTag: tag,
                                        showFormatBtn: false,
                                        hideAsterisk: false,
                                        isFileNameShow: true)
        
        if item.documentModel.url != nil {
            self.docmentView.setFileName(fileName: item.documentModel.title, showBtnUpload: false)
            self.docmentView.imgViewDocument.image = .correct
            self.docmentView.imgViewDocument.tintColor = .themeColor
        }
        
        self.txtViewRemark.text = item.remarkString
        self.txtViewRemark.tag = tag
        self.lblPlaceHolder.isHidden = !item.remarkString.isEmptyString
    }
    
}

extension AddClarificationDocumentTableViewCell: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let fullString = (textView.text ?? "").nsString.replacingCharacters(in: range, with: text)
        
        lblPlaceHolder.isHidden = !fullString.isEmptyString
        
        self.btnTextViewTextChanged?(fullString, textView.tag)
        
        return true
    }
    
}

