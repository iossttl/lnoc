//
//  AddClarificationDocumentViewController.swift
//  LNOC
//
//  Created by Pradip on 03/01/22.
//

import UIKit

class AddClarificationDocumentViewController : HeaderViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewBtnSubmitGradient: GradientView!
    @IBOutlet weak var viewAddClarification: UIView!
    
    @IBOutlet weak var imgPlus: UIImageView!
    
    @IBOutlet weak var lblAddClarificationDocument: UILabel!
    
    @IBOutlet weak var tblView: UITableView!
    
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnAddClarificationDocument: UIButton!
    
    
    // Varibles
    fileprivate var dataSourceDelegate: AddClarificationDocumentDataSourceDelegate!
    fileprivate let webService = AddClarificationDocumentWebServiceModel()
    
    // Public Varibles
    var application_id : String = ""
    
    //MARK: - Override Model
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpIPadConstraints()
        self.setupHeaderView()
        self.setUpView()
        self.setupTableView()
    }
    
    private func setUpIPadConstraints() {
        changeLeadingTrailingForiPad(view: self.view)
        changeHeightForiPad(view: self.btnSubmit)
        changeHeightForiPad(view: self.imgPlus, constant: 10)
    }
    
    private func setupHeaderView() {
        self.setHeaderView_BackImage()
        self.setUpHeaderTitle(strHeaderTitle: Headers.addClarificationDocument)
    }
    
    private func setUpView() {
        self.view.backgroundColor = .lnocBackgroundColor
        
        self.tblView.setCustomCornerRadius(radius: 12)
        self.tblView.backgroundColor = .clear
        
        self.imgPlus.image = UIImage.ic_plus.withRenderingMode(.alwaysTemplate)
        self.imgPlus.tintColor = .flamingoApproxColor
        
        self.lblAddClarificationDocument.text = Headers.addClarificationDocument
        self.lblAddClarificationDocument.font = .regularValueFont
        self.lblAddClarificationDocument.textColor = .flamingoApproxColor
        
        self.btnSubmit.setTitle(CommonButtons.btn_submit, for: .normal)
        self.btnSubmit.setTitleColor(.white, for: .normal)
        self.btnSubmit.titleLabel?.font = UIFont.semiboldLargeFont
        
        self.view.layoutIfNeeded()
        
        self.viewBtnSubmitGradient.setFullCornerRadius()
        self.viewBtnSubmitGradient.addGradient(ofColors: [UIColor.themeDarkOrangeClr.cgColor, UIColor.themeLightOrangeClr.cgColor], direction: .rightLeft)
        
        self.tblView.tableFooterView = self.viewAddClarification
        self.viewAddClarification.subviews.first?.setFullCornerRadius(borderColor: .flamingoApproxColor, borderWidth: 1)
        
        webService.clarificationDocuments = []
        webService.clarificationDocuments.append(ClarificationDocuments(documentModel: DocumentModel(), remarkString: ""))
    }
    
    fileprivate func setupTableView() {
        if dataSourceDelegate == nil {
            dataSourceDelegate = AddClarificationDocumentDataSourceDelegate(list: webService.clarificationDocuments, tbl: self.tblView, _delegate: nil)
            
            dataSourceDelegate.btnDeleteClickedCallBack = btnDeleteClickedCallBack
            dataSourceDelegate.btnTextViewTextChanged = btnTextViewTextChanged
            dataSourceDelegate.btnDocumentChanged = btnDocumentChanged
            
        } else {
            dataSourceDelegate.reloadData(list: webService.clarificationDocuments)
        }
        
        self.tblView.tableFooterView = self.viewAddClarification
        self.tblView.dynamicHeightTableFooterViewCalling {
            self.viewAddClarification.subviews.first?.setFullCornerRadius(borderColor: .flamingoApproxColor, borderWidth: 1)
        }
    }
    
    //MARK: - Button Action Method
    
    private func btnDeleteClickedCallBack(index: Int) {
        self.webService.clarificationDocuments.remove(at: index)
        self.setupTableView()
    }
    
    private func btnTextViewTextChanged(remarkString: String, index: Int) {
        self.webService.clarificationDocuments[index].remarkString = remarkString
        self.dataSourceDelegate.itemList = self.webService.clarificationDocuments
    }
    
    private func btnDocumentChanged(model: DocumentModel, index: Int) {
        self.webService.clarificationDocuments[index].documentModel = model
        self.dataSourceDelegate.itemList = self.webService.clarificationDocuments
    }
    
    @IBAction func btnAddClarificationDocumentClicked(_ sender: UIButton) {
     
        webService.clarificationDocuments.append(ClarificationDocuments(documentModel: DocumentModel(), remarkString: ""))
        setupTableView()
    }
    
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        var isValidate = true
        var index: Int = 0
        
        for model in self.webService.clarificationDocuments.enumerated() {
            if model.element.documentModel.url == nil {
                index = model.offset
                isValidate = false
                Global.shared.showBanner(message: AddClarificationDocumentMessages.clarificationDocumentIsRequired)
                break
            }
            
            if model.element.remarkString.isEmptyString {
                index = model.offset
                isValidate = false
                Global.shared.showBanner(message: AddClarificationDocumentMessages.clarificationRemarksIsRequired)
                break
            }
        }
        
        if !isValidate {
            let indexPath = IndexPath(row: index, section: 0)
            self.tblView.scrollToRow(at: indexPath, at: .middle, animated: true)
            Timer.scheduledTimer(withTimeInterval: 0.3, repeats: false) { tm in
                let cell = self.tblView.cellForRow(at: indexPath) as? AddClarificationDocumentTableViewCell
                cell?.viewShadow.subviews.first?.backgroundColor = .lightGray
                UIView.animate(withDuration: 0.5) {
                    cell?.viewShadow.subviews.first?.backgroundColor = .white
                }
            }
        } else {
            //validate form
            view.endEditing(true)
            CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
            
            webService.application_id = self.application_id
            
            webService.addClarificationDocumentApiCalling { invalidTokenMsg in
                
                CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
                UIAlertController.invalidToken(msg: invalidTokenMsg, onVC: self) {
                    self.btnSubmitClicked(self.btnSubmit)
                }
                
            } block: { isGetSuccess in
                
                CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
                if let getSuccess = isGetSuccess, getSuccess {
                    self.navigationController?.popViewController(animated: true)
                }
            }

        }
    }
}
