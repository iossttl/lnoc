//
//  AddClarificationDocumentDataSourceDelegate.swift
//  LNOC
//
//  Created by Pradip on 03/01/22.
//

import UIKit

class AddClarificationDocumentDataSourceDelegate : NSObject {
    
    //MARK:- Variable Properties
    
    fileprivate weak var tblView: UITableView?
    var itemList: [ClarificationDocuments]
    fileprivate weak var delegate : TblViewDelegate?
    
    fileprivate var documentPickerViewControllerDelegate : DocumentPickerViewControllerDelegate!
    
    var btnDeleteClickedCallBack : ((Int) -> Void)?
    var btnTextViewTextChanged : ((String, Int) -> Void)?
    var btnDocumentChanged : ((DocumentModel, Int) -> Void)?
    
    //MARK:- Initializer
    init(list: [ClarificationDocuments], tbl: UITableView, _delegate : TblViewDelegate?) {
        self.tblView = tbl
        self.itemList = list
        self.delegate = _delegate
        super.init()
        self.setup()
    }
    
    fileprivate func setup() {
        self.tblView?.delegate = self
        self.tblView?.dataSource = self
        
        self.tblView?.separatorStyle = .none
        self.tblView?.estimatedRowHeight = 200
        self.tblView?.rowHeight = UITableView.automaticDimension
        self.tblView?.contentInset = .init(top: 8, left: 0, bottom: 8, right: 0)
        self.tblView?.scrollIndicatorInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        
        self.tblView?.register(cellType: AddClarificationDocumentTableViewCell.self)
        self.tblView?.reloadData()
    }
    
    func reloadData(list: [ClarificationDocuments]) {
        self.itemList = list
        self.tblView?.reloadData()
    }
    
    @objc fileprivate func btnDeleteClicked(sender: UIButton) {
        self.btnDeleteClickedCallBack?(sender.tag)
    }
    
    @objc fileprivate func btnDocumentClicked(_ sender: UIButton) {
        UIView.hideKeyBoard()
        let documentPickerVC = UIDocumentPickerViewController(documentTypes: arrdocTypes, in: .import)
        documentPickerViewControllerDelegate = DocumentPickerViewControllerDelegate(VC: documentPickerVC, delegate: self, sender: sender)
        
        appDelegate.window?.rootViewController?.present(documentPickerVC, animated: true, completion: nil)
    }
    
    
}


extension AddClarificationDocumentDataSourceDelegate: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(with: AddClarificationDocumentTableViewCell.self, for: indexPath)
        
        cell.configurationData(item: itemList[indexPath.row], tag: indexPath.row)
        
        
        cell.btnDelete.superview?.isHidden = false
        if (indexPath.row == 0) {
            cell.btnDelete.superview?.isHidden = true
        }
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteClicked), for: .touchUpInside)
        
        cell.docmentView.btnDocument.tag = indexPath.row
        cell.docmentView.btnDocument.addTarget(self, action: #selector(btnDocumentClicked), for: .touchUpInside)
        
        cell.contentView.layoutIfNeeded()
        cell.btnTextViewTextChanged = self.btnTextViewTextChanged
        
        return cell
    }
}

//MARK:- UITableViewDelegate
extension AddClarificationDocumentDataSourceDelegate: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        delegate?.Table_View(tableView, didSelectRowAt: indexPath)
    }
    
}



extension AddClarificationDocumentDataSourceDelegate : DocumentPickerDelegate {
    
    func DocumentPickerData(url: URL?, errorMsg: String, sender: UIView) {
        if let getURL = url,
           let cell = tblView?.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? AddClarificationDocumentTableViewCell {
            
            let index = sender.tag
            
            if let change_URL = changeURL(url: getURL, name: "AddClarificationDocument_\(UUID().uuidString)") {
                
                cell.docmentView.imgViewDocument.image = .correct
                cell.docmentView.imgViewDocument.tintColor = .themeColor
                cell.docmentView.setFileName(fileName: getURL.lastPathComponent, showBtnUpload: false)
                
                let filename = getURL.lastPathComponent
                
//                if getURL.lastPathComponent.components(separatedBy: ".").count > 1 {
//                    filename = getURL.lastPathComponent.components(separatedBy: ".")[0]
//                }
                
                let model = DocumentModel()
                
                model.title = filename
                model.type = getURL.pathExtension
                model.url = change_URL
                
                self.btnDocumentChanged?(model, index)
                
            }
            
        } else {
            Global.shared.showBanner(message: errorMsg)
        }
    }
    
}
