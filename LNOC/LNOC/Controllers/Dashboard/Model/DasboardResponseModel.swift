//
//  DasboardModel.swift
//  PravasiBhartiyaDivas
//
//  Created by Jayesh on 03/12/20.
//

import UIKit

class DasboardResponseModel: NSObject {
    
    let resultFlag: Bool
    let message: String
    let registeredProperty:String
    let totalNOC:String
    var arrNOCApp:[NOCAppListDataModel] = []
    
    enum Keys:String {
        case list = "applications"
        case registeredProperty = "totalLands"
        case totalNOC = "totalApplication"
    }
    
    init(dict: JSONObject) {
        resultFlag = getBoolean(anything: dict[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dict[CommonAPIConstant.key_message]).decryptStr
        
        registeredProperty = getString(anything: dict[Keys.registeredProperty.rawValue]).decryptStr
        totalNOC = getString(anything: dict[Keys.totalNOC.rawValue]).decryptStr
        
        if let arr = dict[Keys.list.rawValue] as? [JSONObject] {
            arrNOCApp = arr.map({ NOCAppListDataModel(dict: $0) })
        } else {
            arrNOCApp = []
        }
    }
    
}


