//
//  DashboardWebServices.swift
//  ASI
//
//  Created by Jayesh on 30/07/20.
//  Copyright © 2020 Silvertouch. All rights reserved.
//

import Foundation

class DashboardWebServicesModel:NSObject {
    
    fileprivate var dashboardURL: String {
        return BASEURL + API.getDashboard
    }
    
    enum Keys:String {
        case token = "token"
    }
    
    fileprivate var getProfileUrl :String {
         return BASEURL + API.getProfile
     }
    
    private func dashboardDict() -> Dictionary {
        return WShandler.commonDict()
    }
    
    
    func getDashbaord(block: @escaping ((DasboardResponseModel?) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            print("getDashbaord - ", dashboardDict())

            WShandler.shared.postWebRequest(urlStr: dashboardURL, param: dashboardDict()) { (json, flag) in
               
                print("getDashbaord json - ", json)
                
                var responseModel: DasboardResponseModel?
                if (flag == 200) {
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == 2 {
                            UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                            UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                            UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                            UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                            UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                            UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                            Global.shared.changeInitialVC(animated: true, direction: .toLeft)
                            Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        } else {
                            responseModel = DasboardResponseModel(dict: json)
                        }
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    
                }
                block(responseModel)
            }
        } else {
            block(nil)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
    
    
    func GetProfileDetails(invalidTokenMsg : @escaping ((String) -> Swift.Void), block:@escaping ((UserModel?) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            let uuid = UUID().uuidString
            let param = WShandler.commonDict()//(uuid: uuid)
            print("GetProfileDetails - ", param)
            WShandler.shared.postWebRequest(urlStr: self.getProfileUrl, param: param) { (json, flag) in
                
                print("GetProfileDetails json - ", json)

                var responseModel:UserModel?
                if flag == 200 {
                    
                    //Invalid Token
                    if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                        invalidTokenMsg(getString(anything:json[CommonAPIConstant.key_message]).decryptStr)

                    } else if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                       if let dict = json[CommonAPIConstant.key_userDetails] as? Dictionary {
                        responseModel = UserModel(dict: dict, uuid: uuid)
                       }
                       
                    } else {
                       Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                    }
                } else {
                   Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel)
            }
        } else {
            block(nil)
           Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
}
