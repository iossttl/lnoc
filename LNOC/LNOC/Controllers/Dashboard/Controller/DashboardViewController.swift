//
//  DashboardViewController.swift
//  TRIFED
//
//  Created by Jayesh on 24/07/20.
//  Copyright © 2020 Hitesh. All rights reserved.
//

import UIKit
import SideMenu
import AVFoundation
import AVKit
import CHIPageControl

class DashboardViewController: HeaderViewController {
    
    //MARK:- Interface Builder
    //UIView
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewScrlViewMain: UIView!
    @IBOutlet weak var viewNOCApp: UIView!
    @IBOutlet weak var viewRegisteredPropertiesMain: UIView!
    @IBOutlet weak var viewNOCAppMain: UIView!
    @IBOutlet weak var viewRegisteredPropetiesCount: UIView!
    @IBOutlet weak var viewNOCAppCount: UIView!
    @IBOutlet weak var viewSepratorProperties: UIView!
    @IBOutlet weak var viewSepratorNOCApp: UIView!
    @IBOutlet weak var viewNOCandPropertyCount: UIView!
    @IBOutlet weak var viewAddApp: GradientView!
    @IBOutlet weak var viewSeeAll: UIView!
    
    //UIScrollView
    @IBOutlet weak var scrlViewMain: UIScrollView!
    
    @IBOutlet weak var lblRegisterPropertiesStatic: UILabel!
    @IBOutlet weak var lblNOCAppStatic: UILabel!
    @IBOutlet weak var lblRegisterPropertiesCount: UILabel!
    @IBOutlet weak var lblNOCAppCount: UILabel!
    
    
    @IBOutlet weak var btnAddApp: UIButton!
    
    //UITableView
    @IBOutlet weak var tblViewNOCApp: UITableView!
    
    @IBOutlet weak var tblViewNOCAppHeightConstraint: NSLayoutConstraint!
    
    //Collections
    @IBOutlet var lblMainTitles: [LocalizedLabel]!
    @IBOutlet var lblViewMoreAllTitles: [LocalizedLabel]!
    
    var arrNOCList: [NOCAppListDataModel] = []
    
    
    
    //MARK:- Variables
    //Local
    fileprivate var refreshControl = UIRefreshControl()
    fileprivate var isPullToRefresh = false
    fileprivate var nocListTblViewDelegateDataSource : NOCListDashboardTblViewDelegateDataSource!
    fileprivate var dashboardWebServicesModel = DashboardWebServicesModel()
    fileprivate var totalCount = 0
    fileprivate var page = 1
    fileprivate var noRecordsLblHeight : CGFloat = 0
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViewController()
    }

    override func viewWillLayoutSubviews() {
        self.tblViewNOCAppHeightConstraint.constant = 0
        let viewmainHeight = self.viewMain.frame.size.height
        let viewContentHeight = self.viewScrlViewMain.frame.size.height
        noRecordsLblHeight = viewmainHeight - viewContentHeight - 10
    }
    
    //MARK:- Private Methods
    //ViewController set up
    fileprivate func setUpViewController() {
        changeLeadingTrailingForiPad(view: self.view)
        self.view.layoutIfNeeded()
        
        setUpHeader()
        setuplbl()
        setUpBtns()
        setupView()
        setUpScrlView()
        addObserver()
        getWebServicesforDashboard()
        getProfileDataFromWebServices()
    }
    
    fileprivate func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.dashboard)
        setHeaderView_MenuImage()
        showNotification()
    }
    
    fileprivate func setUpScrlView() {
        scrlViewMain.addRefresh(control: refreshControl, target: self, action: #selector(handleRefreshControl))
        refreshControl.attributedTitle = NSAttributedString(string: CommonLabels.pull_to_refresh, attributes: [NSAttributedString.Key.font : UIFont.boldValueFont, NSAttributedString.Key.foregroundColor : UIColor.customSubtitle])
    }
    
    fileprivate func setuplbl() {
        
        self.lblMainTitles.forEach({
            $0.font = IS_IPAD ? .boldSystemFont(withSize: 28) : .boldSystemFont(withSize: 20.0)
            $0.textColor = .customDarkBlueGradient
        })
        
        self.lblViewMoreAllTitles.forEach({
            $0.font = IS_IPAD ? .regularValueFont : .regularSmallValueFont
            $0.textColor = .orange
            $0.superview?.setFullCornerRadius()
            $0.superview?.backgroundColor = .clear
        })
        
        self.lblRegisterPropertiesStatic.font = .semiboldTitleFont
        self.lblRegisterPropertiesStatic.textColor = .black
        
        self.lblNOCAppStatic.font = .semiboldTitleFont
        self.lblNOCAppStatic.textColor = .black
        
        self.lblRegisterPropertiesCount.font = .boldValueFont
        self.lblRegisterPropertiesCount.textColor = .customgreenTheamClr
        
        self.lblNOCAppCount.font = .boldValueFont
        self.lblNOCAppCount.textColor = .orange
    }
    
    fileprivate func setUpBtns() {
        btnAddApp.titleLabel?.font = .boldValueFont
        btnAddApp.setTitleColor(.white, for: .normal)
    }
    
    fileprivate func setupView() {
        //self.view.backgroundColor = .customBGLightSkyBlue
        viewRegisteredPropertiesMain.setCustomCornerRadius(radius: 8.0)
        viewNOCAppMain.setCustomCornerRadius(radius: 8.0)
        viewSepratorProperties.backgroundColor = .customgreenTheamClr
        viewSepratorNOCApp.backgroundColor = .orange
        self.viewAddApp.setFullCornerRadius()
        self.viewAddApp.addGradient(ofColors: [UIColor.themeDarkOrangeClr.cgColor, UIColor.themeLightOrangeClr.cgColor], direction: .leftRight)
        
        #if LNOC
            self.viewRegisteredPropertiesMain.isHidden = true //LNOC
        #else
            self.viewRegisteredPropertiesMain.isHidden = false //LRCS
        #endif
    }
    
    @objc fileprivate func handleRefreshControl(_ control: UIRefreshControl) {
        arrNOCList = []
        totalCount = 0
        page = 1
        isPullToRefresh = true
        getWebServicesforDashboard()
    }
    
    //TableView set up
    func setUpTblView() {
        if self.arrNOCList.count > 0 {
            self.tblViewNOCApp.backgroundView = nil
            if (self.nocListTblViewDelegateDataSource == nil) {
                self.nocListTblViewDelegateDataSource = NOCListDashboardTblViewDelegateDataSource(arrData: arrNOCList, tbl: tblViewNOCApp, delegate: self, showHideProgress: true)
            } else {
                self.nocListTblViewDelegateDataSource.reloadData(arrData: arrNOCList)
            }
        }
    }
    
    //Observer
    fileprivate func addObserver() {
        self.tblViewNOCApp.addObserver(self, forKeyPath: "contentSize", options: NSKeyValueObservingOptions.new, context: nil)
        K_NC.addObserver(self, selector: #selector(UpdateList(_:)), name: .addNoc, object: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let keyPath = keyPath
            else {
            super.observeValue(forKeyPath: nil, of: object, change: change, context: context)
            return
        }
        
        switch (keyPath) {
        case("contentSize"):
            if let object = object as? UITableView {
                if (object == tblViewNOCApp) && self.arrNOCList.count > 0 {
                    if (self.tblViewNOCApp.contentSize.height == 0) {
                        self.tblViewNOCApp.layer.mask = nil
                        self.tblViewNOCAppHeightConstraint.constant = 100
                        self.tblViewNOCApp.backgroundView = UIView.makeNoRecordFoundView(frame: CGRect(x: 16, y: 0, width: SCREEN_WIDTH - 2*16, height: 100), msg: CommonLabels.no_record_found)
                    } else {
                        self.tblViewNOCAppHeightConstraint.constant = self.tblViewNOCApp.contentSize.height + self.tblViewNOCApp.contentInset.top + self.tblViewNOCApp.contentInset.bottom
                        self.tblViewNOCApp.layer.mask = nil
                    }
                    self.tblViewNOCApp.viewWith(radius: 8.0, borderColor: .clear, borderWidth: 0.0)
                }
            }
        default:
            super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
        }
    }
    
    @objc private func UpdateList(_ noti: Notification) {
        arrNOCList = []
        totalCount = 0
        page = 1
        isPullToRefresh = true
        getWebServicesforDashboard()
    }
    
    //MARK:- IBActions
    override func btnBackTapped(_ sender: UIButton) {
        self.showSideMenu()
    }
    
    override func btnNotificationTapped(_ sender: UIButton) {
        let vc = NotificationsListViewController.instantiate(appStoryboard: .NotificationsList)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNOCAppAllClicked(_ sender: Any) {
        let dvc = NOCAppListViewController.instantiate(appStoryboard: .NOCAppList)
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    @IBAction func btnNOCAppAddClicked(_ sender: Any) {
        let dvc = RegisterContainerViewController.instantiate(appStoryboard: .RegisterContainer)
        self.navigationController?.pushViewController(dvc, animated: true)
    }
}

extension DashboardViewController:TblViewDelegate {
    func didSelect(tbl: UITableView, indexPath: IndexPath) {
        let dvc = ApplicationDetailsViewController.instantiate(appStoryboard: .applicationDetails)
        dvc.noc_id = arrNOCList[indexPath.row].noc_id
        self.navigationController?.pushViewController(dvc, animated: true)
    }
}

//Web Services Call
extension DashboardViewController {
    fileprivate func getWebServicesforDashboard() {
        if !isPullToRefresh {
            CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        }
        self.scrlViewMain.isHidden = true
        dashboardWebServicesModel.getDashbaord(block: { (json) in
            
            defer {
                CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            }
        
            if (self.isPullToRefresh) {
                self.isPullToRefresh = false
                self.refreshControl.endRefreshing()
            }
            
            if let responseModel = json {
                self.lblRegisterPropertiesCount.text = responseModel.registeredProperty.is_trimming_WS_NL_to_String ?? "0"
                self.lblNOCAppCount.text = responseModel.totalNOC.is_trimming_WS_NL_to_String ?? "0"
                self.viewRegisteredPropetiesCount.setFullCornerRadius(type: .automatic, borderColor: .customgreenTheamClr, borderWidth: 1.0)
                self.viewNOCAppCount.setFullCornerRadius(type: .automatic, borderColor: .orange, borderWidth: 1.0)
                if responseModel.arrNOCApp.count > 0 {
                    if responseModel.arrNOCApp.count >= 2 {
                        self.arrNOCList.append(responseModel.arrNOCApp[0])
                        self.arrNOCList.append(responseModel.arrNOCApp[1])
                    } else if responseModel.arrNOCApp.count == 1 {
                        self.arrNOCList.append(responseModel.arrNOCApp[0])
                    }
                    self.viewSeeAll.isHidden = false
                    self.setUpTblView()
                    self.scrlViewMain.isHidden = false
                } else {
                    self.viewSeeAll.isHidden = true
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        self.tblViewNOCAppHeightConstraint.constant = self.noRecordsLblHeight
                        let frame = CGRect(x: 0, y: 0, width: self.tblViewNOCApp.frame.size.width, height: self.noRecordsLblHeight)
                        self.tblViewNOCApp.backgroundView = UIView.makeNoRecordFoundView(frame: frame, msg: CommonLabels.no_record_found)
                        self.scrlViewMain.isHidden = false
                    }
                }
            }
        })
    }
    
    func getProfileDataFromWebServices() {
        view.endEditing(true)
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.dashboardWebServicesModel.GetProfileDetails { (invalidTokenMsg) in
            
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            UIAlertController.invalidToken(msg: invalidTokenMsg, onVC: self) {
                self.getProfileDataFromWebServices()
            }
            
        } block: { (responseModel) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if let dictUser = responseModel {
                dictUser.setUserModel()
                
                UserPreferences.set(value: dictUser.email, forKey: UserPreferencesKeys.General.email)
                UserPreferences.set(value: dictUser.id, forKey: UserPreferencesKeys.UserInfo.userID)
                UserPreferences.set(value: dictUser.token, forKey: UserPreferencesKeys.General.token)
                UserPreferences.set(value: dictUser.user_roles_id , forKey: UserPreferencesKeys.General.user_roles_id)
                UserPreferences.set(value: dictUser.first_name, forKey: UserPreferencesKeys.General.first_name)
                UserPreferences.set(value: dictUser.last_name, forKey: UserPreferencesKeys.General.last_name)
            }
        }
    }
}
