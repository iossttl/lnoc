//
//  cellLagilation.swift
//  RajyaSabha
//
//  Created by Mobile Mac on 21/10/21.
//

import UIKit

class cellNOCAppDashboard: UITableViewCell {
    //MARK: - Interface builder
    
    //UIView
    @IBOutlet weak var vwbg: UIView!
    @IBOutlet weak var vwSeparator: UIView!
    @IBOutlet weak var vwDateFees: UIView!
    @IBOutlet weak var progressView: LNOCCustomProgressBarView!
    
    //UILabel
    @IBOutlet weak var lblAppNo: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblCSNoPlotNo: UILabel!
    @IBOutlet weak var lblNOCType: UILabel!
    @IBOutlet weak var lblDivision: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var lblAppStatusStatic: UILabel!
    @IBOutlet weak var lblNameofTransfereeStatic: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblTransferee: UILabel!
    @IBOutlet weak var lblProgressValue: UILabel!
    
    @IBOutlet var lblTitles: [LocalizedLabel]!
    @IBOutlet var lblValues: [UILabel]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupvw()
        setuplbl()
    }
    
    func setupvw(){
        self.vwSeparator.backgroundColor = .orange
        self.selectionStyle = .none
        vwbg.setCustomCornerRadius(radius: 8.0)
    }
    
    func setuplbl(){
        
        self.lblTitles.forEach({
            $0.font = IS_IPAD ? .semiboldLargeFont : .semiboldSmallTitleFont
            $0.textColor = .customDarkBlueGradient
        })
        
        self.lblValues.forEach({
            $0.font = IS_IPAD ? .regularExtraLargeFont : .regularTitleFont
            $0.textColor = .black
        })
        
        self.lblAppNo.font = UIFont.regularTitleFont
        self.lblAppNo.textColor = .orange
    }
    
    func cellConfigure(data:NOCAppListDataModel,showHideProgress:Bool) {
        self.lblAppNo.text = showHideProgress ? data.id : data.app_no
        if data.address.isStringEmpty {
            self.lblTitle.superview?.isHidden = true
        } else {
            self.lblTitle.text = data.address
            self.lblTitle.superview?.isHidden = false
        }
        
        /*if data.transferee.isStringEmpty {
            //self.lblTransferee.text = data.transferee.is_trimming_WS_NL_to_String ?? "-"
            self.lblTransferee.superview?.isHidden = true
        } else {
            self.lblTransferee.text = data.transferee.is_trimming_WS_NL_to_String ?? "-"
            self.lblTransferee.superview?.isHidden = false
        }*/
        
        self.lblCSNoPlotNo.text = data.plot_no.is_trimming_WS_NL_to_String ?? "-"
        //self.lblNOCType.text = data.noc_type.is_trimming_WS_NL_to_String ?? "-"
        self.lblDivision.text = data.division.is_trimming_WS_NL_to_String ?? "-"
        
        self.lblDesc.text = data.desc
        self.lblStatus.text = data.status.is_trimming_WS_NL_to_String ?? "-"
        
        if data.noc_type.isStringEmpty {
            self.lblNOCType.text = data.title.is_trimming_WS_NL_to_String ?? "-"
        } else {
            self.lblNOCType.text = data.noc_type.is_trimming_WS_NL_to_String ?? "-"
        }

        progressView.setCurrentStep(getInteger(anything: data.percentage))
        
        progressView.superview?.isHidden = !showHideProgress
        
        let mutableStr = NSMutableAttributedString()
        mutableStr.append(NSAttributedString(string: "\(progressView.currentStep)" + "% ", attributes: [.font : IS_IPAD ? UIFont.regularExtraLargeFont : UIFont.regularTitleFont, .foregroundColor : UIColor.customDarkBlueGradient]))
        
        mutableStr.append(NSAttributedString(string: CommonLabels.workDone, attributes: [.font : IS_IPAD ? UIFont.regularExtraLargeFont : UIFont.regularTitleFont, .foregroundColor : UIColor.customPercentWorkDone]))
        
        lblProgressValue.attributedText = mutableStr
        
    }
}

class LNOCCustomProgressBarView : UIView {
    
    @IBOutlet weak var oneCircle: UIView!
    @IBOutlet weak var twoCircle: UIView!
    @IBOutlet weak var threeCircle: UIView!
    @IBOutlet weak var fourCircle: UIView!
    
    @IBOutlet weak var oneLine: UIView!
    @IBOutlet weak var twoLine: UIView!
    @IBOutlet weak var threeLine: UIView!
    @IBOutlet weak var fourLine: UIView!
    
    private(set) var currentStep : Int = 0
    
    func setCurrentStep(_ step: Int) {
        self.currentStep = step
        switch step {
        case 1..<26:
            
            setFillViewColor(oneCircle, oneLine)
            
            setUnFillViewColor(twoCircle, threeCircle, fourCircle,
                               twoLine, threeLine, fourLine)
            
        case 26..<51:
            
            setFillViewColor(oneCircle, twoCircle,
                             oneLine, twoLine)
            
            setUnFillViewColor(threeCircle, fourCircle,
                               threeLine, fourLine)
            
        case 51..<76:
            
            setFillViewColor(oneCircle, twoCircle, threeCircle,
                             oneLine, twoLine, threeLine)
            
            setUnFillViewColor(fourCircle, fourLine)
            
        case 76..<101:
            
            setFillViewColor(oneCircle, twoCircle, threeCircle, fourCircle,
                             oneLine, twoLine, threeLine, fourLine)

        default:
            
            setUnFillViewColor(oneCircle, twoCircle, threeCircle, fourCircle,
                               oneLine, twoLine, threeLine, fourLine)
            
        }
        
    }
    
    private func setFillViewColor(_ views:UIView...) {
        views.forEach({ $0.backgroundColor = .progressColor })
    }
    
    private func setUnFillViewColor(_ views:UIView...) {
        views.forEach({ $0.backgroundColor = .unProgressColor })
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        oneCircle.setFullCornerRadius()
        twoCircle.setFullCornerRadius()
        threeCircle.setFullCornerRadius()
        fourCircle.setFullCornerRadius()
        fourLine.roundCorners(corners: [.topRight, .bottomRight], radius: fourLine.frame.height / 2)
    }
    
}
