//
//  PreviousTransferDetailsCell.swift
//  LNOC
//
//  Created by Pradip Patel on 30/12/21.
//  
//

import UIKit

class PreviousTransferDetailsCell: UITableViewCell {

    //MARK:- Outlets
    
    //UIView
    @IBOutlet var viewMain : UIView!
    @IBOutlet var viewSeperator : UIView!
    
    //NSLayoutConstraint
    @IBOutlet var anchorViewMainBottomConstraint : NSLayoutConstraint!
    
    //UILabel
    @IBOutlet var lblNameOfTransferrerTitle: UILabel!
    @IBOutlet var lblNameOfTransferrerValue: UILabel!
    @IBOutlet var lblNameOfTransfereeTitle: UILabel!
    @IBOutlet var lblNameOfTransfereeValue: UILabel!
    @IBOutlet var lblYearOfTransferTitle: UILabel!
    @IBOutlet var lblYearOfTransferValue: UILabel!
    @IBOutlet var lblTypeOfTransferTitle: UILabel!
    @IBOutlet var lblTypeOfTransferValue: UILabel!
    @IBOutlet var lblNOCObtainedTitle: UILabel!
    @IBOutlet var lblNOCObtainedValue: UILabel!
    @IBOutlet var lblObtainedNOCTitle: UILabel!
    
    
    @IBOutlet var imgObtainedNOC: UIImageView!
    
    @IBOutlet var btnObtainedNOC: UIButton!
    
    //Font Collections
    @IBOutlet var labelTitles : [UILabel]!
    @IBOutlet var labelValues : [UILabel]!
    
    //MARK:- Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        setupView()
        setupLabel()
    }
    
    //MARK:- Private Methods
    private func setupView() {
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        self.viewMain.backgroundColor = .customWhite
        self.viewSeperator.backgroundColor = .customSeparator
        
        changeHeightForiPad(view: imgObtainedNOC)
        
        imgObtainedNOC.image = .ic_pdf
    }
    
    private func setupLabel() {
        labelTitles.forEach { (lbl) in
            lbl.font = .boldTitleFont
            lbl.textColor = .customBlue
            lbl.numberOfLines = 0
        }
        
        labelValues.forEach { (lbl) in
            lbl.font = .regularValueFont
            lbl.textColor = .customBlack
            lbl.numberOfLines = 0
        }
        
        lblNameOfTransferrerTitle.text = "1: " + "Name Of Transferrer".localizedString
        lblNameOfTransfereeTitle.text = "2: " + "Name Of Transferee".localizedString
        lblYearOfTransferTitle.text = "3: " + "Year Of Transfer".localizedString
        lblTypeOfTransferTitle.text = "Type Of Transfer".localizedString
        lblNOCObtainedTitle.text = "NOC Obtained".localizedString
        lblObtainedNOCTitle.text = "Obtained NOC".localizedString
        
    }
    
    //MARK:- Public Methods
    
    func configureCell(item: PreviousTransferDetailsModel) {
        lblNameOfTransferrerValue.text = item.name_of_transferer
        lblNameOfTransfereeValue.text = item.name_of_transferee
        lblYearOfTransferValue.text = item.year_of_transfer
        lblTypeOfTransferValue.text = item.type_of_transfer
        lblNOCObtainedValue.text = item.noc_obtained
        
        lblObtainedNOCTitle.superview?.isHidden = item.obtained_noc_doc.isEmptyString
    }
    
}

