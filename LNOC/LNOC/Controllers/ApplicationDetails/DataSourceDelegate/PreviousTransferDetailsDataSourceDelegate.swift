//
//  PreviousTransferDetailsDataSourceDelegate.swift
//  LNOC
//
//  Created by Pradip on 30/12/21.
//

import UIKit

class PreviousTransferDetailsDataSourceDelegate : NSObject {
    
    //MARK:- Variable Properties
    
    fileprivate weak var tblView: UITableView?
    fileprivate var itemList: [PreviousTransferDetailsModel]
    fileprivate weak var delegate : TblViewDelegate?
    
    var btnObtainedNOCClickedCallBack : ((Int) -> Void)?
    
    //MARK:- Initializer
    init(list: [PreviousTransferDetailsModel], tbl: UITableView, _delegate : TblViewDelegate?) {
        self.tblView = tbl
        self.itemList = list
        self.delegate = _delegate
        super.init()
        self.setup()
    }
    
    fileprivate func setup() {
        self.tblView?.backgroundColor = UIColor.clear
        self.tblView?.delegate = self
        self.tblView?.dataSource = self
        
        self.tblView?.separatorStyle = .none
        self.tblView?.estimatedRowHeight = 261
        self.tblView?.rowHeight = UITableView.automaticDimension
        self.tblView?.contentInset = .init(top: 0, left: 0, bottom: 0, right: 0)
        self.tblView?.scrollIndicatorInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        
        self.tblView?.register(cellType: PreviousTransferDetailsCell.self)
        self.tblView?.reloadData()
    }
    
    func reloadData(list: [PreviousTransferDetailsModel]) {
        self.itemList = list
        self.tblView?.reloadData()
    }
    
    @objc fileprivate func btnObtainedNOCClicked(sender: UIButton) {
        self.btnObtainedNOCClickedCallBack?(sender.tag)
    }
    
}


extension PreviousTransferDetailsDataSourceDelegate: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(with: PreviousTransferDetailsCell.self, for: indexPath)
        cell.configureCell(item: itemList[indexPath.row])
        
        cell.viewSeperator.isHidden = false
        
        if (indexPath.row == 0) && (indexPath.row == itemList.count - 1) {
            cell.viewSeperator.isHidden = true
        } else if indexPath.row == (itemList.count - 1) {
            cell.viewSeperator.isHidden = true
        }
        
        cell.btnObtainedNOC.tag = indexPath.row
        cell.btnObtainedNOC.addTarget(self, action: #selector(btnObtainedNOCClicked), for: .touchUpInside)
        
        cell.contentView.layoutIfNeeded()
        
        return cell
    }
}

//MARK:- UITableViewDelegate
extension PreviousTransferDetailsDataSourceDelegate: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        delegate?.Table_View(tableView, didSelectRowAt: indexPath)
    }
    
}


