//
//  ApplicationDetailsViewController.swift
//  LNOC
//
//  Created by Pradip on 29/12/21.
//

import UIKit

class ApplicationDetailsViewController : HeaderViewController {
    
    //MARK: - Outlets
    // UIView
    @IBOutlet var mainView: UIView!
    @IBOutlet var viewListDataStatus: UIView!
    @IBOutlet var viewPreviousTransferDetails: UIView!
    @IBOutlet var viewDocumentMain: UIView!
    @IBOutlet var viewClarificationDocumentMain: UIView!

    // UIStackView
    @IBOutlet var stackViewListDataStatus: UIStackView!
    @IBOutlet var stackViewMain: UIStackView!
    @IBOutlet var stackViewApplicationDetails: UIStackView!
    @IBOutlet var stackViewPropertyDetails: UIStackView!
    @IBOutlet var stackViewBuildingDetails: UIStackView!
    @IBOutlet var stackViewFlatShopDetails: UIStackView!
    @IBOutlet var stackViewPreviousTransferDetails: UIStackView!
    @IBOutlet var stackViewDocument: UIStackView!
    @IBOutlet var stackViewClarificationDocument: UIStackView!
    @IBOutlet var stackViewIsParking: UIStackView!
    @IBOutlet var stackViewParkingNumbers: UIStackView!
    
    // UILabel
    @IBOutlet weak var lblTypeOfPremises: UILabel!
    @IBOutlet var lblApplicationDetails: UILabel!
    @IBOutlet var lblPropertyDetails: UILabel!
    @IBOutlet var lblBuildingDetails: UILabel!
    @IBOutlet var lblFlatShopDetails: UILabel!
    @IBOutlet var lblPreviousTransferDetails: UILabel!
    @IBOutlet var lblDocument: UILabel!
    @IBOutlet weak var lblClarificationDocument: LocalizedLabel!
    
    //UILabel - Application Details
    @IBOutlet var lblOwnerOf: UILabel!
    @IBOutlet var lblCompanyName: UILabel!
    @IBOutlet var lblFirstName: UILabel!
    @IBOutlet var lblMiddleName: UILabel!
    @IBOutlet var lblSurname: UILabel!
    @IBOutlet var lblNOCType: UILabel!
    @IBOutlet var lblDivision: UILabel!
    @IBOutlet weak var lblNameOfTransferor: UILabel!
    @IBOutlet var lblNameOfTheTransferee: UILabel!
    @IBOutlet weak var lblSecondNameOfTheTransferee: UILabel!
    @IBOutlet weak var lblThirdNameOfTheTransferee: UILabel!
    @IBOutlet var lblTransferIsWithinTheFamily: UILabel!
    @IBOutlet var lblRelationWithTheTransferor: UILabel!

    //UILabel - Property Details
    @IBOutlet var lblPropertyDetailsDivision: UILabel!
    @IBOutlet var lblCSNo_PlotNo: UILabel!
    @IBOutlet weak var lblApplicantAddress: UILabel!
    @IBOutlet var lblNameOfSociety: UILabel!
    @IBOutlet var lblPresentNameOfTheLesse: UILabel!
    
    //UILabel - Building Details
    @IBOutlet var lblNameOfBuilding_Premises: UILabel!
    @IBOutlet var lblTotalNumberOfFloorsOfBuilding: UILabel!
    @IBOutlet var lblNoOfLift: UILabel!
    @IBOutlet var lblCompletionOfBuilding_Year: UILabel!
    @IBOutlet var lblSocietyChairmanName: UILabel!
    @IBOutlet var lblChairmanContactNumber: UILabel!
    @IBOutlet var lblSocietySecretaryName: UILabel!
    @IBOutlet var lblSecretaryContactNumber: UILabel!
    @IBOutlet var lblSocietyEmailId: UILabel!
    @IBOutlet weak var lblSocityAddress: UILabel!
    @IBOutlet var lblNoOfMembers: UILabel!

    //UILabel - Flat/ Shop/ office/ Appartment/ Condominium Details
    
    @IBOutlet var lblFlat_Office_Shop_Number: UILabel!
    @IBOutlet var lblFloorOfFlat_Office_Shop: UILabel!
    @IBOutlet var lblAreaOfFlatSqFt_Carpet: UILabel!
    @IBOutlet var lblAreaOfFlatSqMt_Carpet: UILabel!
    @IBOutlet var lblAreaOfFlatSqFt_Built_up: UILabel!
    @IBOutlet var lblAreaOfFlatSqMt_Built_up: UILabel!
    @IBOutlet var lblIs_Parking: UILabel!
    @IBOutlet var lblNumberOfParkingUnderTransfer: UILabel!
    
    // UIButton
    @IBOutlet var btnRemarks: UIButton!
    @IBOutlet var btnNOCCertificateRemarks: UIButton!
    @IBOutlet weak var btnQueryRejectionRemark: LocalizedButton!
    
    @IBOutlet var btnApplicationDetailsExpandCollapse: UIButton!
    @IBOutlet var btnPropertyDetailsExpandCollapse: UIButton!
    @IBOutlet var btnBuildingDetailsExpandCollapse: UIButton!
    @IBOutlet var btnFlatShopDetailsExpandCollapse: UIButton!
    @IBOutlet var btnPreviousTransferDetails: UIButton!
    @IBOutlet var btnDocumentExpandCollapse: UIButton!
    @IBOutlet var btnClarificationDocumentExpandCollapse: UIButton!
    
    // UIButton
    @IBOutlet var imgApplicationDetails: UIImageView!
    @IBOutlet var imgPropertyDetails: UIImageView!
    @IBOutlet var imgBuildingDetails: UIImageView!
    @IBOutlet var imgFlatShopDetails: UIImageView!
    @IBOutlet var imgPreviousTransferDetails: UIImageView!
    @IBOutlet var imgDocument: UIImageView!
    @IBOutlet weak var imgClarification: UIImageView!
    
    @IBOutlet var tblViewPreviousTransferDetails: UITableView!
    @IBOutlet var anchorTblViewPreviousTransferDetailsHeightConstraint: NSLayoutConstraint!
    
    // Collections
    @IBOutlet var lblsHeaderTitles: [UILabel]!
    @IBOutlet var lblsTitles: [UILabel]!
    @IBOutlet var lblsValues: [UILabel]!
    @IBOutlet var imgsPlusMinus: [UIImageView]!
    @IBOutlet var viewsDetailsShadow : [UIView]!
    @IBOutlet var stackViewOtherDetails : [UIStackView]!
    
    var arrOptions = [Headers.AddChallanDetails, Headers.addClarificationDocument]
    var arrChallanList = [ChallanListModel]()
    
    var previousTransferDetailsDataSourceDelegate : PreviousTransferDetailsDataSourceDelegate!
    var previousTransferDetailList : [PreviousTransferDetailsModel] = []
    let nocAppDetailsWebServicesModel = NOCAppDetailsWebServicesModel()
    fileprivate var webServiceModel = ChallanListWebServiceModel()
    var applicationDetailsModel = ApplicationDetailsModel(dict: [:])
    var noc_id = ""
    
    //MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupConstraintIfIsIPad()
        addObserve()
        setupHeader()
        setupView()
        setupLabel()
        setupButton()
        //setupValues()
        //setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        getNOCAppDetails()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        guard let keyPathString = keyPath else {
            super.observeValue(forKeyPath: nil, of: object, change: change, context: context)
            return
        }

        if keyPathString == "contentSize" {
            if self.previousTransferDetailList.count > 0 {
                self.anchorTblViewPreviousTransferDetailsHeightConstraint.constant = self.tblViewPreviousTransferDetails.contentSize.height + 8
                self.viewPreviousTransferDetails.isHidden = false
            } else {
                self.anchorTblViewPreviousTransferDetailsHeightConstraint.constant = 50
                self.viewPreviousTransferDetails.isHidden = true
            }

            self.view.layoutIfNeeded()

            return
        }
        
    }
    
    //MARK: - Private Methods
    
    private func setupConstraintIfIsIPad() {
        changeLeadingTrailingForiPad(view: self.view)
        changeHeightForiPad(view: btnRemarks)
        changeHeightForiPad(view: btnQueryRejectionRemark)
        changeHeightForiPad(view: btnNOCCertificateRemarks)
        imgsPlusMinus.forEach({ changeHeightForiPad(view: $0, constant: 10) })
    }
    
    private func setupHeader() {
        setHeaderView_BackImage()
        setUpHeaderTitle(strHeaderTitle: Headers.applicationDetails)
    }
    
    private func setupView() {
        self.view.backgroundColor = .lnocBackgroundColor
        
        self.btnRemarks.superview?.isHidden = true
        self.btnQueryRejectionRemark.superview?.isHidden = true
        self.btnNOCCertificateRemarks.superview?.isHidden = true
        
        self.viewsDetailsShadow.forEach({
            $0.addShadow(shadowRadius: 6)
            $0.subviews.first?.viewWith(radius: 12)
            $0.backgroundColor = .clear
            $0.subviews.first?.backgroundColor = .white
        })
        
//        self.viewParkingNumbers.setCustomCornerRadius(radius: 0, borderColor: .lightGray, borderWidth: 1)
        
        self.viewListDataStatus.addShadow(shadowRadius: 6)
        
        self.imgsPlusMinus.forEach({ $0.image = .ic_plus })
        self.stackViewOtherDetails.forEach({ $0.isHidden = true })
    }
    
    private func setupLabel() {
        lblsHeaderTitles.forEach({
            $0.font = .boldLargeFont
            $0.textColor = .customBlue
            $0.numberOfLines = 0
        })
        
        lblsTitles.forEach({
            $0.font = .boldTitleFont
            $0.textColor = .customBlue
            $0.numberOfLines = 0
        })
        
        lblsValues.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
            $0.numberOfLines = 0
        })
    }
    
    private func setupButton() {
        //btnRemarks.setTitle("Remarks", for: .normal)
        btnRemarks.addTarget(self, action: #selector(btnRemarksClikced), for: .touchUpInside)
        //btnNOCCertificateRemarks.setTitle("NOC Certificate Remarks", for: .normal)
        btnNOCCertificateRemarks.addTarget(self, action: #selector(btnNOCCertificateRemarksClikced), for: .touchUpInside)
        btnQueryRejectionRemark.addTarget(self, action: #selector(btnQueryRejectionRemarksClikced), for: .touchUpInside)

        [btnRemarks, btnNOCCertificateRemarks, btnQueryRejectionRemark].forEach({
            $0?.titleLabel?.font = .boldMediumFont
            $0?.titleLabel?.adjustsFontSizeToFitWidth = true
            $0?.setTitleColor(.customWhite, for: .normal)
            $0?.layoutIfNeeded()
            $0?.setCustomCornerRadius(radius: $0!.frame.height / 2)
        })
        
        
        self.btnApplicationDetailsExpandCollapse.addTarget(self, action: #selector(btnApplicationDetailsExpandCollapseClicked), for: .touchUpInside)
        self.btnPropertyDetailsExpandCollapse.addTarget(self, action: #selector(btnPropertyDetailsExpandCollapseClicked), for: .touchUpInside)
        self.btnBuildingDetailsExpandCollapse.addTarget(self, action: #selector(btnBuildingDetailsExpandCollapseClicked), for: .touchUpInside)
        self.btnFlatShopDetailsExpandCollapse.addTarget(self, action: #selector(btnFlatShopDetailsExpandCollapseClicked), for: .touchUpInside)
        self.btnPreviousTransferDetails.addTarget(self, action: #selector(btnPreviousTransferDetailsClicked), for: .touchUpInside)
        self.btnDocumentExpandCollapse.addTarget(self, action: #selector(btnDocumentExpandCollapseClicked), for: .touchUpInside)
        self.btnClarificationDocumentExpandCollapse.addTarget(self, action: #selector(btnClarificationDocumentExpandCollapseClicked), for: .touchUpInside)
    }
    
    private func addObserve() {
        tblViewPreviousTransferDetails.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    private func setupValues() {
        
    var listData = [(listDataTitle:String, listDataValue: String)]()
        
//    if !applicationDetailsModel.currently_application_presented_to.isStringEmpty && applicationDetailsModel.currently_application_presented_to != "-" {
        listData.append(("Currently Application Presented To".localizedString,applicationDetailsModel.currently_application_presented_to))
//    }
    
        listData.append(("Application No".localizedString, applicationDetailsModel.noc_id))
        
    if !applicationDetailsModel.current_application_status.isStringEmpty && applicationDetailsModel.current_application_status != "-" {
        listData.append(("Current Application Status".localizedString,applicationDetailsModel.current_application_status))
    }
    
    if !applicationDetailsModel.interim_order_status.isStringEmpty && applicationDetailsModel.interim_order_status != "-" {
        listData.append(("Interim Order Status".localizedString,applicationDetailsModel.interim_order_status))
    }
    
    if !applicationDetailsModel.interim_order_template.isStringEmpty && applicationDetailsModel.interim_order_template != "-" {
        listData.append(("Interim Order Template".localizedString,applicationDetailsModel.interim_order_template))
    }
    
    if !applicationDetailsModel.summary_note_template.isStringEmpty && applicationDetailsModel.summary_note_template != "-" {
        listData.append(("Summery Note Template".localizedString,applicationDetailsModel.summary_note_template))
    }
    
    if !applicationDetailsModel.final_order_status.isStringEmpty && applicationDetailsModel.final_order_status != "-" {
        listData.append(("Final Order Status".localizedString,applicationDetailsModel.final_order_status))
    }
    
    if !applicationDetailsModel.challan_verification_status.isStringEmpty && applicationDetailsModel.challan_verification_status != "-" {
        listData.append(("Challan Verification Status".localizedString,applicationDetailsModel.challan_verification_status))
    }
    
    if !applicationDetailsModel.challan_remark.isStringEmpty && applicationDetailsModel.challan_remark != "-" {
        listData.append(("Challan Remark".localizedString,applicationDetailsModel.challan_remark))
    }
    
    if !applicationDetailsModel.remark.isStringEmpty && applicationDetailsModel.remark != "-" {
        listData.append(("Remark".localizedString,applicationDetailsModel.remark))
    }
        
        self.stackViewListDataStatus.subviews.forEach({ $0.removeFromSuperview() })

        for item in listData {
            let clearView = UIView()
            clearView.backgroundColor = .clear
            
            let labelTitle = UILabel()
            labelTitle.font = .boldTitleFont
            labelTitle.textColor = .customBlue
            labelTitle.numberOfLines = 0
            labelTitle.text = item.listDataTitle + ":"
            clearView.addSubview(labelTitle)
            
            let labelValue = UILabel()
            labelValue.font = .regularValueFont
            labelValue.textColor = .customBlack
            labelValue.numberOfLines = 0
            labelValue.text = item.listDataValue
            clearView.addSubview(labelValue)
            
            labelTitle.addConstraint(topAnchr: clearView.topAnchor,
                                     leftAnchr: clearView.leftAnchor,
                                     rightAnchr: clearView.rightAnchor,
                                     bottomAnchr: nil)
            
            labelValue.addConstraint(topAnchr: labelTitle.bottomAnchor,
                                     leftAnchr: clearView.leftAnchor,
                                     rightAnchr: clearView.rightAnchor,
                                     bottomAnchr: clearView.bottomAnchor,
                                     padding: .init(top: 8, left: 0, bottom: 0, right: 0))
            
            stackViewListDataStatus.addArrangedSubview(clearView)
        }
                
        self.btnRemarks.superview?.isHidden = self.applicationDetailsModel.remark_arr.count == 0
        self.btnQueryRejectionRemark.superview?.isHidden = self.applicationDetailsModel.remark_arr_reject.count == 0
        self.btnNOCCertificateRemarks.superview?.isHidden = self.applicationDetailsModel.remark_arr_certi.count == 0

        
        //Applicant Details
        self.lblOwnerOf.text = applicationDetailsModel.ownership_of
        
        if applicationDetailsModel.company_name.isEmptyString {
            self.lblFirstName.text = applicationDetailsModel.first_name
            self.lblMiddleName.text = applicationDetailsModel.middle_name
            self.lblSurname.text = applicationDetailsModel.last_name
            self.lblCompanyName.superview?.isHidden = true
        } else {
            self.lblCompanyName.text = applicationDetailsModel.company_name
            self.lblFirstName.superview?.isHidden = true
            self.lblMiddleName.superview?.isHidden = true
            self.lblSurname.superview?.isHidden = true
        }
        
        self.lblNOCType.text = applicationDetailsModel.noctypes
        self.lblDivision.text = applicationDetailsModel.division
        self.lblNameOfTransferor.text = applicationDetailsModel.name_of_transferor
        self.lblNameOfTheTransferee.text = applicationDetailsModel.name_of_the_transferee
        self.lblTransferIsWithinTheFamily.text = applicationDetailsModel.is_transfer_within_family
      
        if !applicationDetailsModel.second_name_of_the_transferee.isStringEmpty && applicationDetailsModel.second_name_of_the_transferee != "-" {
            self.lblSecondNameOfTheTransferee.text = applicationDetailsModel.second_name_of_the_transferee
        } else {
            self.lblSecondNameOfTheTransferee.text = "-"
        }
        
        if !applicationDetailsModel.third_name_of_the_transferee.isStringEmpty && applicationDetailsModel.third_name_of_the_transferee != "-" {
            self.lblThirdNameOfTheTransferee.text = applicationDetailsModel.third_name_of_the_transferee
        } else {
            self.lblThirdNameOfTheTransferee.text = "-"
        }
        
        if !applicationDetailsModel.relation_with_transferer.isStringEmpty && applicationDetailsModel.relation_with_transferer != "-" {
            self.lblRelationWithTheTransferor.text = applicationDetailsModel.relation_with_transferer
        } else {
            self.lblRelationWithTheTransferor.text = "-"
        }
        
        //Property Details
        self.lblPropertyDetailsDivision.text = applicationDetailsModel.division
        self.lblCSNo_PlotNo.text = applicationDetailsModel.cs_number
        self.lblNameOfSociety.text = applicationDetailsModel.name_of_society
        
        if !applicationDetailsModel.applicant_address.isStringEmpty && applicationDetailsModel.applicant_address != "-" {
            self.lblApplicantAddress.text = applicationDetailsModel.applicant_address
        } else {
            self.lblApplicantAddress.text = "-"
        }
        
        if !applicationDetailsModel.present_name_of_lease.isStringEmpty && applicationDetailsModel.present_name_of_lease != "-" {
            self.lblPresentNameOfTheLesse.text = applicationDetailsModel.present_name_of_lease
        } else {
            self.lblPresentNameOfTheLesse.text = "-"
        }
        
        //Building Details
        self.lblNameOfBuilding_Premises.text = applicationDetailsModel.name_of_building_premises
        self.lblTotalNumberOfFloorsOfBuilding.text = applicationDetailsModel.total_floors_of_building
        
        if !applicationDetailsModel.no_of_lift.isStringEmpty && applicationDetailsModel.no_of_lift != "-" {
            self.lblNoOfLift.text = applicationDetailsModel.no_of_lift
        } else {
            self.lblNoOfLift.text = "-"
        }
        
        if !applicationDetailsModel.completion_of_building.isStringEmpty && applicationDetailsModel.completion_of_building != "-" {
            self.lblCompletionOfBuilding_Year.text = applicationDetailsModel.completion_of_building
        } else {
            self.lblCompletionOfBuilding_Year.text = "-"
        }
        
        if !applicationDetailsModel.society_chairman_name.isStringEmpty && applicationDetailsModel.society_chairman_name != "-" {
            self.lblSocietyChairmanName.text = applicationDetailsModel.society_chairman_name
        } else {
            self.lblSocietyChairmanName.text = "-"
        }
        
        if !applicationDetailsModel.chairman_contact_no.isStringEmpty && applicationDetailsModel.chairman_contact_no != "-" {
            self.lblChairmanContactNumber.text = applicationDetailsModel.chairman_contact_no
        } else {
            self.lblChairmanContactNumber.text = "-"
        }
        
        if !applicationDetailsModel.society_secretary_name.isStringEmpty && applicationDetailsModel.society_secretary_name != "-" {
            self.lblSocietySecretaryName.text = applicationDetailsModel.society_secretary_name
        } else {
            self.lblSocietySecretaryName.text = "-"
        }
        
        if !applicationDetailsModel.secretary_contact_no.isStringEmpty && applicationDetailsModel.secretary_contact_no != "-" {
            self.lblSecretaryContactNumber.text = applicationDetailsModel.secretary_contact_no
        } else {
            self.lblSecretaryContactNumber.text = "-"
        }
        
        if !applicationDetailsModel.society_email_id.isStringEmpty && applicationDetailsModel.society_email_id != "-" {
            self.lblSocietyEmailId.text = applicationDetailsModel.society_email_id
        } else {
            self.lblSocietyEmailId.text = "-"
        }
        
        if !applicationDetailsModel.society_address.isStringEmpty && applicationDetailsModel.society_address != "-" {
            self.lblSocityAddress.text = applicationDetailsModel.society_address
        } else {
            self.lblSocityAddress.text = "-"
        }
        
        if !applicationDetailsModel.no_of_members.isStringEmpty && applicationDetailsModel.no_of_members != "-" {
            self.lblNoOfMembers.text = applicationDetailsModel.no_of_members
        } else {
            self.lblNoOfMembers.text = "-"
        }
        
        //Flat/Shop/Office/Appartment/Condominium Details
        self.lblTypeOfPremises.text = applicationDetailsModel.type_of_premises.isEmptyString ? "-" : applicationDetailsModel.type_of_premises
        self.lblFlat_Office_Shop_Number.text = applicationDetailsModel.flat_number.isEmptyString ? "-" : applicationDetailsModel.flat_number
        self.lblFloorOfFlat_Office_Shop.text = applicationDetailsModel.floor_of_flat.isEmptyString ? "-" : applicationDetailsModel.floor_of_flat
        self.lblAreaOfFlatSqFt_Carpet.text = applicationDetailsModel.area_of_flat_sq_ft.isEmptyString ? "-" : applicationDetailsModel.area_of_flat_sq_ft
        self.lblAreaOfFlatSqMt_Carpet.text = applicationDetailsModel.area_of_flat_sq_mt.isEmptyString ? "-" : applicationDetailsModel.area_of_flat_sq_mt
        self.lblAreaOfFlatSqFt_Built_up.text = applicationDetailsModel.area_of_flat_sq_ft_builtup.isEmptyString ? "-" : applicationDetailsModel.area_of_flat_sq_ft_builtup
        self.lblAreaOfFlatSqMt_Built_up.text = applicationDetailsModel.area_of_flat_sq_mt_builtup.isEmptyString ? "-" : applicationDetailsModel.area_of_flat_sq_mt_builtup
        self.lblIs_Parking.text = applicationDetailsModel.is_parking.isEmptyString ? "-" : applicationDetailsModel.is_parking
        self.lblNumberOfParkingUnderTransfer.text = getString(anything: applicationDetailsModel.parking_number.count)//applicationDetailsModel.number_of_parking
        
        // coming from api boolean
        let boolean = getBoolean(anything: applicationDetailsModel.is_parking)
        //self.lblIs_Parking.text = boolean ? "Yes" : "No"
        self.stackViewIsParking.subviews.forEach({
            $0.isHidden = $0 == self.lblIs_Parking.superview ? false : !boolean
        })
        
        if boolean {
            self.stackViewParkingNumbers.subviews.forEach({ $0.removeFromSuperview() })
            for dic in self.applicationDetailsModel.parking_number {
                let pnView = ParkingNumbersLabel.loadNib()

                pnView.lblTitles.forEach({
                    $0.font = .boldTitleFont
                    $0.textColor = .customBlue
                    $0.numberOfLines = 0
                })
                
                pnView.lblValues.forEach({
                    $0.font = .regularValueFont
                    $0.textColor = .customBlack
                    $0.numberOfLines = 0
                })
                
                pnView.viewMain.layer.borderWidth = 2
                pnView.viewMain.layer.borderColor = UIColor.lightGray.cgColor

                pnView.lblParking.text = dic.parking
                pnView.lblPASqFTCarpet.text = dic.parking_area_sq_ft
                pnView.lblPASqMTCarpet.text = dic.parking_area_sq_mt
                pnView.lblPASqFTBuiltUp.text = dic.parking_area_sq_ft_builtup
                pnView.lblPASqMTBuiltUp.text = dic.parking_area_sq_mt_builtup
                pnView.lblParkingNumber.text = dic.parking_number

                self.stackViewParkingNumbers.addArrangedSubview(pnView)
            }
        }
        
        //Previous Transfer Details
        previousTransferDetailList = applicationDetailsModel.previous_transfer_details
        
        self.setupTableView()
        
        
        /*let documents_keys = self.applicationDetailsModel.documents.keys
        var uplioadingDocument : [(doc_title:String, doc_url:String)] = []
        
        for document_key in documents_keys {
            if let dict = self.applicationDetailsModel.documents[document_key] as? Dictionary,
               !getString(anything: dict["document"]).isStringEmpty {
                uplioadingDocument.append((getString(anything: dict["title"]), getString(anything: dict["document"])))
            }
        }*/
        
        //Documents
        if self.applicationDetailsModel.documents.count > 0 {
            self.stackViewDocument.subviews.forEach({
                $0.removeFromSuperview()
            })
            
            self.applicationDetailsModel.documents.enumerated().forEach({
                
                let model = DocumentModel()
                model.title = $0.element.title
                model.date = Date()
                model.url = URL(string: $0.element.url) ?? URL(string: $0.element.url.urlQueryAllowed)
                
                let docView = DocumentView.loadNib()
                docView.isShowFileSize = true
                docView.configureViewForShowing(model: model, btnTag: $0.offset + 1)
                docView.showDocumentViewChangeImage(isEdit: false)
                docView.btnDocument.accessibilityHint = model.url?.absoluteString
                docView.btnClose.accessibilityHint = model.url?.absoluteString
                docView.btnDocument.addTarget(self, action: #selector(btnDocumentOpenClick), for: .touchUpInside)
                docView.btnClose.addTarget(self, action: #selector(btnDocumentOpenClick), for: .touchUpInside)
                
                self.stackViewDocument.addArrangedSubview(docView)
            })
            
            viewDocumentMain.isHidden = false
        } else {
            viewDocumentMain.isHidden = true
        }
        
        //Clarifications Documents
        if self.applicationDetailsModel.user_clarification_details.count > 0 {
            self.stackViewClarificationDocument.subviews.forEach({
                $0.removeFromSuperview()
            })
            
            self.applicationDetailsModel.user_clarification_details.enumerated().forEach({
                
                let model = DocumentModel()
                model.title = $0.element.title
//                model.date = Date()
                model.url = URL(string: $0.element.url) ?? URL(string: $0.element.url.urlQueryAllowed)
                
                let docView = DocumentView.loadNib()
                docView.isShowFileSize = true
                docView.configureViewForShowing(model: model, btnTag: $0.offset + 1)
                docView.showDocumentViewChangeImage(isEdit: false)
                docView.btnDocument.accessibilityHint = model.url?.absoluteString
                docView.btnClose.accessibilityHint = model.url?.absoluteString
                docView.btnDocument.addTarget(self, action: #selector(btnDocumentOpenClick), for: .touchUpInside)
                docView.btnClose.addTarget(self, action: #selector(btnDocumentOpenClick), for: .touchUpInside)
                
                docView.lblDocumentName.text = RemarksLabels.Remark
                docView.lblDocumentInfo.text = model.title
                
                docView.lblDocumentInfo.textColor = .customBlue
                docView.lblDocumentInfo.numberOfLines = 0
                docView.lblDocumentInfo.font = .boldValueFont
                docView.lblDocumentName.textColor = .themeDarkBlueClr

                self.stackViewClarificationDocument.addArrangedSubview(docView)
            })
            
            viewClarificationDocumentMain.isHidden = false
        } else {
            viewClarificationDocumentMain.isHidden = true
        }
        
        
    }
    
    fileprivate func setupTableView() {
        if previousTransferDetailsDataSourceDelegate == nil {
            previousTransferDetailsDataSourceDelegate = PreviousTransferDetailsDataSourceDelegate(list: previousTransferDetailList, tbl: tblViewPreviousTransferDetails, _delegate: nil)
            previousTransferDetailsDataSourceDelegate.btnObtainedNOCClickedCallBack = btnObtainedNOCClickedCallBack
        } else {
            previousTransferDetailsDataSourceDelegate.reloadData(list: previousTransferDetailList)
        }
        
    }
    
    private func btnObtainedNOCClickedCallBack(indexClicked: Int) {
//        let dvc = PDFDocumentViewerViewController.instantiate(appStoryboard: .pdfDocumentViewer)
//        dvc.documentURLStr = previousTransferDetailList[indexClicked].obtained_noc_doc
//        self.navigationController?.pushViewController(dvc, animated: true)
        openURL(urlString:  previousTransferDetailList[indexClicked].obtained_noc_doc)
    }
    
    //MARK: - Button Action Methods
    override func btnFavourite(_ sender: UIButton) {
        
        var list = [KTItem]()

        self.applicationDetailsModel.action.forEach { doc in
            let item = KTItem(id:nil, title: doc.title, image:nil)
            list.append(item)
        }
        
        if self.applicationDetailsModel.add_challan {
            let item1 = KTItem(id:nil, title:Headers.AddChallanDetails, image:nil)
            list.append(item1)
        }
        
        if self.arrChallanList.count > 0 {
            let itemList = KTItem(id:nil, title: Headers.ChallanList, image:nil)
            list.append(itemList)
        }
        
        if self.applicationDetailsModel.add_clarification {
            let item2 = KTItem(id:nil, title:Headers.addClarificationDocument, image:nil)
            list.append(item2)
        }
        
        DispatchQueue.main.async {
            self.showPopupOn(sender, list: list) { (item) in
            let stringValue = getString(anything: item.title)

            if stringValue == Headers.AddChallanDetails {
                let dvc = AddDetailsViewController.instantiate(appStoryboard: .AddDetails)
                dvc.applications_id = self.noc_id
                dvc.applications_no = self.applicationDetailsModel.noc_id
                dvc.arrList = self.arrChallanList
                dvc.previousVc = self.view
                dvc.recovery_amount = self.applicationDetailsModel.recovery_amount
                self.navigationController?.pushViewController(dvc, animated: true)
               
            } else if stringValue == Headers.ChallanList {
                let dvc = ChallanViewController.instantiate(appStoryboard: .challanList)
                dvc.arrList = self.arrChallanList
                dvc.applicationId = self.noc_id
                self.navigationController?.pushViewController(dvc, animated: true)
                
            } else if stringValue == Headers.addClarificationDocument {
                let dvc = AddClarificationDocumentViewController.instantiate(appStoryboard: .addClarificationDocument)
                dvc.application_id = self.noc_id
                self.navigationController?.pushViewController(dvc, animated: true)
                
            } else if let doc = self.applicationDetailsModel.action.first(where: { $0.title == stringValue }) {
//                let dvc = PDFDocumentViewerViewController.instantiate(appStoryboard: .pdfDocumentViewer)
//                dvc.documentURLStr = doc.url
//                self.navigationController?.pushViewController(dvc, animated: true)
                openURL(urlString:  doc.url)
            }
          }
        }
    }
    
    @objc func btnDocumentOpenClick(sender: UIButton) {
//        let dvc = PDFDocumentViewerViewController.instantiate(appStoryboard: .pdfDocumentViewer)
//        dvc.documentURLStr = getString(anything: sender.accessibilityHint)
//        self.navigationController?.pushViewController(dvc, animated: true)
        openURL(urlString: getString(anything: sender.accessibilityHint))
    }
    
    @objc fileprivate func btnRemarksClikced(sender: UIButton) {
        let vc = RemarksListPopupViewController.instantiate(appStoryboard: .remarksListPopup)
        vc.type = 0
        vc.arrayData = self.applicationDetailsModel.remark_arr
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc fileprivate func btnQueryRejectionRemarksClikced(sender: UIButton) {
        let vc = RemarksListPopupViewController.instantiate(appStoryboard: .remarksListPopup)
        vc.type = 1
        vc.arrayData = self.applicationDetailsModel.remark_arr_reject
//        let vc = RemarksTabViewController.instantiate(appStoryboard: .RemarksTabContainer)
//        vc.selectedIndex = 2
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc fileprivate func btnNOCCertificateRemarksClikced(sender: UIButton) {
        let vc = RemarksListPopupViewController.instantiate(appStoryboard: .remarksListPopup)
        vc.type = 2
        vc.arrayData = self.applicationDetailsModel.remark_arr_certi
        self.navigationController?.pushViewController(vc, animated: true)
        
//        let dvc = RemarksListPopupViewController.instantiate(appStoryboard: .remarksListPopup)
//        dvc.modalPresentationStyle = .overCurrentContext
//        dvc.strHeaderTitle = "NOC Certificate Remarks"
//        dvc.arrayData = tempData
//        self.present(dvc, animated: false, completion: nil)
    }
    
    @objc fileprivate func btnApplicationDetailsExpandCollapseClicked(sender: UIButton) {
        helpingExpandCollapse(self.stackViewApplicationDetails, self.imgApplicationDetails)
    }
    
    @objc fileprivate func btnPropertyDetailsExpandCollapseClicked(sender: UIButton) {
        helpingExpandCollapse(self.stackViewPropertyDetails, self.imgPropertyDetails)
    }
    
    @objc fileprivate func btnBuildingDetailsExpandCollapseClicked(sender: UIButton) {
        helpingExpandCollapse(self.stackViewBuildingDetails, self.imgBuildingDetails)
    }
    
    @objc fileprivate func btnFlatShopDetailsExpandCollapseClicked(sender: UIButton) {
        helpingExpandCollapse(self.stackViewFlatShopDetails, self.imgFlatShopDetails)
    }
    
    @objc fileprivate func btnPreviousTransferDetailsClicked(sender: UIButton) {
        helpingExpandCollapse(self.stackViewPreviousTransferDetails, self.imgPreviousTransferDetails)
    }
    
    @objc fileprivate func btnDocumentExpandCollapseClicked(sender: UIButton) {
        helpingExpandCollapse(self.stackViewDocument, self.imgDocument)
    }
    
    @objc fileprivate func btnClarificationDocumentExpandCollapseClicked(sender: UIButton) {
        helpingExpandCollapse(self.stackViewClarificationDocument, self.imgClarification)
    }
    
    private func helpingExpandCollapse(_ stackView: UIStackView, _ imgView: UIImageView) {
        self.stackViewOtherDetails.forEach({
            $0.isHidden = $0 == stackView ? !$0.isHidden : true
        })
        
        let hidden = stackView.isHidden
        let img = hidden ? UIImage.ic_plus : UIImage.ic_minus
        
        self.imgsPlusMinus.forEach({
            $0.image = $0 == imgView ? img : .ic_plus
        })
    }
    
}

extension ApplicationDetailsViewController : UIPopoverPresentationControllerDelegate {
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
}

//WebServices
extension ApplicationDetailsViewController {

    func getNOCAppDetails() {
        self.nocAppDetailsWebServicesModel.noc_id = self.noc_id
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.mainView.isHidden = true
        nocAppDetailsWebServicesModel.getNOCAppDetails(block:  { (json) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            self.applicationDetailsModel = json?.applicationDetail ?? ApplicationDetailsModel(dict: [:])
            self.setupValues()
            self.mainView.isHidden = false
            self.updateOptions()
            self.getChallanListData()
        })
    }
    
    func getChallanListData() {
        webServiceModel.getChallanListApi(applications_id: self.noc_id) { list, isSucess, totalRecords, txtmessage  in
            if isSucess {
                self.arrChallanList = list
                self.updateOptions()
            }
        }
    }
    
    func updateOptions() {
        if self.applicationDetailsModel.add_clarification || self.applicationDetailsModel.add_challan || self.applicationDetailsModel.action.count > 0 || self.arrChallanList.count > 0 {
            self.showFavourite()
            self.viewHeader.btnFavourite.setImage(.dots, for: .normal)
        } else {
            self.viewHeader.btnFavourite.superview?.isHidden = true
        }
    }
}
