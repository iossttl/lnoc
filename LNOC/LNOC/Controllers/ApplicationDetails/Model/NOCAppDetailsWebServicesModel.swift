//
//  PlayerListWebServiceModel.swift
//  Khel Mahakumbh
//
//  Created by Dhruv Patel on 27/12/21.
//
import UIKit

class NOCAppDetailsWebServicesModel : NSObject {
    
    //MARK:- Variables
    //Public
    var noc_id = ""
    
    //MARK:- Enum
    enum Keys: String {
        case noc_id = "noc_id"
        case user_id = "user_id"
    }
    
    fileprivate var getNOCAppDetailsUrl:String {
        BASEURL + API.applicationdetail
    }
    
    private func nocAppDetailsDict() -> Dictionary {
        var commonDict = WShandler.commonDict()
        commonDict[Keys.user_id.rawValue] = UserPreferences.string(forKey: UserPreferencesKeys.UserInfo.userID)
        commonDict[Keys.noc_id.rawValue] = getString(anything: noc_id).encryptStr
        return commonDict
    }
        
    func getNOCAppDetails(block: @escaping ((NOCAppDetailsResponseModel?) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            let param = self.nocAppDetailsDict()
            print(param,"param")
            WShandler.shared.postWebRequest(urlStr: self.getNOCAppDetailsUrl, param: param) { (json, flag) in
                var responseModel: NOCAppDetailsResponseModel?
                if (flag == 200) {
                        if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                                if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == 2 {
                                    UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                                    UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                                    UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                                    UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                                    UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                                    UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                                    Global.shared.changeInitialVC(animated: true, direction: .toLeft)
                                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                                } else {
                                    responseModel = NOCAppDetailsResponseModel(dict: json)
                                }
                            
                        } else {
                            Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        }
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel)
            }
            
        }  else {
            block(nil)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
}



