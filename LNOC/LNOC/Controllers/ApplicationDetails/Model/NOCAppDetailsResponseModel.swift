//
//  NOCAppDetailsResponseModel.swift
//  LNOC
//
//  Created by Gunjan on 21/01/22.
//

import Foundation

class NOCAppDetailsResponseModel {
    let resultFlag: Bool
    let message: String
    let applicationDetail:ApplicationDetailsModel
    
    enum Keys:String {
        case applicationDetail = "applicationDetail"
    }
    
    init(dict: JSONObject) {
        resultFlag = getBoolean(anything: dict[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dict[CommonAPIConstant.key_message]).decryptStr
        
        if let dictData = dict[Keys.applicationDetail.rawValue] as? Dictionary {
            applicationDetail = ApplicationDetailsModel(dict: dictData)
        } else {
            applicationDetail = ApplicationDetailsModel(dict: [:])
        }
    }
}
