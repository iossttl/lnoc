//
//  ApplicationDetailsModel.swift
//  LNOC
//
//  Created by Pradip on 29/12/21.
//

import UIKit

class ApplicationDetailsModel:NSObject {

    var currently_application_presented_to:String
    var current_application_status: String
    var interim_order_status: String
    var interim_order_template: String
    var summary_note_template:String
    var final_order_status: String
    var challan_verification_status: String
    var challan_remark: String
    var remark: String
        
    let first_name:String
    let middle_name:String
    let last_name:String
    let noctypes:String
    let division:String
    let name_of_the_transferee:String
    let is_transfer_within_family:String
    let relation_with_transferer:String
    let cs_number:String
    let name_of_society:String
    let present_name_of_lease:String
    let name_of_building_premises:String
    let total_floors_of_building:String
    let no_of_lift:String
    let completion_of_building:String
    let society_chairman_name:String
    let chairman_contact_no:String
    let society_secretary_name:String
    let secretary_contact_no:String
    let society_email_id:String
    let no_of_members:String
    let flat_number:String
    let floor_of_flat:String
    let area_of_flat_sq_ft:String
    let area_of_flat_sq_mt:String
    let area_of_flat_sq_ft_builtup:String
    let area_of_flat_sq_mt_builtup:String
    let is_parking:String
    let number_of_parking:String
    var parking_number : [ParkingNumberModel] = []
    var previous_transfer_details:[PreviousTransferDetailsModel] = []
    var documents:[AppDetailsDocumentsModel] = []
    var user_clarification_details : [UserClarificationDocumentsModel] = []
    let remark_arr : [RemarksListModel]
    let remark_arr_certi : [RemarksListModel]
    let remark_arr_reject : [RemarksListModel]
    let type_of_premises : String
    let action : [docModel]
    let add_challan : Bool
    let add_clarification : Bool
    let recovery_amount : Double
    let noc_id : String
    let company_name : String
    let name_of_transferor : String
    let applicant_address : String
    let society_address : String
    let ownership_of : String
    let salutation : String
    let second_name_of_the_transferee : String
    let third_name_of_the_transferee : String
    
    enum keys: String {
        case currently_application_presented_to = "currently_application_presented_to"
        case current_application_status = "current_application_status"
        case interim_order_status = "interim_order_status"
        case interim_order_template = "interim_order_template"
        case summary_note_template = "summary_note_template"
        case final_order_status = "final_order_status"
        case challan_verification_status = "challan_verification_status"
        case challan_remark = "challan_remark"
        case remark = "remark"
        case remark_arr = "remark_arr"
        case remark_arr_certi = "remark_arr_certi"
        case remark_arr_reject = "remark_arr_reject"

        case first_name = "first_name"
        case middle_name = "middle_name"
        case last_name = "last_name"
        case noctypes = "noctypes"
        case division = "division"
        case name_of_the_transferee = "name_of_the_transferee"
        case is_transfer_within_family = "is_transfer_within_family"
        case relation_with_transferer = "relation_with_transferer"
        case cs_number = "cs_number"
        case name_of_society = "name_of_society"
        case present_name_of_lease = "present_name_of_lease"
        case name_of_building_premises = "name_of_building_premises"
        case total_floors_of_building = "total_floors_of_building"
        case no_of_lift = "no_of_lift"
        case completion_of_building = "completion_of_building"
        case society_chairman_name = "society_chairman_name"
        case chairman_contact_no = "chairman_contact_no"
        case society_secretary_name = "society_secretary_name"
        case secretary_contact_no = "secretary_contact_no"
        case society_email_id = "society_email_id"
        case no_of_members = "no_of_members"
        case flat_number = "flat_number"
        case floor_of_flat = "floor_of_flat"
        case area_of_flat_sq_ft = "area_of_flat_sq_ft"
        case area_of_flat_sq_mt = "area_of_flat_sq_mt"
        case area_of_flat_sq_ft_builtup = "area_of_flat_sq_ft_builtup"
        case area_of_flat_sq_mt_builtup = "area_of_flat_sq_mt_builtup"
        case is_parking = "is_parking"
        case number_of_parking = "number_of_parking"
        case parking_number = "parking_number"
        case previous_transfer_details = "previous_transfer_details"
        case documents = "doc"
        case user_clarification_details = "user_clarification_details"
        case type_of_premises = "type_of_premises"
        case add_challan = "add_challan"
        case add_clarification = "add_clarification"
        case action = "action"
        case noc_id = "noc_id"
        case recovery_amount = "recovery_amount"
        case company_name = "company_name"
        case name_of_transferor = "name_of_transferor"
        case applicant_address = "applicant_address"
        case society_address = "society_address"
        case ownership_of = "ownership_of"
        case salutation = "salutation"
        case second_name_of_the_transferee = "second_name_of_the_transferee"
        case third_name_of_the_transferee = "third_name_of_the_transferee"
    }
    
    init(dict:Dictionary) {
        
        noc_id = getString(anything: dict[keys.noc_id.rawValue]).decryptStr
        currently_application_presented_to = getString(anything: dict[keys.currently_application_presented_to.rawValue]).decryptStr
        current_application_status = getString(anything: dict[keys.current_application_status.rawValue]).decryptStr
        interim_order_status = getString(anything: dict[keys.interim_order_status.rawValue]).decryptStr
        interim_order_template = getString(anything: dict[keys.interim_order_template.rawValue]).decryptStr
        summary_note_template = getString(anything: dict[keys.summary_note_template.rawValue]).decryptStr
        final_order_status = getString(anything: dict[keys.final_order_status.rawValue]).decryptStr
        challan_verification_status = getString(anything: dict[keys.challan_verification_status.rawValue]).decryptStr
        challan_remark = getString(anything: dict[keys.challan_remark.rawValue]).decryptStr
        remark = getString(anything: dict[keys.remark.rawValue]).decryptStr
        type_of_premises = getString(anything: dict[keys.type_of_premises.rawValue]).decryptStr

        company_name = getString(anything: dict[keys.company_name.rawValue]).decryptStr
        first_name = getString(anything: dict[keys.first_name.rawValue]).decryptStr
        middle_name = getString(anything: dict[keys.middle_name.rawValue]).decryptStr
        last_name = getString(anything: dict[keys.last_name.rawValue]).decryptStr
        noctypes = getString(anything: dict[keys.noctypes.rawValue]).decryptStr
        division = getString(anything: dict[keys.division.rawValue]).decryptStr
        name_of_the_transferee = getString(anything: dict[keys.name_of_the_transferee.rawValue]).decryptStr
        is_transfer_within_family = getString(anything: dict[keys.is_transfer_within_family.rawValue]).decryptStr
        relation_with_transferer = getString(anything: dict[keys.relation_with_transferer.rawValue]).decryptStr
        cs_number = getString(anything: dict[keys.cs_number.rawValue]).decryptStr
        name_of_society = getString(anything: dict[keys.name_of_society.rawValue]).decryptStr
        present_name_of_lease = getString(anything: dict[keys.present_name_of_lease.rawValue]).decryptStr
        name_of_building_premises = getString(anything: dict[keys.name_of_building_premises.rawValue]).decryptStr
        total_floors_of_building = getString(anything: dict[keys.total_floors_of_building.rawValue]).decryptStr
        no_of_lift = getString(anything: dict[keys.no_of_lift.rawValue]).decryptStr
        completion_of_building = getString(anything: dict[keys.completion_of_building.rawValue]).decryptStr
        society_chairman_name = getString(anything: dict[keys.society_chairman_name.rawValue]).decryptStr
        chairman_contact_no = getString(anything: dict[keys.chairman_contact_no.rawValue]).decryptStr
        society_secretary_name = getString(anything: dict[keys.society_secretary_name.rawValue]).decryptStr
        secretary_contact_no = getString(anything: dict[keys.secretary_contact_no.rawValue]).decryptStr
        society_email_id = getString(anything: dict[keys.society_email_id.rawValue]).decryptStr
        no_of_members = getString(anything: dict[keys.no_of_members.rawValue]).decryptStr
        flat_number = getString(anything: dict[keys.flat_number.rawValue]).decryptStr
        floor_of_flat = getString(anything: dict[keys.floor_of_flat.rawValue]).decryptStr
        area_of_flat_sq_ft = getString(anything: dict[keys.area_of_flat_sq_ft.rawValue]).decryptStr
        area_of_flat_sq_mt = getString(anything: dict[keys.area_of_flat_sq_mt.rawValue]).decryptStr
        area_of_flat_sq_ft_builtup = getString(anything: dict[keys.area_of_flat_sq_ft_builtup.rawValue]).decryptStr
        area_of_flat_sq_mt_builtup = getString(anything: dict[keys.area_of_flat_sq_mt_builtup.rawValue]).decryptStr
        is_parking = getString(anything: dict[keys.is_parking.rawValue]).decryptStr
        number_of_parking = getString(anything: dict[keys.number_of_parking.rawValue]).decryptStr
        recovery_amount = getDouble(anything: getString(anything: dict[keys.recovery_amount.rawValue]).decryptStr)
        
        if let parkDict = dict[keys.parking_number.rawValue] as? [Dictionary] {
            self.parking_number = parkDict.map({ ParkingNumberModel(dict: $0)})
        }
        
        if let arr = dict[keys.previous_transfer_details.rawValue] as? [Dictionary] {
            previous_transfer_details = arr.map({PreviousTransferDetailsModel(dict: $0)})
        }
         
        if let arrDoc = dict[keys.documents.rawValue] as? [Dictionary] {
            documents = arrDoc.map({AppDetailsDocumentsModel(dict: $0)}).filter({ !$0.url.isEmptyString })
        }
        
        if let arrDoc = dict[keys.user_clarification_details.rawValue] as? [Dictionary] {
            user_clarification_details = arrDoc.map({ UserClarificationDocumentsModel(dict: $0)})
        }
        
        let arr = dict[keys.remark_arr.rawValue] as? [Dictionary] ?? []
        self.remark_arr = arr.map({ RemarksListModel(dict: $0)})
        
        let arr2 = dict[keys.remark_arr_certi.rawValue] as? [Dictionary] ?? []
        remark_arr_certi = arr2.map({ RemarksListModel(dict: $0)})
        
        let arr3 = dict[keys.remark_arr_reject.rawValue] as? [Dictionary] ?? []
        remark_arr_reject = arr3.map({ RemarksListModel(dict: $0)})
        
        add_challan = (getString(anything: dict[keys.add_challan.rawValue]).decryptStr == "Y")
        add_clarification = (getString(anything: dict[keys.add_clarification.rawValue]).decryptStr == "Y")
        action = (dict[keys.action.rawValue] as? [Dictionary] ?? [[:]]).map({ docModel.init(dict: $0) })

        name_of_transferor = getString(anything: dict[keys.name_of_transferor.rawValue]).decryptStr
        applicant_address = getString(anything: dict[keys.applicant_address.rawValue]).decryptStr
        society_address = getString(anything: dict[keys.society_address.rawValue]).decryptStr
        ownership_of = getString(anything: dict[keys.ownership_of.rawValue]).decryptStr
        salutation = getString(anything: dict[keys.salutation.rawValue]).decryptStr
        second_name_of_the_transferee = getString(anything: dict[keys.second_name_of_the_transferee.rawValue]).decryptStr
        third_name_of_the_transferee = getString(anything: dict[keys.third_name_of_the_transferee.rawValue]).decryptStr
        
        super.init()
    }
}


class PreviousTransferDetailsModel {
    
    let name_of_transferer:String
    let name_of_transferee:String
    let year_of_transfer:String
    let type_of_transfer:String
    let noc_obtained:String
    let obtained_noc_doc:String
    
    
    enum Keys: String {
        case name_of_transferer = "name_of_transferer"
        case name_of_transferee = "name_of_transferee"
        case year_of_transfer = "year_of_transfer"
        case type_of_transfer = "type_of_transfer"
        case noc_obtained = "noc_obtained"
        case obtained_noc_doc = "obtained_noc_doc"
    }
    
    
    
    
    init(dict:Dictionary) {
        name_of_transferer = getString(anything: dict[Keys.name_of_transferer.rawValue]).decryptStr
        name_of_transferee = getString(anything: dict[Keys.name_of_transferee.rawValue]).decryptStr
        year_of_transfer = getString(anything: dict[Keys.year_of_transfer.rawValue]).decryptStr
        type_of_transfer = getString(anything: dict[Keys.type_of_transfer.rawValue]).decryptStr
        noc_obtained = getString(anything: dict[Keys.noc_obtained.rawValue]).decryptStr
        obtained_noc_doc = getString(anything: dict[Keys.obtained_noc_doc.rawValue]).decryptStr
    }
}

class AppDetailsDocumentsModel {
    
    let title: String
    let url: String
    
    enum Keys:String {
        case title = "title"
        case url = "url"
    }
    
    init(dict:Dictionary) {
        title = getString(anything: dict[Keys.title.rawValue]).decryptStr
        url = getString(anything: dict[Keys.url.rawValue]).decryptStr
    }
}

class UserClarificationDocumentsModel {
    
    let title: String
    let url: String
    
    enum Keys:String {
        case title = "user_clarification_remark"
        case url = "user_clarification_doc"
    }

    init(dict:Dictionary) {
        title = getString(anything: dict[Keys.title.rawValue]).decryptStr
        url = getString(anything: dict[Keys.url.rawValue]).decryptStr
    }
}

class RemarksListModel {

    let sr_no: String
    let from: String
    let date: String
    let remarks: String
    let currently_presented_to: String
    
    enum Keys:String {
        case sr_no = "sr_no"
        case from = "from"
        case date = "date"
        case remarks = "remarks"
        case currently_presented_to = "currently_presented_to"
    }
    
    init(dict:Dictionary) {
        sr_no = getString(anything: dict[Keys.sr_no.rawValue]).decryptStr
        from = getString(anything: dict[Keys.from.rawValue]).decryptStr
        date = getString(anything: dict[Keys.date.rawValue]).decryptStr
        remarks = getString(anything: dict[Keys.remarks.rawValue]).decryptStr
        currently_presented_to = getString(anything: dict[Keys.currently_presented_to.rawValue]).decryptStr
    }
}

class ParkingNumberModel {

    let parking : String
    let parking_area_sq_ft : String
    let parking_area_sq_ft_builtup : String
    let parking_area_sq_mt : String
    let parking_area_sq_mt_builtup : String
    let parking_number : String

    enum Keys:String {
        case parking = "parking"
        case parking_area_sq_ft = "parking_area_sq_ft"
        case parking_area_sq_ft_builtup = "parking_area_sq_ft_builtup"
        case parking_area_sq_mt = "parking_area_sq_mt"
        case parking_area_sq_mt_builtup = "parking_area_sq_mt_builtup"
        case parking_number = "parking_number"
    }
    
    init(dict:Dictionary) {
        parking = getString(anything: dict[Keys.parking.rawValue]).decryptStr
        parking_area_sq_ft = getString(anything: dict[Keys.parking_area_sq_ft.rawValue]).decryptStr
        parking_area_sq_ft_builtup = getString(anything: dict[Keys.parking_area_sq_ft_builtup.rawValue]).decryptStr
        parking_area_sq_mt = getString(anything: dict[Keys.parking_area_sq_mt.rawValue]).decryptStr
        parking_area_sq_mt_builtup = getString(anything: dict[Keys.parking_area_sq_mt_builtup.rawValue]).decryptStr
        parking_number = getString(anything: dict[Keys.parking_number.rawValue]).decryptStr
    }
}
