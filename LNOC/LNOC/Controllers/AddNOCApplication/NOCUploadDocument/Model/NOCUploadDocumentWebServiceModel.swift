//
//  NOCUploadDocumentWebServiceModel.swift
//  LNOC
//
//  Created by Pradip on 31/12/21.
//

import UIKit

class NOCUploadDocumentWebServiceModel : NSObject {
    
    var uploadDocument: [DocumentModel] = []
    var ms_noc_types_id: String = ""
    var doc_Index:Int = 0
    var uploadedDict: Dictionary = [:]
    
    fileprivate var getAddDetailsUrl : String {
         return BASEURL //+ API.register
     }
   
    enum keys : String {
        case applicationNo = "applicationNo"
        case challanNumber = "challanNumber"
        case amount = "amount"
        case accountHead = "accountHead"
        case bankName = "bankName"
        case challandate = "challandate"

        case affidavitDoc = "affidavitDoc"
        case challanDoc = "challanDoc"
        case otherDoc = "otherDoc"
        case ms_noc_types_id = "ms_noc_types_id"
        case uploaded_document_data = "uploaded_document_data"
    }
    
    fileprivate var getNocDocUrl : String {
        return BASEURL + API.getNOC_documents
    }
    
    fileprivate var getUploadDocUrl : String {
        return BASEURL + API.upload_nocdocument
    }
    
    //MARK:- Initializer
    override init() {
        super.init()
    }
    
     
//    //Register
//     func getRegister(block:@escaping ((UserModel?) -> Swift.Void)) {
//         if (WShandler.shared.CheckInternetConnectivity()) {
//            let uuid = UUID().uuidString
//            var param = WShandler.commonDict()//(uuid: uuid)
//            let dict = registerDetailsDict(uuid: uuid)
//            for key in registerDetailsDict.keys {
//                param[key] = dict[key]
//            }
//
//            print(param,"param")
//             WShandler.shared.postWebRequest(urlStr: getRegister, param: param) { (json, flag) in
//                 var responseModel:UserModel?
//                 if flag == 200 {
//                     if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
//                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]))
//                        print(json)
//                        if let dict = json[CommonAPIConstant.key_userDetails] as? Dictionary {
//                            responseModel = UserModel(dict: dict, uuid: uuid)
//                        }
//
//                     } else {
//                        Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]))
//                     }
//                 } else {
//                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]))
//                 }
//                 block(responseModel)
//             }
//         } else {
//             block(nil)
//            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
//         }
//     }
    
    
    
    func getDocumentData(block: @escaping ((NOCDocumentResponseModel?) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            var param = WShandler.commonDict()
            param[keys.ms_noc_types_id.rawValue] = ms_noc_types_id.encryptStr
            print(param)
            WShandler.shared.postWebRequest(urlStr:getNocDocUrl,param: param) { (json, flag) in
                var responseModel:NOCDocumentResponseModel?
                print(json)
                if (flag == 200) {
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        responseModel = NOCDocumentResponseModel(dict: json)
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel)
            }
        }else{
            block(nil)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
    
    
    func addNOCUploadDocument(block: @escaping ((Dictionary?, _ isSucess : Bool) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            
            let param = WShandler.commonDict()//(uuid: uuid)
            
            var documentModels : [DocumentModel] = []
            
            let model = uploadDocument[doc_Index]
            
            let docModel = DocumentModel()
            docModel.key = model.key
            docModel.url = model.url
            docModel.type = model.type
            docModel.title = model.title
            documentModels.append(docModel)
            
            print(param,"param addapplication",documentModels.count)
            
            WShandler.shared.multipartWebRequest(urlStr: getUploadDocUrl, dictParams: param, documents: documentModels) { (json, flag) in
                var responseModel:Dictionary?
                var isSuccsees:Bool
                if (flag == 200) {
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        //responseModel = DropDownResponseModel(dictState: json)
                        if let dict = json[keys.uploaded_document_data.rawValue] as? [Dictionary]{
                            responseModel = dict[0]
                            isSuccsees = true
                        }else {
                            isSuccsees = false
                        }
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        isSuccsees = false
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    isSuccsees = false
                }
                block(responseModel,isSuccsees)
            }
        } else {
            block(nil,false)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
}
