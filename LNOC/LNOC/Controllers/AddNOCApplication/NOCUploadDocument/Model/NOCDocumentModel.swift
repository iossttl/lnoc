//
//  NOCDocumentModel.swift
//  LNOC
//
//  Created by Dhruv Patel on 04/02/22.
//

import Foundation
import UIKit
import MobileCoreServices

class NOCDocumentModel: NSObject {
    
    //MARK:- Variables
    let title: String
    var key: String
    var isUploaded:Bool
    
    //MARK:- Enum
    enum Keys: String {
        case title = "title"
        case key = "key"
    }
    
    //MARK:- Initializer
    init(dict: Dictionary) {
        title = getString(anything: dict[Keys.title.rawValue]).decryptStr
        key = getString(anything: dict[Keys.key.rawValue]).decryptStr
        print(key)
        isUploaded = false
    }
}

class NOCDocumentResponseModel: NSObject {
    let resultFlag: Bool
    let message: String
    let list: [NOCDocumentModel]

    enum Keys: String {
        case list = "list"
        case noc_documents = "noc_documents"
    }
    
    init(dict: [String : Any]) {
        resultFlag = getBoolean(anything: dict[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dict[CommonAPIConstant.key_message]).decryptStr
        if let arr = dict[Keys.noc_documents.rawValue] as? [Dictionary] {
            list = arr.map({ NOCDocumentModel(dict: $0) })
        } else {
            list = []
        }
        super.init()
    }
}

let dicNocTypeDocumentInfo : [NOCTypeDocumentInfo] = [
    NOCTypeDocumentInfo(allowedDocuTypes: arrdocTypesForProfile, doc_key: "share_certificate", maxDocumentLimit: 20, isRequired: true, allowedDocuFormatstr: CommonLabels.maxDocMBAllowed),
    
    NOCTypeDocumentInfo(allowedDocuTypes: arrdocTypesForProfile, doc_key: "noc_from_society", maxDocumentLimit: 20, isRequired: true, allowedDocuFormatstr: CommonLabels.maxDocMBAllowed),
    
    NOCTypeDocumentInfo(allowedDocuTypes: arrdocTypesForProfile, doc_key: "occupation_certificate_of_society", maxDocumentLimit: 20, isRequired: true, allowedDocuFormatstr: CommonLabels.maxDocMBAllowed),
    
    NOCTypeDocumentInfo(allowedDocuTypes: arrdocTypesForProfile, doc_key: "draft_copy", maxDocumentLimit: 100, isRequired: true, allowedDocuFormatstr: CommonLabels.maxDocMBAllowed),
    
    NOCTypeDocumentInfo(allowedDocuTypes: arrdocTypesForProfile, doc_key: "previous_transfer_property_doc", maxDocumentLimit: 100, isRequired: true, allowedDocuFormatstr: CommonLabels.maxDocMBAllowed),
    
    NOCTypeDocumentInfo(allowedDocuTypes: arrdocTypesForProfile, doc_key: "previous_noc", maxDocumentLimit: 20, isRequired: false, allowedDocuFormatstr: CommonLabels.maxDocMBAllowed),
        
    NOCTypeDocumentInfo(allowedDocuTypes: arrdocTypesForProfile, doc_key: "id_proof_buyer", maxDocumentLimit: 20, isRequired: true, allowedDocuFormatstr: CommonLabels.maxDocMBAllowed),

    NOCTypeDocumentInfo(allowedDocuTypes: arrdocTypesForProfile, doc_key: "id_proof_seller", maxDocumentLimit: 20, isRequired: true, allowedDocuFormatstr: CommonLabels.maxDocMBAllowed),
    
    NOCTypeDocumentInfo(allowedDocuTypes: arrdocTypesForProfile, doc_key: "will_probate_doc", maxDocumentLimit: 20, isRequired: false, allowedDocuFormatstr: CommonLabels.maxDocMBAllowed),

    NOCTypeDocumentInfo(allowedDocuTypes: ONLY_PDF_Types, doc_key: "other_doc", maxDocumentLimit: 20, isRequired: false, allowedDocuFormatstr: CommonLabels.maxPdfMBAllowed)
]

struct NOCTypeDocumentInfo {
    let allowedDocuTypes : [String]
    let doc_key : String
    let maxDocumentLimit : Double
    let isRequired : Bool
    let allowedDocuFormatstr : String
}
