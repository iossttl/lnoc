//
//  NOCUploadDocumentViewController.swift
//  LNOC
//
//  Created by Pradip on 31/12/21.
//

import UIKit

class NOCUploadDocumentViewController : UIViewController {
    
    @IBOutlet var mainView: UIView!
    @IBOutlet var viewsDocumentShadow : UIView!
    @IBOutlet weak var viewPrevious: GradientView!
    @IBOutlet weak var viewNext: GradientView!
    
    @IBOutlet var lblDocumentTitle: UILabel!
    
    @IBOutlet var stackViewDocument: UIStackView!
    
    // Variables
    // Public
    var nocDocumentType : NOCType = NOCType.sale
    var noc_Type_Id: String = ""
    var arrDocOptions: [NOCDocumentModel] = []

    
    // FilePrivate
    fileprivate var documentViews : [DocumentView2] = []
    fileprivate var documentPickerViewControllerDelegate : DocumentPickerViewControllerDelegate!
    fileprivate var webService = NOCUploadDocumentWebServiceModel()
    
    weak var registerContainerViewController : RegisterContainerViewController?
    
    //MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupConstraintIfIsIPad()
        setupView()
        setupStackView()
    }
    
    //MARK: - Private Methods
    private func setupConstraintIfIsIPad() {
        changeLeadingTrailingForiPad(view: self.mainView)
    }
    
    private func setupView() {
        self.view.backgroundColor = .lnocBackgroundColor
        
        self.lblDocumentTitle.textColor = .customBlue
        self.lblDocumentTitle.font = .boldLargeFont
        self.lblDocumentTitle.numberOfLines = 0
        
        self.viewsDocumentShadow.addShadow(shadowRadius: 6)
        self.viewsDocumentShadow.subviews.first?.viewWith(radius: 12)
        self.viewsDocumentShadow.backgroundColor = .clear
        self.viewsDocumentShadow.subviews.first?.backgroundColor = .white
        
        (self.viewNext.subviews.first as? UIButton)?.contentEdgeInsets = .init(top: 0, left: 16, bottom: 0, right: 16)
        
        self.viewNext.addGradient(ofColors: [UIColor.themeLightOrangeClr.cgColor, UIColor.themeDarkOrangeClr.cgColor], direction: .leftRight)
        self.viewPrevious.addGradient(ofColors: [UIColor.themeLightOrangeClr.cgColor, UIColor.themeDarkOrangeClr.cgColor], direction: .leftRight)
            self.viewNext.setFullCornerRadius()
            self.viewPrevious.setFullCornerRadius()
    }
    
   /* func setupStackView() {
        self.stackViewDocument?.subviews.forEach({
            $0.removeFromSuperview()
        })
        
        documentViews = []
        webService.uploadDocument = []
        
        nocDocumentType.uplopadDocuments.enumerated().forEach({
            let docView = DocumentView2.loadNib()
            
            docView.configureTitle(str: $0.element.title,
                                   btnTag: $0.offset,
                                   showFormatBtn: false,
                                   hideAsterisk: $0.element.isOptional,
                                   isFileNameShow: true)
            docView.btnDocument.addTarget(self, action: #selector(btnDocumentClicked), for: .touchUpInside)
            
            self.stackViewDocument?.addArrangedSubview(docView)
            
            documentViews.append(docView)
            
            let model = DocumentModel()
            model.key = $0.element.uploadingKey
            webService.uploadDocument.append(model)
        })
    }*/
    
    
    func setupStackView() {
        self.stackViewDocument?.subviews.forEach({
            $0.removeFromSuperview()
        })
        
        documentViews = []
        webService.uploadDocument = []
        
        /*nocDocumentType.uplopadDocuments.enumerated().forEach({
            let docView = DocumentView2.loadNib()
            
            docView.configureTitle(str: $0.element.title,
                                   btnTag: $0.offset,
                                   showFormatBtn: false,
                                   hideAsterisk: $0.element.isOptional,
                                   isFileNameShow: true)
            docView.btnDocument.addTarget(self, action: #selector(btnDocumentClicked), for: .touchUpInside)
            
            self.stackViewDocument?.addArrangedSubview(docView)
            
            documentViews.append(docView)
            
            let model = DocumentModel()
            model.key = $0.element.uploadingKey
            webService.uploadDocument.append(model)
        })*/
        
        var counterOffset = 0
        for model in arrDocOptions{
            
            let info = dicNocTypeDocumentInfo.first(where: { $0.doc_key == model.key })
            
            let docView = DocumentView2.loadNib()
            
            docView.configureTitle(str: model.title,
                                   btnTag: counterOffset,
                                   showFormatBtn: false,
                                   hideAsterisk: !(info?.isRequired ?? false),
                                   isFileNameShow: true)
            docView.btnDocument.accessibilityHint = model.key
            docView.btnDocument.addTarget(self, action: #selector(btnDocumentClicked), for: .touchUpInside)
            docView.btnUpload.addTarget(self, action: #selector(btnUploadClicked), for: .touchUpInside)
            docView.lblMaxSize.text = (info?.allowedDocuFormatstr ?? "").replacingOccurrences(of: "abc", with: getString(anything: info?.maxDocumentLimit))
            
            //CommonLabels.maxDocMBAllowed.replacingOccurrences(of: "abc", with: getString(anything: documentSizeLimit))// "Max. \(documentSizeLimit)MB (Allowed pdf/image)"
            
            self.stackViewDocument?.addArrangedSubview(docView)
            
            documentViews.append(docView)
            
            let docModel = DocumentModel()
            docModel.key = model.key
            webService.uploadDocument.append(docModel)
            counterOffset = counterOffset + 1
        }
    }
    
    //MARK:- Button Action Method
    
    @objc fileprivate func btnDocumentClicked(_ sender: UIButton) {
        UIView.hideKeyBoard()
        let info = dicNocTypeDocumentInfo.first(where: { $0.doc_key == (sender.accessibilityHint ?? "") })
        let documentPickerVC = UIDocumentPickerViewController(documentTypes: info?.allowedDocuTypes ?? [], in: .import)
        documentPickerViewControllerDelegate = DocumentPickerViewControllerDelegate(VC: documentPickerVC, delegate: self, sender: sender, file_Size: info?.maxDocumentLimit ?? 0)
        self.present(documentPickerVC, animated: true, completion: nil)
    }
    
    @objc fileprivate func btnUploadClicked(_ sender: UIButton) {
        
        let index = sender.tag
        webService.doc_Index = index
        addNOCApplicationWebServices(index: index)
    }
    
    @IBAction func btnNextOrSubmitClickHandler(_ sender: UIButton) {
        var boolean = true
        var titleStr = ""
//        for (index, item) in webService.uploadDocument.enumerated() {
//
//            if item.url == nil && !nocDocumentType.uplopadDocuments[index].isOptional {
//                boolean = false
//                titleStr = nocDocumentType.uplopadDocuments[index].title
//                break
//            }
//        }
        
        var isRequired = false
        
        if webService.uploadedDict.count != arrDocOptions.count {
            for model in arrDocOptions {
                isRequired = dicNocTypeDocumentInfo.first(where: { $0.doc_key == model.key })?.isRequired ?? false
                
                if !model.isUploaded && isRequired {
                    titleStr = model.title
                    boolean = false
                    break
                }
            }
        }
        
        if !boolean {
            Global.shared.showBanner(message: titleStr + " " + CommonLabels.isRequired)
        } else {
            fillModel()
            self.registerContainerViewController?.addNOCApplicationWebServices()
//            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    @IBAction func btnPreviousClickHandler(_ sender: UIButton) {
            registerContainerViewController?.removeOldController()
            registerContainerViewController?.selectedTab = 1
            registerContainerViewController?.addAccordingToSelectedTab()
    }
}

extension NOCUploadDocumentViewController : DocumentPickerDelegate {
    
    func DocumentPickerData(url: URL?, errorMsg: String, sender: UIView) {
        if let getURL = url {
            
            let index = sender.tag
            if webService.uploadDocument.count > index {
                
                if let change_URL = changeURL(url: getURL, name: webService.uploadDocument[index].key) {
                    
                    self.documentViews[sender.tag].imgViewDocument.image = .correct
                    self.documentViews[sender.tag].imgViewDocument.tintColor = .themeColor
                    self.documentViews[sender.tag].setFileName(fileName: getURL.lastPathComponent, showBtnUpload: true)
                    
                   /* var filename = getURL.lastPathComponent
                    
                    if getURL.lastPathComponent.components(separatedBy: ".").count > 1 {
                        filename = getURL.lastPathComponent.components(separatedBy: ".")[0]
                    }*/
                    
                    webService.uploadDocument[index].title = getURL.lastPathComponent
                    webService.uploadDocument[index].type = getURL.pathExtension
                    webService.uploadDocument[index].url = change_URL
                    self.arrDocOptions[index].isUploaded = false
                }
            }
            
        } else {
            Global.shared.showBanner(message: errorMsg)
        }
    }
    
}

extension NOCUploadDocumentViewController{
    
    func fillModel(){
        var arrFile = [Dictionary]()
        
        for (key,value) in webService.uploadedDict{
            print(value)
            if let dict = value as? Dictionary{
                arrFile.append(dict)
            }
        }
        
        self.registerContainerViewController?.webserviceModel.arrUplodedFile = arrFile
        
        
    }
    func getWebserviceforDocument() {
        self.arrDocOptions = []
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        webService.ms_noc_types_id = noc_Type_Id
        webService.getDocumentData { (responseModel) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if let response = responseModel {
                print(response)
                self.arrDocOptions.append(contentsOf: response.list)
                self.setupStackView()
            }
        }
    }
    
    func addNOCApplicationWebServices(index:Int) {
        view.endEditing(true)
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.webService.addNOCUploadDocument { responseModel,isSuccess in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if isSuccess{
                print(responseModel)
                if let dict = responseModel,let fileName = responseModel?["uploaded_file_name"] as? String, let keyName = responseModel?["key"] as? String {
                    self.webService.uploadedDict[keyName] = dict
                    self.documentViews[index].btnUpload.setTitle("Uploaded", for: .normal)
                    self.arrDocOptions[index].isUploaded = true
                }
              
            }
        }
    }
}


