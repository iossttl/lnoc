//
//  PreviousPropertyDetailWebServiceModel.swift
//  LNOC
//
//  Created by Dhruv Patel on 12/01/22.
//

import Foundation



class PreviousPropertyDetailWebServiceModel: NSObject {
    
    var NOCtype:String = ""
    
    fileprivate var getNOCTypeUrl : String {
         return BASEURL + API.getNOCType
     }
   
    enum keys : String {
        case nocType = "NOCType"
    
    }
    
    //MARK:- Initializer
    override init() {
        super.init()
    }
    
         

    
    
    func getNOCTypeDataData(block: @escaping ((DropDownResponseModel?) -> Swift.Void)) {
         
         if (WShandler.shared.CheckInternetConnectivity()) {
             let param = WShandler.commonDict()
             WShandler.shared.getWebRequest(urlStr:getNOCTypeUrl,param: param) { (json, flag) in
                 var responseModel:DropDownResponseModel?
                 if (flag == 200) {
                     if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                         responseModel = DropDownResponseModel(dictNOCType: json)
                     } else {
                         Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                     }
                 } else {
                     Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                 }
                 block(responseModel)
             }
             }else{
             block(nil)
             Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
         }
     }
    
}
