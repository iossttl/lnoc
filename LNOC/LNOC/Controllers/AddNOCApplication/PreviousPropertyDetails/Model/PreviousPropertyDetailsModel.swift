//
//  PreviousPropertyDetailsModel.swift
//  LNOC
//
//  Created by Dhruv on 31/12/21.
//

import UIKit

class PreviousPropertyDetailsModel: NSObject {
    
    
    var  nameofTransferer: String
    var  nameofTransferee: String
    var  year: String
    var  type: String
    var  type_Id : String
    var  NOCObtained : String
    var  ObtainedNOCDoc:String
    var  DocumentDict:JSONObject = [:]
    
    enum Keys: String {
        case nameofTransferer = "nameofTransferer"
        case nameofTransferee = "nameofTransferee"
        case year = "year"
        case type = "type"
        case type_id = "type_ID"
        case NOCObtained = "NOCObtained"
        case ObtainedNOCDoc = "ObtainedNOCDoc"
        case DocumentDict = "DocumentDict"
    }
    
    init(dict: Dictionary) {
        nameofTransferer = getString(anything: dict[Keys.nameofTransferer.rawValue])
        nameofTransferee = getString(anything: dict[Keys.nameofTransferee.rawValue])
        year = getString(anything: dict[Keys.year.rawValue])
        type = getString(anything: dict[Keys.type.rawValue])
        NOCObtained = getString(anything: dict[Keys.NOCObtained.rawValue])
        ObtainedNOCDoc = getString(anything: dict[Keys.ObtainedNOCDoc.rawValue])
        if let dict = dict[Keys.DocumentDict.rawValue] as? JSONObject {
            DocumentDict = dict
        }
        type_Id = getString(anything: dict[Keys.type_id.rawValue])
        super.init()
    }
}
