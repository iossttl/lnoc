//
//  PermissionForFilmingOperationMonumentTableViewCell.swift
//  ASI
//
//  Created by Vikram Jagad on 27/07/20.
//  Copyright © 2020 Silvertouch. All rights reserved.
//

import UIKit

class PreviousPropertyDetailsCell: UITableViewCell {

    //MARK:- Outlets
    
    //UIView
    @IBOutlet var viewMain : UIView!
    @IBOutlet var viewSeperator : UIView!
    
    //NSLayoutConstraint
    @IBOutlet var anchorViewMainBottomConstraint : NSLayoutConstraint!
    
    //UILabel
    @IBOutlet var lblNameofTransfererTitle: UILabel!
    @IBOutlet var lblNameofTransfererValue: UILabel!
    @IBOutlet var lblNameofTransfereeTitle: UILabel!
    @IBOutlet var lblNameofTransfereeValue: UILabel!
    @IBOutlet var lblYearTitle: UILabel!
    @IBOutlet var lblYearValue: UILabel!
    @IBOutlet var lblTypeOfTransferTitle: UILabel!
    @IBOutlet var lblTypeOfTransferValue: UILabel!
    @IBOutlet var lblNOCObtainedTitle: UILabel!
    @IBOutlet var lblNOCObtainedValue: UILabel!
    @IBOutlet var lblObtainedNOCTitle: UILabel!
    @IBOutlet var lblObtainedNOCValue: UILabel!
    
    //UIButton
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var btnEdit: UIButton!
    
    //Font Collections
    @IBOutlet var labelTitles : [UILabel]!
    @IBOutlet var labelValues : [UILabel]!
    
    //MARK:- Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        setupView()
        setupLabel()
    }
    
    //MARK:- Private Methods
    private func setupView() {
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        self.viewMain.backgroundColor = .customWhite
        self.viewSeperator.backgroundColor = .customgreenTheamClr
        
        btnDelete.setImage(.ic_header_delete, for: .normal)
        btnDelete.tintColor = .orange
        
        btnEdit.setImage(.ic_edit, for: .normal)
        btnEdit.tintColor = .darkBlue
    }
    
    private func setupLabel() {
        labelTitles.forEach { (lbl) in
            lbl.font = .regularValueFont
            lbl.textColor = .customBlackTitle
            lbl.numberOfLines = 0
        }
        
        labelValues.forEach { (lbl) in
            lbl.font = .semiboldValueFont
            lbl.textColor = .customBlack
            lbl.numberOfLines = 0
        }
    }
    
    //MARK:- Public Methods
    func configureCell(item: PreviousPropertyDetailsModel) {
        lblNameofTransfererValue.text = item.nameofTransferer
        lblNameofTransfereeValue.text = item.nameofTransferee
        lblYearValue.text = item.year
        lblTypeOfTransferValue.text = item.type
        lblNOCObtainedValue.text = item.NOCObtained
        lblObtainedNOCValue.text = item.ObtainedNOCDoc
        
        lblObtainedNOCValue.superview?.isHidden = item.ObtainedNOCDoc.isStringEmpty
        
    }
    
}
