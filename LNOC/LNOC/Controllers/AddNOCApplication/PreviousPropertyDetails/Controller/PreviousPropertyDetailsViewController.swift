//
//  PreviousPropertyDetailsViewController.swift
//  LNOC
//
//  Created by Dhruv on 31/12/21.
//

import UIKit

class PreviousPropertyDetailsViewController: HeaderViewController {

    //MARK: - Outlets
    //UIView
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewScrlViewMain: UIView!
    @IBOutlet weak var viewAddPreviousDetails: UIView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var viewPrevious: GradientView!
    @IBOutlet weak var viewNext: GradientView!
    @IBOutlet var  ObtainedNOCStackView: UIStackView!

    //UIScrollView
    @IBOutlet weak var scrlViewMain: UIScrollView!
    
    @IBOutlet weak var lblYear: UILabel!
    @IBOutlet weak var lblTypeOfTransfer: UILabel!
    @IBOutlet weak var lblYes: UILabel!
    @IBOutlet weak var lblNo: UILabel!
    
    @IBOutlet weak var imgYes: UIImageView!
    @IBOutlet weak var imgNo: UIImageView!
    @IBOutlet weak var imgPlusUpdate: UIImageView!
    @IBOutlet weak var imgDownArrow: UIImageView!
    
    
    //UIButton
    @IBOutlet weak var btnAddPreviousTransfer: UIButton!
    
    //UITableView
    @IBOutlet weak var tblViewPreviousTransfer: UITableView!
    
    @IBOutlet weak var tblViewPreviousTransferHeightConstraint: NSLayoutConstraint!
    
    //Collections
    @IBOutlet var lblTitles: [LocalizedLabel]!
    @IBOutlet var viewSeprators: [UIView]!
    @IBOutlet var tfValues: [UITextField]!
    
    @IBOutlet weak var tfNameOfTransferer: UITextField!
    @IBOutlet weak var tfNameOfTransferee: UITextField!
    @IBOutlet weak var tfYearOfTransfer: UITextField!
    
    // Variables
    var arrPreviousDetailsList: [PreviousPropertyDetailsModel] = []
    let uploadedObtainedDocument = DocumentView2.loadNib()
    
    var strNOCObtained = ""
    var selectedIndex = 0
    var fileName = ""
    var DocParamDict = JSONObject()
    
    var type_of_transfer = String()
    
    weak var registerContainerViewController : RegisterContainerViewController?
    private var arrNOCOptions = [DropDownModel]()
    fileprivate var previousPropertyTblViewDataSourceDelegate:PreviousPropertyTblViewDataSourceDelegate?
    fileprivate var documentPickerViewControllerDelegate: DocumentPickerViewControllerDelegate!
    fileprivate var webServiceModel = PreviousPropertyDetailWebServiceModel()

    
    //MARK: - Override Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpViewController()
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        guard let keyPathString = keyPath else {
            super.observeValue(forKeyPath: nil, of: object, change: change, context: context)
            return
        }
        
        if keyPathString == "contentSize" {
            
            if arrPreviousDetailsList.count > 0 {
                self.tblViewPreviousTransferHeightConstraint.constant = self.tblViewPreviousTransfer.contentSize.height
            }
            
            UIView.animate(withDuration: 0.25) { [weak self] in
                self?.view.layoutIfNeeded()
            }
            
            return
        }
        
        super.observeValue(forKeyPath: nil, of: object, change: change, context: context)
        
    }
    
    //MARK: - Private Methods
    
    //ViewController set up
    fileprivate func setUpViewController() {
        changeLeadingTrailingForiPad(view: self.view)
        changeHeightForiPad(view: imgPlusUpdate, constant: 10)
        self.view.layoutIfNeeded()
        setUpBtns()
        setupView()
        setUpTblView()
//        setUpScrlView()
        addObserve()
        setupLabels()
        //getWebServicesforDashboard()
        
        self.arrNOCOptions = [DropDownModel(dict: [DropDownModel.Keys.title.rawValue: CommonLabels.select,
                                                   DropDownModel.Keys.id.rawValue: ""])]
        getWebserviceforNOCType()
    }
    
    fileprivate var isValidTransferer: Bool {
        if (getString(anything: self.tfNameOfTransferer.text).isStringEmpty) {
            Global.shared.showBanner(message: PreviousPropertyDetailsMessages.PleaseEnterNameofTransferrer)
            _ = tfNameOfTransferer.becomeFirstResponder()
            return false
        }
        return true
    }
    
    fileprivate var isValidTransferee: Bool {
        if (getString(anything: self.tfNameOfTransferee.text).isStringEmpty) {
            Global.shared.showBanner(message: PreviousPropertyDetailsMessages.PleaseEnterNameofTransferee)
            _ = tfNameOfTransferee.becomeFirstResponder()
            return false
        }
        return true
    }
    
    fileprivate var isValidYear: Bool {
        if (getString(anything: self.tfYearOfTransfer.text).isStringEmpty) {
            Global.shared.showBanner(message: PreviousPropertyDetailsMessages.PleaseEnterYearofTransfer)
            _ = tfYearOfTransfer.becomeFirstResponder()
            return false
        } else if (getString(anything: self.tfYearOfTransfer.text)).count < 4 {
            Global.shared.showBanner(message: PreviousPropertyDetailsMessages.YearNotlessthan4digits)
            _ = tfYearOfTransfer.becomeFirstResponder()
            return false
        }
        return true
    }
    
    fileprivate var isValidTypeofTransfer: Bool {
        if ((getString(anything: self.lblTypeOfTransfer.text).isStringEmpty) || (getString(anything: self.lblTypeOfTransfer.text)) == CommonLabels.select) {
            Global.shared.showBanner(message: PreviousPropertyDetailsMessages.PleaseSelectTypeofTransfer)
            return false
        }
        return true
    }
    
    fileprivate var isValidNOCDoc: Bool {
        if (self.strNOCObtained == "yes" && fileName.isStringEmpty) {
            Global.shared.showBanner(message: PreviousPropertyDetailsMessages.PleaseUploadNOCObtainedDoc)
            return false
        }
        return true
    }
    
    func isValidAddDetails() -> Bool {
        return (self.isValidTransferer && self.isValidTransferee && self.isValidYear && self.isValidTypeofTransfer && self.isValidNOCDoc)
    }
    
    func setupLabels() {
        lblTitles.forEach({
            $0.font = .regularTitleFont
            $0.textColor = .customBlackTitle
        })
        
        self.lblYes.font = .regularTitleFont
        self.lblNo.font = .regularTitleFont
        
        self.lblYes.textColor = .customBlackTitle
        self.lblNo.textColor = .customBlackTitle
        
        lblTypeOfTransfer.font = .regularValueFont
        lblTypeOfTransfer.textColor = .customBlack
        
        tfValues.forEach { tf in
            tf.font = .regularValueFont
            tf.textColor = .customBlack
        }
        
        self.lblTypeOfTransfer.text = CommonLabels.select
    }
    
    private func setUpTblView() {
        self.tblViewPreviousTransfer.setCustomCornerRadius(radius: 8, borderColor: .customgreenTheamClr, borderWidth: 1)
        self.tblViewPreviousTransfer.superview?.isHidden = true
        self.tblViewPreviousTransfer.backgroundColor = .white
        self.tblViewPreviousTransfer.tableFooterView = UIView()
        self.previousPropertyTblViewDataSourceDelegate = PreviousPropertyTblViewDataSourceDelegate(list: arrPreviousDetailsList, tbl: tblViewPreviousTransfer, _delegate: nil)
        self.previousPropertyTblViewDataSourceDelegate?.deleteRecordCallBack = deleteRecordCallBack
        self.previousPropertyTblViewDataSourceDelegate?.editRecordCallBack = editRecordCallBack
    }
    
    fileprivate func deleteRecordCallBack(index: Int) {
        self.arrPreviousDetailsList.remove(at: index)
        self.previousPropertyTblViewDataSourceDelegate?.reloadData(list: self.arrPreviousDetailsList)
        if self.arrPreviousDetailsList.count == 0 {
            self.tblViewPreviousTransfer.superview?.isHidden = true
            //self.anchorTblViewMonumentRecordHeight.constant = 60
        }
        
        self.btnAddPreviousTransfer.setTitle(CommonButtons.btn_Add_Previous_Transfer_Details, for: .normal)
        self.btnAddPreviousTransfer.accessibilityHint = nil
        self.imgPlusUpdate.image = .ic_plus
    }
    
    fileprivate func editRecordCallBack(index: Int) {
        self.btnAddPreviousTransfer.setTitle(CommonButtons.btn_update, for: .normal)
        self.btnAddPreviousTransfer.accessibilityHint = "btnEditPreviousTransfer"
        self.btnAddPreviousTransfer.tag = index
        self.imgPlusUpdate.image = .ic_correct_border
        
        let item = self.arrPreviousDetailsList[index]
        
        self.tfNameOfTransferer.text = item.nameofTransferer
        self.tfNameOfTransferee.text = item.nameofTransferee
        self.tfYearOfTransfer.text = item.year
        self.lblTypeOfTransfer.text = item.type
        self.strNOCObtained = item.NOCObtained
        
        let btn = UIButton()
        btn.tag = self.strNOCObtained == "Yes" ? 0 : 1
        self.btnNOCObtained(btn)
        
        if btn.tag == 0 {
            self.fileName = item.ObtainedNOCDoc
            self.DocParamDict = item.DocumentDict
            self.uploadedObtainedDocument.setFileName(fileName: getString(anything: DocParamDict[MultipartRequestKeys.key_documentTitle]))
            self.uploadedObtainedDocument.imgViewDocument.image = .correct
            self.uploadedObtainedDocument.imgViewDocument.tintColor = .themeColor
        } else {
            self.fileName = ""
            self.DocParamDict = [:]
            self.uploadedObtainedDocument.imgViewDocument.image = .ic_plus
            self.uploadedObtainedDocument.imgViewDocument.tintColor = .orange
        }
        
    }
    
    private func addObserve() {
        tblViewPreviousTransfer.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
    }
    
    fileprivate func setupView() {
        self.view.backgroundColor = .lnocBackgroundColor
//        self.view.backgroundColor = UIColor(232, 238, 243)
        self.viewSeprators.forEach({
            $0.backgroundColor = .customSeparator
        })
        
        self.viewAddPreviousDetails.setFullCornerRadius(type: .automatic, borderColor: .customgreenTheamClr, borderWidth: 1.0)
        self.viewShadow.addShadow()
        self.viewShadow.subviews.first?.viewWith(radius: 8)
        self.setUpDocView()
        self.tfYearOfTransfer.delegate = self
        
        self.viewNext.addGradient(ofColors: [UIColor.themeLightOrangeClr.cgColor, UIColor.themeDarkOrangeClr.cgColor], direction: .leftRight)
        self.viewPrevious.addGradient(ofColors: [UIColor.themeLightOrangeClr.cgColor, UIColor.themeDarkOrangeClr.cgColor], direction: .leftRight)
        self.viewNext.setFullCornerRadius()
        self.viewPrevious.setFullCornerRadius()
    }
    
    fileprivate func setUpBtns() {
        self.imgDownArrow.image = .ic_down_arrow
        self.imgDownArrow.tintColor = .customBlackTitle
        self.imgYes.image = .radio_selected
        self.imgNo.image = .radio_UnSelected
        self.strNOCObtained = "yes"
        self.imgPlusUpdate.image = .ic_plus
        
        self.btnAddPreviousTransfer.setTitle(CommonButtons.btn_Add_Previous_Transfer_Details, for: .normal)
        self.btnAddPreviousTransfer.accessibilityHint = nil
        self.btnAddPreviousTransfer.titleLabel?.font = .boldTitleMediumFont
    }
    
    fileprivate func setUpDocView() {
        self.ObtainedNOCStackView.subviews.forEach({
            $0.removeFromSuperview()
        })
        self.uploadedObtainedDocument.configureTitle(str: "Obtained NOC".localizedString,
                                       btnTag: 0,
                                       showFormatBtn: false,
                                       hideAsterisk: true,
                                       isFileNameShow: true)
        self.uploadedObtainedDocument.lblMaxSize.text = CommonLabels.maxDocMBAllowed.replacingOccurrences(of: "abc", with: getString(anything: documentSizeLimit))// "Max. \(documentSizeLimit)MB (Allowed pdf/image)"
        self.uploadedObtainedDocument.btnDocument.addTarget(self, action: #selector(btnDocumentTapped), for: .touchUpInside)
        self.ObtainedNOCStackView.addArrangedSubview(uploadedObtainedDocument)
    }
    
    //MARK: - Button Action Methods
    
    @IBAction func btnAddPreviousDetails(_ sender: UIButton) {
        UIView.hideKeyBoard()
        
        if self.isValidAddDetails() {
            
            let itemModel = PreviousPropertyDetailsModel(dict: [:])
            itemModel.nameofTransferer = getString(anything: self.tfNameOfTransferer.text)
            itemModel.nameofTransferee = getString(anything: self.tfNameOfTransferee.text)
            itemModel.year = getString(anything: self.tfYearOfTransfer.text)
            itemModel.type = getString(anything: self.lblTypeOfTransfer.text)
            itemModel.type_Id = self.type_of_transfer
            itemModel.NOCObtained = self.strNOCObtained
            itemModel.ObtainedNOCDoc = self.strNOCObtained == "no" ? "" : fileName
            itemModel.DocumentDict = self.strNOCObtained == "no" ? [:] : DocParamDict
            
            if sender.accessibilityHint != nil {
                self.arrPreviousDetailsList[sender.tag] = itemModel
            } else {
                self.arrPreviousDetailsList.append(itemModel)
            }
            
            self.btnAddPreviousTransfer.accessibilityHint = nil
            self.btnAddPreviousTransfer.setTitle(CommonButtons.btn_Add_Previous_Transfer_Details, for: .normal)
            self.imgPlusUpdate.image = .ic_plus
            
            self.lblYear.text = CommonLabels.select
            self.lblTypeOfTransfer.text = CommonLabels.select
            self.tfNameOfTransferer.text = ""
            self.tfNameOfTransferee.text = ""
            self.tfYearOfTransfer.text = ""
            self.DocParamDict = [:]
            
            self.previousPropertyTblViewDataSourceDelegate?.reloadData(list: self.arrPreviousDetailsList)
            self.tblViewPreviousTransfer.superview?.isHidden = false
            self.imgYes.image = .radio_selected
            self.imgNo.image = .radio_UnSelected
            self.strNOCObtained = "yes"
            self.setUpDocView()
            self.fileName = ""
            self.type_of_transfer = ""
            self.ObtainedNOCStackView.superview?.isHidden = false
            self.uploadedObtainedDocument.imgViewDocument.image = .ic_plus
            self.uploadedObtainedDocument.imgViewDocument.tintColor = .orange
            
        }
    }
    
    @IBAction func btnSelectYear(_ sender: UIButton) {
        let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: lblYear.text), inputFormat: DateFormats.yyyy), maximumDate: Date(), minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dobString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.yyyy)
            self.lblYear.text = dobString
            
        }, animated: true, origin: sender)
        
        datePickerVC.lblTitleText = CommonLabels.select
        datePickerVC.lblTitleColor = .customDarkBlueGradient
        datePickerVC.lblTitleFont = .boldMediumFont
        datePickerVC.btnCancelTitle = CommonButtons.cancel
        datePickerVC.btnCancelTitleColor = .customSubtitle
        datePickerVC.btnCancelTitleFont = .regularValueFont
        datePickerVC.btnDoneTitle = CommonButtons.done
        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)
    }
    
    @IBAction func btnSelectType(_ sender: UIButton) {
        let list = arrNOCOptions.map({ $0.title })
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblTypeOfTransfer.text, arrSource: [list], doneAction: { (rows, value) in
            
            self.lblTypeOfTransfer.text = getString(anything: value)
            
            if getString(anything: value) != CommonLabels.select{

                self.type_of_transfer = self.arrNOCOptions[rows[0]].id
               
            }
            
        }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func btnNOCObtained(_ sender: UIButton) {
        if sender.tag == 0 {
            self.imgYes.image = .radio_selected
            self.imgNo.image = .radio_UnSelected
            self.strNOCObtained = "yes"
            self.ObtainedNOCStackView.superview?.isHidden = false
        } else {
            self.imgYes.image = .radio_UnSelected
            self.imgNo.image = .radio_selected
            self.strNOCObtained = "no"
            self.ObtainedNOCStackView.superview?.isHidden = true
        }
    }
    
    @IBAction func btnNextOrSubmitClickHandler(_ sender: UIButton) {
        if arrPreviousDetailsList.count > 0 {
            self.registerContainerViewController?.webserviceModel.arrPreviousDetailsList = arrPreviousDetailsList
            self.registerContainerViewController?.removeOldController()
            self.registerContainerViewController?.selectedTab = 2
            self.registerContainerViewController?.addAccordingToSelectedTab()
        } else {
            self.registerContainerViewController?.removeOldController()
            self.registerContainerViewController?.selectedTab = 2
            self.registerContainerViewController?.addAccordingToSelectedTab()
//            Global.shared.showBanner(message: PreviousPropertyDetailsMessages.PleaseEnterPreviousDetails)
        }
    }
    
    @IBAction func btnPreviousClickHandler(_ sender: UIButton) {
        self.registerContainerViewController?.removeOldController()
        self.registerContainerViewController?.selectedTab = 0
        self.registerContainerViewController?.addAccordingToSelectedTab()
    }
    
    @objc fileprivate func btnDocumentTapped(_ sender: UIButton) {
        self.selectedIndex = sender.tag
        UIView.hideKeyBoard()
        let aDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: arrdocTypesForProfile, in: .import)
        self.documentPickerViewControllerDelegate = DocumentPickerViewControllerDelegate(VC: aDocumentPickerViewController, delegate: self, sender: sender, file_Size: documentSizeLimit)
        self.present(aDocumentPickerViewController, animated: true, completion: nil)
    }
}

extension PreviousPropertyDetailsViewController {
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let fullString = NSString(string: getString(anything: textField.text)).replacingCharacters(in: range, with: string)
        if textField == tfYearOfTransfer {
            let numberSet = NSCharacterSet(charactersIn: NUMERIC).inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if (string == numberFiltered) && (fullString.count < YEAR_VALIDATION + 1) {
                if (fullString.hasPrefix("1") || fullString.hasPrefix("2")) || string.isEmpty {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        }
        return true
    }
}

extension PreviousPropertyDetailsViewController : DocumentPickerDelegate {
    
    func DocumentPickerData(url: URL?,errorMsg: String, sender: UIView) {

        if let getURL = url {
            let change_URL = changeURL(url: getURL, name: ProfileWebServiceModel.keys.pan.rawValue)
            self.uploadedObtainedDocument.imgViewDocument.image = .correct
            self.uploadedObtainedDocument.imgViewDocument.tintColor = .themeColor
            var dict: JSONObject = [:]
            dict[MultipartRequestKeys.key_documentKey] = "obtained_noc_doc"
            dict[MultipartRequestKeys.key_documentURL] = change_URL
            dict[MultipartRequestKeys.key_documentType] = getURL.pathExtension
            dict[MultipartRequestKeys.key_documentTitle] = getURL.lastPathComponent
            self.uploadedObtainedDocument.setFileName(fileName: getURL.lastPathComponent)
            self.fileName = getString(anything: getURL.lastPathComponent)
            self.DocParamDict = dict
            //self.webServiceModel.scriptPanDic = dict
        } else {
            Global.shared.showBanner(message: errorMsg)
        }
    }
    
}

extension PreviousPropertyDetailsViewController: ValidationProtocol {
    
    func isValid() -> Bool {
        if self.arrPreviousDetailsList.count > 0 {
            return true
        }
        return false
    }
    
}

extension PreviousPropertyDetailsViewController {
    
    func getWebserviceforNOCType() {
           CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        webServiceModel.getNOCTypeDataData { (responseModel) in
               CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
               if let response = responseModel {
                   print(response)
                   self.arrNOCOptions.append(contentsOf: response.list)
               }
           }
       }
    
}
