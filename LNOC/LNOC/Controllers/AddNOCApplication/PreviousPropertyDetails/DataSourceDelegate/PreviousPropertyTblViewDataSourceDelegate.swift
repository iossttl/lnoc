//
//  PermissionForFilmingOperationMonumentTableViewDataSourceDelegate.swift
//  ASI
//
//  Created by Vikram Jagad on 27/07/20.
//  Copyright © 2020 Silvertouch. All rights reserved.
//

import UIKit

class PreviousPropertyTblViewDataSourceDelegate: NSObject {

    //MARK:- Variable Properties
    
    fileprivate weak var tblView: UITableView?
    fileprivate var itemList: [PreviousPropertyDetailsModel]
    fileprivate weak var delegate : TblViewDelegate?
    var deleteRecordCallBack: ((Int) -> Void)?
    var editRecordCallBack: ((Int) -> Void)?
    
    //MARK:- Initializer
    init(list: [PreviousPropertyDetailsModel], tbl: UITableView, _delegate : TblViewDelegate?) {
        self.tblView = tbl
        self.itemList = list
        self.delegate = _delegate
        super.init()
        self.setup()
    }
    
    fileprivate func setup() {
        self.tblView?.backgroundColor = UIColor.clear
        self.tblView?.delegate = self
        self.tblView?.dataSource = self
        
        self.tblView?.separatorStyle = .none
        self.tblView?.estimatedRowHeight = 353
        self.tblView?.rowHeight = UITableView.automaticDimension
        self.tblView?.contentInset = .init(top: 12, left: 0, bottom: 0, right: 0)
        self.tblView?.scrollIndicatorInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        
        self.tblView?.register(cellType: PreviousPropertyDetailsCell.self)
        self.tblView?.reloadData()
    }
    
    func reloadData(list: [PreviousPropertyDetailsModel]) {
        self.itemList = list
        self.tblView?.reloadData()
    }
    
    @objc fileprivate func btnDeleteClicked(_ sender: UIButton) {
        deleteRecordCallBack?(sender.tag)
    }
    
    @objc fileprivate func btnEditClicked(_ sender: UIButton) {
        editRecordCallBack?(sender.tag)
    }
    
}


extension PreviousPropertyTblViewDataSourceDelegate: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(with: PreviousPropertyDetailsCell.self, for: indexPath)
        cell.configureCell(item: itemList[indexPath.row])
        
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteClicked), for: .touchUpInside)
        
        cell.btnEdit.tag = indexPath.row
        cell.btnEdit.addTarget(self, action: #selector(btnEditClicked), for: .touchUpInside)
    
        cell.viewSeperator.isHidden = false
        
        if (indexPath.row == 0) && (indexPath.row == itemList.count - 1) {
            cell.viewSeperator.isHidden = true
        } else if indexPath.row == (itemList.count - 1) {
            cell.viewSeperator.isHidden = true
        }
        
        cell.contentView.layoutIfNeeded()
        
        return cell
    }
}

//MARK:- UITableViewDelegate
extension PreviousPropertyTblViewDataSourceDelegate: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        delegate?.Table_View(tableView, didSelectRowAt: indexPath)
    }
    
}

