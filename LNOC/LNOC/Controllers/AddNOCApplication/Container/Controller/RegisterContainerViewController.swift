//
//  RegisterContainerViewController.swift
//  Social Justice
//
//  Created by Vikram Jagad on 26/02/21.
//

import UIKit

protocol ValidationProtocol {
    func isValid() -> Bool
}

class RegisterContainerViewController: HeaderViewController {
    //MARK:- Interface Builder
    //UIView
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewDummyForColor: UIView!
    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var viewBottomBtnsShadow: UIView!
    @IBOutlet weak var viewPrevious: GradientView!
    @IBOutlet weak var viewNext: GradientView!
    
    //UICollectionView
    @IBOutlet weak var colView: UICollectionView!
    
    //UIButton
    @IBOutlet weak var btnSaveAsDraft: LocalizedButton!
    @IBOutlet weak var btnNextOrSubmit: LocalizedButton!
    @IBOutlet weak var ColViewHeight: NSLayoutConstraint!
    
    //MARK:- Variables
    //Private
    private var delegateDataSource: RegisterContainerColViewDelegateDataSource!
    private var tabData: [RegisterContainerTabModel] = []
    private var tabControllers: [UIViewController] = []
    //Public
    var selectedTab: Int = 0
    let webserviceModel = RegisterContainerWebserviceModel()
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpVC()
    }
    
    //MARK:- Private Methods
    private func setUpVC() {
        setUpHeader()
        setUpIPadConstraints()
        setUpView()
        setUpBtns()
        setUpTabData()
        setUpColView()
        addNewController()
    }
    
    private func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.NOCApplicationForm)
        setHeaderView_BackImage()
    }
    
    private func setUpIPadConstraints() {
        changeLeadingTrailingForiPad(view: viewMain)
        changeHeightForiPad(view: colView)
        changeHeightForiPad(view: btnSaveAsDraft)
    }
    
    private func setUpView() {
        self.view.backgroundColor = .lnocBackgroundColor
        viewDummyForColor.backgroundColor = .customgreenTheamClr
        viewBottomBtnsShadow.addShadow(shadowOffset: CGSize(width: 0, height: -1))
    }
    
    private func setUpTabData() {
        tabData = [RegisterContainerTabModel(dict: [.title: "Owner & Property Detail".localizedString]),
                   RegisterContainerTabModel(dict: [.title: "Previous TransferDetails".localizedString]),
                   RegisterContainerTabModel(dict: [.title: "Upload Documents".localizedString])]
        tabData[selectedTab].isSelected = true
        
//        let grivanceRegistrationVC = PropertyDetailViewController.instantiate(appStoryboard: .propertyDetail)
//        let informerDetailsVC = RegisterViewController.instantiate(appStoryboard: .register)
//        let victimDetailsVC = RegisterViewController.instantiate(appStoryboard: .register)
//        tabControllers = [grivanceRegistrationVC, informerDetailsVC, victimDetailsVC]
        let grivanceRegistrationVC = PropertyDetailViewController.instantiate(appStoryboard: .propertyDetail)
        grivanceRegistrationVC.registerContainerViewController = self
        grivanceRegistrationVC.nocTypeChangeCallBack = { [weak self] in
            (self?.tabControllers[2] as? NOCUploadDocumentViewController)?.nocDocumentType = grivanceRegistrationVC.nocType
            (self?.tabControllers[2] as? NOCUploadDocumentViewController)?.noc_Type_Id = grivanceRegistrationVC.Noc_Type_Id
//            (self?.tabControllers[2] as? NOCUploadDocumentViewController)?.setupStackView()
            (self?.tabControllers[2] as? NOCUploadDocumentViewController)?.getWebserviceforDocument()
        }
        
        
        let previousPropertyDetails = PreviousPropertyDetailsViewController.instantiate(appStoryboard: .previousPropertyDetails)
        previousPropertyDetails.registerContainerViewController = self
        
        let documentVC = NOCUploadDocumentViewController.instantiate(appStoryboard: .nocUploadDocument)
        documentVC.registerContainerViewController = self
        
        tabControllers = [grivanceRegistrationVC, previousPropertyDetails, documentVC]
        
    }
    
    private func setUpBtns() {
//        btnSaveAsDraft.setTitleColor(.customWhite, for: .normal)
//        btnSaveAsDraft.backgroundColor = .lightGray
        //btnSaveAsDraft.setTitle("saveAsDraft", for: .normal)
        viewPrevious.superview?.isHidden = true
        
//        btnNextOrSubmit.setTitleColor(.customWhite, for: .normal)
//        btnNextOrSubmit.backgroundColor = .customBlue
//        btnNextOrSubmit.setTitle("next", for: .normal)
        
//        self.viewNext.addGradient(ofColors: [UIColor(253, 174, 57).cgColor, UIColor(250, 94, 47).cgColor], direction: .leftRight)
//        self.viewPrevious.addGradient(ofColors: [UIColor(253, 174, 57).cgColor, UIColor(250, 94, 47).cgColor], direction: .leftRight)
        
        self.viewNext.addGradient(ofColors: [UIColor.themeLightOrangeClr.cgColor, UIColor.themeDarkOrangeClr.cgColor], direction: .leftRight)
        self.viewPrevious.addGradient(ofColors: [UIColor.themeLightOrangeClr.cgColor, UIColor.themeDarkOrangeClr.cgColor], direction: .leftRight)
            self.viewNext.setFullCornerRadius()
            self.viewPrevious.setFullCornerRadius()
    }
    
    private func setUpColView() {
        ColViewHeight.constant = Constants.RegisterGrievance.Container.colViewHeight + 5
        if (delegateDataSource == nil) {
            delegateDataSource = RegisterContainerColViewDelegateDataSource(arrData: tabData, delegate: self, scrl: colView)
        } else {
            delegateDataSource.reloadScrlView(arr: tabData)
        }
    }
    
    private func addConstraints(toVC: UIViewController) {
        toVC.view.translatesAutoresizingMaskIntoConstraints = false
        toVC.view.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor).isActive = true
        toVC.view.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor).isActive = true
        toVC.view.topAnchor.constraint(equalTo: viewContainer.topAnchor).isActive = true
        //viewContainer.addConstraint(NSLayoutConstraint(item: toVC.view as Any, attribute: .top, relatedBy: .equal, toItem: viewContainer, attribute: .top, multiplier: 1, constant: 0))
        toVC.view.bottomAnchor.constraint(equalTo: viewContainer.bottomAnchor).isActive = true
    }
    
    private func addNewController() {
        if (tabControllers.indices.contains(selectedTab)) {
            let currentVC = tabControllers[selectedTab]
            currentVC.willMove(toParent: self)
            addChild(currentVC)
            viewContainer.addSubview(currentVC.view)
            addConstraints(toVC: currentVC)
            //currentVC.view.frame = viewContainer.frame
            currentVC.didMove(toParent: self)
        }
    }
    
    //MARK:- Public Methods
    func removeOldController() {
        if (tabControllers.indices.contains(selectedTab)) {
            let currentVC = tabControllers[selectedTab]
            currentVC.willMove(toParent: nil)
            currentVC.removeFromParent()
            currentVC.view.removeFromSuperview()
            currentVC.didMove(toParent: nil)
        }
    }
    
    func addAccordingToSelectedTab() {
        if (selectedTab == 0) {
            //btnNextOrSubmit.setTitle("Next", for: .normal)
            viewPrevious.superview?.isHidden = true
        } else if (selectedTab == tabData.count - 1) {
            viewPrevious.superview?.isHidden = false
            //btnNextOrSubmit.setTitle("Submit", for: .normal)
        } else {
            //btnNextOrSubmit.setTitle("Next", for: .normal)
            viewPrevious.superview?.isHidden = false
        }
        for i in 0..<tabData.count {
            if (i == selectedTab) {
                tabData[i].isSelected = true
            } else if (i < selectedTab) {
                //Validation goes here.
                tabData[i].isCompleted = true
            } else if (i > selectedTab) {
                tabData[i].isCompleted = false
                tabData[i].isSelected = false
            }
        }
        setUpColView()
        addNewController()
    }
    
    //MARK:- IBActions
    override func btnBackTapped(_ sender: UIButton) {
        if (selectedTab == 0) {
            UIAlertController.showWith(title: CommonMessages.wantToQuit, msg: "", style: .alert, onVC: self, actionTitles: [CommonButtons.yes,CommonButtons.no], actionStyles: [.destructive,.default], actionHandlers: [{ (yesAction) in
                self.navigationController?.popViewController(animated: true)
            },nil])
        } else {
            removeOldController()
            selectedTab -= 1
            addAccordingToSelectedTab()
        }
    }
    
    @IBAction func btnNextOrSubmitClickHandler(_ sender: UIButton) {
        if selectedTab < self.tabData.count - 1 {
            let VC = tabControllers[selectedTab] as? ValidationProtocol
            if !(VC?.isValid() ?? true) {
//                return
            }
            removeOldController()
            selectedTab += 1
            addAccordingToSelectedTab()
        }
    }
    
    @IBAction func btnSaveAsDraftClickHandler(_ sender: UIButton) {
        if selectedTab > 0 {
            removeOldController()
            selectedTab -= 1
            addAccordingToSelectedTab()
        }
    }
    
    
}

//MARK:- ColViewDelegate Methods
extension RegisterContainerViewController: ColViewDelegate {
    func didSelect(colView: UICollectionView, indexPath: IndexPath) {
        
    }
}

extension RegisterContainerViewController{
    func addNOCApplicationWebServices() {
        view.endEditing(true)
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.webserviceModel.addNOCApplicationDetail { responseModel,isSuccess in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if isSuccess{
                print(responseModel)
                K_NC.post(name: .addNoc, object: nil)
                self.navigationController?.popViewController(animated: true)

            }
        }
    }
}

