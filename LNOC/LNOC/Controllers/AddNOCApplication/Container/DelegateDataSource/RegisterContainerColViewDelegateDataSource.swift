//
//  RegisterGrievanceContainerColViewDelegateDataSource.swift
//  Social Justice
//
//  Created by Vikram Jagad on 26/02/21.
//

import UIKit

class RegisterContainerColViewDelegateDataSource: NSObject, CustomProtocolForDelegateDataSource {
    //MARK:- Types
    typealias T = RegisterContainerTabModel
    typealias ScrlView = UICollectionView
    typealias Delegate = ColViewDelegate
    
    //MARK:- Variables
    internal var arrSource: [T]
    internal var delegate: Delegate
    internal var scrlView: ScrlView
    
    //MARK:- Initializer
    required init(arrData: [T], delegate: Delegate, scrl: ScrlView) {
        arrSource = arrData
        self.delegate = delegate
        scrlView = scrl
        super.init()
        setUpScrlView()
    }
    
    //MARK:- Internal Methods
    internal func setUpScrlView() {
        registerCell()
        scrlView.delegate = self
        scrlView.dataSource = self
        //scrlView.contentInset = UIEdgeInsets(top: Constants.RegisterGrievance.Container.topSpacing, left: Constants.RegisterGrievance.Container.leadingSpacing, bottom: Constants.RegisterGrievance.Container.topSpacing, right: Constants.RegisterGrievance.Container.leadingSpacing)
    }
    
    internal func registerCell() {
        scrlView.register(cellType: RegisterContainerColViewCell.self)
    }
    
    //MARK:- Public Methods
    func reloadScrlView(arr: [T]) {
        arrSource = arr
        scrlView.reloadData()
    }
}

//MARK:- UICollectionViewDelegate Methods
extension RegisterContainerColViewDelegateDataSource: UICollectionViewDelegate {
    
}

//MARK:- UICollectionViewDataSource Methods
extension RegisterContainerColViewDelegateDataSource: UICollectionViewDataSource {
    func collectionView(_ collectionView: ScrlView, numberOfItemsInSection section: Int) -> Int {
        return arrSource.count
    }
    
    func collectionView(_ collectionView: ScrlView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(with: RegisterContainerColViewCell.self, for: indexPath)
        cell.configureCell(data: arrSource[indexPath.row])
        if (indexPath.row == arrSource.count - 1) {
            cell.viewSeparatorRight.isHidden = true
        } else {
            cell.viewSeparatorRight.isHidden = false
        }
        if (indexPath.row == 0) {
            cell.viewSeparatorLeft.isHidden = true
        } else {
            cell.viewSeparatorLeft.isHidden = false
        }
        return cell
    }
    
    func collectionView(_ collectionView: ScrlView, didSelectItemAt indexPath: IndexPath) {
        delegate.didSelect(colView: collectionView, indexPath: indexPath)
    }
}

//MARK:- UICollectionViewDelegateFlowLayout Methods
extension RegisterContainerColViewDelegateDataSource: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: ScrlView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width:CGFloat = (SCREEN_WIDTH)
        print(width/3)
        return CGSize(width: width/3, height: Constants.RegisterGrievance.Container.colViewHeight)
    }
    
    func collectionView(_ collectionView: ScrlView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: ScrlView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
