//
//  RegisterGrievanceContainerColViewCell.swift
//  Social Justice
//
//  Created by Vikram Jagad on 26/02/21.
//

import UIKit

class RegisterContainerColViewCell: UICollectionViewCell {
    //MARK:- Interface Builder
    //UIView
    @IBOutlet weak var viewSeparatorLeft: UIView!
    @IBOutlet weak var viewSeparatorRight: UIView!
    @IBOutlet weak var viewCornerWhite: UIView!
    @IBOutlet weak var viewCornerLightBlue: UIView!
    @IBOutlet weak var imgTick: UIImageView!
    
    //Collection
    @IBOutlet var viewSeparators: [UIView]!
    
    //UILabel
    @IBOutlet weak var lblTitle: UILabel!
    
    //MARK:- Lifecycle Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpCell()
    }
    
    //MARK:- Private Methods
    private func setUpCell() {
        setUpView()
        setUpLbl()
    }
    
    private func setUpView() {
        for view in viewSeparators {
            view.backgroundColor = UIColor(183, 255, 215)
        }
        imgTick.backgroundColor = UIColor(183, 255, 215)
        imgTick.setFullCornerRadius()
    }
    
    private func setUpLbl() {
        lblTitle.font = IS_IPAD ? .regularValueFont : .regularSmallFont
        lblTitle.textColor = .customWhite
    }
    
    //MARK:- Public Methods
    func configureCell(data: RegisterContainerTabModel) {
        lblTitle.text = data.title
        if (data.isCompleted) {
            imgTick.image = .ic_correct_white_bg
        } else if (data.isSelected) {
            imgTick.image = .ic_correct_white_bg
        } else {
            imgTick.image = nil
        }
        
        imgTick.setFullCornerRadius()
    }
}
