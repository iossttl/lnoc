//
//  RegisterGrievanceContainerWebserviceModel.swift
//  Social Justice
//
//  Created by Vikram Jagad on 26/02/21.
//

import UIKit

class RegisterContainerWebserviceModel: NSObject {
//    var grienvanceRegistrationModel: LoginWebserviceModel!
//    var informationDetailsModel: InformerDetailsWebserviceModel!
//    var victimDetailsModel: VictimDetailsWebserviceModel!
//    var victimIdentificationModel: VictimIdentificationWebserviceModel!
//    var victimResidentialAddressModel: VictimResidentialAddressWebserviceModel!
//    var grievanceDetailsModel: GrievanceDetailsWebserviceModel!
//
//    var isdraft : Bool = true
//    var id_SaveAsDraft : String = ""
//    var isEdited = false
    
    var propertydetailmodel : PropertyDetailWebServiceModel!
    var arrPreviousDetailsList: [PreviousPropertyDetailsModel] = []
    var arrUplodedFile: [Dictionary] = []
    
    
    var isdraft : Bool = true
    var id_SaveAsDraft : String = ""
    var isEdited = false
    
    fileprivate var addNOCApplicationURL: String {
        return BASEURL + API.addNOCApplication
    }
    
    private func grivienceRegistrationURLDict() -> Dictionary {
        
        var dict: Dictionary = [Keys.citizen_users_id.rawValue:UserModel.currentUser.id.decryptStr]
        var arrParking : [Dictionary] = []

        if id_SaveAsDraft != "" {
            dict[Keys.id.rawValue] = id_SaveAsDraft
        }
        
        // 1. PropertyDetail
        if (propertydetailmodel != nil) {
            dict[Keys.nocType.rawValue] = propertydetailmodel.NOCtype
            dict[Keys.nocType_Id.rawValue] = propertydetailmodel.NOCtype_Id
            dict[Keys.Division_Id.rawValue] = propertydetailmodel.Division_Id
            dict[Keys.Division.rawValue] = propertydetailmodel.Division
            dict[Keys.NameOFTranferee.rawValue] = propertydetailmodel.NameOfTransferee
            dict[Keys.TranserWithinFamily.rawValue] = propertydetailmodel.TransferWithinFamily
            dict[Keys.RelationWithTransferor.rawValue] = propertydetailmodel.RelationWithTransferor
            dict[Keys.CSNo.rawValue] = propertydetailmodel.CSNo
            dict[Keys.nameOFSociety.rawValue] = propertydetailmodel.NameOfSociety
            dict[Keys.PresentNameOfLesse.rawValue] = propertydetailmodel.PresentNameOfLesse
            dict[Keys.NameOfBuilding.rawValue] = propertydetailmodel.NameOfBuilding
            dict[Keys.TotalNoOFFloor.rawValue] = propertydetailmodel.TotalNoOfFloorOfBuilding
            dict[Keys.NoOfLift.rawValue] = propertydetailmodel.NoOfLift
            dict[Keys.CompletionYear.rawValue] = propertydetailmodel.CompletionOfBuilding
            dict[Keys.ChairmaanName.rawValue] = propertydetailmodel.SocietyChairmanName
            dict[Keys.ChaiemanContactNO.rawValue] = propertydetailmodel.ChairmanContact
            dict[Keys.secretoryName.rawValue] = propertydetailmodel.SecretoryName
            dict[Keys.SecretoryContactNO.rawValue] = propertydetailmodel.SecretoryContactNO
            dict[Keys.SocietyEmail.rawValue] = propertydetailmodel.SocietyEmail
            dict[Keys.NoOFMember.rawValue] = propertydetailmodel.NoOfMember
            dict[Keys.type_of_premises.rawValue] = propertydetailmodel.PremisesType
            dict[Keys.FlatNumber.rawValue] = propertydetailmodel.FlatNumber
            dict[Keys.FloorOfFlat.rawValue] = propertydetailmodel.FloorOfFlat
            dict[Keys.carpetAreaFlatFeet.rawValue] = propertydetailmodel.CarpetAreaOfFlatFeet
            dict[Keys.CarpetAreaFlatMT.rawValue] = propertydetailmodel.CarpetAreofFlatMT
            dict[Keys.BuiltUpAreaFlatFeet.rawValue] = propertydetailmodel.BuiltupAreaOfFlatFeet
            dict[Keys.BuiltUpAreaFlatMT.rawValue] = propertydetailmodel.BuiltUpAreaOfFlatMt
            dict[Keys.isParkig.rawValue] = propertydetailmodel.IsParking
            dict[Keys.Parking.rawValue] = propertydetailmodel.Parking_Type
            dict[Keys.carpetAreaParkingFeet.rawValue] = propertydetailmodel.CarpetAreofParkingFeet
            dict[Keys.CarpetAreaParkingMT.rawValue] = propertydetailmodel.CarpetAreofParkingMT
            dict[Keys.numberOfParkingUnderTransfer.rawValue] = propertydetailmodel.NoOfParkingUnderTransfer
            dict[Keys.BuiltUpAreaParkingFeet.rawValue] = propertydetailmodel.BuiltupAreaOfParkingFeet
            dict[Keys.BuiltUpAreaParkingMT.rawValue] = propertydetailmodel.BuiltUpAreaOfParkingMt
            
            dict[Keys.name_of_transferor.rawValue] = propertydetailmodel.name_of_transferor
            dict[Keys.applicant_address.rawValue] = propertydetailmodel.applicant_address
            dict[Keys.society_address.rawValue] = propertydetailmodel.society_address
            dict[Keys.ownership_of.rawValue] = propertydetailmodel.ownerShipOf
            dict[Keys.salutation.rawValue] = propertydetailmodel.salutation
            dict[Keys.first_name.rawValue] = propertydetailmodel.firstName
            dict[Keys.middle_name.rawValue] = propertydetailmodel.middleName
            dict[Keys.last_name.rawValue] = propertydetailmodel.lastname
            dict[Keys.company_name.rawValue] = propertydetailmodel.companyName
            dict[Keys.second_name_of_the_transferee.rawValue] = propertydetailmodel.second_name_of_the_transferee
            dict[Keys.third_name_of_the_transferee.rawValue] = propertydetailmodel.third_name_of_the_transferee
            
            for model  in propertydetailmodel.arrparkingDetail {
                var dictP = Dictionary()
                dictP[Keys.Parking.rawValue] = model.Parking_Type
                dictP[Keys.carpetAreaParkingFeet.rawValue] = model.CarpetAreofParkingFeet
                dictP[Keys.CarpetAreaParkingMT.rawValue] = model.CarpetAreofParkingMT
                dictP[Keys.BuiltUpAreaParkingFeet.rawValue] = model.BuiltupAreaOfParkingFeet
                dictP[Keys.BuiltUpAreaParkingMT.rawValue] = model.BuiltUpAreaOfParkingMt
                dictP[Keys.ParkingNumbers.rawValue] = model.ParkingNumber
                arrParking.append(dictP.encryptDic())
            }
        }
        
        // 1. PreviousDetail
        var arr_Previous_Property_Detail : [Dictionary] = []
        for model  in arrPreviousDetailsList {
            var dictP = Dictionary()
            dictP[Keys.nameofTransferer.rawValue] = model.nameofTransferer
            dictP[Keys.nameofTransferee.rawValue] = model.nameofTransferee
            dictP[Keys.year.rawValue] = model.year
            dictP[Keys.type.rawValue] = model.type_Id
            dictP[Keys.NOCObtained.rawValue] = model.NOCObtained
            //dict[Keys.ObtainedNOCDoc.rawValue] = model.ObtainedNOCDoc
            arr_Previous_Property_Detail.append(dictP.encryptDic())
        }
        
        dict = dict.encryptDic()
        
        dict[Keys.ParkingNumbers.rawValue] = arrParking
        dict[Keys.previous_transfer_details.rawValue] = arr_Previous_Property_Detail
        dict[Keys.uploaded_document_data.rawValue] = arrUplodedFile
        
        return dict//[Keys.grievance_data.rawValue: dict]
    }
    
    
    public enum Keys:String {
        case id = "id"
        case language = "language"
        case userid = "userid"
        case access_token = "access_token"
        case isdraft = "isdraft"
        
        //NEW
        case citizen_users_id = "citizen_users_id"
        case nocType = "noctypes"
        case nocType_Id = "ms_noc_types_id"
        case Division_Id = "ms_divisions_id"
        case Division = "division"
        case NameOFTranferee = "name_of_the_transferee"
        case TranserWithinFamily = "is_transfer_within_family"
        case RelationWithTransferor = "relation_with_transferer"
        case CSNo = "ms_cs_numbers_id"
        case nameOFSociety = "name_of_society"
        case PresentNameOfLesse = "present_name_of_lease"
        case NameOfBuilding = "name_of_building_premises"
        case TotalNoOFFloor = "total_floors_of_building"
        case NoOfLift = "no_of_lift"
        case CompletionYear = "completion_of_building"
        case ChairmaanName = "society_chairman_name"
        case ChaiemanContactNO = "chairman_contact_no"
        case secretoryName = "society_secretary_name"
        case SecretoryContactNO = "secretary_contact_no"
        case SocietyEmail = "society_email_id"
        case NoOFMember = "no_of_members"
        case type_of_premises = "type_of_premises"
        case FlatNumber = "flat_number"
        case FloorOfFlat = "floor_of_flat"
        case carpetAreaFlatFeet = "area_of_flat_sq_ft"
        case CarpetAreaFlatMT = "area_of_flat_sq_mt"
        case BuiltUpAreaFlatFeet = "area_of_flat_sq_ft_builtup"
        case BuiltUpAreaFlatMT = "area_of_flat_sq_mt_builtup"
        case isParkig = "is_parking"
        case Parking = "parking"
        case carpetAreaParkingFeet = "parking_area_sq_ft"
        case CarpetAreaParkingMT = "parking_area_sq_mt"
        case numberOfParkingUnderTransfer = "number_of_parking"
        case BuiltUpAreaParkingFeet = "parking_area_sq_ft_builtup"
        case BuiltUpAreaParkingMT = "parking_area_sq_mt_builtup"
        case ParkingNumbers = "parking_number"
        case divisions = "divisions"
        case landList = "landList"
        case name_of_transferor = "name_of_transferor"
        case applicant_address = "applicant_address"
        case society_address = "society_address"
        case ownership_of = "ownership_of"
        case salutation = "salutation"
        case first_name = "first_name"
        case middle_name = "middle_name"
        case last_name = "last_name"
        case company_name = "company_name"
        case second_name_of_the_transferee = "second_name_of_the_transferee"
        case third_name_of_the_transferee = "third_name_of_the_transferee"
        
        //Previous_transfer_details
        case previous_transfer_details = "previous_transfer_details"
        case nameofTransferer = "name_of_transferer"
        case nameofTransferee = "name_of_transferee"
        case year = "year_of_transfer"
        case type = "type_of_transfer"
        case NOCObtained = "noc_obtained"
        case ObtainedNOCDoc = "ObtainedNOCDoc"
        
        //uploaded_document_data
        case uploaded_document_data = "uploaded_document_data"
        
    }
    
    //MARK:- Initializer
    override init() {
        super.init()
    }
    
    func addNOCApplicationDetail(block: @escaping ((Dictionary?, _ isSucess : Bool) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            
            var param = WShandler.commonDict()//(uuid: uuid)
            
            var documentModels : [DocumentModel] = []
            
            var keyCounter = 0
            for model  in arrPreviousDetailsList {
                let docModel = DocumentModel()
                if model.NOCObtained == "yes"{
                    let key = model.DocumentDict[MultipartRequestKeys.key_documentKey] as! String
                    docModel.key = "\(key)[\(getString(anything: keyCounter))]"
                    docModel.url = model.DocumentDict[MultipartRequestKeys.key_documentURL] as? URL
                    docModel.type = model.DocumentDict[MultipartRequestKeys.key_documentType] as! String
                    docModel.title = model.DocumentDict[MultipartRequestKeys.key_documentTitle] as! String
                    documentModels.append(docModel)
                }else{
                    docModel.key = "obtained_noc_doc[\(getString(anything: keyCounter))]"
                    docModel.type = ""
                    docModel.title = ""
                    docModel.document = Data()
                    documentModels.append(docModel)
                    //                    param["obtained_noc_doc[\(getString(anything: keyCounter))]"] = ""
                }
                keyCounter = keyCounter + 1
            }
            
            let dictPropertyDetail = grivienceRegistrationURLDict()
            for key in dictPropertyDetail.keys {
                param[key] = dictPropertyDetail[key]
            }
            
            print(param,"param addapplication",documentModels.count)
            
            
            WShandler.shared.multipartWebRequest(urlStr: addNOCApplicationURL, dictParams: param, documents: documentModels) { (json, flag) in
                var responseModel:Dictionary?
                var isSuccsees:Bool
                if (flag == 200) {
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        //responseModel = DropDownResponseModel(dictState: json)
                        responseModel = json
                        isSuccsees = true
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        isSuccsees = false
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    isSuccsees = false
                }
                block(responseModel,isSuccsees)
            }
        } else {
            block(nil,false)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
    
    //Add NOCApplication
    /*  func addNOCApplicationDetail(block:@escaping ((Dictionary?, _ isSucess : Bool) -> Swift.Void)) {
     if (WShandler.shared.CheckInternetConnectivity()) {
     let uuid = UUID().uuidString
     var param = WShandler.commonDict()//(uuid: uuid)
     
     let dictPropertyDetail = grivienceRegistrationURLDict(uuid: uuid)
     for key in dictPropertyDetail.keys {
     param[key] = dictPropertyDetail[key]
     }
     print(param,"param")
     WShandler.shared.postWebRequest(urlStr: addNOCApplicationURL, param: param) { (json, flag) in
     
     var responseModel:Dictionary?
     if flag == 200 {
     responseModel = json
     if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
     Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]))
     print(json)
     block(responseModel, true)
     } else {
     Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]))
     block(responseModel, false)
     
     }
     } else {
     Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]))
     block(responseModel, false)
     
     }
     block(responseModel, false)
     }
     } else {
     block(nil,false)
     Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
     }
     }*/
}
