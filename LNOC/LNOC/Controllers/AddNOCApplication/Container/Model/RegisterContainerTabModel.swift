//
//  RegisterGrievanceContainerTabModel.swift
//  Social Justice
//
//  Created by Vikram Jagad on 26/02/21.
//

import UIKit

struct RegisterContainerTabModel {
    //MARK:- Variables
    let title: String
    var isSelected: Bool = false
    var isCompleted: Bool = false
    
    //MARK:- Enum
    enum Keys: String {
        case title
    }
    
    //MARK:- Initializer
    init(dict: [RegisterContainerTabModel.Keys : Any]) {
        title = getString(anything: dict[.title])
    }
}
