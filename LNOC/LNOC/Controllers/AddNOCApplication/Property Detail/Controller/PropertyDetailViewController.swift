//
//  PropertyDetailViewController.swift
//  LNOC
//
//  Created by Dhruv Patel on 30/12/21.
//

import UIKit

class PropertyDetailViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewForScroll: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewForShadow: UIView!
    
    @IBOutlet weak var viewRelationWithTransferor: UIView!
    @IBOutlet weak var viewIsParking: UIView!
    @IBOutlet weak var viewNext: GradientView!
    
    @IBOutlet weak var stackViewParking: UIStackView!
    @IBOutlet weak var stackViewParkingNumbers: UIStackView!
    @IBOutlet weak var viewBgNumberOfParking: UIView!
    
    
    @IBOutlet weak var SwitchWithinFamily: CustomSwitch!
    @IBOutlet weak var switchIsParking: CustomSwitch!
    
    @IBOutlet weak var imgTransferWithinFamily: UIImageView!
    @IBOutlet weak var imgIsParking: UIImageView!
    
    //    @IBOutlet weak var lblParking: UILabel!
    @IBOutlet weak var lblNocType: UILabel!
    @IBOutlet weak var lblDivision: UILabel!
    @IBOutlet weak var lblOwnerShipOf: UILabel!
    @IBOutlet weak var lblSalutation: UILabel!
    
    @IBOutlet weak var lblTypeOfPremises: UILabel!
    @IBOutlet weak var txtApplicantAddress: UITextField!
    @IBOutlet weak var txtSocietyAddress: UITextField!
    @IBOutlet weak var txtNameOfTransferor: UITextField!
    @IBOutlet weak var txtNameOfTransferee: UITextField!
    @IBOutlet weak var txtNameOfTransferee_two: UITextField!
    @IBOutlet weak var txtNameOfTransferee_three: UITextField!
    @IBOutlet weak var txtRelation: UITextField!
    @IBOutlet weak var txtCSPlotNo: UITextField!
    @IBOutlet weak var txtNameOfSociety: UITextField!
    @IBOutlet weak var txtpPresentNameOfLesse: UITextField!
    @IBOutlet weak var txtNameBuilding: UITextField!
    @IBOutlet weak var txtTotalNoOfFloorsOfBuilding: UITextField!
    @IBOutlet weak var txtNoOfLift: UITextField!
    @IBOutlet weak var txtCompletionOfBuilding: UITextField!
    @IBOutlet weak var txtChairManName: UITextField!
    @IBOutlet weak var txtChairmanContactNo: UITextField!
    @IBOutlet weak var txtSecretaryName: UITextField!
    @IBOutlet weak var txtSecretaryContactNo: UITextField!
    @IBOutlet weak var TxtSocietyEmailID: UITextField!
    @IBOutlet weak var txtNoOfMembers: UITextField!
    @IBOutlet weak var txtFlatNo: UITextField!
    @IBOutlet weak var txtFloorOfFlat: UITextField!
    @IBOutlet weak var txtFlatCarpetAreaFeet: UITextField!
    @IBOutlet weak var txtFlatCarpetAreaMT: UITextField!
    @IBOutlet weak var txtFlatBuiltUpAreaFeet: UITextField!
    @IBOutlet weak var txtFlatBuiltUpAreaMt: UITextField!
    @IBOutlet weak var txtFirstName: UITextField!
    @IBOutlet weak var txtMiddleName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtCompanyName: UITextField!
    //    @IBOutlet weak var txtParkingCarpetAreaFeet: UITextField!
    //    @IBOutlet weak var txtParkingCarpetAreaMt: UITextField!
    @IBOutlet weak var txtParkingUnderTransfer: UITextField!
    //    @IBOutlet weak var txtParkingBuiltUpAreafeet: UITextField!
    //    @IBOutlet weak var txtParkingBuiltUpAreaMt: UITextField!
    
    @IBOutlet weak var btnPlusParking: UIButton!
    
    //Collections
    @IBOutlet var lblTitles: [LocalizedLabel]!
    @IBOutlet var txtValues: [UITextField]!
    @IBOutlet var lblAsterisks: [LocalizedLabel]!
    @IBOutlet var lblUnderLine: [UILabel]!
    @IBOutlet var imgDropDown: [UIImageView]!
    @IBOutlet var lblValueDropDown: [UILabel]!
    
    // Variables
    
    private var arrParkingOptions = [String]()
    private var arrNOCOptions = [DropDownModel]()
    private var arrDivisions = [DropDownModel]()
    private var arrOwnerShipOf = [PropertyDetailLabels.Ownershipof,"Personal".localizedString,"Company".localizedString]
    private var arrSalutation = [PropertyDetailLabels.Salutation,"Shri","Shrimati","Kumar","Kumari"]
    private var arrPremisesOptions = [DropDownModel]()
    
    
    var nocType : NOCType = .sale
    var Noc_Type_Id : String = ""
    var nocTypeChangeCallBack : (() -> Void)?
    
    fileprivate var webServiceModel = PropertyDetailWebServiceModel()
    weak var registerContainerViewController : RegisterContainerViewController?
    
    
    
    
    //MARK: - Override Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpIPadConstraints()
        setUpView()
        
        self.arrParkingOptions.append(CommonLabels.select)
        self.arrParkingOptions.append("Open")
        self.arrParkingOptions.append("Cover")
        
        //        self.arrPremisesOptions.append(DropDownModel(dict: [DropDownModel.Keys.title.rawValue: "Flat", DropDownModel.Keys.id.rawValue: "1"]))
        //        self.arrPremisesOptions.append(DropDownModel(dict: [DropDownModel.Keys.title.rawValue: "Shop", DropDownModel.Keys.id.rawValue: "2"]))
        
        //        self.getWebserviceforNOCType()
        self.getDropDownDataFromWebServices()
        self.getDivisionDataFromWebServices()
        
        
        
    }
    
    fileprivate func setUpIPadConstraints() {
        //changeLeadingTrailingForiPad(view: viewForScroll)
        changeHeightForiPad(view: self.btnPlusParking, constant: 10)
        //        changeHeightForiPad(view: txtFullName)
        //        changeHeightForiPad(view: btnSubmit)
    }
    
    fileprivate func setUpView() {
        self.view.backgroundColor = .lnocBackgroundColor
        
        self.viewMain.isHidden = false
        self.viewForShadow.isHidden = false
        
        //        self.viewForScroll.backgroundColor = UIColor.darkBlue.withAlphaComponent(0.2)
        self.viewMain.setCustomCornerRadius(radius: 12)
        self.viewForShadow.setCustomCornerRadius(radius: 12)
        self.viewForShadow.addShadow()
        
        self.stackViewParking.subviews.forEach({ $0.isHidden = $0 == self.viewIsParking ? false : true })
        self.viewRelationWithTransferor.isHidden = true
        
        self.txtCSPlotNo.delegate = self
        self.txtTotalNoOfFloorsOfBuilding.delegate = self
        self.txtNoOfLift.delegate = self
        self.txtCompletionOfBuilding.delegate = self
        self.txtChairmanContactNo.delegate = self
        self.txtSecretaryContactNo.delegate = self
        self.txtNoOfMembers.delegate = self
        self.txtFlatNo.delegate = self
        self.txtFloorOfFlat.delegate = self
        self.txtFlatCarpetAreaFeet.delegate = self
        self.txtFlatBuiltUpAreaFeet.delegate = self
        //        self.txtParkingCarpetAreaFeet.delegate = self
        //        self.txtParkingBuiltUpAreafeet.delegate = self
        self.txtParkingUnderTransfer.delegate = self
        
        self.lblTitles.forEach({
            $0.font = .regularTitleFont
            $0.textColor = .customBlackTitle
        })
        
        for lbl in lblAsterisks {
            lbl.textColor = .customRed
            lbl.font = .regularTitleFont
            lbl.text = "*"
        }
        
        self.txtValues.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
            //            $0.textAlignment = isRightToLeftLocalization ? .right : .left
        })
        
        self.lblValueDropDown.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
            $0.text = CommonLabels.select
            
        })
        
        lblNocType.text = PropertyDetailLabels.selectNOCType
        
        self.imgDropDown.forEach({
            $0.tintColor = .customBlackTitle
            $0.image = .ic_down_arrow
        })
        
        self.lblUnderLine.forEach { (lbl) in
            lbl.backgroundColor = .customSeparator
        }
        self.lblOwnerShipOf.text = PropertyDetailLabels.Ownershipof
        self.lblSalutation.text = PropertyDetailLabels.Salutation
        
        txtFlatCarpetAreaMT.isUserInteractionEnabled = false
        txtFlatBuiltUpAreaMt.isUserInteractionEnabled = false
        //        txtParkingCarpetAreaMt.isUserInteractionEnabled = false
        //        txtParkingBuiltUpAreaMt.isUserInteractionEnabled = false
        //
        //        [txtFlatCarpetAreaMT,
        //         txtFlatBuiltUpAreaMt,
        //         txtParkingCarpetAreaMt,
        //         txtParkingBuiltUpAreaMt].forEach({
        //            $0?.textColor = .darkGray
        //            $0?.text = "0.00"
        //        })
        
        
        SwitchWithinFamily.addTarget(self, action: #selector(switchValueUpdates), for: .valueChanged)
        SwitchWithinFamily.onTintColor = .customgreenTheamClr
        SwitchWithinFamily.offTintColor = .lightGray
        SwitchWithinFamily.isOn = false
        
        switchIsParking.addTarget(self, action: #selector(switchValueUpdates), for: .valueChanged)
        switchIsParking.onTintColor = .customgreenTheamClr
        switchIsParking.offTintColor = .lightGray
        switchIsParking.isOn = false
        
        self.btnPlusParking.setImage(.ic_plus, for: .normal)
        self.btnPlusParking.setTitle(nil, for: .normal)
        self.btnPlusParking.imageEdgeInsets = .init(top: 0, left: 0, bottom: 0, right: 0)
        self.btnPlusParking.contentEdgeInsets = .init(top: 5, left: IS_IPAD ? 45 : 35, bottom: 5, right: 5)
        
        self.txtParkingUnderTransfer.text = "0"
        self.txtParkingUnderTransfer.isUserInteractionEnabled = true
        
        self.stackViewParkingNumbers.superview?.setCustomCornerRadius(radius: 0, borderColor: .lightGray, borderWidth: 1)
        self.stackViewParkingNumbers.subviews.forEach({ $0.removeFromSuperview() })
        
        let pvView = ParkingNumbersTextField.loadNib()
        pvView.setTextField(font: .regularValueFont, textColor: .customBlack)
        pvView.setSeperator(isHidden: true)
        pvView.setCloseButton(isHidden: true)
        pvView.setDropDownButton(isHidden: false, target: self, action: #selector(pvViewBtnDropDownClicked))
        self.stackViewParkingNumbers.addArrangedSubview(pvView)
        
        self.viewNext.addGradient(ofColors: [UIColor.themeLightOrangeClr.cgColor, UIColor.themeDarkOrangeClr.cgColor], direction: .leftRight)
        self.viewNext.setFullCornerRadius()
        
        #if targetEnvironment(simulator)
            setTestData()
        #endif
                
    }
    
    
    func setTestData(){
        txtApplicantAddress.text = "Society"
        txtNameOfTransferor.text = "ABCDE"
        txtNameOfTransferee.text = "dhruv"
        txtNameOfSociety.text = "arti"
        txtpPresentNameOfLesse.text = "abc"
        txtNameBuilding.text = "53"
        txtTotalNoOfFloorsOfBuilding.text = "5"
        txtNoOfLift.text = "1"
        txtCompletionOfBuilding.text = "1999"
        txtChairManName.text = "ggg"
        txtChairmanContactNo.text = "9874563210"
        txtSecretaryName.text = "ppp"
        txtSecretaryContactNo.text = "7896541230"
        TxtSocietyEmailID.text = ""
        txtNoOfMembers.text = "90"
        txtFlatNo.text = "26"
        txtFloorOfFlat.text = "5"
        txtFlatCarpetAreaFeet.text = "700"
        txtFlatCarpetAreaMT.text = "13.05"
        txtFlatBuiltUpAreaFeet.text = "800"
        txtFlatBuiltUpAreaMt.text = "100.5"
        
        
    }
    
    //MARK: - Button Actions Methods
    
    @objc func switchValueUpdates(sender : CustomSwitch) {
        if sender == SwitchWithinFamily {
            
            self.viewRelationWithTransferor.isHidden = !sender.isOn
            
        } else {
            let subviews = self.stackViewParkingNumbers.subviews
            subviews.enumerated().forEach({
                if $0.offset != 0 {
                    $0.element.removeFromSuperview()
                } else {
                    ($0.element as? ParkingNumbersTextField)?.txtParkingNumber.text = ""
                }
            })
            
            txtParkingUnderTransfer.text = "1"
            txtParkingUnderTransfer.isUserInteractionEnabled = true
            txtParkingUnderTransfer.textColor = .customBlack
            
            self.stackViewParking.subviews.forEach({ $0.isHidden = $0 == self.viewIsParking ? false : !sender.isOn })
        }
    }
    
    
    @IBAction func btnNextOrSubmitClickHandler(_ sender: UIButton) {
        if isValid() {
            fillModel()
            
            
            //            addNOCApplicationWebServices()
            //
            registerContainerViewController?.webserviceModel.propertydetailmodel = webServiceModel
            registerContainerViewController?.removeOldController()
            registerContainerViewController?.selectedTab += 1
            registerContainerViewController?.addAccordingToSelectedTab()
        }
    }
    
    @IBAction func TapOnBtnNOCType(_ sender: UIButton) {
        var list = arrNOCOptions.map({ $0.title })
        list.insert(PropertyDetailLabels.selectNOCType, at: 0)
        
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblNocType.text, arrSource: [list], doneAction: { (rows, value) in
            
            self.lblNocType.text = getString(anything: value)
            
            if getString(anything: value) != PropertyDetailLabels.selectNOCType{
                
                self.webServiceModel.NOCtype_Id = self.arrNOCOptions[rows[0] - 1].id
                self.webServiceModel.NOCtype = getString(anything: value)
                self.nocType = NOCType.list[rows[0] - 1]
                self.Noc_Type_Id = self.arrNOCOptions[rows[0] - 1].id
                self.nocTypeChangeCallBack?()
            }
        }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func TapOnBtnDivision(_ sender: UIButton) {
        var list = self.arrDivisions.map({ $0.title })
        list.insert(CommonLabels.select, at: 0)
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblDivision.text, arrSource: [list], doneAction: { (rows, value) in
            
            self.lblDivision.text = getString(anything: value)
            if let index = rows.first{
                if getString(anything: value) == CommonLabels.select {
                    self.webServiceModel.Division_Id = ""
                } else {
                    self.webServiceModel.Division_Id = self.arrDivisions[index-1].id
                }
            }
        }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func TapOnBtnTypeOfPremises(_ sender: UIButton) {
        var list = self.arrPremisesOptions.map({ $0.title })
        list.insert(CommonLabels.select, at: 0)
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblTypeOfPremises.text, arrSource: [list], doneAction: { (rows, value) in
            
            self.lblTypeOfPremises.text = getString(anything: value)
            if let index = rows.first{
                if getString(anything: value) == CommonLabels.select {
                    self.webServiceModel.PremisesType_Id = ""
                } else {
                    self.webServiceModel.PremisesType_Id = self.arrPremisesOptions[index-1].id
                }
            }
        }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func TapOnBtnParking(_ sender: UIButton) {
        /* StringPickerViewController.show(pickerType: .single, initialSelection: self.lblParking.text, arrSource: [self.arrParkingOptions], doneAction: { (rows, value) in
         
         //           / self.lblParking.text = getString(anything: value)
         if getString(anything: value) == CommonLabels.select {
         self.webServiceModel.Parking_Type = ""
         } else {
         self.webServiceModel.Parking_Type = getString(anything: value)
         }
         
         
         }, animated: true, origin: sender, onVC: self)*/
    }
    
    
    
    @IBAction func TapOnBtnOwnershipOf(_ sender: UIButton) {
        
         StringPickerViewController.show(pickerType: .single, initialSelection: self.lblOwnerShipOf.text, arrSource: [self.arrOwnerShipOf], doneAction: { (rows, value) in
         
         //           / self.lblParking.text = getString(anything: value)
             if getString(anything: value) == PropertyDetailLabels.Ownershipof {
                 self.webServiceModel.ownerShipOf = ""
             } else {
                 self.webServiceModel.ownerShipOf = getString(anything: value)
             }
            self.lblOwnerShipOf.text = getString(anything: value)
             if self.lblOwnerShipOf.text == "Company" {
                 self.lblSalutation.superview?.isHidden = true
                 self.txtFirstName.superview?.isHidden = true
                 self.txtMiddleName.superview?.isHidden = true
                 self.txtLastName.superview?.isHidden = true
                 
                 self.lblSalutation.text = PropertyDetailLabels.Salutation
                 self.txtFirstName.text = ""
                 self.txtMiddleName.text = ""
                 self.txtLastName.text = ""
                 
                 self.txtCompanyName.superview?.isHidden = false
                 
             } else if self.lblOwnerShipOf.text == "Personal" {
                 self.lblSalutation.superview?.isHidden = false
                 self.txtFirstName.superview?.isHidden = false
                 self.txtMiddleName.superview?.isHidden = false
                 self.txtLastName.superview?.isHidden = false
                 
                 self.txtCompanyName.superview?.isHidden = true
                 self.txtCompanyName.text = ""
                 
             } else {
                 self.lblSalutation.superview?.isHidden = true
                 self.txtFirstName.superview?.isHidden = true
                 self.txtMiddleName.superview?.isHidden = true
                 self.txtLastName.superview?.isHidden = true
                 
                 self.lblSalutation.text = PropertyDetailLabels.Salutation
                 self.txtFirstName.text = ""
                 self.txtMiddleName.text = ""
                 self.txtLastName.text = ""
                 
                 self.txtCompanyName.superview?.isHidden = true
                 self.txtCompanyName.text = ""
             }
         
         }, animated: true, origin: sender, onVC: self)
    }
    
    
    
    @IBAction func TapOnBtnSalutation(_ sender: UIButton) {
        
         StringPickerViewController.show(pickerType: .single, initialSelection: self.lblSalutation.text, arrSource: [self.arrSalutation], doneAction: { (rows, value) in
         
         //           / self.lblParking.text = getString(anything: value)
         if getString(anything: value) == "Salutation" {
         self.webServiceModel.salutation = ""
         } else {
         self.webServiceModel.salutation = getString(anything: value)
         }
             self.lblSalutation.text = getString(anything: value)
         
         }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func btnPlusParkingClicked(_ sender: UIButton) {
        
        if getInteger(anything: txtParkingUnderTransfer.text) < self.stackViewParkingNumbers.subviews.count + 1 {
            return
        }
        
        (self.stackViewParkingNumbers.subviews[self.stackViewParkingNumbers.subviews.count - 1] as? ParkingNumbersTextField)?.setSeperator(isHidden: false)
        let pvView = ParkingNumbersTextField.loadNib()
        pvView.setTextField(font: .regularValueFont, textColor: .customBlack)
        pvView.setSeperator(isHidden: true)
        pvView.btnCross.tag = self.stackViewParkingNumbers.subviews.count
        pvView.setCloseButton(isHidden: false, target: self, action: #selector(pvViewBtnCloseClicked))
        pvView.btnDropDown.tag = self.stackViewParkingNumbers.subviews.count
        pvView.setDropDownButton(isHidden: false, target: self, action: #selector(pvViewBtnDropDownClicked))
        
        self.stackViewParkingNumbers.addArrangedSubview(pvView)
    }
    
    @objc func pvViewBtnCloseClicked(_ sender: UIButton) {
        self.stackViewParkingNumbers.subviews[sender.tag].removeFromSuperview()
        
        let subViews = self.stackViewParkingNumbers.subviews
        subViews.enumerated().forEach({
            let pvView = ($0.element as! ParkingNumbersTextField)
            pvView.btnCross.tag = $0.offset
            pvView.btnDropDown.tag = $0.offset
            pvView.setSeperator(isHidden: (subViews.count - 1) == $0.offset)
        })
    }
    
    @objc func pvViewBtnDropDownClicked(_ sender: UIButton) {
        let subViews = self.stackViewParkingNumbers.subviews
        subViews.enumerated().forEach({
            let pvView = ($0.element as! ParkingNumbersTextField)
            
            if sender.tag == pvView.btnDropDown.tag{
                StringPickerViewController.show(pickerType: .single, initialSelection: pvView.lblParkigType.text, arrSource: [self.arrParkingOptions], doneAction: { (rows, value) in
                    
                    pvView.lblParkigType.text = getString(anything: value)
                }, animated: true, origin: sender, onVC: self)
            }
        })
    }
}

//MARK: - Validations
extension PropertyDetailViewController: ValidationProtocol {
    
    func isValid() -> Bool {
        var isValid : Bool = false
        if getString(anything: self.lblOwnerShipOf.text) == PropertyDetailLabels.Ownershipof {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseSelectOwnership)
        } else if getString(anything: self.lblOwnerShipOf.text) == "Personal".localizedString && self.lblSalutation.text == PropertyDetailLabels.Salutation {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseSelectSalutation)
        } else if getString(anything: self.lblOwnerShipOf.text) == "Personal".localizedString && getString(anything: self.txtFirstName.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseSelectFirstName)
            self.txtFirstName.becomeFirstResponder()
        } else if getString(anything: self.lblOwnerShipOf.text) == "Personal".localizedString && getString(anything: self.txtMiddleName.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseSelectMiddleName)
            self.txtMiddleName.becomeFirstResponder()
        } else if getString(anything: self.lblOwnerShipOf.text) == "Personal".localizedString && getString(anything: self.txtLastName.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseSelectLastname)
            self.txtLastName.becomeFirstResponder()
        } else if getString(anything: self.lblOwnerShipOf.text) == "Company".localizedString && getString(anything: self.txtCompanyName.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseSelectCompanyName)
            self.txtCompanyName.becomeFirstResponder()
        } else if getString(anything: self.lblNocType.text) == PropertyDetailLabels.selectNOCType {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseSelectNocType)
            self.lblNocType.becomeFirstResponder()
            
        } else if getString(anything: self.lblDivision.text) == CommonLabels.select {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseSelectDivision)
            self.lblDivision.becomeFirstResponder()
            
        } else if getString(anything: self.txtNameOfTransferor.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.NameOfTheTransferorisrequired)
            self.txtNameOfTransferor.becomeFirstResponder()
            
        } else if getString(anything: self.txtNameOfTransferee.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.NameOfTheTransfereeisrequired)
            self.txtNameOfTransferee.becomeFirstResponder()
            
        } else if getString(anything: self.txtApplicantAddress.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.ApplicantAddressisrequired)
            self.txtApplicantAddress.becomeFirstResponder()
            
        } else if getString(anything: self.txtCSPlotNo.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.CSNoPlotNoisrequired)
            self.txtCSPlotNo.becomeFirstResponder()
            
        } else if getString(anything: self.txtNameOfSociety.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.NameofSocietyisrequired)
            self.txtNameOfSociety.becomeFirstResponder()
            
        } else if getString(anything: self.txtNameBuilding.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.NameofBuildingPremisesisrequired)
            self.txtNameBuilding.becomeFirstResponder()
            
        } else if getString(anything: self.txtTotalNoOfFloorsOfBuilding.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.Totalnumberoffloorsofbuildingisrequired)
            self.txtTotalNoOfFloorsOfBuilding.becomeFirstResponder()
            
        } /*else if getString(anything: self.txtNoOfLift.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.NoofLiftisrequired)
            self.txtNoOfLift.becomeFirstResponder()
            
        }*/ else if !getString(anything: self.txtCompletionOfBuilding.text).isStringEmpty && getString(anything: self.txtCompletionOfBuilding.text).count < 4 {
            Global.shared.showBanner(message: PropertyDetailsMessages.completionofbuildingisrequired)
            self.txtCompletionOfBuilding.becomeFirstResponder()
            
        }/* else if getString(anything: self.txtChairManName.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.societychairmannameisrequired)
            self.txtChairManName.becomeFirstResponder()
            
        }*/ else if isValidMobileNumberChairMan() == false{
            
        } /*else if getString(anything: self.txtSecretaryName.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.societysecretarynameisrequired)
            self.txtSecretaryName.becomeFirstResponder()
            
        }*/ else if isValidMobileNumberSecretary() == false {
            
            
        } else if !getString(anything: self.TxtSocietyEmailID.text).isStringEmpty,!getString(anything: self.TxtSocietyEmailID.text).isValidEmail {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseEnteraValidEmailAddress)
            self.TxtSocietyEmailID.becomeFirstResponder()
            
        }/* else if getString(anything: self.txtNoOfMembers.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.NoOfMembersisrequired)
            self.txtNoOfMembers.becomeFirstResponder()
            
        }*/ else if getString(anything: self.lblTypeOfPremises.text) == CommonLabels.select {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseselectTypeOfPremises)
            self.lblTypeOfPremises.becomeFirstResponder()
            
        } else if getString(anything: self.txtFlatNo.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.FlatNumberisrequired)
            self.txtFlatNo.becomeFirstResponder()
            
        } else if getString(anything: self.txtFloorOfFlat.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.FloorOfFlatisrequired)
            self.txtFloorOfFlat.becomeFirstResponder()
            
        }/* else if getString(anything: self.txtFlatCarpetAreaFeet.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.AreaOfFlatSqFtisrequired)
            self.txtFlatCarpetAreaFeet.becomeFirstResponder()
            
        } else if getString(anything: self.txtFlatBuiltUpAreaFeet.text).isStringEmpty {
            Global.shared.showBanner(message: PropertyDetailsMessages.Pleaseenteravalue)
            self.txtFlatBuiltUpAreaFeet.becomeFirstResponder()
            
        }*/ else if self.switchIsParking.isOn {
            let subviews = self.stackViewParkingNumbers.subviews
            var isFilled = true
            if subviews.count == getInteger(anything: txtParkingUnderTransfer.text)
            {
                subviews.enumerated().forEach({
                    if let element = $0.element as? ParkingNumbersTextField{
                        /*if getString(anything: element.lblParkigType.text) == CommonLabels.select {
                         Global.shared.showBanner(message: "Please select parking type")
                         isFilled = false
                         }else if getString(anything: element.txtParkingCarpetAreaFeet.text).isStringEmpty {
                         isFilled = false
                         Global.shared.showBanner(message: "Please select parking type")
                         element.txtParkingCarpetAreaFeet.becomeFirstResponder()
                         }else   if getString(anything: element.txtParkingCarpetAreaMt.text).isStringEmpty {
                         isFilled = false
                         Global.shared.showBanner(message: "Please select parking type")
                         element.txtParkingCarpetAreaMt.becomeFirstResponder()
                         }else if getString(anything: element.txtParkingBuiltUpAreafeet.text).isStringEmpty {
                         isFilled = false
                         Global.shared.showBanner(message: "Please select parking type")
                         element.txtParkingBuiltUpAreafeet.becomeFirstResponder()
                         }else if getString(anything: element.txtParkingBuiltUpAreaMt.text).isStringEmpty {
                         isFilled = false
                         Global.shared.showBanner(message: "Please select parking type")
                         element.txtParkingBuiltUpAreaMt.becomeFirstResponder()
                         }else*/ if getString(anything: element.txtParkingNumber.text).isStringEmpty {
                             isFilled = false
                             Global.shared.showBanner(message: PropertyDetailsMessages.ParkingNumberisrequired)
                             element.txtParkingNumber.becomeFirstResponder()
                         }
                    }
                })
            }else {
                isFilled = false
                Global.shared.showBanner(message: PropertyDetailsMessages.ParkingDetailsDontMatch)
                self.txtParkingUnderTransfer.becomeFirstResponder()
            }
            
            
            //            if isFilled {
            isValid = isFilled
            //            }
            
        } else {
            isValid = true
        }
        return isValid
    }
    
    func isValidMobileNumberChairMan() -> Bool {
        if !getString(anything: self.txtChairmanContactNo.text).isStringEmpty && (getString(anything: self.txtChairmanContactNo.text)).count < 10 {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseEnterAValueMatchingThePattern)
            //Global.shared.showBanner(message: PropertyDetailsMessages.chairmancontactnumberisrequired)
            self.txtChairmanContactNo.becomeFirstResponder()
            return false
            
        }/* else if (getString(anything: self.txtChairmanContactNo.text)).count < 10 {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseEnterAValueMatchingThePattern)
            //            Global.shared.showBanner(message: PropertyDetailsMessages.chairmancontactnumberisrequired)
            self.txtChairmanContactNo.becomeFirstResponder()
            return false
            
        }*/ else {
            return true
        }
    }
    
    func isValidMobileNumberSecretary() -> Bool {
        if !getString(anything: self.txtSecretaryContactNo.text).isStringEmpty && (getString(anything: self.txtSecretaryContactNo.text)).count < 10 {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseEnterAValueMatchingThePattern)
            //Global.shared.showBanner(message: PropertyDetailsMessages.secretarycontactnumberisrequired)
            self.txtSecretaryContactNo.becomeFirstResponder()
            return false
            
        }/* else if (getString(anything: self.txtSecretaryContactNo.text)).count < 10 {
            Global.shared.showBanner(message: PropertyDetailsMessages.pleaseEnterAValueMatchingThePattern)
            self.txtSecretaryContactNo.becomeFirstResponder()
            return false
            
        }*/ else {
            return true
        }
    }
}


//MARK: - UITextField Delegate
extension PropertyDetailViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let fullString = NSString(string: textField.text ?? "").replacingCharacters(in: range, with: string)
        
        if (textField == txtChairmanContactNo) || ( textField == txtSecretaryContactNo ) {
            
            let numberSet = NSCharacterSet(charactersIn: NUMERIC).inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if (string == numberFiltered) && (fullString.count < (MOBILE_LENGTH_VALIDATION + 1)) {
                if (fullString.hasPrefix("6") || fullString.hasPrefix("7") || fullString.hasPrefix("8") || fullString.hasPrefix("9")) || string.isEmpty {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
            
        } else if (textField == txtTotalNoOfFloorsOfBuilding) || (textField == txtNoOfLift) {
            
            let numberSet = CharacterSet(charactersIn: NUMERIC).inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return (string == numberFiltered) && (fullString.count < 4)
            
        } else if (textField == txtCSPlotNo) {
            
            return fullString.count < 12
            
        } else if (textField == txtCompletionOfBuilding) {
            
            let numberSet = NSCharacterSet(charactersIn: NUMERIC).inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if (string == numberFiltered) && (fullString.count < 5) {
                if (fullString.hasPrefix("1") || fullString.hasPrefix("2")) || string.isEmpty {
                    return true
                } else {
                    return false
                }
            } else {
                return false
            }
        } else if (textField == txtNoOfMembers) {
            
            let numberSet = CharacterSet(charactersIn: NUMERIC).inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return (string == numberFiltered) && (fullString.count < 6)
            
        } else if (textField == txtFlatNo) || (textField == txtFloorOfFlat) {
            
            return fullString.count < 21
            
        } /*else if (textField == txtFlatCarpetAreaFeet) || (textField == txtFlatBuiltUpAreaMt) || (textField == txtParkingCarpetAreaFeet) || (textField == txtParkingBuiltUpAreafeet) {
         
         if fullString.first == "." || fullString.components(separatedBy: ".").count > 2 {
         return false
         }
         
         let numberSet = CharacterSet(charactersIn: NUMERIC + ".").inverted
         let compSepByCharInSet = string.components(separatedBy: numberSet)
         let numberFiltered = compSepByCharInSet.joined(separator: "")
         return (string == numberFiltered) && (fullString.count < 9)
         
         } */else if (textField == txtParkingUnderTransfer)  {
            
            let numberSet = CharacterSet(charactersIn: "123456789").inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return (string == numberFiltered) && (fullString.count < 2)
            
         }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == txtFlatCarpetAreaFeet{
            txtFlatCarpetAreaMT.text = getAreaFromFeetToMT(Feet: getDouble(anything: txtFlatCarpetAreaFeet.text))
        } else if textField == txtFlatBuiltUpAreaFeet {
            txtFlatBuiltUpAreaMt.text = getAreaFromFeetToMT(Feet: getDouble(anything: txtFlatBuiltUpAreaFeet.text))
        } /*else if textField == txtParkingCarpetAreaFeet {
         txtParkingCarpetAreaMt.text = getAreaFromFeetToMT(Feet: getInteger(anything: txtParkingCarpetAreaFeet.text))
         } else if textField == txtParkingBuiltUpAreafeet {
         txtParkingBuiltUpAreaMt.text = getAreaFromFeetToMT(Feet: getInteger(anything: txtParkingBuiltUpAreafeet.text))
         } */else if textField == txtParkingUnderTransfer {
            txtParkingUnderTransfer.isUserInteractionEnabled = false
            txtParkingUnderTransfer.textColor = .darkGray
         }
    }
    
    func getAreaFromFeetToMT(Feet:Double) -> String {
        let areaInMT = String(format: "%.2f", (Double(Feet)*(0.0929)))
        //getString(anything: (Double(Feet)/10.764))
        return areaInMT
    }
}

//MARK:-  Web Service model
extension PropertyDetailViewController {
    
    func getWebserviceforNOCType() {
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        webServiceModel.getNOCTypeDataData { (responseModel) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if let response = responseModel {
                print(response)
                self.arrNOCOptions.append(contentsOf: response.list)
            }
        }
    }
    
    func getDropDownDataFromWebServices() {
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.webServiceModel.getDropDownData { arr_noc,arr_premies in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            self.arrNOCOptions = arr_noc
            self.arrPremisesOptions = arr_premies
        }
    }
    
    
    func getDivisionDataFromWebServices() {
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.webServiceModel.getDropDownListApi { arr_Divisions in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            self.arrDivisions = arr_Divisions
        }
    }
    
    
    func fillModel(){
        
        if getString(anything: lblOwnerShipOf.text) == "Personal".localizedString {
            self.webServiceModel.firstName = getString(anything: self.txtFirstName.text)
            self.webServiceModel.middleName = getString(anything: self.txtMiddleName.text)
            self.webServiceModel.lastname = getString(anything: self.txtLastName.text)
        } else if getString(anything: lblOwnerShipOf.text) == "Company".localizedString {
            self.webServiceModel.companyName = getString(anything: self.txtCompanyName.text)
        }
        
        self.webServiceModel.name_of_transferor = getString(anything: self.txtNameOfTransferor.text)
        self.webServiceModel.second_name_of_the_transferee = getString(anything: self.txtNameOfTransferee_two.text)
        self.webServiceModel.third_name_of_the_transferee = getString(anything: self.txtNameOfTransferee_three.text)
        self.webServiceModel.society_address = getString(anything: self.txtSocietyAddress.text)
        self.webServiceModel.applicant_address = getString(anything: self.txtApplicantAddress.text)
        self.webServiceModel.NOCtype = getString(anything: lblNocType.text)
        self.webServiceModel.Division = getString(anything: lblDivision.text)
        self.webServiceModel.NameOfTransferee = getString(anything: txtNameOfTransferee.text)
        self.webServiceModel.CSNo = getString(anything: txtCSPlotNo.text)
        self.webServiceModel.NameOfSociety = getString(anything: txtNameOfSociety.text)
        self.webServiceModel.PresentNameOfLesse = getString(anything: txtpPresentNameOfLesse.text)
        self.webServiceModel.NameOfBuilding = getString(anything: txtNameBuilding.text)
        self.webServiceModel.TotalNoOfFloorOfBuilding = getString(anything: txtTotalNoOfFloorsOfBuilding.text)
        self.webServiceModel.NoOfLift = getString(anything: txtNoOfLift.text)
        self.webServiceModel.CompletionOfBuilding = getString(anything: txtCompletionOfBuilding.text)
        self.webServiceModel.SocietyChairmanName = getString(anything: txtChairManName.text)
        self.webServiceModel.ChairmanContact = getString(anything: txtChairmanContactNo.text)
        self.webServiceModel.SecretoryName = getString(anything: txtSecretaryName.text)
        self.webServiceModel.SecretoryContactNO = getString(anything: txtSecretaryContactNo.text)
        self.webServiceModel.SocietyEmail = getString(anything: TxtSocietyEmailID.text)
        self.webServiceModel.NoOfMember = getString(anything: txtNoOfMembers.text)
        self.webServiceModel.PremisesType = getString(anything: lblTypeOfPremises.text)
        self.webServiceModel.FlatNumber = getString(anything: txtFlatNo.text)
        self.webServiceModel.FloorOfFlat = getString(anything: txtFloorOfFlat.text)
        self.webServiceModel.CarpetAreaOfFlatFeet = getString(anything: txtFlatCarpetAreaFeet.text)
        self.webServiceModel.CarpetAreofFlatMT = getString(anything: txtFlatCarpetAreaMT.text)
        self.webServiceModel.BuiltupAreaOfFlatFeet = getString(anything: txtFlatBuiltUpAreaFeet.text)
        self.webServiceModel.BuiltUpAreaOfFlatMt = getString(anything: txtFlatBuiltUpAreaMt.text)
        
        if SwitchWithinFamily.isOn {
            self.webServiceModel.TransferWithinFamily = getString(anything: SwitchWithinFamily.isOn)
            self.webServiceModel.RelationWithTransferor = getString(anything: txtRelation.text)
        } else {
            self.webServiceModel.TransferWithinFamily = "0"
            self.webServiceModel.RelationWithTransferor = "-"
        }
        
        if self.switchIsParking.isOn {
            self.webServiceModel.IsParking = getString(anything: switchIsParking.isOn)
            let subviews = self.stackViewParkingNumbers.subviews
            var arrParkingNO : [ParkingDetailDataModel] = []
            subviews.enumerated().forEach({
                if let element = $0.element as? ParkingNumbersTextField{
                    let parkingDetailDataModel = ParkingDetailDataModel()
                    
                    parkingDetailDataModel.Parking_Type = getString(anything: element.lblParkigType.text)
                    parkingDetailDataModel.CarpetAreofParkingFeet = getString(anything: element.txtParkingCarpetAreaFeet.text)
                    parkingDetailDataModel.CarpetAreofParkingMT = getString(anything: element.txtParkingCarpetAreaMt.text)
                    parkingDetailDataModel.BuiltupAreaOfParkingFeet = getString(anything: element.txtParkingBuiltUpAreafeet.text)
                    parkingDetailDataModel.BuiltUpAreaOfParkingMt = getString(anything: element.txtParkingBuiltUpAreaMt.text)
                    parkingDetailDataModel.ParkingNumber = getString(anything: element.txtParkingNumber.text)
                    
                    arrParkingNO.append(parkingDetailDataModel)
                }
            })
            /*self.webServiceModel.CarpetAreofParkingFeet = getString(anything: txtParkingCarpetAreaFeet.text)
             self.webServiceModel.CarpetAreofParkingMT = getString(anything: txtParkingCarpetAreaMt.text)
             self.webServiceModel.BuiltupAreaOfParkingFeet = getString(anything: txtParkingBuiltUpAreafeet.text)
             self.webServiceModel.BuiltUpAreaOfParkingMt = getString(anything: txtParkingBuiltUpAreaMt.text)*/
            self.webServiceModel.NoOfParkingUnderTransfer = getString(anything: txtParkingUnderTransfer.text)
            //            self.webServiceModel.Parking_Type = getString(anything: lblParking.text)
            
            self.webServiceModel.arrparkingDetail = arrParkingNO
            
        }else {
            self.webServiceModel.IsParking = "0"
            self.webServiceModel.CarpetAreofParkingFeet = ""
            self.webServiceModel.CarpetAreofParkingMT = ""
            self.webServiceModel.BuiltupAreaOfParkingFeet = ""
            self.webServiceModel.BuiltUpAreaOfParkingMt = ""
            self.webServiceModel.NoOfParkingUnderTransfer = "0"
            //            self.webServiceModel.Parking_Type = ""
            self.webServiceModel.parkingNos = []
            self.webServiceModel.arrparkingDetail = []
        }
    }
}

