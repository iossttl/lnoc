//
//  PropertyDetailModel.swift
//  LNOC
//
//  Created by Dhruv Patel on 04/01/22.
//

import Foundation


class PropertyDetailWebServiceModel: NSObject {
    
    var NOCtype:String = ""
    var NOCtype_Id:String = ""
    var Division_Id : String = ""
    var Division : String = ""
    var NameOfTransferee : String = ""
    var TransferWithinFamily: String = ""
    var RelationWithTransferor:String = ""
    var CSNo:String = ""
    var NameOfSociety:String = ""
    var PresentNameOfLesse : String = ""
    var NameOfBuilding : String = ""
    var TotalNoOfFloorOfBuilding : String = ""
    var NoOfLift : String = ""
    var CompletionOfBuilding:String = ""
    var SocietyChairmanName:String = ""
    var ChairmanContact:String = ""
    var SecretoryName : String = ""
    var SecretoryContactNO : String = ""
    var SocietyEmail : String = ""
    var NoOfMember : String = ""
    var PremisesType_Id : String = ""
    var PremisesType : String = ""
    var FlatNumber:String = ""
    var FloorOfFlat:String = ""
    var CarpetAreaOfFlatFeet:String = ""
    var CarpetAreofFlatMT : String = ""
    var BuiltupAreaOfFlatFeet : String = ""
    var BuiltUpAreaOfFlatMt : String = ""
    var IsParking: String = ""
    var Parking_Type:String = ""
    var CarpetAreofParkingFeet : String = ""
    var CarpetAreofParkingMT : String = ""
    var BuiltupAreaOfParkingFeet : String = ""
    var BuiltUpAreaOfParkingMt : String = ""
    var NoOfParkingUnderTransfer:String = ""
    var parkingNos: [String] = []
    
    var ownerShipOf:String = ""
    var salutation:String = ""
    var firstName:String = ""
    var middleName:String = ""
    var lastname:String = ""
    var companyName:String = ""
    var name_of_transferor:String = ""
    var second_name_of_the_transferee:String = ""
    var third_name_of_the_transferee:String = ""
    var applicant_address:String = ""
    var society_address:String = ""
    
    var arrparkingDetail : [ParkingDetailDataModel] = []

    fileprivate var getNOCTypeUrl : String {
        return BASEURL + API.getNOCType
    }
    
    fileprivate var landFilterURL:String {
        BASEURL + API.landfilterdropdown
    }
    
    enum keys : String {
        case citizen_users_id = "citizen_users_id"
        case nocType = "noctypes"
        case nocType_Id = "ms_noc_types_id"
        case Division_Id = "ms_divisions_id"
        case Division = "division"
        case NameOFTranferee = "name_of_the_transferee"
        case TranserWithinFamily = "is_transfer_within_family"
        case RelationWithTransferor = "relation_with_transferer"
        case CSNo = "ms_cs_numbers_id"
        case nameOFSociety = "name_of_society"
        case PresentNameOfLesse = "present_name_of_lease"
        case NameOfBuilding = "name_of_building_premises"
        case TotalNoOFFloor = "total_floors_of_building"
        case NoOfLift = "no_of_lift"
        case CompletionYear = "completion_of_building"
        case ChairmaanName = "society_chairman_name"
        case ChaiemanContactNO = "chairman_contact_no"
        case secretoryName = "society_secretary_name"
        case SecretoryContactNO = "secretary_contact_no"
        case SocietyEmail = "society_email_id"
        case NoOFMember = "no_of_members"
        case type_of_premises = "type_of_premises"
        case FlatNumber = "flat_number"
        case FloorOfFlat = "floor_of_flat"
        case carpetAreaFlatFeet = "area_of_flat_sq_ft"
        case CarpetAreaFlatMT = "area_of_flat_sq_mt"
        case BuiltUpAreaFlatFeet = "area_of_flat_sq_ft_builtup"
        case BuiltUpAreaFlatMT = "area_of_flat_sq_mt_builtup"
        case isParkig = "is_parking"
        case Parking = "parking"
        case carpetAreaParkingFeet = "parking_area_sq_ft"
        case CarpetAreaParkingMT = "parking_area_sq_mt"
        case numberOfParkingUnderTransfer = "number_of_parking"
        case BuiltUpAreaParkingFeet = "parking_area_sq_ft_builtup"
        case BuiltUpAreaParkingMT = "parking_area_sq_mt_builtup"
        case ParkingNumbers = "parking_number"
        case divisions = "divisions"
        case landList = "landList"
        
        case ownerShipOf = "ownership_of"
        case salutation = "salutation"
        case firstName = "first_name"
        case middleName = "middle_name"
        case lastname = "last_name"
        case companyName = "company_name"
        case second_name_of_the_transferee = "second_name_of_the_transferee"
        case third_name_of_the_transferee = "third_name_of_the_transferee"
        
        case name_of_transferor = "name_of_transferor"
        case applicant_address = "applicant_address"
        case society_address = "society_address"
        
    }
    
    //MARK:- Initializer
    override init() {
        super.init()
    }
    
    func getNOCTypeDataData(block: @escaping ((DropDownResponseModel?) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {

            let param = WShandler.commonDict()
            WShandler.shared.getWebRequest(urlStr:getNOCTypeUrl,param: param) { (json, flag) in
                var responseModel:DropDownResponseModel?
                if (flag == 200) {
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        responseModel = DropDownResponseModel(dictNOCType: json)
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel)
            }
        }else{
            block(nil)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
    
    
    
    func getDropDownData(block: @escaping (([DropDownModel],[DropDownModel]) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {

            let param = WShandler.commonDict()
            WShandler.shared.getWebRequest(urlStr:getNOCTypeUrl,param: param) { (json, flag) in
                var NOCModel:[DropDownModel] = []
                var PremisesModel:[DropDownModel] = []

                if (flag == 200) {
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
//                        responseModel = DropDownResponseModel(dictNOCType: json)
                        NOCModel = (json[keys.landList.rawValue] as? [Dictionary] ?? []).map({ DropDownModel(dict: $0) })
                        PremisesModel = (json[keys.type_of_premises.rawValue] as? [Dictionary] ?? []).map({ DropDownModel(dict: $0) })
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(NOCModel,PremisesModel)
            }
        }else{
            block([],[])
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
    
    //Get Division
    func getDropDownListApi(block:@escaping (([DropDownModel]) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            
            let param = WShandler.commonDict()
            
            WShandler.shared.getWebRequest(urlStr: landFilterURL, param: param) { (json, flag) in
                
                if (flag == 200) {
                    
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        let divisions = (json[keys.divisions.rawValue] as? [Dictionary] ?? []).map({ DropDownModel(dict: $0) })
                        block(divisions)
                        
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        block([])
                    }
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block([])
                }
            }
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block([])
        }
    }
    
}
