//
//  ParkingDetailDataModel.swift
//  LNOC
//
//  Created by Dhruv Patel on 02/02/22.
//

import Foundation


class ParkingDetailDataModel: NSObject {
    
    var Parking_Type:String = ""
    var CarpetAreofParkingFeet : String = ""
    var CarpetAreofParkingMT : String = ""
    var BuiltupAreaOfParkingFeet : String = ""
    var BuiltUpAreaOfParkingMt : String = ""
    var ParkingNumber:String = ""

    enum Keys: String {
        case Parking_Type = "nameofTransferer"
        case CarpetAreofParkingFeet = "nameofTransferee"
        case CarpetAreofParkingMT = "year"
        case BuiltupAreaOfParkingFeet = "type"
        case BuiltUpAreaOfParkingMt = "NOCObtained"
        case ParkingNumber = "ObtainedNOCDoc"
      
    }
    
   /* init(dict: Dictionary) {
        Parking_Type = getString(anything: dict[Keys.Parking_Type.rawValue])
        CarpetAreofParkingFeet = getString(anything: dict[Keys.CarpetAreofParkingFeet.rawValue])
        CarpetAreofParkingMT = getString(anything: dict[Keys.CarpetAreofParkingMT.rawValue])
        BuiltupAreaOfParkingFeet = getString(anything: dict[Keys.BuiltupAreaOfParkingFeet.rawValue])
        BuiltUpAreaOfParkingMt = getString(anything: dict[Keys.BuiltUpAreaOfParkingMt.rawValue])
        ParkingNumber = getString(anything: dict[Keys.ParkingNumber.rawValue])
        super.init()
    }*/
    
    //MARK:- Initializer
    override init() {
        super.init()
    }
}
