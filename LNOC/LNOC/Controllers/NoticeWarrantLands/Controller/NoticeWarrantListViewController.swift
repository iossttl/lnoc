//
//  PlayerListViewController.swift
//  Khel Mahakumbh
//
//  Created by Dhruv Patel on 27/12/21.
//

import UIKit

class NoticeWarrantListViewController: HeaderViewController {

    
    @IBOutlet weak var tblList: UITableView!

    fileprivate var noticeWarrantTblViewDelegateDataSource : NoticeWarrantTblViewDelegateDataSource!
    
    var isPullToRefresh = false
    var refreshControl = UIRefreshControl()
    var arrNoticeWarrant : [NoticeWarrantListDataModel] = []
    var pageNo = 1
    var dictData:Dictionary = [:]
    fileprivate var webServiceModel = NoticeWarrantWebServicesModel()

    var land_id = ""
    var filter_Notice_Type = ""
    var filter_Template_Type = ""
    var filter_Template = ""
    var filter_Status = ""
    var filter_Template_id = ""
    var filter_Status_id = ""
    var filter_Land_Owner = ""
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpHeader()
        self.setUpPullToRefresh()
        setUpTblView()
        showFilter()
        self.getListData()
        changeLeadingTrailingForiPad(view: self.view)
    }
    
    deinit {
        DebugLog("Deinit NoticeWarrantListViewController")
    }
    
    fileprivate func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.NoticeWarrantLands)
        setHeaderView_BackImage()

    }
    
    override func btnSyncTapped(_ sender: UIButton) {
        let dvc = WarrantFilterViewController.instantiate(appStoryboard: .warrantFilter)
        dvc.delegate = self
        dvc.filter_Notice_Type = self.filter_Notice_Type
        dvc.filter_template_Type = self.filter_Template_Type
        dvc.filter_template = self.filter_Template
        dvc.filter_template_id = self.filter_Template_id
        dvc.filter_status = self.filter_Status
        dvc.filter_status_id = self.filter_Status_id
        dvc.filter_LandOwner = self.filter_Land_Owner
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    fileprivate func setUpPullToRefresh() {
        refreshControl = self.tblList.addRefreshControl(target: self, action: #selector(pullToRefreshCalled(_:)))
        refreshControl.attributedTitle = NSAttributedString(string: CommonLabels.pull_to_refresh, attributes: [NSAttributedString.Key.font : UIFont.boldValueFont, NSAttributedString.Key.foregroundColor : UIColor.customSubtitle])
    }
    
    //MARK:- Selector Methods
    @objc fileprivate func pullToRefreshCalled(_ sender: UIRefreshControl) {
        self.arrNoticeWarrant = []
        isPullToRefresh = true
        self.pageNo = 1
        isPullToRefresh = false
        self.getListData()
        self.refreshControl.endRefreshing()
    }
    
    fileprivate func setUpTblView() {
//        let jsonData = Global.shared.readJsonFile(ofName: "home")
//               if let list = (jsonData["warrant"] as? [Dictionary]) {
//                   arrNoticeWarrant = list.map({NoticeWarrantListDataModel(dict:$0)})
//               }
        
        if (self.noticeWarrantTblViewDelegateDataSource == nil) {
            self.noticeWarrantTblViewDelegateDataSource = NoticeWarrantTblViewDelegateDataSource(arrData: self.arrNoticeWarrant, tbl: self.tblList, delegate: self, BtnDelegate: self)
        } else {
            self.noticeWarrantTblViewDelegateDataSource.reloadData(arrData: self.arrNoticeWarrant)
        }
    }
    
    
}

extension NoticeWarrantListViewController:TblViewDelegate {
    
}

extension NoticeWarrantListViewController:BtnDeleteViewDocClickDelegate {
    
    func btnDeleteTapped(index: Int) {
        let model = self.arrNoticeWarrant[index]

        self.webServiceModel.land_notice_warrant_generate_id = model.id
        self.deleteListData()
        
    }
    
    func btnViewDocTapped(index: Int) {
        let model = self.arrNoticeWarrant[index]
//        let dvc = PDFDocumentViewerViewController.instantiate(appStoryboard: .pdfDocumentViewer)
//        dvc.documentURLStr = model.doc_URL
//        self.navigationController?.pushViewController(dvc, animated: true)
        openURL(urlString:  model.doc_URL)
    }
}

//MARK: - ------ Filter Screen Delegate Methods -----------
extension NoticeWarrantListViewController : ProtocolForNoticeFilter {
    
    func implimant(NoticeType: String, TemplateType: String, Template: String, Template_Id: String, Status: String, status_Id: String, landOwner:String) {
 
        self.filter_Notice_Type = NoticeType
        self.filter_Template_Type = TemplateType
        self.filter_Template = Template
        self.filter_Status = Status
        self.filter_Template_id = Template_Id
        self.filter_Status_id = status_Id
        self.filter_Land_Owner = landOwner
//        self.pageNo = 1
//        totalCount = 0
        self.getListData()
    }
}

//MARK:-  Web Service model
extension NoticeWarrantListViewController {
    func getListData() {
        self.arrNoticeWarrant = []
        
        self.webServiceModel.land_id = self.land_id
        self.webServiceModel.notice_type = self.filter_Notice_Type
        self.webServiceModel.notice_name = self.filter_Template_Type
        self.webServiceModel.template = self.filter_Template
        self.webServiceModel.land_owner = self.filter_Land_Owner
        self.webServiceModel.status = self.filter_Status_id
        self.webServiceModel.Pageno = self.pageNo
        
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.webServiceModel.getNoticeList { arr_warrant in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            self.arrNoticeWarrant = arr_warrant
            self.setUpTblView()
        }
    }
    
    func deleteListData() {
        
        
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.webServiceModel.getDeleteNotice { json,is_success in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if is_success{
                self.getListData()
            }
        }
    }
    
    
}




