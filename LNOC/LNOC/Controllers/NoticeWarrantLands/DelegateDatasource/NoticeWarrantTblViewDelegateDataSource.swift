//
//  PlayerListDelegateDataSource.swift
//  Khel Mahakumbh
//
//  Created by Dhruv Patel on 27/12/21.
//

import UIKit

@objc protocol BtnDeleteViewDocClickDelegate {
    @objc optional func btnDeleteTapped(index:Int)
    @objc optional func btnViewDocTapped(index:Int)
}

class NoticeWarrantTblViewDelegateDataSource : NSObject {
    //MARK:- Variables
    //Private
    fileprivate var arrSource: [NoticeWarrantListDataModel]
    fileprivate let tblView: UITableView
    fileprivate weak var delegate: TblViewDelegate?
    fileprivate var btnDeleteClickDelegate:BtnDeleteViewDocClickDelegate?
    
    
    //MARK:- Initializer
    init(arrData: [NoticeWarrantListDataModel], tbl: UITableView, delegate: TblViewDelegate,BtnDelegate:BtnDeleteViewDocClickDelegate) {
        arrSource = arrData
        tblView = tbl
        self.delegate = delegate
        self.btnDeleteClickDelegate = BtnDelegate
        super.init()
        setUp()
    }
    
    //MARK:- Private Methods
    fileprivate func setUp() {
        setUpColView()
    }
    
    fileprivate func setUpColView() {
        registerCell()
        tblView.estimatedRowHeight = 371
        tblView.rowHeight = UITableView.automaticDimension
        tblView.delegate = self
        tblView.dataSource = self
        setUpBackgroundView()
        
        /*var frame = CGRect.zero
        frame.size.height = .leastNormalMagnitude
        tblView.tableHeaderView = UIView(frame: frame)*/
    }
    
    fileprivate func registerCell() {
        tblView.register(cellType: NoticeWarrantTblCell.self)
    }
    
    private func setUpBackgroundView() {
        if (self.arrSource.count > 0) {
            self.tblView.backgroundView = nil
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                let frame = CGRect(x: 0, y: 0, width: self.tblView.frame.size.width, height: self.tblView.frame.size.height / 1.35)
                self.tblView.backgroundView = UIView.makeNoRecordFoundView(frame: frame, msg: CommonLabels.no_record_found)
            }
        }
    }
    
    //MARK:- Public Methods
    func reloadData(arrData: [NoticeWarrantListDataModel]) {
        arrSource = arrData
        tblView.reloadData()
        setUpBackgroundView()
    }
}

//MARK:- UITableViewDelegate Methods
extension NoticeWarrantTblViewDelegateDataSource : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelect?(tbl: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        delegate?.willDisplay?(tbl: tableView, indexPath: indexPath)
    }
}

//MARK:- UITableViewDataSource Methods
extension NoticeWarrantTblViewDelegateDataSource : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: NoticeWarrantTblCell.self, for: indexPath)
        cell.configureCell(model: arrSource[indexPath.row])
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(btnDeleteTapped(_:)), for: .touchUpInside)
        cell.btnViewDoc.tag = indexPath.row
        cell.btnViewDoc.addTarget(self, action: #selector(btnViewDocTapped(_:)), for: .touchUpInside)
        return cell
    }
}

extension NoticeWarrantTblViewDelegateDataSource:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.tbldidScroll?(scrollView: scrollView)
    }
}
extension NoticeWarrantTblViewDelegateDataSource {
    @objc func btnDeleteTapped(_ sender:UIButton) {
        btnDeleteClickDelegate?.btnDeleteTapped?(index: sender.tag)
    }
    
    @objc func btnViewDocTapped(_ sender:UIButton) {
        btnDeleteClickDelegate?.btnViewDocTapped?(index: sender.tag)
    }
}
