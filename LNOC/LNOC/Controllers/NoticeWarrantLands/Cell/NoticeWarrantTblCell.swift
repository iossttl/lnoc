//
//  PlayerListTableViewCell.swift
//  Khel Mahakumbh
//
//  Created by Dhruv Patel on 27/12/21.
//

import UIKit

class NoticeWarrantTblCell: UITableViewCell {

    
    @IBOutlet weak var viewMain: UIView!
    
    @IBOutlet weak var btnViewDoc: UIButton!
    @IBOutlet weak var btnDelete: UIButton!
 
    @IBOutlet weak var lblId: UILabel!
    @IBOutlet weak var lblNoticeType: UILabel!
    @IBOutlet weak var lblTemplateType: UILabel!
    @IBOutlet weak var lblTemplate: UILabel!
    @IBOutlet weak var lblLandOwner: UILabel!
    @IBOutlet weak var lblStatus: UILabel!
    @IBOutlet weak var lblAction: UILabel!
    
    
    @IBOutlet var lblStatic:[UILabel]!
    @IBOutlet var lblValues:[UILabel]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setUpUi()
    }

    fileprivate func setUpUi() {
        self.selectionStyle = .none
        viewMain.viewWith(radius: 8.0)
        viewMain.addShadow()
        
        lblStatic.forEach { (lbl) in
            lbl.font = .boldTitleFont
            lbl.textColor = .customBlue
            lbl.numberOfLines = 0
        }
        
        lblValues.forEach { (lbl) in
            lbl.font = .regularValueFont
            lbl.textColor = .customBlack
            lbl.numberOfLines = 0
        }
        
        btnDelete.setImage(.ic_header_delete, for: .normal)
        btnDelete.tintColor = .orange
    }
    
    func configureCell(model : NoticeWarrantListDataModel) {
        //print(model.name)
        
        lblId.text = getString(anything: model.id)
        lblNoticeType.text = getString(anything: model.notice_type)
        lblTemplateType.text = getString(anything: model.notice_name)
        lblTemplate.text = getString(anything: model.template)
        lblLandOwner.text = getString(anything: model.land_owner)
        lblStatus.text = getString(anything: model.status)
        
        if getString(anything: model.status) == "0" {
            lblStatus.text = NoticeListLabels.active
        }else {
            lblStatus.text = NoticeListLabels.inactive
        }
        //lblAction.text = getString(anything: model.action)

    }
    
}
