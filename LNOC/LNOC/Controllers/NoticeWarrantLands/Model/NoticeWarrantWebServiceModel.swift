//
//  PlayerListWebServiceModel.swift
//  Khel Mahakumbh
//
//  Created by Dhruv Patel on 27/12/21.
//
import UIKit

class NoticeWarrantWebServicesModel : NSObject {
    
    
    var land_id = ""
    var notice_type = ""
    var notice_name = ""
    var template = ""
    var land_owner = ""
    var status = ""
    var Pageno = 1
    var land_notice_warrant_generate_id = ""

   
    

    //MARK:- Enum
    enum Keys: String {
        case land_id = "land_id"
        case notice_type = "notice_type"
        case notice_name = "notice_name"
        case template = "template"
        case land_owner = "land_owner"
        case status = "status"
        case user_id = "user_id"
        case page = "page"
        case land_notice_warrant_generate_id = "land_notice_warrant_generate_id"
    }
    
    fileprivate var getWarrantListUrl:String {
        BASEURL + API.noticewarrent_list
    }
    
    fileprivate var deleteWarrantUrl:String {
        BASEURL + API.deletewarrent
    }
    
    private func nocWarrantListDict() -> Dictionary {
        var dict : [String:Any] = [:]
        let commonDict = WShandler.commonDict()
        
        for key in commonDict.keys.sorted() {
            dict[key] = commonDict[key] as! String
        }
        dict[Keys.land_id.rawValue] = land_id.encryptStr
        dict[Keys.notice_type.rawValue] = notice_type.encryptStr
        dict[Keys.notice_name.rawValue] = notice_name.encryptStr
        dict[Keys.template.rawValue] = template.encryptStr
        dict[Keys.land_owner.rawValue] = land_owner.encryptStr
        dict[Keys.status.rawValue] = status.encryptStr
        dict[Keys.page.rawValue] = getString(anything: Pageno)
        
        return dict
    }
    
    func getNoticeList(block: @escaping (([NoticeWarrantListDataModel]) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            let param = self.nocWarrantListDict()
            print(param,"param")
            WShandler.shared.postWebRequest(urlStr: self.getWarrantListUrl, param: param) { (json, flag) in
                
                var responseModel = [NoticeWarrantListDataModel]()
                print(json)
                if (flag == 200) {
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == 2 {
                            UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                            UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                            UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                            UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                            UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                            UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                            Global.shared.changeInitialVC(animated: true, direction: .toLeft)
                            Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).encryptStr)
                        } else {
                            responseModel = (json["warrentDetails"] as? [Dictionary] ?? []).map({ NoticeWarrantListDataModel(dict: $0) })
                        }
                        
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                    }
                    
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel)
            }
        }  else {
            block([])
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
    
    
    func getDeleteNotice(block: @escaping ((Dictionary, _ isSucess : Bool) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            var param = WShandler.commonDict()
            param[Keys.user_id.rawValue] = UserModel.currentUser.id
            param[Keys.land_notice_warrant_generate_id.rawValue] = land_notice_warrant_generate_id.encryptStr
            print(param,"param")
            WShandler.shared.postWebRequest(urlStr: self.deleteWarrantUrl, param: param) { (json, flag) in
                
                print(json)
                if (flag == 200) {
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        block(json,true)
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        block([:],false)
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block([:],false)
                }
            }
        }  else {
            block([:],false)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
}
