//
//  PlayerListDataModel.swift
//  Khel Mahakumbh
//
//  Created by Dhruv Patel on 27/12/21.
//

import UIKit

class NoticeWarrantListDataModel : NSObject {
    
    
    let app_no: String
    let id: String
    let notice_type: String
    let notice_name:String
    let template_type: String
    let template : String
    let land_owner : String
    let status: String
    let action: String
    let ms_notice_templates_id: String
    let doc_Title : String
    let doc_URL : String
    
    enum Keys: String {
        case app_no = "app_no"
        case id = "id"
        case notice_type = "notice_type"
        case notice_name = "notice_name"
        case template_type = "template_type"
        case template = "template"
        case land_owner = "name_of_owner"
        case status = "status"
        case action = "action"
        case ms_notice_templates_id = "ms_notice_templates_id"
        case doc = "doc"
        case doc_title = "title"
        case doc_Url = "url"
    }
    
    
   
    /*doc =     {
        title = "Download Notice/Warrant Pdf";
        url = "http://lris.php-staging.com/uploads/export-pdf/18856__1644240238.pdf";
    };*/
   
    
    init(dict: Dictionary) {
        app_no = getString(anything: dict[Keys.app_no.rawValue]).decryptStr
        id = getString(anything: dict[Keys.id.rawValue]).decryptStr
        notice_type = getString(anything: dict[Keys.notice_type.rawValue]).decryptStr
        notice_name = getString(anything: dict[Keys.notice_name.rawValue]).decryptStr

        template_type = getString(anything: dict[Keys.template_type.rawValue]).decryptStr
        template = getString(anything: dict[Keys.template.rawValue]).decryptStr
        land_owner = getString(anything: dict[Keys.land_owner.rawValue]).decryptStr
        status = getString(anything: dict[Keys.status.rawValue]).decryptStr
        action = getString(anything: dict[Keys.action.rawValue]).decryptStr
        ms_notice_templates_id = getString(anything: dict[Keys.ms_notice_templates_id.rawValue]).decryptStr
        
        if let dictDoc = dict[Keys.doc.rawValue] as? Dictionary {
            doc_Title = getString(anything: dictDoc[Keys.doc_title.rawValue]).decryptStr
            doc_URL = getString(anything: dictDoc[Keys.doc_Url.rawValue]).decryptStr
        }else{
            doc_Title = ""
            doc_URL = ""
        }
        super.init()
    }
}
