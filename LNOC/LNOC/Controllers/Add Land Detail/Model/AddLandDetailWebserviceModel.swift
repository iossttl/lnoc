//
//  AddLandDetailWebserviceModel.swift
//  LNOC
//
//  Created by Dhruv Patel on 07/01/22.
//

import Foundation

class AddLandDetailWebServiceModel: NSObject {
    
//    var citizen_users_id : String = ""
    var crr_number : String = ""
    var cs_number: String = ""
    var ms_divisions_id:String = ""
    var address_of_owner:String = ""
    var location_of_property:String = ""
    var ms_tenure_of_properties_id : String = ""
    var interest_rate : String = ""
    var area : String = ""
    var pending_tax : String = ""
    var interest:String = ""
    var ms_assessed_properties_id:String = ""
    var name_of_owner : String = ""
    var assessment_amount : String = ""
    var rent : String = ""
    var due_date : String = ""
    var deed_no : String = ""
    var comm_date : String = ""
    var due_date1 : String = ""
    var due_date2 : String = ""
    var due_date3 : String = ""
    var due_date4 : String = ""
    var deed_date : String = ""
    var exp_date : String = ""
    var pr_yr : String = ""
    var lease_start_date : String = ""
    var lease_end_date : String = ""
    var lease_period : String = ""
    var fee : String = ""
   

    
    
    
    
   /* var ChairmanContact:String = ""
    var SecretoryName : String = ""
    var SecretoryContactNO : String = ""
    var SocietyEmail : String = ""
    var NoOfMember : String = ""
    var FlatNumber:String = ""
    var FloorOfFlat:String = ""
    var CarpetAreaOfFlatFeet:String = ""
    var CarpetAreofFlatMT : String = ""
    var BuiltupAreaOfFlatFeet : String = ""
    var BuiltUpAreaOfFlatMt : String = ""
    var IsParking: Bool = false
    var Parking:String = ""
    var CarpetAreofParkingFeet : String = ""
    var CarpetAreofParkingMT : String = ""
    var BuiltupAreaOfParkingFeet : String = ""
    var BuiltUpAreaOfParkingMt : String = ""
    var NoOfParkingUnderTransfer:String = ""*/

    fileprivate var getAddLandDetail:String {
         return BASEURL + API.addLand
     }
    
    fileprivate var getUseOfLandUrl : String {
         return BASEURL + API.getUseOfLand
     }
    
    fileprivate var landFilterURL:String {
        BASEURL + API.landfilterdropdown
    }
    
    //MARK:- Enum
    enum keys : String {
        case divisions = "divisions"
        case tenures = "tenures"
        case nocType = "NOCType"
        case Division = "Division"
        case NameOFTranferee = "Name_Of_Transferee"
        case TranserWithinFamily = "Transfer_Within_Family"
        case RelationWithTransferor = "Relation_With_Transferor"
        case CSNo = "CS_No"
        case nameOFSociety = "Name_Of_Society"
        case PresentNameOfLesse = "Present_Name_OF_Lesse"
        case NameOfBuilding = "Name_OF_Building"
        case TotalNoOFFloor = "Total_No_OF_Floor"
        case NoOfLift = "No_Of_Lift"
        case CompletionYear = "Completion_Year"
        case ChairmaanName = "Chairman_Name"
        case ChaiemanContactNO = "Chairman_ContactNo"
        case secretoryName = "Secrotory_Name"
        case SecretoryContactNO = "Secretory_ContactNo"
        case SocietyEmail = "SocietyEmail"
        case NoOFMember = "No_Of_Member"
        case FlatNumber = "Flat_Number"
        case FloorOfFlat = "Floor_Of_Flat"
        case carpetAreaFlatFeet = "Area_Flat_Feet_Carpet"
        case CarpetAreaFlatMT = "Area_Flat_MT_Carpet"
        case BuiltUpAreaFlatFeet = "Area_Flat_Feet_BuiltUp"
        case BuiltUpAreaFlatMT = "Area_Flat_MT_BuiltUp"
        case isParkig = "Is_Parking"
        case Parking = "Parking"
        case carpetAreaParkingFeet = "Area_Parking_Feet_Carpet"
        case CarpetAreaParkingMT = "Area_Parking_MT_Carpet"
        case BuiltUpAreaParkingFeet = "Area_Parking_Feet_BuiltUp"
        case BuiltUpAreaParkingMT = "Area_Parking_MT_BuiltUp"
        case numberOfParkingUnderTransfer = "Number_OF_Parking_Under_Transfer"
        //NEW
        case citizen_users_id = "citizen_users_id"
        case crr_number = "crr_number"
        case cs_number = "cs_number"
        case ms_divisions_id = "ms_divisions_id"
        case address_of_owner = "address_of_owner"
        case location_of_property = "location_of_property"
        case ms_tenure_of_properties_id = "ms_tenure_of_properties_id"
        case interest_rate = "interest_rate"
        case area = "area"
        case pending_tax = "pending_tax"
        case interest = "interest"
        case ms_assessed_properties_id = "ms_assessed_properties_id"
        case name_of_owner = "name_of_owner"
        case assessment_amount = "assessment_amount"
        case rent = "rent"
        case due_date = "due_date"
        case deed_no = "deed_no"
        case comm_date = "comm_date"
        case fee = "fee"
        case due_date1 = "due_date1"
        case due_date2 = "due_date2"
        case due_date3 = "due_date3"
        case due_date4 = "due_date4"
        case deed_date = "deed_date"
        case exp_date = "exp_date"
        case pr_yr = "pr_yr"
        case lease_start_date = "lease_start_date"
        case lease_end_date = "lease_end_date"
        case lease_period = "lease_period"


    }
    
    //MARK:- Initializer
    override init() {
        super.init()
    }
    
 
    
    private func addLandDetailsDict() -> Dictionary {
        return [keys.citizen_users_id.rawValue: UserModel.currentUser.id,
                keys.name_of_owner.rawValue: self.name_of_owner,
                keys.crr_number.rawValue: self.crr_number,
                keys.cs_number.rawValue: self.cs_number,
                keys.ms_divisions_id.rawValue:self.ms_divisions_id,
                keys.address_of_owner.rawValue:self.address_of_owner,                keys.location_of_property.rawValue: self.location_of_property,
                keys.ms_tenure_of_properties_id.rawValue: self.ms_tenure_of_properties_id,
                keys.ms_assessed_properties_id.rawValue: self.ms_assessed_properties_id,
                keys.interest_rate.rawValue:self.interest_rate,
                keys.area.rawValue:self.area,
                keys.pending_tax.rawValue: self.pending_tax,
                keys.interest.rawValue: self.interest,
                keys.assessment_amount.rawValue:self.assessment_amount,
                keys.rent.rawValue:self.rent,
                keys.due_date.rawValue:self.due_date,
                keys.deed_no.rawValue:self.deed_no,
                keys.fee.rawValue:self.fee,
                keys.comm_date.rawValue:self.comm_date,
                keys.due_date1.rawValue:self.due_date1,
                keys.due_date2.rawValue:self.due_date2,
                keys.due_date3.rawValue:self.due_date3,
                keys.due_date4.rawValue:self.due_date4,
                keys.deed_date.rawValue:self.deed_date,
                keys.exp_date.rawValue:self.exp_date,
                keys.pr_yr.rawValue:self.pr_yr,
                keys.lease_start_date.rawValue:self.lease_start_date,
                keys.lease_end_date.rawValue:self.lease_end_date,
                keys.lease_period.rawValue:self.lease_period
                


                /*,//EncryptionModel.default.encrypt(str: self.mobilenumber, key: uuid),
                keys.ChaiemanContactNO.rawValue: self.ChairmanContact, //EncryptionModel.default.encrypt(str: self.gender, key: uuid),
                keys.secretoryName.rawValue: self.SecretoryName, //EncryptionModel.default.encrypt(str: self.token, key: uuid),
                keys.SecretoryContactNO.rawValue:self.SecretoryContactNO, //EncryptionModel.default.encrypt(str: self.password, key: uuid),
                keys.SocietyEmail.rawValue:self.SocietyEmail,
                keys.NoOFMember.rawValue: self.NoOfMember,//EncryptionModel.default.encrypt(str: self.email, key: uuid),
                keys.FlatNumber.rawValue: self.FlatNumber,//EncryptionModel.default.encrypt(str: self.mobilenumber, key: uuid),
                keys.FloorOfFlat.rawValue: self.FloorOfFlat, //EncryptionModel.default.encrypt(str: self.gender, key: uuid),
                keys.carpetAreaFlatFeet.rawValue: self.CarpetAreaOfFlatFeet, //EncryptionModel.default.encrypt(str: self.token, key: uuid),
                keys.CarpetAreaFlatMT.rawValue:self.CarpetAreofFlatMT, //EncryptionModel.default.encrypt(str: self.password, key: uuid),
                keys.BuiltUpAreaFlatFeet.rawValue:self.BuiltupAreaOfFlatFeet,
                keys.BuiltUpAreaFlatMT.rawValue: self.BuiltUpAreaOfFlatMt,//EncryptionModel.default.encrypt(str: self.email, key: uuid),
                keys.isParkig.rawValue: self.IsParking,//EncryptionModel.default.encrypt(str: self.mobilenumber, key: uuid),
                keys.Parking.rawValue: self.Parking, //EncryptionModel.default.encrypt(str: self.gender, key: uuid),
                keys.carpetAreaParkingFeet.rawValue: self.CarpetAreofParkingFeet, //EncryptionModel.default.encrypt(str: self.token, key: uuid),
                keys.CarpetAreaParkingMT.rawValue:self.CarpetAreofParkingMT, //EncryptionModel.default.encrypt(str: self.password, key: uuid),
                keys.numberOfParkingUnderTransfer.rawValue:self.NoOfParkingUnderTransfer,
                keys.BuiltUpAreaParkingFeet.rawValue:self.BuiltupAreaOfParkingFeet,
                keys.BuiltUpAreaParkingMT.rawValue:self.BuiltUpAreaOfFlatMt*/]//EncryptionModel.default.encrypt(str: self.last_name, key: uuid)]
    }
         

    
    
    func getUseOfLandData(block: @escaping ((DropDownResponseModel?) -> Swift.Void)) {
         
         if (WShandler.shared.CheckInternetConnectivity()) {
             let param = WShandler.commonDict()
             WShandler.shared.getWebRequest(urlStr:getUseOfLandUrl,param: param) { (json, flag) in
                 var responseModel:DropDownResponseModel?
                 if (flag == 200) {
                     if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                         responseModel = DropDownResponseModel(dictUseOfLand: json)
                     } else {
                         Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                     }
                 } else {
                     Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                 }
                 block(responseModel)
             }
             }else{
             block(nil)
             Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
         }
     }
    
    
    //MARK:- Public Methods
    func getFilterDropDownListApi(block:@escaping (([DropDownModel], _ isSucess : Bool, _ tenures : [DropDownModel]) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {

            let param = WShandler.commonDict()
 
            WShandler.shared.getWebRequest(urlStr: landFilterURL, param: param) { (json, flag) in
                
                if (flag == 200) {
                    
                    //Invalid Token
                    if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                        
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                        UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                        Global.shared.changeInitialVC(animated: true, direction: .toLeft)
                        block([], false, [])
                        
                    } else if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        let divisions = (json[keys.divisions.rawValue] as? [Dictionary] ?? []).map({ DropDownModel(dict: $0) })
                        let tenures = (json[keys.tenures.rawValue] as? [Dictionary] ?? []).map({ DropDownModel(dictTenures: $0) })
                        block(divisions, true, tenures)

                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        block([], false, [])
                    }
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block([], false, [])
                }
            }
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block([], false, [])
        }
    }
    
    //Add Land
     func addLand(block:@escaping ((Dictionary?, _ isSucess : Bool) -> Swift.Void)) {
         if (WShandler.shared.CheckInternetConnectivity()) {
            var param = WShandler.commonDict()//(uuid: uuid)
             let dict = addLandDetailsDict().encryptDic()
            for key in dict.keys {
                param[key] = dict[key]
            }
            
            print(param,"param")
             WShandler.shared.postWebRequest(urlStr: getAddLandDetail, param: param) { (json, flag) in
                 var responseModel:Dictionary?
                 if flag == 200 {
                     if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                         Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        print(json,"json")
                        responseModel = json
                         block(responseModel, true)
                     } else {
                         Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                        block(responseModel, false)

                     }
                 } else {
                     Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block(responseModel, false)

                 }
                block(responseModel, false)
             }
         } else {
             block(nil,false)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
         }
     }
    
}
