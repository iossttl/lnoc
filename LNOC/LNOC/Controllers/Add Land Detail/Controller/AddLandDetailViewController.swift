//
//  AddLandDetailViewController.swift
//  LNOC
//
//  Created by Dhruv Patel on 05/01/22.
//

import UIKit

class AddLandDetailViewController: HeaderViewController {
    
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var viewForScroll: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewForShadow: UIView!
    
    @IBOutlet weak var viewBgSave: GradientView!
    
    @IBOutlet weak var viewBgCancel: GradientView!
    
    @IBOutlet weak var viewBgAssesmentAmount: UIView!
    
    @IBOutlet weak var viewBgRent: UIView!
    
    
    @IBOutlet weak var viewBgDueDate: UIView!
    
    @IBOutlet weak var viewBgDeedNo: UIView!
    
    @IBOutlet weak var viewBgFee: UIView!
    
    
    @IBOutlet weak var viewBgDueDate1: UIView!
    
    @IBOutlet weak var viewBgDueDate2: UIView!

    
    @IBOutlet weak var viewBgDueDate3: UIView!

    
    @IBOutlet weak var viewBgDueDate4: UIView!
    
    @IBOutlet weak var viewBgCommDate: UIView!

    
    @IBOutlet weak var viewBgDeedDate: UIView!
    
    @IBOutlet weak var viewBgExpdate: UIView!
    
    @IBOutlet weak var viewBgPRYear: UIView!
    
    @IBOutlet weak var viewBgLeaseStartDate: UIView!
    
    @IBOutlet weak var viewBgLeaseEnd: UIView!
    
    @IBOutlet weak var viewBgLEasePeriod: UIView!
    
    //UITextField
    @IBOutlet weak var txtCRRNumber: UITextField!
    
    @IBOutlet weak var txtCSNumber: UITextField!
    
    @IBOutlet weak var txtLandHolder: UITextField!
    
    @IBOutlet weak var txtAddressOfOwner: UITextField!
    
    @IBOutlet weak var txtLocationOfProperty: UITextField!
    
    @IBOutlet weak var txtAssessmentAmount: UITextField!
    
    @IBOutlet weak var txtIntrestRate: UITextField!
    
    @IBOutlet weak var txtArea: UITextField!
    
    @IBOutlet weak var txtPendingTax: UITextField!
    
    @IBOutlet weak var txtIntrest: UITextField!
    
    @IBOutlet weak var txtRent: UITextField!
    
 
    
    @IBOutlet weak var txtDeedNo: UITextField!
    
    @IBOutlet weak var txtFee: UITextField!
    
    


    
    @IBOutlet weak var txtLeasePeriod: UITextField!
    
    
    
    
  
    
    //UILabel
    @IBOutlet weak var lblDivision: UILabel!
    
    @IBOutlet weak var lblLandTenture: UILabel!

    
    @IBOutlet weak var lblUseOfLand: UILabel!
    
    
    
    @IBOutlet weak var lblDueDate: UILabel!
    
    @IBOutlet weak var lblDueDate1: UILabel!
    
    @IBOutlet weak var lblDueDate2: UILabel!
    
    @IBOutlet weak var lblDueDate3: UILabel!
    
    @IBOutlet weak var lblDueDate4: UILabel!
    
    @IBOutlet weak var lblCommDate: UILabel!
    
    @IBOutlet weak var lblDeedDate: UILabel!
    
    @IBOutlet weak var lblExpDate: UILabel!
    
    @IBOutlet weak var lblPRYear: UILabel!
    
    @IBOutlet weak var lblLeaseStartDate: UILabel!
    
    @IBOutlet weak var lblLeaseEndDate: UILabel!
    
   //UIButton
    @IBOutlet weak var btnSave: UIButton!
    
    @IBOutlet weak var btnCancel: UIButton!
    
    
    //Collections
    @IBOutlet var lblTitles: [LocalizedLabel]!
    
    
    @IBOutlet var txtValues: [UITextField]!
    
    
    @IBOutlet var lblAsterisks: [LocalizedLabel]!
    
    
    @IBOutlet var lblUnderLine: [UILabel]!
    
    @IBOutlet var imgDropDownCalnder: [UIImageView]!

    @IBOutlet var imgDropDown: [UIImageView]!
    
    @IBOutlet var lblValueDropDown: [UILabel]!
    
    @IBOutlet var ViewBg: [UIView]!
    
    
    private var arrDivisionOptions = [String]()
    private var arrLandTentureOptions = [String]()
    private var validationType: Int = 0
    private var arrUseOfLandOptions = [DropDownModel]()
    private var arrLandTenure = [DropDownModel]()
    private var arrDivisions = [DropDownModel]()

    fileprivate var webServiceModel = AddLandDetailWebServiceModel()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpIPadConstraints()
        self.setUpView()
        self.setUpHeader()
        self.getWebserviceforUseOfLand()
        self.getDataFromWebServices()

      /*  // Do any additional setup after loading the view.
        
//        self.arrLandTentureOptions.append(CommonLabels.select)
//        self.arrLandTentureOptions.append("Land Tenure")
//        self.arrLandTentureOptions.append("Land Tenure Abolition (LTA)")
//        self.arrLandTentureOptions.append("Increase Land Revenue (ILR)")
//        self.arrLandTentureOptions.append("Land Newly Assessed (LNA)")
//        self.arrLandTentureOptions.append("Government Toka")
//        self.arrLandTentureOptions.append("llnd lnam")
//        self.arrLandTentureOptions.append("Tenancy at Wiil")
//        self.arrLandTentureOptions.append("Bombay Backbay Reclamation (BBR)")
//        self.arrLandTentureOptions.append("Lease Hold")
//        self.arrLandTentureOptions.append("Antop Hil")
//        self.arrLandTentureOptions.append("FlateQC")
        
//        self.arrUseOfLandOptions = [DropDownModel(dict: [DropDownModel.Keys.title.rawValue: CommonLabels.select, DropDownModel.Keys.id.rawValue: ""])]
//        self.arrUseOfLandOptions.append("Comercial")
//        self.arrUseOfLandOptions.append("Residental")
//        self.arrUseOfLandOptions.append("Resi. + Comm.")
//        self.arrUseOfLandOptions.append("Trust")
//        self.arrUseOfLandOptions.append("Individual")
//        self.arrUseOfLandOptions.append("Other")
        
        
//        self.arrDivisionOptions.append(CommonLabels.select)
//        self.arrDivisionOptions.append("Fort")
//        self.arrDivisionOptions.append("Colaba")
//        self.arrDivisionOptions.append("Worli")
//        self.arrDivisionOptions.append("Dadar")
//        self.arrDivisionOptions.append("Sion")*/
        
    }
    
    
    
    
    fileprivate func setUpIPadConstraints() {
        changeLeadingTrailingForiPad(view: viewForScroll)
        changeHeightForiPad(view: btnSave,constant: 40)
        changeHeightForiPad(view: btnCancel)
    }
    
    
    fileprivate func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.addLand)
        setHeaderView_BackImage()
    }
    
    
    fileprivate func setUpView() {
        
        self.viewMain.isHidden = false
        self.viewForShadow.isHidden = false
        
        self.viewForScroll.backgroundColor = UIColor.darkBlue.withAlphaComponent(0.2)
        self.viewMain.setCustomCornerRadius(radius: 12)
        self.viewForShadow.setCustomCornerRadius(radius: 12)
        self.viewForShadow.addShadow()
        
        btnSave.setTitle(CommonButtons.save, for: .normal)
        btnSave.titleLabel?.font = .regularValueFont
        btnSave.setTitleColor(.customWhite, for: .normal)
        
        btnCancel.setTitle(CommonButtons.cancel, for: .normal)
        btnCancel.titleLabel?.font = .regularValueFont
        btnCancel.setTitleColor(.customWhite, for: .normal)
        
        viewBgSave.setFullCornerRadius()
        viewBgSave.backgroundColor = .themeColor
        viewBgCancel.setFullCornerRadius()
        viewBgCancel.backgroundColor = .customTertinaryBlue

        
        
        self.ViewBg.forEach({
            $0.isHidden = true
        })
        
        
        self.lblTitles.forEach({
            $0.font = .regularTitleFont
            $0.textColor = .customBlackTitle
        })
        
        for lbl in lblAsterisks {
            lbl.textColor = .customRed
            lbl.font = .regularTitleFont
            lbl.text = "*"
        }
        
        self.txtValues.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
            //            $0.textAlignment = isRightToLeftLocalization ? .right : .left
        })
        
        self.lblValueDropDown.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
            $0.text = CommonLabels.select
            
        })
        
        self.imgDropDown.forEach({
            $0.tintColor = .customBlackTitle
            $0.image = .ic_down_arrow
        })
        
        self.lblUnderLine.forEach { (lbl) in
            lbl.backgroundColor = .customSeparator
        }
        
        self.txtLandHolder.text = UserModel.currentUser.first_name.decryptStr + " " + UserModel.currentUser.middle_name.decryptStr + " " + UserModel.currentUser.last_name.decryptStr
        
        
        self.txtAssessmentAmount.delegate = self
        self.txtIntrestRate.delegate = self
        self.txtArea.delegate = self
        self.txtPendingTax.delegate = self
        self.txtIntrest.delegate = self
        self.txtRent.delegate = self
        self.txtFee.delegate = self
    }
    
    func setUpViewOnSelection(tenure:String){
        
        
        self.ViewBg.forEach({
            $0.isHidden = true
        })
        
        
        if tenure == "Land Tenure" || tenure == "FlateQC"{
              validationType = 0
              
          }else if tenure == "1" || tenure == "2"{
              validationType = 1
              
              viewBgAssesmentAmount.isHidden = false
              
          }else if tenure == "3" || tenure == "4"{
              validationType = 2
              
              viewBgRent.isHidden = false
              viewBgDueDate.isHidden = false
              viewBgDeedNo.isHidden = false
              
          }else if tenure == "5"{
              validationType = 3
              
              viewBgRent.isHidden = false
              viewBgDueDate.isHidden = false
              
          }else if tenure == "6" || tenure == "9"{
              validationType = 4
              
              viewBgFee.isHidden = false
              viewBgCommDate.isHidden = false
              
          }else if tenure == "7"{
              validationType = 5
              
              viewBgRent.isHidden = false
              viewBgDueDate1.isHidden = false
              viewBgDueDate2.isHidden = false
              viewBgDueDate3.isHidden = false
              viewBgDueDate4.isHidden = false
              viewBgDeedDate.isHidden = false
              viewBgCommDate.isHidden = false
              viewBgExpdate.isHidden = false
              viewBgPRYear.isHidden = false
              viewBgLeaseStartDate.isHidden = false
              viewBgLeaseEnd.isHidden = false
              viewBgLEasePeriod.isHidden = false
          }else if tenure == "8"{
              validationType = 6
              viewBgRent.isHidden = false
              viewBgDueDate1.isHidden = false
              viewBgDueDate2.isHidden = false
              viewBgDueDate3.isHidden = false
              viewBgDueDate4.isHidden = false
              viewBgDeedDate.isHidden = false
              viewBgCommDate.isHidden = false
              viewBgExpdate.isHidden = false
              viewBgPRYear.isHidden = false
          }
        
     /*   if tenure == "Land Tenure" || tenure == "FlateQC"{
            validationType = 0
            
        }else if tenure == "Land Tenure Abolition (LTA)" || tenure == "Increase Land Revenue (ILR)"{
            validationType = 1
            
            viewBgAssesmentAmount.isHidden = false
            
        }else if tenure == "Land Newly Assessed (LNA)" || tenure == "Government Toka"{
            validationType = 2
            
            viewBgRent.isHidden = false
            viewBgDueDate.isHidden = false
            viewBgDeedNo.isHidden = false
            
        }else if tenure == "llnd lnam"{
            validationType = 3
            
            viewBgRent.isHidden = false
            viewBgDueDate.isHidden = false
            
        }else if tenure == "Tenancy at Wiil" || tenure == "Antop Hil"{
            validationType = 4
            
            viewBgFee.isHidden = false
            viewBgCommDate.isHidden = false
            
        }else if tenure == "Bombay Backbay Reclamation (BBR)"{
            validationType = 5
            
            viewBgRent.isHidden = false
            viewBgDueDate1.isHidden = false
            viewBgDueDate2.isHidden = false
            viewBgDueDate3.isHidden = false
            viewBgDueDate4.isHidden = false
            viewBgDeedDate.isHidden = false
            viewBgCommDate.isHidden = false
            viewBgExpdate.isHidden = false
            viewBgPRYear.isHidden = false
            viewBgLeaseStartDate.isHidden = false
            viewBgLeaseEnd.isHidden = false
            viewBgLEasePeriod.isHidden = false
        }else if tenure == "Lease Hold"{
            validationType = 6
            viewBgRent.isHidden = false
            viewBgDueDate1.isHidden = false
            viewBgDueDate2.isHidden = false
            viewBgDueDate3.isHidden = false
            viewBgDueDate4.isHidden = false
            viewBgDeedDate.isHidden = false
            viewBgCommDate.isHidden = false
            viewBgExpdate.isHidden = false
            viewBgPRYear.isHidden = false
        }*/
        
        
    }
    
    @IBAction func TapOnDevision(_ sender: UIButton) {
        var list = self.arrDivisions.map({ $0.title })
        list.insert(CommonLabels.select, at: 0)
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblDivision.text, arrSource: [list], doneAction: { (rows, value) in
            self.lblDivision.text = getString(anything: value)
            if let index = rows.first{
                if getString(anything: value) == CommonLabels.select {
                    self.webServiceModel.ms_divisions_id = ""
                } else {
                    self.webServiceModel.ms_divisions_id = self.arrDivisions[index-1].id
                }
            }
        }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func TapOnLandTanture(_ sender: UIButton) {
        
        var list = self.arrLandTenure.map({ $0.title })
        list.insert(CommonLabels.select, at: 0)
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblLandTenture.text, arrSource: [list], doneAction: { (rows, value) in
            self.lblLandTenture.text = getString(anything: value)
            
            if let index = rows.first{
                if getString(anything: value) == CommonLabels.select {
                    self.webServiceModel.ms_tenure_of_properties_id = ""
                    self.setUpViewOnSelection(tenure: "")

                } else {
                    self.webServiceModel.ms_tenure_of_properties_id = self.arrLandTenure[index-1].id
                    self.setUpViewOnSelection(tenure: getString(anything: self.arrLandTenure[index-1].id))

                }
            }
        }, animated: true, origin: sender, onVC: self)
       /* StringPickerViewController.show(pickerType: .single, initialSelection: self.lblLandTenture.text, arrSource: [self.arrLandTentureOptions], doneAction: { (rows, value) in
            
            self.lblLandTenture.text = getString(anything: value)
            self.setUpViewOnSelection(tenure: getString(anything: value))
            
        }, animated: true, origin: sender, onVC: self)*/
    }
    
    @IBAction func TapOnUseOfLand(_ sender: UIButton) {
        var list = arrUseOfLandOptions.map({ $0.title })
        list.insert(CommonLabels.select, at: 0)
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblUseOfLand.text, arrSource: [list], doneAction: { (rows, value) in
            self.lblUseOfLand.text = getString(anything: value)
            if let index = rows.first{
                if getString(anything: value) == CommonLabels.select {
                    self.webServiceModel.ms_assessed_properties_id = ""
                } else {
                    self.webServiceModel.ms_assessed_properties_id = self.arrUseOfLandOptions[index-1].id
                }
            }
        }, animated: true, origin: sender, onVC: self)
    }
    
    
    @IBAction func TapOnDueDateBtn(_ sender: UIButton) {
        
        if sender.tag == 0{
            dueDatePicker(lbl: lblDueDate,sender: sender)
        } else if sender.tag == 1{
            dueDatePicker(lbl: lblDueDate1,sender: sender)
        } else if sender.tag == 2{
            dueDatePicker(lbl: lblDueDate2,sender: sender)
        } else if sender.tag == 3{
            dueDatePicker(lbl: lblDueDate3,sender: sender)
        } else if sender.tag == 4{
            dueDatePicker(lbl: lblDueDate4,sender: sender)
        }
    }
    
    
    func dueDatePicker(lbl:UILabel,sender:UIButton){
        let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: lbl.text), inputFormat: DateFormats.dd_MM_yyyy_dashed), maximumDate: nil, minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dueDateString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.dd_MM_yyyy_dashed)
            lbl.text = dueDateString
            
        }, animated: true, origin: sender)
        
        datePickerVC.lblTitleText = CommonLabels.select
        datePickerVC.lblTitleColor = .customDarkBlueGradient
        datePickerVC.lblTitleFont = .boldMediumFont
        datePickerVC.btnCancelTitle = CommonButtons.cancel
        datePickerVC.btnCancelTitleColor = .customSubtitle
        datePickerVC.btnCancelTitleFont = .regularValueFont
        datePickerVC.btnDoneTitle = CommonButtons.done
        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)
    }
    
   /* @IBAction func TapOnDueDate1Btn(_ sender: UIButton) {
        
        
        dueDatePicker(lbl: lblDueDate1,sender: sender)
       /* let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblDueDate1.text), inputFormat: DateFormats.dd_MM_yyyy), maximumDate: nil, minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dobString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.yyyy_MM_dd_dashed)
            self.lblDueDate1.text = dobString
            
        }, animated: true, origin: sender)
        
//        datePickerVC.lblTitleText = CommonLabels.select
//        datePickerVC.lblTitleColor = .customDarkBlueGradient
//        datePickerVC.lblTitleFont = .boldMediumFont
//        datePickerVC.btnCancelTitle = CommonButtons.cancel
//        datePickerVC.btnCancelTitleColor = .customSubtitle
//        datePickerVC.btnCancelTitleFont = .regularValueFont
//        datePickerVC.btnDoneTitle = CommonButtons.done
//        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
//        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)*/
    }
    
    @IBAction func TapOnDueDate2Btn(_ sender: UIButton) {
        dueDatePicker(lbl: lblDueDate2,sender: sender)

        let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblDueDate2.text), inputFormat: DateFormats.dd_MM_yyyy), maximumDate: nil, minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dobString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.yyyy_MM_dd_dashed)
            self.lblDueDate2.text = dobString
            
        }, animated: true, origin: sender)
        
//        datePickerVC.lblTitleText = CommonLabels.select
//        datePickerVC.lblTitleColor = .customDarkBlueGradient
//        datePickerVC.lblTitleFont = .boldMediumFont
//        datePickerVC.btnCancelTitle = CommonButtons.cancel
//        datePickerVC.btnCancelTitleColor = .customSubtitle
//        datePickerVC.btnCancelTitleFont = .regularValueFont
//        datePickerVC.btnDoneTitle = CommonButtons.done
//        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
//        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)
    }
    
    
    @IBAction func TapOnDueDate3Btn(_ sender: UIButton) {
        
        dueDatePicker(lbl: lblDueDate3,sender: sender)

        let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblDueDate3.text), inputFormat: DateFormats.dd_MM_yyyy), maximumDate: nil, minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dobString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.yyyy_MM_dd_dashed)
            self.lblDueDate3.text = dobString
            
        }, animated: true, origin: sender)
        
//        datePickerVC.lblTitleText = CommonLabels.select
//        datePickerVC.lblTitleColor = .customDarkBlueGradient
//        datePickerVC.lblTitleFont = .boldMediumFont
//        datePickerVC.btnCancelTitle = CommonButtons.cancel
//        datePickerVC.btnCancelTitleColor = .customSubtitle
//        datePickerVC.btnCancelTitleFont = .regularValueFont
//        datePickerVC.btnDoneTitle = CommonButtons.done
//        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
//        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)
    }
    
    @IBAction func TapOnDueDate4Btn(_ sender: UIButton) {
        
        dueDatePicker(lbl: lblDueDate4,sender: sender)

        let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblDueDate4.text), inputFormat: DateFormats.dd_MM_yyyy), maximumDate: nil, minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dobString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.yyyy_MM_dd_dashed)
            self.lblDueDate4.text = dobString
            
        }, animated: true, origin: sender)
        
//        datePickerVC.lblTitleText = CommonLabels.select
//        datePickerVC.lblTitleColor = .customDarkBlueGradient
//        datePickerVC.lblTitleFont = .boldMediumFont
//        datePickerVC.btnCancelTitle = CommonButtons.cancel
//        datePickerVC.btnCancelTitleColor = .customSubtitle
//        datePickerVC.btnCancelTitleFont = .regularValueFont
//        datePickerVC.btnDoneTitle = CommonButtons.done
//        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
//        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)
    }*/
    
    @IBAction func TapOnCommDateBtn(_ sender: UIButton) {
        let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblCommDate.text), inputFormat: DateFormats.dd_MM_yyyy_dashed), maximumDate: nil, minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dobString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.dd_MM_yyyy_dashed)
            self.lblCommDate.text = dobString
            
        }, animated: true, origin: sender)
        
        datePickerVC.lblTitleText = CommonLabels.select
        datePickerVC.lblTitleColor = .customDarkBlueGradient
        datePickerVC.lblTitleFont = .boldMediumFont
        datePickerVC.btnCancelTitle = CommonButtons.cancel
        datePickerVC.btnCancelTitleColor = .customSubtitle
        datePickerVC.btnCancelTitleFont = .regularValueFont
        datePickerVC.btnDoneTitle = CommonButtons.done
        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)
    }
    
    @IBAction func TapOnDeedDateBtn(_ sender: UIButton) {
        let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblDeedDate.text), inputFormat: DateFormats.dd_MM_yyyy_dashed), maximumDate: nil, minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dobString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.dd_MM_yyyy_dashed)
            self.lblDeedDate.text = dobString
            
        }, animated: true, origin: sender)
        
        datePickerVC.lblTitleText = CommonLabels.select
        datePickerVC.lblTitleColor = .customDarkBlueGradient
        datePickerVC.lblTitleFont = .boldMediumFont
        datePickerVC.btnCancelTitle = CommonButtons.cancel
        datePickerVC.btnCancelTitleColor = .customSubtitle
        datePickerVC.btnCancelTitleFont = .regularValueFont
        datePickerVC.btnDoneTitle = CommonButtons.done
        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)
    }
    
    @IBAction func TapOnExpDateBtn(_ sender: UIButton) {
        let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblExpDate.text), inputFormat: DateFormats.dd_MM_yyyy_dashed), maximumDate: nil, minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dobString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.dd_MM_yyyy_dashed)
            self.lblExpDate.text = dobString
            
        }, animated: true, origin: sender)
        
        datePickerVC.lblTitleText = CommonLabels.select
        datePickerVC.lblTitleColor = .customDarkBlueGradient
        datePickerVC.lblTitleFont = .boldMediumFont
        datePickerVC.btnCancelTitle = CommonButtons.cancel
        datePickerVC.btnCancelTitleColor = .customSubtitle
        datePickerVC.btnCancelTitleFont = .regularValueFont
        datePickerVC.btnDoneTitle = CommonButtons.done
        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)
    }
    
    @IBAction func TapOnPRYearBtn(_ sender: UIButton) {
        let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblPRYear.text), inputFormat: DateFormats.dd_MM_yyyy_dashed), maximumDate: nil, minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dobString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.dd_MM_yyyy_dashed)
            self.lblPRYear.text = dobString
            
        }, animated: true, origin: sender)
        
        datePickerVC.lblTitleText = CommonLabels.select
        datePickerVC.lblTitleColor = .customDarkBlueGradient
        datePickerVC.lblTitleFont = .boldMediumFont
        datePickerVC.btnCancelTitle = CommonButtons.cancel
        datePickerVC.btnCancelTitleColor = .customSubtitle
        datePickerVC.btnCancelTitleFont = .regularValueFont
        datePickerVC.btnDoneTitle = CommonButtons.done
        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)
    }
    
    @IBAction func TapOnLeaseStartDateBtn(_ sender: UIButton) {
        let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblLeaseStartDate.text), inputFormat: DateFormats.dd_MM_yyyy_dashed), maximumDate: nil, minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dateString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.dd_MM_yyyy_dashed)
            self.lblLeaseStartDate.text = dateString
            self.lblLeaseEndDate.text = dateString
            self.txtLeasePeriod.text = "0"
            
        }, animated: true, origin: sender)
        
        datePickerVC.lblTitleText = CommonLabels.select
        datePickerVC.lblTitleColor = .customDarkBlueGradient
        datePickerVC.lblTitleFont = .boldMediumFont
        datePickerVC.btnCancelTitle = CommonButtons.cancel
        datePickerVC.btnCancelTitleColor = .customSubtitle
        datePickerVC.btnCancelTitleFont = .regularValueFont
        datePickerVC.btnDoneTitle = CommonButtons.done
        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)
    }
    
    @IBAction func TapOnLeaseEndDateBtn(_ sender: UIButton) {
        
        if lblLeaseStartDate.text != CommonLabels.select{
            let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblLeaseEndDate.text), inputFormat: DateFormats.dd_MM_yyyy_dashed), maximumDate: nil, minimumDate: DateConverter.getDateFromDateString(aDateString: getString(anything: lblLeaseStartDate.text), inputFormat: DateFormats.dd_MM_yyyy_dashed), datePickerMode: .date, doneAction: { (selectedDate) in
                
                let dateString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.dd_MM_yyyy_dashed)
                self.lblLeaseEndDate.text = dateString
                if let date = selectedDate{
                    if let startDate = DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblLeaseStartDate.text), inputFormat: DateFormats.dd_MM_yyyy_dashed){
                        self.txtLeasePeriod.text = getString(anything: date.years(from: startDate))
                        
                    }
                }
                
                
                
            }, animated: true, origin: sender)
            
            datePickerVC.lblTitleText = CommonLabels.select
            datePickerVC.lblTitleColor = .customDarkBlueGradient
            datePickerVC.lblTitleFont = .boldMediumFont
            datePickerVC.btnCancelTitle = CommonButtons.cancel
            datePickerVC.btnCancelTitleColor = .customSubtitle
            datePickerVC.btnCancelTitleFont = .regularValueFont
            datePickerVC.btnDoneTitle = CommonButtons.done
            datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
            datePickerVC.btnDoneTitleFont = .regularValueFont
            datePickerVC.show(onVC: self)
        }
        
    }
    
 
    @IBAction func TapOnSave(_ sender: UIButton) {
        if isValid(){
            print("Valid")
            fillModel()
            addLandWebServices()
        }
    }
    
    @IBAction func TapOnCancel(_ sender: UIButton) {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
    
    
}


extension AddLandDetailViewController{
    func isValid() -> Bool {
        var isValid : Bool = false
        if getString(anything: self.txtCRRNumber.text).isStringEmpty {
            Global.shared.showBanner(message: AddLandDetailsMessages.CRRNumberisrequired)
            self.txtCRRNumber.becomeFirstResponder()
            
        } else if getString(anything: self.txtCSNumber.text).isStringEmpty {
            Global.shared.showBanner(message: AddLandDetailsMessages.CSNumberisrequired)
            self.txtCSNumber.becomeFirstResponder()
            
        } else if getString(anything: self.lblDivision.text) == CommonLabels.select {
            Global.shared.showBanner(message: AddLandDetailsMessages.Divisionisrequired)
            self.lblDivision.becomeFirstResponder()
        } else if getString(anything: self.txtLandHolder.text).isStringEmpty {
            Global.shared.showBanner(message: AddLandDetailsMessages.LandHolderisrequired)
            self.txtLandHolder.becomeFirstResponder()
        
        } else if getString(anything: self.txtAddressOfOwner.text).isStringEmpty {
            Global.shared.showBanner(message: AddLandDetailsMessages.AddressOfOwnerisrequired)
            self.txtAddressOfOwner.becomeFirstResponder()
            
        } else if getString(anything: self.txtLocationOfProperty.text).isStringEmpty {
            Global.shared.showBanner(message: AddLandDetailsMessages.LocationOfpropertyisrequired)
            self.txtLocationOfProperty.becomeFirstResponder()
            
        }  else if getString(anything: self.lblLandTenture.text) == CommonLabels.select {
            Global.shared.showBanner(message: AddLandDetailsMessages.LandTenureisrequired)
            self.lblLandTenture.becomeFirstResponder()
            
        } else if getString(anything: self.lblUseOfLand.text) == CommonLabels.select {
            Global.shared.showBanner(message: AddLandDetailsMessages.UseofLandisrequired)
            self.lblUseOfLand.becomeFirstResponder()
            
        } else if validationType == 1,(getString(anything: self.txtAssessmentAmount.text).isStringEmpty) {
            Global.shared.showBanner(message: AddLandDetailsMessages.AssessmentAmountisrequired)
            self.txtAssessmentAmount.becomeFirstResponder()
            
        } else if getString(anything: self.txtIntrestRate.text).isStringEmpty {
            Global.shared.showBanner(message: AddLandDetailsMessages.InterestRateisrequired)
            self.txtIntrestRate.becomeFirstResponder()
            
        } else if getString(anything: self.txtArea.text).isStringEmpty {
            Global.shared.showBanner(message: AddLandDetailsMessages.Areaisrequired)
            self.txtArea.becomeFirstResponder()
            
        } else if getString(anything: self.txtPendingTax.text).isStringEmpty {
            Global.shared.showBanner(message: AddLandDetailsMessages.PendingTaxisrequired)
            self.txtPendingTax.becomeFirstResponder()
            
        }  else if getString(anything: self.txtIntrest.text).isStringEmpty {
            Global.shared.showBanner(message: AddLandDetailsMessages.Interestisrequired)
            self.txtIntrest.becomeFirstResponder()
            
        } else if validationType == 2{
            if getString(anything: self.txtRent.text).isStringEmpty {
                Global.shared.showBanner(message: AddLandDetailsMessages.Rentisrequired)
                self.txtRent.becomeFirstResponder()
            } else if getString(anything: self.lblDueDate.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.DueDateisrequired)
            } else if getString(anything: self.txtDeedNo.text).isStringEmpty {
                Global.shared.showBanner(message: AddLandDetailsMessages.DeedNoisrequired)
                self.txtDeedNo.becomeFirstResponder()
            }else {
                isValid = true
            }
        }else if validationType == 3{
            if getString(anything: self.txtRent.text).isStringEmpty {
                Global.shared.showBanner(message: AddLandDetailsMessages.Rentisrequired)
                self.txtRent.becomeFirstResponder()
                
            } else if getString(anything: self.lblDueDate.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.DueDateisrequired)
            }else {
                isValid = true
            }
        }else if validationType == 4{
            if getString(anything: self.txtFee.text).isStringEmpty {
                Global.shared.showBanner(message: AddLandDetailsMessages.Feeisrequired)
                self.txtFee.becomeFirstResponder()
            } else if getString(anything: self.lblCommDate.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.CommDateisrequired)
            }else {
                isValid = true
            }
        }else if validationType == 5{
            if getString(anything: self.txtRent.text).isStringEmpty {
                Global.shared.showBanner(message: AddLandDetailsMessages.Rentisrequired)
                self.txtRent.becomeFirstResponder()
            } else if getString(anything: self.lblDueDate1.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.DueDate1isrequired)
            } else if getString(anything: self.lblDueDate2.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.DueDate2isrequired)
            } else if getString(anything: self.lblDueDate3.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.DueDate3isrequired)
            } else if getString(anything: self.lblDueDate4.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.DueDate4isrequired)
            } else if getString(anything: self.lblDeedDate.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.DeedDateisrequired)
            } else if getString(anything: self.lblCommDate.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.CommDateisrequired)
            } else if getString(anything: self.lblExpDate.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.ExpDateisrequired)
            } else if getString(anything: self.lblPRYear.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.PRYearisrequired)
            } else if getString(anything: self.lblLeaseStartDate.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.LeaseStartDateisrequired)
            } else if getString(anything: self.lblLeaseEndDate.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.LeaseEndDateisrequired)
            } else if getString(anything: self.txtLeasePeriod.text).isStringEmpty {
                Global.shared.showBanner(message: AddLandDetailsMessages.LeasePeriodisrequired)
                self.txtLeasePeriod.becomeFirstResponder()
            }else {
                isValid = true
            }
        } else if validationType == 6{
            if getString(anything: self.txtRent.text).isStringEmpty {
                Global.shared.showBanner(message: AddLandDetailsMessages.Rentisrequired)
                self.txtRent.becomeFirstResponder()
                
            }  else if getString(anything: self.lblDueDate1.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.DueDate1isrequired)
            } else if getString(anything: self.lblDueDate2.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.DueDate2isrequired)
            } else if getString(anything: self.lblDueDate3.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.DueDate3isrequired)
            } else if getString(anything: self.lblDueDate4.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.DueDate4isrequired)
            } else if getString(anything: self.lblDeedDate.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.DeedDateisrequired)
            } else if getString(anything: self.lblCommDate.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.CommDateisrequired)
            } else if getString(anything: self.lblExpDate.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.ExpDateisrequired)
            } else if getString(anything: self.lblPRYear.text) == CommonLabels.select {
                Global.shared.showBanner(message: AddLandDetailsMessages.PRYearisrequired)
            } else {
                isValid = true
            }
        } else {
            isValid = true
        }
        return isValid
    }
}

//MARK:-  Web Service model
extension AddLandDetailViewController{
    
    func getWebserviceforUseOfLand() {
           CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        webServiceModel.getUseOfLandData { (responseModel) in
               CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
               if let response = responseModel {
                   print(response)
                   self.arrUseOfLandOptions.append(contentsOf: response.list)
               }
           }
       }
    
    func getDataFromWebServices() {
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.webServiceModel.getFilterDropDownListApi { arr_Divisions, isSucess, tenures in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            self.arrDivisions = arr_Divisions
            self.arrLandTenure = tenures
        }
    }
    
    
    fileprivate func fillModel() {
        self.webServiceModel.crr_number = getString(anything: txtCRRNumber.text)
        self.webServiceModel.cs_number = getString(anything: txtCSNumber.text)
        self.webServiceModel.address_of_owner = getString(anything: txtAddressOfOwner.text)
        self.webServiceModel.location_of_property = getString(anything: txtLocationOfProperty.text)
        self.webServiceModel.interest_rate = getString(anything: txtIntrestRate.text).trimming_WS
        self.webServiceModel.pending_tax = getString(anything: txtPendingTax.text).trimming_WS
        self.webServiceModel.area = getString(anything: txtArea.text).trimming_WS
        self.webServiceModel.interest = getString(anything: txtIntrest.text)
        self.webServiceModel.name_of_owner = getString(anything: txtLandHolder.text)
        
        if validationType == 1{
            self.webServiceModel.assessment_amount = getString(anything: txtAssessmentAmount.text)
            self.webServiceModel.rent = ""
            self.webServiceModel.fee = ""
            self.webServiceModel.deed_no = ""
            self.webServiceModel.comm_date = ""
            self.webServiceModel.due_date = ""
            self.webServiceModel.due_date1 = ""
            self.webServiceModel.due_date2 = ""
            self.webServiceModel.due_date3 = ""
            self.webServiceModel.due_date4 = ""
            self.webServiceModel.exp_date = ""
            self.webServiceModel.deed_date = ""
            self.webServiceModel.pr_yr = ""
            self.webServiceModel.lease_start_date = ""
            self.webServiceModel.lease_end_date = ""
            self.webServiceModel.lease_period = ""
        }else if validationType == 2{
            self.webServiceModel.assessment_amount = ""
            self.webServiceModel.rent = getString(anything: txtRent.text)
            self.webServiceModel.deed_no = getString(anything: txtDeedNo.text)
            self.webServiceModel.due_date = getString(anything: lblDueDate.text)
            self.webServiceModel.fee = ""
            self.webServiceModel.comm_date = ""
            self.webServiceModel.due_date1 = ""
            self.webServiceModel.due_date2 = ""
            self.webServiceModel.due_date3 = ""
            self.webServiceModel.due_date4 = ""
            self.webServiceModel.exp_date = ""
            self.webServiceModel.deed_date = ""
            self.webServiceModel.pr_yr = ""
            self.webServiceModel.lease_start_date = ""
            self.webServiceModel.lease_end_date = ""
            self.webServiceModel.lease_period = ""
        }else if validationType == 3{
            self.webServiceModel.assessment_amount = ""
            self.webServiceModel.fee = ""
            self.webServiceModel.rent = getString(anything: txtRent.text)
            self.webServiceModel.due_date = getString(anything: lblDueDate.text)
            self.webServiceModel.deed_no = ""
            self.webServiceModel.comm_date = ""
            self.webServiceModel.due_date1 = ""
            self.webServiceModel.due_date2 = ""
            self.webServiceModel.due_date3 = ""
            self.webServiceModel.due_date4 = ""
            self.webServiceModel.exp_date = ""
            self.webServiceModel.deed_date = ""
            self.webServiceModel.pr_yr = ""
            self.webServiceModel.lease_start_date = ""
            self.webServiceModel.lease_end_date = ""
            self.webServiceModel.lease_period = ""
        }else if validationType == 4{
            self.webServiceModel.assessment_amount = ""
            self.webServiceModel.rent = ""
            self.webServiceModel.fee = getString(anything: txtFee.text)
            self.webServiceModel.deed_no = ""
            self.webServiceModel.comm_date = getString(anything: lblCommDate.text)
            self.webServiceModel.due_date = ""
            self.webServiceModel.due_date1 = ""
            self.webServiceModel.due_date2 = ""
            self.webServiceModel.due_date3 = ""
            self.webServiceModel.due_date4 = ""
            self.webServiceModel.exp_date = ""
            self.webServiceModel.deed_date = ""
            self.webServiceModel.pr_yr = ""
            self.webServiceModel.lease_start_date = ""
            self.webServiceModel.lease_end_date = ""
            self.webServiceModel.lease_period = ""
        }else if validationType == 5{
            self.webServiceModel.assessment_amount = ""
            self.webServiceModel.fee = ""
            self.webServiceModel.deed_no = ""
            self.webServiceModel.rent = getString(anything: txtRent.text)
            self.webServiceModel.comm_date = getString(anything: lblCommDate.text)
            self.webServiceModel.due_date = ""
            self.webServiceModel.due_date1 = getString(anything: lblDueDate1.text)
            self.webServiceModel.due_date2 = getString(anything: lblDueDate2.text)
            self.webServiceModel.due_date3 = getString(anything: lblDueDate3.text)
            self.webServiceModel.due_date4 = getString(anything: lblDueDate4.text)
            self.webServiceModel.exp_date = getString(anything: lblExpDate.text)
            self.webServiceModel.deed_date = getString(anything: lblDeedDate.text)
            self.webServiceModel.pr_yr = getString(anything: lblPRYear.text)
            self.webServiceModel.lease_start_date = getString(anything: lblLeaseStartDate.text)
            self.webServiceModel.lease_end_date = getString(anything: lblLeaseEndDate.text)
            self.webServiceModel.lease_period = getString(anything: txtLeasePeriod.text)
        }else if validationType == 6{
            self.webServiceModel.assessment_amount = ""
            self.webServiceModel.rent = getString(anything: txtRent.text)
            self.webServiceModel.fee = ""
            self.webServiceModel.deed_no = ""
            self.webServiceModel.comm_date = getString(anything: lblCommDate.text)
            self.webServiceModel.due_date = ""
            self.webServiceModel.due_date1 = getString(anything: lblDueDate1.text)
            self.webServiceModel.due_date2 = getString(anything: lblDueDate2.text)
            self.webServiceModel.due_date3 = getString(anything: lblDueDate3.text)
            self.webServiceModel.due_date4 = getString(anything: lblDueDate4.text)
            self.webServiceModel.exp_date = getString(anything: lblExpDate.text)
            self.webServiceModel.deed_date = getString(anything: lblDeedDate.text)
            self.webServiceModel.pr_yr = getString(anything: lblPRYear.text)
            self.webServiceModel.lease_start_date = ""
            self.webServiceModel.lease_end_date = ""
            self.webServiceModel.lease_period = ""
        }
//        self.webServiceModel.assessment_amount = getString(anything: txtAssessmentAmount.text)
//        self.webServiceModel.rent = getString(anything: txtRent.text)
//        self.webServiceModel.fee = getString(anything: txtFee.text)
//        self.webServiceModel.deed_no = getString(anything: txtDeedNo.text)
//        self.webServiceModel.comm_date = getString(anything: lblCommDate.text)
//        self.webServiceModel.due_date = getString(anything: lblDueDate.text)
//        self.webServiceModel.due_date1 = getString(anything: lblDueDate1.text)
//        self.webServiceModel.due_date2 = getString(anything: lblDueDate2.text)
//        self.webServiceModel.due_date3 = getString(anything: lblDueDate3.text)
//        self.webServiceModel.due_date4 = getString(anything: lblDueDate4.text)
//        self.webServiceModel.exp_date = getString(anything: lblExpDate.text)
//        self.webServiceModel.deed_date = getString(anything: lblDeedDate.text)
//        self.webServiceModel.pr_yr = getString(anything: lblPRYear.text)
//        self.webServiceModel.lease_start_date = getString(anything: lblLeaseStartDate.text)
//        self.webServiceModel.lease_end_date = getString(anything: lblLeaseEndDate.text)
//        self.webServiceModel.lease_period = getString(anything: txtLeasePeriod.text)

        
        

        
        
        
    }
    
    
    func addLandWebServices() {
        view.endEditing(true)
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.webServiceModel.addLand { responseModel,isSuccess in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if isSuccess{
                NotificationCenter.default.post(name: .addLand, object: nil)
                let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
                self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                
            }
        }
    }
}


//MARK: - UITextField Delegate
extension AddLandDetailViewController {
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let fullString = NSString(string: textField.text ?? "").replacingCharacters(in: range, with: string)
        
        if (textField == txtIntrest) || (textField == txtArea) || (textField == txtPendingTax) || (textField == txtIntrestRate) || (textField == txtFee)  || (textField == txtAssessmentAmount)  || (textField == txtRent){
            
            if fullString.first == "." || fullString.components(separatedBy: ".").count > 2 {
                return false
            }
            let numberSet = CharacterSet(charactersIn: NUMERIC + ".").inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return (string == numberFiltered) && (fullString.count < 12)
            
        }
        
        return true
    }
    
   
}

