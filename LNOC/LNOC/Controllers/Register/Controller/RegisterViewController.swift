//
//  RegisterViewController.swift
//  NWM
//
//  Created by Gaurang Patel on 14/04/21.
//

import UIKit

class RegisterViewController: HeaderViewController {

    //MARK: - Outlet
    //View
    @IBOutlet weak var viewBtnSubmitGradient: GradientView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewForScroll: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewForShadow: UIView!
    @IBOutlet weak var viewOTP: UIView!
    @IBOutlet weak var viewOTPEmail: UIView!
    
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtMiddleName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtEmailOTP: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var txtConfirmPassword: UITextField!
    
    @IBOutlet weak var btnSelectGender : UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnPassword: UIButton!
    @IBOutlet weak var btnConPassword: UIButton!
    @IBOutlet weak var btnSendOTP: UIButton!
    @IBOutlet weak var btnSendEmailOTP: UIButton!
    @IBOutlet weak var btnVerifyEmailOtp: UIButton!
    @IBOutlet weak var btnVerifyMobileOtp: UIButton!
    
    @IBOutlet weak var lblOTPTime: UILabel!
    @IBOutlet weak var lblGetResendOTP: LocalizedLabel!
    @IBOutlet weak var lblOTPSent: UILabel!
    @IBOutlet weak var lblOTPTimeEmail: UILabel!
    @IBOutlet weak var lblGetResendOTPEmail: LocalizedLabel!
    @IBOutlet weak var lblOTPSentEmail: UILabel!
    @IBOutlet weak var txtOTP: UITextField!
    @IBOutlet weak var lblVerifyEmailOtp: LocalizedLabel!
    @IBOutlet weak var lblVerifyMobileOtp: LocalizedLabel!
    
    @IBOutlet weak var lblGender: UILabel!
    
    @IBOutlet weak var lblDateOfBirth: UILabel!
    @IBOutlet weak var lblState: UILabel!
    
    @IBOutlet weak var lblDistrict: UILabel!
    @IBOutlet weak var lblCity: UILabel!


    @IBOutlet weak var txtIhaveReadPrivacyPolicyStatic: NWM_TextView!
    
    @IBOutlet weak var imgEmailVerified : UIImageView!
    @IBOutlet weak var imgMobileVerified : UIImageView!
    @IBOutlet weak var imgGenderDownArrow : UIImageView!
    @IBOutlet weak var imgViewPrivacyPolicy: UIImageView!
    @IBOutlet weak var imgViewCalnder: UIImageView!
    
    
    @IBOutlet var lblTitles: [LocalizedLabel]!
    @IBOutlet var txtValues: [UITextField]!
    @IBOutlet var lblAsterisks: [LocalizedLabel]!
    @IBOutlet var lblSeprator: [UILabel]!
    
    @IBOutlet var imgDropDown: [UIImageView]!
    
    
    fileprivate var registerResponseModel = RegisterWebServiceModel()
    fileprivate var timer: Timer?
    fileprivate var timerEmail: Timer?
    fileprivate var totalSecond = 30
    fileprivate var totalSecondEmail = 30

    private var selectedGenderType : Gender = .male
    private var arrGenderOptions = [String]()
    private var arrStateOptions = [String]()
    var arrDistrictOptions: [DropDownModel] = []
    var arrCityOptions: [DropDownModel] = []

    fileprivate var privacyPolicy: Bool = false
    fileprivate var key_privacy_policy = "key_privacy_policy"
    fileprivate var key_check_privacy_policy = "key_check_privacy_policy"
    
    //Public
    var isPopedUp = false
    var signUpDoneBlock: (() -> ())!
    
    
    //MARK: - Override Method
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpIPadConstraints()
        self.setUpHeader()
        self.setupLbl()
        self.fontSetUp()
        self.setUpView()
        self.setUpImg()
        
        
        self.arrGenderOptions.append(CommonLabels.select)
        self.arrGenderOptions += self.selectedGenderType.arrayTitle
        self.arrStateOptions.append(RegistrtionLabels.selectState)
        self.arrStateOptions.append("MAHARASHRTA")
        
        arrDistrictOptions = [DropDownModel(dict: [DropDownModel.Keys.title.rawValue: RegistrtionLabels.selectDistrict,
                                                   DropDownModel.Keys.id.rawValue: ""])]
        arrCityOptions = [DropDownModel(dict: [DropDownModel.Keys.title.rawValue: RegistrtionLabels.selectCity,
                                                   DropDownModel.Keys.id.rawValue: ""])]

    }

    
    //MARK: - private Method
    
    fileprivate func setUpView() {

        self.view.layoutIfNeeded()
        
        self.viewMain.setCustomCornerRadius(radius: 12)
        self.viewForShadow.setCustomCornerRadius(radius: 12)
        self.viewForShadow.addShadow()
        self.viewOTP.isHidden = true
        self.viewOTPEmail.isHidden = true
        
        self.btnSendOTP.superview?.setFullCornerRadius(borderColor: .customgreenTheamClr, borderWidth: 1)
        self.btnSendOTP.superview?.backgroundColor = .clear
        
        self.btnSendEmailOTP.superview?.setFullCornerRadius(borderColor: .customgreenTheamClr, borderWidth: 1)
        self.btnSendEmailOTP.superview?.backgroundColor = .clear
        
        self.btnVerifyEmailOtp.superview?.setFullCornerRadius()
        self.btnVerifyEmailOtp.superview?.backgroundColor = .customgreenTheamClr
        
        self.btnVerifyMobileOtp.superview?.setFullCornerRadius()
        self.btnVerifyMobileOtp.superview?.backgroundColor = .customgreenTheamClr

        self.lblSeprator.forEach({
            $0.backgroundColor = .customSeparator
        })
        
        self.txtEmail.delegate = self
        self.txtMobileNo.delegate = self
        self.txtOTP.delegate = self
        self.txtEmailOTP.delegate = self
        self.txtPassword.delegate = self
        self.txtConfirmPassword.delegate = self
        self.viewBtnSubmitGradient.setFullCornerRadius()
        
        btnPassword.setImage(.ic_password_show, for: .normal)
        btnPassword.tintColor = .customGray
        btnConPassword.setImage(.ic_password_show, for: .normal)
        btnConPassword.tintColor = .customGray
        
        self.lblDateOfBirth.text = CommonLabels.select
        self.lblGender.text = CommonLabels.select
        self.lblState.text = RegistrtionLabels.selectState
        self.lblDistrict.text = RegistrtionLabels.selectDistrict
        self.lblCity.text = RegistrtionLabels.selectCity
        
        self.viewBtnSubmitGradient.addGradient(ofColors: [UIColor.themeDarkOrangeClr.cgColor, UIColor.themeLightOrangeClr.cgColor], direction: .rightLeft)
    }
    
    fileprivate func setUpImg() {
        imgViewPrivacyPolicy.image = .checkbox
        imgViewPrivacyPolicy.tintColor = .customOrange
        
        imgEmailVerified.image = .correct
        imgEmailVerified.isHidden = true
        imgMobileVerified.image = .correct
        imgMobileVerified.isHidden = true
        
        self.imgGenderDownArrow.image = .ic_down_arrow
        self.imgGenderDownArrow.tintColor = .customGray
    }
    
    fileprivate func setupLbl() {
        
        let privicyPolicy = NSMutableAttributedString(string: "CommonLabels.IhavereadprivacyPolicy", attributes: [NSAttributedString.Key.font:UIFont.regularTitleFont])
        
        privicyPolicy.addAttributes([NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue, NSAttributedString.Key.link : key_privacy_policy], range: "CommonLabels.IhavereadprivacyPolicy".nsString.range(of: "CommonLabels.privacyPolicy"))
        
        txtIhaveReadPrivacyPolicyStatic.attributedText = privicyPolicy
        txtIhaveReadPrivacyPolicyStatic.delegate = self
        txtIhaveReadPrivacyPolicyStatic.tintColor = UIColor.customOrange
        txtIhaveReadPrivacyPolicyStatic.isEditable = false
        txtIhaveReadPrivacyPolicyStatic.otherTextClickCallBack = {
            self.btnPrivacyPolicyTickTapped(1)
        }
        
    }
    
    fileprivate func fontSetUp() {
        self.lblTitles.forEach({
            $0.font = .regularTitleFont
            $0.textColor = .customBlackTitle
        })
        
        for lbl in lblAsterisks{
            lbl.textColor = .customRed
            lbl.font = .regularTitleFont
            lbl.text = "*"
        }
        
        self.txtValues.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
        })
        
        self.imgDropDown.forEach({
            $0.tintColor = .customgreenTheamClr
            $0.image = .ic_down_arrow
        })
        
        imgViewCalnder.image = .calender
        imgViewCalnder.tintColor = .customgreenTheamClr
        
        lblGetResendOTP.text = SignUp_EditProfileLabels.Send_OTP
        lblGetResendOTP.textColor = .customgreenTheamClr
        lblGetResendOTP.font = .regularTitleFont
        
        lblGetResendOTPEmail.text = SignUp_EditProfileLabels.Send_OTP
        lblGetResendOTPEmail.textColor = .customgreenTheamClr
        lblGetResendOTPEmail.font = .regularTitleFont
        
        lblVerifyEmailOtp.text = SignUp_EditProfileLabels.verifyOTP
        lblVerifyEmailOtp.textColor = .customWhite
        lblVerifyEmailOtp.font = .regularTitleFont
        
        lblVerifyMobileOtp.text = SignUp_EditProfileLabels.verifyOTP
        lblVerifyMobileOtp.textColor = .customWhite
        lblVerifyMobileOtp.font = .regularTitleFont
        
        lblOTPTime.font = .regularTitleFont
        lblOTPTime.textColor = .customBlack
        lblOTPSent.font = .regularTitleFont
        
        lblOTPTimeEmail.font = .regularTitleFont
        lblOTPTimeEmail.textColor = .customBlack
        lblOTPSentEmail.font = .regularTitleFont

        self.lblDateOfBirth.textColor = .customBlack
        self.lblDateOfBirth.font = .regularValueFont
        
        self.lblGender.textColor = .customBlack
        self.lblGender.font = .regularValueFont
        
        self.lblState.textColor = .customBlack
        self.lblState.font = .regularValueFont
        
        self.lblDistrict.textColor = .customBlack
        self.lblDistrict.font = .regularValueFont
        
        self.lblCity.textColor = .customBlack
        self.lblCity.font = .regularValueFont
        
        self.btnRegister.titleLabel?.font = UIFont.semiboldLargeFont
    }
    
    fileprivate func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle: "Sign Up")
        setHeaderView_BackImage()
    }
    
    fileprivate func setUpIPadConstraints() {
        changeLeadingTrailingForiPad(view: viewForScroll)
        changeHeightForiPad(view: txtFullName)
        changeHeightForiPad(view: btnRegister)
    
    }
    
    fileprivate func setDistrictData() {
//        arrDistrictOptions = [DropDownModel(dict: [DropDownModel.Keys.title.rawValue: CommonLabels.select,
//                                                   DropDownModel.Keys.id.rawValue: ""]),
//                              DropDownModel(dict: [DropDownModel.Keys.title.rawValue: "MUMBAI",
//                                                  DropDownModel.Keys.id.rawValue: "1"]),
//                             DropDownModel(dict: [DropDownModel.Keys.title.rawValue: "NASHIK",
//                                                  DropDownModel.Keys.id.rawValue: "2"])]
        getWebserviceforDistrict()
    }
    
    fileprivate func invalidateTimer() {
        totalSecond = 30
        imgMobileVerified.isHidden = true
        txtOTP.resignFirstResponder()
        viewOTP.isUserInteractionEnabled = true
        lblOTPTime.text = ""
        timer?.invalidate()
        timer?.fire()
    }
    
    fileprivate func invalidateTimerEmail() {
        totalSecondEmail = 30
        imgEmailVerified.isHidden = true
        txtEmailOTP.resignFirstResponder()
        viewOTPEmail.isUserInteractionEnabled = true
        lblOTPTimeEmail.text = ""
        timerEmail?.invalidate()
        timerEmail?.fire()
    }
    
    //MARK: - Button Action Selector Methods
    @objc fileprivate func timerCalled(_ t: Timer) {
        if (totalSecond == 0) {
            invalidateTimer()
            btnSendOTP.isUserInteractionEnabled = true
            self.btnSendOTP.superview?.isHidden = false
            btnSendOTP.superview?.setFullCornerRadius(borderColor: .customgreenTheamClr, borderWidth: 1)
            lblGetResendOTP.textColor = .customgreenTheamClr
            return
        }
        totalSecond = totalSecond - 1
        lblOTPTime.text = String(format: "%02d:%02d", (totalSecond % 3600) / 60, (totalSecond % 3600) % 60)
    }
    
    @objc fileprivate func timerCalledEmail(_ t: Timer) {
        if (totalSecondEmail == 0) {
            invalidateTimerEmail()
            btnSendEmailOTP.isUserInteractionEnabled = true
            btnSendEmailOTP.superview?.setFullCornerRadius(borderColor: .customgreenTheamClr, borderWidth: 1)
            lblGetResendOTPEmail.textColor = .customgreenTheamClr
            btnSendEmailOTP.superview?.isHidden = false
            return
        }
        totalSecondEmail = totalSecondEmail - 1
        lblOTPTimeEmail.text = String(format: "%02d:%02d", (totalSecondEmail % 3600) / 60, (totalSecondEmail % 3600) % 60)
    }
    
    @IBAction func btnPrivacyPolicyTickTapped(_ sender: Any) {
        privacyPolicy = !privacyPolicy
        self.imgViewPrivacyPolicy.image = privacyPolicy ? .checkboxSelected : .checkbox
        self.imgViewPrivacyPolicy.tintColor = .customOrange
    }
    
    @IBAction func TapOnSubmitBtn(_ sender: LocalizedButton) {
        if self.isValid() {
            UIView.hideKeyBoard()
//            self.verifyOtpBoth()
            self.fillModel()
            self.getDataFromWebServices()
        }
//        verifyOtpBoth()
    }
    
    @IBAction func TapOnDateOfBirthBtn(_ sender: UIButton) {
        
        let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblDateOfBirth.text), inputFormat: DateFormats.yyyy_MM_dd_dashed), maximumDate: Date(), minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dobString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.yyyy_MM_dd_dashed)
            self.lblDateOfBirth.text = dobString
            
        }, animated: true, origin: sender)
        
        datePickerVC.lblTitleText = CommonLabels.select
        datePickerVC.lblTitleColor = .customDarkBlueGradient
        datePickerVC.lblTitleFont = .boldMediumFont
        datePickerVC.btnCancelTitle = CommonButtons.cancel
        datePickerVC.btnCancelTitleColor = .customSubtitle
        datePickerVC.btnCancelTitleFont = .regularValueFont
        datePickerVC.btnDoneTitle = CommonButtons.done
        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)
    }
    
    @IBAction func TapOnGenderBtn(_ sender: UIButton) {
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblGender.text, arrSource: [self.arrGenderOptions], doneAction: { (rows, value) in
            
            self.lblGender.text = getString(anything: value)
            if self.arrGenderOptions[1] == self.lblGender.text {
                self.selectedGenderType = .male
            } else if self.arrGenderOptions[2] == self.lblGender.text {
                self.selectedGenderType = .female
            } else if self.arrGenderOptions[3] == self.lblGender.text {
                self.selectedGenderType = .transgender
            }
            
        }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func TapOnSelectStateBtn(_ sender: UIButton) {
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblState.text, arrSource: [self.arrStateOptions], doneAction: { (rows, value) in
            self.lblState.text = getString(anything: value)
            if getString(anything: value) == "MAHARASHRTA"{
                self.getWebserviceforDistrict()
            }else{
                self.lblDistrict.text = RegistrtionLabels.selectDistrict
                self.arrDistrictOptions.removeAll()
                self.arrDistrictOptions.append(DropDownModel(dict: [DropDownModel.Keys.title.rawValue: RegistrtionLabels.selectDistrict,
                                                           DropDownModel.Keys.id.rawValue: ""]))
                self.lblCity.text = RegistrtionLabels.selectCity
                self.arrCityOptions.removeAll()
                self.arrCityOptions.append(DropDownModel(dict: [DropDownModel.Keys.title.rawValue: RegistrtionLabels.selectCity,
                                                           DropDownModel.Keys.id.rawValue: ""]))
            }
        }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func TapOnSslectDistrictBtn(_ sender: UIButton) {
        let list = arrDistrictOptions.map({ $0.title })
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblDistrict.text, arrSource: [list], doneAction: { (rows, value) in
            self.lblDistrict.text = getString(anything: value)
            self.dismiss(animated: true, completion: nil)
            if let index = rows.first{
                self.registerResponseModel.districts_id = self.arrDistrictOptions[index].id
                self.lblCity.text = RegistrtionLabels.selectCity
                if !self.arrDistrictOptions[index].id.isEmptyString{
                    self.getWebserviceCity()
                }
            }
        }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func TapOnSelectCityBtn(_ sender: UIButton) {
        let list = arrCityOptions.map({ $0.title })
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblCity.text, arrSource: [list], doneAction: { (rows, value) in
            self.lblCity.text = getString(anything: value)
            if let index = rows.first{
                self.registerResponseModel.city_id = self.arrCityOptions[index].id
            }
            
        }, animated: true, origin: sender, onVC: self)
        
    }
    
    @IBAction func TapOnEmailOTP(_ sender: UIButton) {
        if isValidEmail() {
            self.getOTPEmail()
        }
    }
    
    @IBAction func TapOnGetResendOTP(_ sender: UIButton) {
        if self.isValidMobileNumber() {
            self.getOTPMobile()
        }
    }
    
    @IBAction func TapOnHideShowPassword(_ sender: UIButton) {
        if (sender.isSelected) {
            sender.setImage(.ic_password_show, for: .normal)
            sender.tintColor = .customGray
            txtPassword.isSecureTextEntry = true
        } else {
            sender.setImage(.ic_password_not_show, for: .normal)
            sender.tintColor = .customGray
            txtPassword.isSecureTextEntry = false
        }
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func TapOnHideShowConPassword(_ sender: UIButton) {
        if (sender.isSelected) {
            sender.setImage(.ic_password_show, for: .normal)
            sender.tintColor = .customGray
            txtConfirmPassword.isSecureTextEntry = true
        } else {
            sender.setImage(.ic_password_not_show, for: .normal)
            sender.tintColor = .customGray
            txtConfirmPassword.isSecureTextEntry = false
        }
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnVerifyEmailOtpClicked(_ sender: UIButton) {
        if isValidEmailOtp() {
            verifyOtpEmail()
        }
    }
    
    @IBAction func btnVerifyMobileOtpClicked(_ sender: UIButton) {
        if isValidMobileOtp() {
            verifyOtpMobile()
        }
    }
    
}

extension RegisterViewController: UITextViewDelegate {
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        if URL.absoluteString == key_privacy_policy {
//            let webViewVC = WebViewViewController.instantiate(appStoryboard: .webView)
//            webViewVC.typeOfWebView = .privacyPolicy
//            self.navigationController?.pushViewController(webViewVC, animated: true)
            return false
        }
        return true
    }
}

//MARK:- Validations
extension RegisterViewController {
    func isValid() -> Bool {
        var isValid : Bool = false
        if getString(anything: self.txtFullName.text).isStringEmpty {
            Global.shared.showBanner(message: RegisterMessages.firstNameIsRequired)
            self.txtFullName.becomeFirstResponder()
            
        } else if getString(anything: self.txtMiddleName.text).isStringEmpty {
            Global.shared.showBanner(message: RegisterMessages.middleNameIsRequired)
            self.txtMiddleName.becomeFirstResponder()
            
        }  else if getString(anything: self.txtLastName.text).isStringEmpty {
            Global.shared.showBanner(message: RegisterMessages.lastNameIsRequired)
            self.txtLastName.becomeFirstResponder()
            
        } else if (self.lblDateOfBirth.text ?? "").isStringEmpty || self.lblDateOfBirth.text == CommonLabels.select {
            Global.shared.showBanner(message: RegisterMessages.dateOfBirthIsRequired)
            self.lblDateOfBirth.becomeFirstResponder()
            
        }/* else if getString(anything: self.txtEmail.text).isStringEmpty {
            Global.shared.showBanner(message: RegisterMessages.emailIsRequired)
            self.txtEmail.becomeFirstResponder()
        
        } else if !getString(anything: self.txtEmail.text).isValidEmail {
            Global.shared.showBanner(message: RegisterMessages.pleaseEnteraValidEmailAddress)
            self.txtEmail.becomeFirstResponder()
            
        } */else if isValidEmail() == false {
            
        } else if imgEmailVerified.isHidden {
            Global.shared.showBanner(message: RegisterMessages.emailIsNotVerify)
            self.txtEmail.becomeFirstResponder()
            return false
            
        } else if isValidMobileNumber() == false {
            
        } else if imgMobileVerified.isHidden {
            Global.shared.showBanner(message: RegisterMessages.mobileNumberIsNotVerify)
            self.txtMobileNo.becomeFirstResponder()
            return false
            
        } /*else if getString(anything: self.txtOTP.text).isStringEmpty {
            Global.shared.showBanner(message: RegisterMessages.msg_please_enter_OTP)
            self.txtOTP.becomeFirstResponder()
            
//        } else if (self.lblGender.text ?? "").isStringEmpty || self.lblGender.text == CommonLabels.select {
//            Global.shared.showBanner(message: RegisterMessages.selectGender)
            
        }*/ else if (self.lblState.text ?? "").isStringEmpty || self.lblState.text == RegistrtionLabels.selectState {
            Global.shared.showBanner(message: RegisterMessages.pleaseSelectYourState)
            
        } else if (self.lblDistrict.text ?? "").isStringEmpty || self.lblDistrict.text == RegistrtionLabels.selectDistrict {
            Global.shared.showBanner(message: RegisterMessages.pleaseSelectYourDistrict)

        } else if getString(anything: self.txtPassword.text).count == 0 {
            Global.shared.showBanner(message: RegisterMessages.passwordIsRequired)
            self.txtPassword.becomeFirstResponder()
            
        } else if !getString(anything: self.txtPassword.text).trimming_WS.isValidPassword() {
            //Global.shared.showBanner(message: RegisterMessages.msg_password)
            UIAlertController.showWith(title: CommonLabels.alert, msg: RegisterMessages.password_not_in_valid_formate, actionTitles: [ CommonButtons.ok], actionStyles: [.default], actionHandlers: [{ (yesAction) in
                self.txtPassword.becomeFirstResponder()
            }])
            
            
        }  else if getString(anything: self.txtConfirmPassword.text).count == 0 {
            Global.shared.showBanner(message: RegisterMessages.confirmPasswordIsRequired)
            self.txtConfirmPassword.becomeFirstResponder()
    
        }else if getString(anything: self.txtPassword.text) != getString(anything: self.txtConfirmPassword.text) {
            Global.shared.showBanner(message: RegisterMessages.thePasswordAndConfirmPasswordShouldBeSame)
            self.txtConfirmPassword.becomeFirstResponder()
            
        } else {
            isValid = true
        }
        
        return isValid
    }
    
    func isValidMobileNumber() -> Bool {
        if getString(anything: self.txtMobileNo.text).isStringEmpty {
            Global.shared.showBanner(message: RegisterMessages.mobileNumberIsRequired)
            self.txtMobileNo.becomeFirstResponder()
            return false
            
        } else if (getString(anything: self.txtMobileNo.text)).count < 10 {
            Global.shared.showBanner(message: RegisterMessages.mobileNumberIsNotValidMinimum10NumberRequired)
            self.txtMobileNo.becomeFirstResponder()
            return false
            
        } else {
            return true
        }
    }
    
    func isValidEmail() -> Bool {
        if getString(anything: self.txtEmail.text).isStringEmpty {
            Global.shared.showBanner(message: RegisterMessages.emailIsRequired)
            self.txtEmail.becomeFirstResponder()
            return false
        } else if !getString(anything: self.txtEmail.text).isValidEmail {
            Global.shared.showBanner(message: RegisterMessages.pleaseEnteraValidEmailAddress)
            self.txtEmail.becomeFirstResponder()
            return false
        } else {
            return true
        }
        
    }
    
    func isValidEmailOtp() -> Bool {
        if getString(anything: self.txtEmailOTP.text).isStringEmpty {
            Global.shared.showBanner(message: RegisterMessages.otpIsRequired)
            txtEmailOTP.becomeFirstResponder()
            return false
        } else if getString(anything: self.txtEmailOTP.text).count < 6 {
            Global.shared.showBanner(message: RegisterMessages.minimum6Required)
            txtEmailOTP.becomeFirstResponder()
            return false
        } else {
            UIView.hideKeyBoard()
            return true
        }
    }
    
    func isValidMobileOtp() -> Bool {
        if getString(anything: self.txtOTP.text).isStringEmpty {
            Global.shared.showBanner(message: RegisterMessages.otpIsRequired)
            txtOTP.becomeFirstResponder()
            return false
        } else if getString(anything: self.txtOTP.text).count < 6 {
            Global.shared.showBanner(message: RegisterMessages.minimum6Required)
            txtOTP.becomeFirstResponder()
            return false
        } else {
            UIView.hideKeyBoard()
            return true
        }
    }
    
}

//MARK:- UITextField Delegate
extension RegisterViewController {
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let fullString = NSString(string: textField.text ?? "").replacingCharacters(in: range, with: string)
        
        if (textField == txtMobileNo) {
            if !(viewOTP.isHidden) {
                txtOTP.text = ""
                viewOTP.isHidden = true
                invalidateTimer()
            }
            lblGetResendOTP.text = SignUp_EditProfileLabels.Send_OTP
            lblGetResendOTP.textColor = .customgreenTheamClr
            btnSendOTP.superview?.setFullCornerRadius(borderColor: .customgreenTheamClr, borderWidth: 1)
            btnSendOTP.superview?.isHidden = false
            btnSendOTP.isUserInteractionEnabled = true
            imgMobileVerified.isHidden = true
            
            let numberSet = NSCharacterSet(charactersIn: NUMERIC).inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if (string == numberFiltered) && (fullString.count < (MOBILE_LENGTH_VALIDATION + 1)) {
                if (fullString.hasPrefix("6") || fullString.hasPrefix("7") || fullString.hasPrefix("8") || fullString.hasPrefix("9")) || fullString.isEmpty {
                    return true
                }else{
                    return false
                }
            } else {
                return false
            }
            
        } else if (textField == txtEmail) {
            if !(viewOTPEmail.isHidden) {
                txtEmailOTP.text = ""
                viewOTPEmail.isHidden = true
                invalidateTimerEmail()
            }
            lblGetResendOTPEmail.text = SignUp_EditProfileLabels.Send_OTP
            lblGetResendOTPEmail.textColor = .customgreenTheamClr
            btnSendEmailOTP.superview?.setFullCornerRadius(borderColor: .customgreenTheamClr, borderWidth: 1)
            btnSendEmailOTP.superview?.isHidden = false
            btnSendEmailOTP.isUserInteractionEnabled = true
            imgEmailVerified.isHidden = true
            return true
            
        } else if textField == txtOTP || textField == txtEmailOTP {
           
            let numberSet = NSCharacterSet(charactersIn: NUMERIC).inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return (string == numberFiltered) && (fullString.count < (OTP_LENGTH + 1))
            
        } else if (textField == self.txtPassword) || (textField == self.txtConfirmPassword) {
            if string == " " {
                return false
            }
        }
        
        return true
    }
}

//MARK:-  Web Service model
extension RegisterViewController {
    
    fileprivate func fillModel() {
        self.registerResponseModel.first_name = getString(anything: txtFullName.text)
        self.registerResponseModel.Middle_Name = getString(anything: txtMiddleName.text)
        self.registerResponseModel.last_name = getString(anything: txtLastName.text)
        self.registerResponseModel.dob = getString(anything: lblDateOfBirth.text)
        self.registerResponseModel.email = getString(anything: txtEmail.text).trimming_WS
        self.registerResponseModel.mobilenumber = getString(anything: txtMobileNo.text).trimming_WS
        self.registerResponseModel.state_id = "24"
        self.registerResponseModel.password = getString(anything: txtPassword.text)
        
//        if self.lblGender.text != CommonLabels.select {
//            self.registerResponseModel.gender = getString(anything: self.selectedGenderType.id)
//        } else {
//            self.registerResponseModel.gender = ""
//        }
    }
    
    fileprivate func getDataFromWebServices() {
        view.endEditing(true)
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.registerResponseModel.getRegister { (responseModel) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if let dictUser = responseModel {
                dictUser.setUserModel()
                
                UserPreferences.set(value: true, forKey: UserPreferencesKeys.General.isUserLoggedIn)
                UserPreferences.set(value: false, forKey: UserPreferencesKeys.General.isSkip)

                UserPreferences.set(value: getString(anything: dictUser.email), forKey: UserPreferencesKeys.General.email)
                UserPreferences.set(value: getString(anything: dictUser.id), forKey: UserPreferencesKeys.UserInfo.userID)
                    UserPreferences.set(value: getString(anything: dictUser.token), forKey: UserPreferencesKeys.General.token)
                UserPreferences.set(value: getString(anything: dictUser.user_roles_id) , forKey: UserPreferencesKeys.General.user_roles_id)
                UserPreferences.set(value: getString(anything: dictUser.first_name), forKey: UserPreferencesKeys.General.first_name)
                UserPreferences.set(value: getString(anything: dictUser.last_name), forKey: UserPreferencesKeys.General.last_name)

                self.txtValues.forEach({
                    $0.text = ""
                })
                
                if self.isPopedUp {
                    self.signUpDoneBlock?()
                } else {
                    Global.shared.changeInitialVC(animated: true, direction: .toRight)
                }
                
            }

        }
    }
    
    fileprivate func getOTPEmail() {
        UIView.hideKeyBoard()
        
        CustomActivityIndicator.sharedInstance.showIndicator(view: view)
        self.registerResponseModel.email = getString(anything: txtEmail.text).trimming_WS
        self.registerResponseModel.getOTPEmailreq { (json) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if json != nil {
                self.invalidateTimerEmail()
                self.btnSendEmailOTP.isUserInteractionEnabled = false
                self.btnSendEmailOTP.superview?.setFullCornerRadius(borderColor: .customGray, borderWidth: 1)
                self.lblGetResendOTPEmail.textColor = .customGray
                self.lblOTPTimeEmail.text = String(format: "%02d:%02d", (self.totalSecondEmail % 3600) / 60, (self.totalSecondEmail % 3600) % 60)
                self.txtEmailOTP.text = ""
                self.timerEmail = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerCalledEmail(_:)), userInfo: nil, repeats: true)
                self.viewOTPEmail.isHidden = false
                
                //self.txtOTP.text = data.otp
                self.lblOTPSentEmail.text = SignUp_EditProfileLabels.otp_has_been_sent_on_your_email //+ " \(self.registerResponseModel.mobilenumber.substring(to: 2))xxxxxx\(self.registerResponseModel.mobilenumber.substring(from: 8))"
            }
        }
    }
    
    fileprivate func verifyOtpEmail() {
        
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.registerResponseModel.email = getString(anything: txtEmail.text).trimming_WS
        self.registerResponseModel.email_otp = getString(anything: txtEmailOTP.text).trimming_WS
        
        self.registerResponseModel.verifyOTEmailPreq { (result) in

            if result {
                self.invalidateTimerEmail()
            }
            
            self.btnSendEmailOTP.superview?.isHidden = result
            self.imgEmailVerified.isHidden = !result
            self.viewOTPEmail.isHidden = result
            
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
        }
    }
    
    fileprivate func getOTPMobile() {
        UIView.hideKeyBoard()
        
        CustomActivityIndicator.sharedInstance.showIndicator(view: view)
        self.registerResponseModel.mobilenumber = getString(anything: txtMobileNo.text).trimming_WS
        self.registerResponseModel.getOTPreq { (json) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if json != nil {
                self.invalidateTimer()
                self.btnSendOTP.isUserInteractionEnabled = false
                self.btnSendOTP.superview?.setFullCornerRadius(borderColor: .customGray, borderWidth: 1)
                                self.lblGetResendOTP.textColor = .customGray
                self.lblOTPTime.text = String(format: "%02d:%02d", (self.totalSecond % 3600) / 60, (self.totalSecond % 3600) % 60)
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerCalled), userInfo: nil, repeats: true)
                self.viewOTP.isHidden = false
                self.txtOTP.text = ""
                self.lblOTPSent.text = SignUp_EditProfileLabels.otp_has_been_sent_on_registered_mobile_number + " \(self.registerResponseModel.mobilenumber.substring(to: 2))xxxxxx\(self.registerResponseModel.mobilenumber.substring(from: 8))"
            }
        }
    }

    fileprivate func verifyOtpMobile() {
        
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.registerResponseModel.mobilenumber = getString(anything: txtMobileNo.text).trimming_WS
        self.registerResponseModel.mobile_otp = getString(anything: txtOTP.text).trimming_WS
        self.registerResponseModel.verifyOTPreq { (result) in
            
            if result {
                self.invalidateTimer()
            }
            
            self.btnSendOTP.superview?.isHidden = result
            self.imgMobileVerified.isHidden = !result
            self.viewOTP.isHidden = result
            
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
        }
    }
    
    fileprivate func getWebserviceforDistrict() {
        arrDistrictOptions.removeAll()
        arrDistrictOptions.append(DropDownModel(dict: [DropDownModel.Keys.title.rawValue: RegistrtionLabels.selectDistrict,
                                                   DropDownModel.Keys.id.rawValue: ""]))
           CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
           registerResponseModel.getDistrictData { (responseModel) in
               CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
               if let response = responseModel {
                   print(response)
                   self.arrDistrictOptions.append(contentsOf: response.list)
               }
           }
       }
    
    fileprivate func getWebserviceCity() {
        arrCityOptions.removeAll()
        arrCityOptions.append(DropDownModel(dict: [DropDownModel.Keys.title.rawValue: RegistrtionLabels.selectCity,
                                                   DropDownModel.Keys.id.rawValue: ""]))
           CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
           registerResponseModel.getCityData { (responseModel) in
               CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
               if let response = responseModel {
                   print(response)
                   self.arrCityOptions.append(contentsOf: response.list)
               }
           }
       }
}

class NWM_TextView: UITextView {
    
    var otherTextClickCallBack: (() -> Void)?
    private var isClickCount = 0
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {

        guard let pos = closestPosition(to: point) else { return false }

        
        guard let range = tokenizer.rangeEnclosingPosition(pos, with: .character, inDirection: .layout(.left)) else { return false }

        let startIndex = offset(from: beginningOfDocument, to: range.start)

        let boolLink =  attributedText.attribute(.link, at: startIndex, effectiveRange: nil) != nil
        print(boolLink)
        
        if !boolLink {
            isClickCount += 1
            if isClickCount > 1 {
                isClickCount = 0
                otherTextClickCallBack?()
            }
        }
        
        return boolLink
    }
}
