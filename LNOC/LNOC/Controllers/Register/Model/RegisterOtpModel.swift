//
//  RegisterOtpModel.swift
//  NWM
//
//  Created by Vikram Jagad on 19/06/21.
//

import UIKit

class RegisterOtpModel: NSObject {

    let resultFlag:Bool
    let message:String
    let otp:String
    
    
    enum Keys:String {
        case otp = "otp"
    }
    
    init(dict:Dictionary,uuid:String) {
        self.resultFlag = getBoolean(anything: dict[CommonAPIConstant.key_resultFlag])
        self.message = getString(anything: dict[CommonAPIConstant.key_message])
        self.otp = getString(anything: dict[Keys.otp.rawValue])
      //  self.otp = EncryptionModel.default.decrypt(str: getString(anything: dict[Keys.otp.rawValue]), key: uuid)
                                                   
    }
    
}
