//
//  LoginModel.swift
//  ASIDEMO
//
//  Created by Dhruv Patel on 13/01/21.
//

import UIKit

class RegisterModel: NSObject {
    
    var userId:String = ""
    var applicantName:String = ""
    var fullName : String = ""
    var email:String = ""
    var mobilenumber : String = ""
    var gender : String = ""
    var dob : String = ""
    var token:String = ""
    var password:String = ""
    var message:String = ""
    
    override init() {
        super.init()
    }
    
    enum keys:String {
        case userid = "userId"
        case applicantName = "applicantName"
        case fullName = "fullName"
        case email = "email"
        case mobilenumber = "mobilenumber"
        case gender = "gender"
        case dob = "dob"
        case token = "token"
        case password = "password"
        case message = "message"
    }
    
    init(dict:[String:Any]) {
        self.userId = getString(anything:dict[keys.userid.rawValue])
        self.applicantName = getString(anything: dict[keys.applicantName.rawValue])
        self.fullName = getString(anything: dict[keys.fullName.rawValue])
        self.email = getString(anything: dict[keys.email.rawValue])
        self.mobilenumber = getString(anything: dict[keys.mobilenumber.rawValue])
        self.gender = getString(anything: dict[keys.gender.rawValue])
        self.dob = getString(anything: dict[keys.dob.rawValue])
        self.token =  getString(anything: dict[keys.token.rawValue])
        self.message = getString(anything: dict[keys.message.rawValue])
    }
}
