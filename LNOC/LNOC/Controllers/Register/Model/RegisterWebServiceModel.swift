//
//  LoginServiceModel.swift
//  ASIDEMO
//
//  Created by Dhruv Patel on 13/01/21.
//

import UIKit

class RegisterWebServiceModel: NSObject {
    
    var token:String = ""
    var password:String = ""
    var first_name : String = ""
    var Middle_Name : String = ""
    var last_name : String = ""
    var mobile_otp : String = ""
    var email_otp : String = ""
    var gender : String = ""
    var dob : String = ""
    var email:String = ""
    var mobilenumber : String = ""
    var state_id: String = ""
    var districts_id: String = ""
    var city_id: String = ""
    //    var district_code : String = ""
    
    
    
    fileprivate var getRegister:String {
        return BASEURL + API.register
    }
    
    fileprivate var getSendOTPUrl : String {
        return BASEURL + API.sendotp
    }

//    fileprivate var mobileOTPVerifyUrl : String {
//        return BASEURL + API.mobileOTPVerify
//    }
    
    fileprivate var OTPVerifyUrl : String {
        return BASEURL + API.verifyotp
    }
    
//
//    fileprivate var emailOTPVerifyUrl : String {
//        return BASEURL + API.verifyotp
//    }
    
    fileprivate var getDistrictUrl : String {
        return BASEURL + API.getdistricts
    }
    
    fileprivate var getCityUrl : String {
        return BASEURL + API.getCity
    }
    
    enum keys : String {
        case token = "token"
        case first_name = "first_name"
        case middle_name = "middle_name"
        case last_name = "last_name"
        case gender = "gender"
        case mobile_otp = "mobile_otp"
        case date_of_birth = "date_of_birth"
        case email = "email"
        case mobilenumber = "mobile"
        case state_id = "state_id"
        case districts_id = "districts_id"
        case city_id = "city_id"
        case password = "password"
        case district_code = "district_code"
        case email_otp = "email_otp"
    }
    
    //MARK:- Initializer
    override init() {
        super.init()
    }
    
    private func registerDetailsDict() -> Dictionary {
        return [keys.token.rawValue: self.token,
                keys.first_name.rawValue:self.first_name,
                keys.middle_name.rawValue:self.Middle_Name,
                keys.last_name.rawValue:self.last_name,
                keys.gender.rawValue: self.gender,
                keys.date_of_birth.rawValue:self.dob,
                keys.email.rawValue: self.email,
                keys.mobilenumber.rawValue: self.mobilenumber,
                keys.state_id.rawValue:self.state_id,
                keys.districts_id.rawValue:self.districts_id,
                keys.city_id.rawValue:self.city_id,
                keys.password.rawValue:self.password]
    }
    
    private func getOtpEmailDict(uuid: String) -> Dictionary {
        return [keys.email.rawValue: self.email]// EncryptionModel.default.encrypt(str: self.mobilenumber, key: uuid)]
    }
    
    fileprivate var verifyOtpDict : [String : Any] {
        return [keys.mobilenumber.rawValue: self.mobilenumber,
                keys.mobile_otp.rawValue: self.mobile_otp]
    }
    
//    private func verifyOtpDictBoth() -> Dictionary {
//        return [keys.mobilenumber.rawValue: self.mobilenumber,
//                keys.mobile_otp.rawValue: self.mobile_otp,
//                keys.email.rawValue: self.email,
//                keys.email_otp.rawValue: self.email_otp]
//    }
    
    private func verifyOtpEmailDict() -> Dictionary {
        return [keys.email.rawValue: self.email,
                keys.email_otp.rawValue: self.email_otp]
    }
    
    //Register
    func getRegister(block:@escaping ((UserModel?) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            let uuid = UUID().uuidString
            var param = WShandler.commonDict()//(uuid: uuid)
            let dict = registerDetailsDict().encryptDic()
            for key in dict.keys {
                param[key] = dict[key]
            }
            
            print(param,"param")
            WShandler.shared.postWebRequest(urlStr: getRegister, param: param) { (json, flag) in
                var responseModel:UserModel?
                if flag == 200 {
                    if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        print(json)
                        if let dict = json[CommonAPIConstant.key_userDetails] as? Dictionary {
                            responseModel = UserModel(dict: dict, uuid: uuid)
                        }
                        
                    } else {
                        Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel)
            }
        } else {
            block(nil)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
    
    //Send/Resend Otp
    func getOTPreq(block:@escaping ((RegisterOtpModel?) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            let uuid = UUID().uuidString
            var param = WShandler.commonDict()//(uuid: uuid)
            param[keys.mobilenumber.rawValue] = self.mobilenumber.encryptStr
            
            WShandler.shared.postWebRequest(urlStr: getSendOTPUrl, param: param) { (json, flag) in
                var responseModel:RegisterOtpModel?
                if flag == 200 {
                    if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                        responseModel = RegisterOtpModel(dict: json, uuid: uuid)
                        
                        //block(true,getString(anything: json[CommonAPIConstant.key_otp]))
                        
                    } else {
                        Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                        
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    
                }
                block(responseModel)
            }
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block(nil)
        }
    }
    
    
    //Verify Otp
    func verifyOTPreq(block:@escaping ((Bool) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            var param = WShandler.commonDict()//(uuid: uuid)
            let dict = verifyOtpDict.encryptDic()
            for key in dict.keys {
                param[key] = dict[key]
            }
            
            WShandler.shared.postWebRequest(urlStr: OTPVerifyUrl, param: param) { (json, flag) in
                
                if flag == 200 {
                    if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                        block(true)
                        
                    } else {
                        Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                        block(false)
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block(false)
                }
            }
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block(false)
        }
    }
    
    
    //Verify Otp
//    func verifyOTPBothreq(block:@escaping ((Bool) -> Swift.Void)) {
//
//        if (WShandler.shared.CheckInternetConnectivity()) {
//
//            var param = WShandler.commonDict()//(uuid: uuid)
//            let dict = verifyOtpDictBoth().encryptDic()
//            for key in dict.keys {
//                param[key] = dict[key]
//            }
//
//            WShandler.shared.postWebRequest(urlStr: OTPVerifyUrl, param: param) { (json, flag) in
//
//                if flag == 200 {
//                    if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
//                        block(true)
//
//                    } else {
//                        Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
//                        block(false)
//                    }
//                } else {
//                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
//                    block(false)
//                }
//            }
//        } else {
//            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
//            block(false)
//        }
//    }
    
    //Send/Resend Otp
    func getOTPEmailreq(block:@escaping ((RegisterOtpModel?) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            let uuid = UUID().uuidString
            var param = WShandler.commonDict()
            param[keys.email.rawValue] = self.email.encryptStr
            
            WShandler.shared.postWebRequest(urlStr: getSendOTPUrl, param: param) { (json, flag) in
                var responseModel:RegisterOtpModel?
                if flag == 200 {
                    if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                        responseModel = RegisterOtpModel(dict: json, uuid: uuid)
                        
                        //block(true,getString(anything: json[CommonAPIConstant.key_otp]))
                        
                    } else {
                        Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                        
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    
                }
                block(responseModel)
            }
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block(nil)
        }
    }
    
    //Verify Otp
    func verifyOTEmailPreq(block:@escaping ((Bool) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            var param = WShandler.commonDict()//(uuid: uuid)
            let dict = verifyOtpEmailDict().encryptDic()
            for key in dict.keys {
                param[key] = dict[key]
            }
            print(param)
            WShandler.shared.postWebRequest(urlStr: OTPVerifyUrl, param: param) { (json, flag) in
                
                if flag == 200 {
                    if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                        block(true)
                        
                    } else {
                        Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                        block(false)
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block(false)
                }
            }
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block(false)
        }
    }
    
    func getDistrictData(block: @escaping ((DropDownResponseModel?) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            let param = WShandler.commonDict()
            WShandler.shared.getWebRequest(urlStr:getDistrictUrl,param: param) { (json, flag) in
                var responseModel:DropDownResponseModel?
                if (flag == 200) {
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        responseModel = DropDownResponseModel(dictDistrict: json)
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel)
            }
        }else{
            block(nil)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
    
    func getCityData(block: @escaping ((DropDownResponseModel?) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            var param = WShandler.commonDict()
            param[keys.district_code.rawValue] = districts_id.encryptStr
            WShandler.shared.getWebRequest(urlStr:getCityUrl,param: param) { (json, flag) in
                var responseModel:DropDownResponseModel?
                if (flag == 200) {
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        responseModel = DropDownResponseModel(dictCity: json)
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel)
            }
        }else{
            block(nil)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
    
}
