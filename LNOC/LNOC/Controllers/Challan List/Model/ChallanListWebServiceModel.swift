//
//  LandListWebServiceModel.swift
//  LNOC
//
//  Created by STTL Mac on 07/01/22.
//

import Foundation

class ChallanListWebServiceModel : NSObject {
    
    //MARK:- Public Methods
    func getChallanListApi(applications_id : String, block:@escaping (([ChallanListModel], _ isSucess : Bool, _ totalRecords : Int, _ txtmessage : String) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {

            var param = WShandler.commonDict()
            param["applications_id"] = applications_id.encryptStr
//            param["users_id"] = UserPreferences.string(forKey: UserPreferencesKeys.UserInfo.userID)
            
            WShandler.shared.postWebRequest(urlStr: BASEURL + API.challan_list, param: param) { (json, flag) in
                
                if (flag == 200) {
                    
                    //Invalid Token
                    if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                        
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                        UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                        Global.shared.changeInitialVC(animated: true, direction: .toLeft)
                        block([], false, 0, "")
                    
                    } else if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        let list = (json["challan_list"] as? [[String : Any]] ?? []).map({ ChallanListModel(dict: $0) })
                        block(list, true, list.count, getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        block([], false, 0, "")
                    }
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block([], false, 0, "")
                }
            }
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block([], false, 0, "")
        }
    }
}
