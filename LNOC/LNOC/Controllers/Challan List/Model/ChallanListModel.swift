//
//  LandDetailsModel.swift
//  LNOC
//
//  Created by STTL Mac on 04/01/22.
//

import Foundation
import SwiftUI

class ChallanListModel : NSObject {
    
    var account_head = ""
    var application_id = ""
    var challan_no = ""
    var challan_date = ""
    var amount = ""
    var bank_name = ""
    var affidavit = ""
    var challan = ""
    var otherDoc = ""
    var citizen_users_id = ""
    var remarks = ""
    var status = ""
    var users_id = ""
    
    enum Keys: String {
        case account_head = "account_head"
        case application_id = "applications_id"
        case challan_no = "challen_number"
        case challan_date = "challen_date"
        case amount = "amount"
        case bank_name = "bank_name"
        case affidavit = "affidavit"
        case challan = "challan"
        case otherDoc = "other_doc"
        case citizen_users_id = "citizen_users_id"
        case remarks = "remarks"
        case status = "status"
        case users_id = "users_id"
    }
    
    init(dict: Dictionary) {
        
        account_head = getString(anything: dict[Keys.account_head.rawValue]).decryptStr
        application_id = getString(anything: dict[Keys.application_id.rawValue]).decryptStr
        challan_no = getString(anything: dict[Keys.challan_no.rawValue]).decryptStr
        challan_date = getString(anything: dict[Keys.challan_date.rawValue]).decryptStr
        amount = getString(anything: dict[Keys.amount.rawValue]).decryptStr
        bank_name = getString(anything: dict[Keys.bank_name.rawValue]).decryptStr
        affidavit = getString(anything: dict[Keys.affidavit.rawValue]).decryptStr
        challan = getString(anything: dict[Keys.challan.rawValue]).decryptStr
        otherDoc = getString(anything: dict[Keys.otherDoc.rawValue]).decryptStr
        citizen_users_id = getString(anything: dict[Keys.citizen_users_id.rawValue]).decryptStr
        remarks = getString(anything: dict[Keys.remarks.rawValue]).decryptStr
        status = getString(anything: dict[Keys.status.rawValue]).decryptStr
        users_id = getString(anything: dict[Keys.users_id.rawValue]).decryptStr
     
        super.init()
    }
}
