//
//  LandListViewController.swift
//  LNOC
//
//  Created by Dhruv on 30/12/21.
//

import UIKit



class ChallanViewController: HeaderViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var tblList: UITableView!
        
    var arrList = [ChallanListModel]()

    fileprivate var webServiceModel = ChallanListWebServiceModel()
    var isPullToRefresh = false
    var refreshControl = UIRefreshControl()
    var totalCount = 0
    var pageNo = 1
    var isDataLoadingOn = false
    var applicationId = ""
    
    //MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupConstraintIfIsIPad()
        self.setupHeaderView()
        self.tblList.register(cellType: ChallanListCell.self)
//        self.getListData()
        self.setUpPullToRefresh()
        self.tblList.contentInset = .init(top: 16, left: 0, bottom: 0, right: 0)
        self.setUpBackgroundView()
    }

    private func setupHeaderView() {
        setUpHeaderTitle(strHeaderTitle: Headers.ChallanList)
        setHeaderView_BackImage()
    }
    
    //MARK: - Private Methods
    private func setupConstraintIfIsIPad() {
        changeLeadingTrailingForiPad(view: self.view)
    }
    
    fileprivate func setUpPullToRefresh() {
        refreshControl = self.tblList.addRefreshControl(target: self, action: #selector(pullToRefreshCalled(_:)))
        refreshControl.attributedTitle = NSAttributedString(string: CommonLabels.pull_to_refresh, attributes: [NSAttributedString.Key.font : UIFont.boldValueFont, NSAttributedString.Key.foregroundColor : UIColor.customSubtitle])
    }
    
    //MARK:- Selector Methods
    @objc fileprivate func pullToRefreshCalled(_ sender: UIRefreshControl) {
        isPullToRefresh = true
        self.pageNo = 1
        totalCount = 0
        self.getListData()
    }
 
    fileprivate func setUpBackgroundView() {
        if (self.arrList.count > 0) {
            self.tblList.backgroundView = nil
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                let frame = CGRect(x: 0, y: 0, width: self.tblList.frame.size.width, height: self.tblList.frame.size.height / 1.35)
                self.tblList.backgroundView = UIView.makeNoRecordFoundView(frame: frame, msg: CommonLabels.no_record_found)
            }
        }
        self.tblList.reloadData()
    }
    
    @objc fileprivate func btnDocumentClicked(_ sender:UIButton) {
//        let dvc = PDFDocumentViewerViewController.instantiate(appStoryboard: .pdfDocumentViewer)
//        dvc.documentURLStr = sender.accessibilityHint ?? ""
//        self.navigationController?.pushViewController(dvc, animated: true)
        openURL(urlString: sender.accessibilityHint ?? "")
    }
}

//MARK: - ------ UITableView DataSource, Delegate Methods -----------
extension ChallanViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(with: ChallanListCell.self, for: indexPath)
        cell.cellConfigure(item: self.arrList[indexPath.row])
        
        cell.btnAffidavit.accessibilityHint = self.arrList[indexPath.row].affidavit
        cell.btnAffidavit.addTarget(self, action: #selector(btnDocumentClicked), for: .touchUpInside)
        cell.btnChallan.accessibilityHint = self.arrList[indexPath.row].challan
        cell.btnChallan.addTarget(self, action: #selector(btnDocumentClicked), for: .touchUpInside)
        cell.btnOtherDoc.accessibilityHint = self.arrList[indexPath.row].otherDoc
        cell.btnOtherDoc.addTarget(self, action: #selector(btnDocumentClicked), for: .touchUpInside)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
//        if self.isDataLoadingOn == false && (indexPath.row == arrList.count - 1) && (arrList.count < totalCount) {
//            self.pageNo += 1
//            self.getListData()
//        }
    }
}

//MARK:- Webservice Functions
extension ChallanViewController {
    func getListData() {

        if !(isPullToRefresh) {
            CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        }

        webServiceModel.getChallanListApi(applications_id: self.applicationId) { list, isSucess, totalRecords, txtmessage  in

            if (self.isPullToRefresh) {
                self.isPullToRefresh = false
                self.refreshControl.endRefreshing()
            } else {
                CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            }

            self.isDataLoadingOn = false

            if isSucess {
//            self.totalCount = totalRecords
//            if (self.pageNo == 1) {
                self.arrList = list
//            } else {
//                self.arrList += list
//            }
                self.setUpBackgroundView()
            }
        }
    }
}
 
