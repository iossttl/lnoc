//
//  CellLandList.swift
//  RajyaSabha
//
//  Created by Mobile Mac on 21/10/21.
//

import UIKit

class ChallanListCell : UITableViewCell {
   //MARK: - Interface builder
    
    //UIView
    @IBOutlet weak var vwbg: UIView!
    @IBOutlet weak var vwSeparator: UIView!
    
    //UILabel
    @IBOutlet weak var lblAppId: UILabel!
    @IBOutlet weak var lblChallanNo: UILabel!
    @IBOutlet weak var lblChallanDate: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblBankName: UILabel!
    @IBOutlet weak var lblAccHead: UILabel!
    
    @IBOutlet weak var lblAffidavit: LocalizedLabel!
    @IBOutlet weak var lblChallan: LocalizedLabel!
    @IBOutlet weak var lblOtherDoc: LocalizedLabel!
    
    @IBOutlet weak var btnAffidavit: UIButton!
    @IBOutlet weak var btnChallan: UIButton!
    @IBOutlet weak var btnOtherDoc: UIButton!
    
    @IBOutlet var lblTitles: [LocalizedLabel]!
    @IBOutlet var lblValues: [UILabel]!
    @IBOutlet var lblAttachments: [UILabel]!
    @IBOutlet var viewBorder: [UIView]!
    @IBOutlet var imgAttachment: [UIImageView]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupvw()
        setuplbl()
    }
    
    func setupvw(){
        self.vwSeparator.backgroundColor = .orange
        selectionStyle = .none
        vwbg.setCustomCornerRadius(radius: 8.0)
    }
    
    func setuplbl() {
        
        self.lblTitles.forEach({
            $0.font = IS_IPAD ? .semiboldMediumFont : .semiboldSmallTitleFont
            $0.textColor = .customDarkBlueGradient
        })
        
        self.lblValues.forEach({
            $0.font = IS_IPAD ? .regularLargeFont : .regularTitleFont
            $0.textColor = .black
        })
        
        self.lblAttachments.forEach({
            $0.font = IS_IPAD ? .regularLargeFont : .regularSmallFont
            $0.textColor = .black
        })
        
        self.lblAppId.font = IS_IPAD ? .regularLargeFont : .regularTitleFont
        self.lblAppId.textColor = .orange
    }
    
    func cellConfigure(item: ChallanListModel) {
        self.lblAppId.text = "#" + (item.application_id.isEmptyString ? "-" : item.application_id)
        self.lblChallanNo.text = item.challan_no.isEmptyString ? "-" : item.challan_no
        self.lblChallanDate.text = item.challan_date.isEmptyString ? "-" : item.challan_date
        self.lblAmount.text = item.amount.isEmptyString ? "-" : item.amount
        self.lblBankName.text = item.bank_name.isEmptyString ? "-" : item.bank_name
        self.lblAccHead.text = item.account_head.isEmptyString ? "-" : item.account_head
        
        self.lblAffidavit.superview?.isHidden = item.affidavit.isStringEmpty
        self.lblChallan.superview?.isHidden = item.challan.isStringEmpty
        self.lblOtherDoc.superview?.isHidden = item.otherDoc.isStringEmpty
        
        self.viewBorder.forEach { vw in
            vw.setCustomCornerRadius(radius: 8.0, borderColor: .gray, borderWidth: 1.0)
        }
        
        self.imgAttachment.forEach { img in
            img.image = .ic_attach
            img.tintColor = .themeDarkBlueClr
        }
    }
}
