//
//  FeedbackViewController.swift
//  LNOC
//
//  Created by Gunjan on 10/01/22.
//

import UIKit

class FeedbackViewController: HeaderViewController {
    
    //View
    @IBOutlet weak var viewBtnSubmitGradient: GradientView!
    @IBOutlet weak var viewForScroll: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewForShadow: UIView!
    
    //UIScrollView
    @IBOutlet weak var scrollView: UIScrollView!
    
    //UITextField
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtMobile: UITextField!
    @IBOutlet weak var tvDesc: UITextView!
    
    //UIButton
    @IBOutlet weak var btnSubmit: UIButton!
    
    //UILabel
    @IBOutlet weak var lblCounter: UILabel!
    
    //Collections
    @IBOutlet var lblTitles: [LocalizedLabel]!
    @IBOutlet var txtValues: [UITextField]!
    @IBOutlet var lblAsterisks: [LocalizedLabel]!
    @IBOutlet var lblSeprator: [UILabel]!
    
    fileprivate var feedbackWebServicesModel = FeedbackWebServicesModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
            setUpUI()
        // Do any additional setup after loading the view.
    }
    
    fileprivate func setUpUI() {
        setUpIPadConstraints()
        setUpHeader()
        setUpView()
        setUpBtn()
        fontSetUp()
    }
    
    fileprivate func setUpIPadConstraints() {
        changeLeadingTrailingForiPad(view: viewMain.superview ?? UIView())
    
    }
    
    fileprivate func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.Feedback)
        setHeaderView_BackImage()
    }
    
    fileprivate func setUpView() {
        self.viewMain.setCustomCornerRadius(radius: 12)
        self.viewForShadow.setCustomCornerRadius(radius: 12)
        self.viewForShadow.addShadow()
        
        self.viewBtnSubmitGradient.addGradient(ofColors: [UIColor.themeDarkOrangeClr.cgColor, UIColor.themeLightOrangeClr.cgColor], direction: .rightLeft)
        
        self.viewBtnSubmitGradient.setFullCornerRadius()
    }
    
    fileprivate func setUpBtn() {
        self.btnSubmit.titleLabel?.font = UIFont.semiboldLargeFont
    }
    
    fileprivate func fontSetUp() {
        self.lblTitles.forEach({
            $0.font = .regularTitleFont
            $0.textColor = .customBlackTitle
        })
        
        for lbl in lblAsterisks{
            lbl.textColor = .customRed
            lbl.font = .regularTitleFont
            lbl.text = "*"
        }
        
        self.txtValues.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
            $0.delegate = self
        })
        
        self.tvDesc.font = .regularTitleFont
        self.tvDesc.textColor = .customBlack
        self.lblCounter.font = .regularSmallFont
        self.tvDesc.delegate = self
        lblCounter.text =  "\(k_address_Length) " + CommonLabels.character
    }
        
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        if isValid() {
            feedbackWebServicesModel.name = getString(anything: txtName.text)
            feedbackWebServicesModel.email = getString(anything: txtEmail.text)
            feedbackWebServicesModel.mobile = getString(anything: txtMobile.text)
            feedbackWebServicesModel.desc = getString(anything: tvDesc.text)
            self.getWebServicesforSubmitFeedback()
        }
    }
}

extension FeedbackViewController {
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let fullString = NSString(string: textField.text ?? "").replacingCharacters(in: range, with: string)
        
        if (textField == txtMobile) {
            let numberSet = NSCharacterSet(charactersIn: NUMERIC).inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if (string == numberFiltered) && (fullString.count < (MOBILE_LENGTH_VALIDATION + 1)) {
                if (fullString.hasPrefix("6") || fullString.hasPrefix("7") || fullString.hasPrefix("8") || fullString.hasPrefix("9")) || fullString.isEmpty {
                    return true
                }else{
                    return false
                }
            } else {
                return false
            }
            
        }
        
        return true
    }
}

//MARK:- Validations
extension FeedbackViewController {
    func isValid() -> Bool {
        var isValid : Bool = false
        if getString(anything: self.txtName.text).isStringEmpty {
            Global.shared.showBanner(message: FeedbackMessages.msg_enter_name)
            self.txtName.becomeFirstResponder()
            
        } else if getString(anything: self.txtEmail.text).isStringEmpty {
            Global.shared.showBanner(message: FeedbackMessages.msg_enter_email)
            self.txtEmail.becomeFirstResponder()
        
        } else if !getString(anything: self.txtEmail.text).isValidEmail {
            Global.shared.showBanner(message: FeedbackMessages.msg_please_enter_valid_email)
            self.txtEmail.becomeFirstResponder()
            
        } else if isValidMobileNumber() == false {
            
        } else if getString(anything: self.tvDesc.text).isStringEmpty {
            Global.shared.showBanner(message: FeedbackMessages.msg_enter_desc)
            self.tvDesc.becomeFirstResponder()
        
        } else {
            isValid = true
        }
        
        return isValid
    }
    
    func isValidMobileNumber() -> Bool {
        if getString(anything: self.txtMobile.text).isStringEmpty {
            Global.shared.showBanner(message: FeedbackMessages.mobileNumberIsRequired)
            self.txtMobile.becomeFirstResponder()
            return false
            
        } else if (getString(anything: self.txtMobile.text)).count < 10 {
            Global.shared.showBanner(message: FeedbackMessages.mobileNumberIsNotValidMinimum10NumberRequired)
            self.txtMobile.becomeFirstResponder()
            return false
            
        } else {
            return true
        }
    }
}

//MARK:- UITextViewDelegate Methods
extension FeedbackViewController : UITextViewDelegate {
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if textView == tvDesc {
            let fullString = NSString(string: getString(anything: textView.text)).replacingCharacters(in: range, with: text)
            let count = k_address_Length - fullString.count
            if fullString.count < (k_address_Length + 1) {
                
                lblCounter.text =  "\(count) " + CommonLabels.character
                return true
            }else{
                return false
            }
        }
        return true
    }
}


//Web Services Call
extension FeedbackViewController {
    
    fileprivate func getWebServicesforSubmitFeedback() {
        
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        feedbackWebServicesModel.getFeedbackResponse { json in
            defer {
                CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            }
            Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
            self.navigationController?.popViewController(animated: true)
        }
    }
}
