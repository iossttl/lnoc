//
//  PlayerListWebServiceModel.swift
//  Khel Mahakumbh
//
//  Created by Dhruv Patel on 27/12/21.
//
import UIKit

class FeedbackWebServicesModel : NSObject {
    
    //MARK:- Variables
    //Public
    var name = ""
    var email = ""
    var mobile = ""
    var desc = ""

    //MARK:- Enum
    enum Keys: String {
        case name = "name"
        case email = "email"
        case mobile = "mobile"
        case desc = "description"
    }
    
    fileprivate var getFeedbackUrl:String {
        BASEURL + API.feedback
    }
    
    private func feedbackDict() -> Dictionary {
        var dict : [String:Any] = [Keys.name.rawValue:getString(anything: self.name),
                                   Keys.email.rawValue:getString(anything: self.email),
                                   Keys.mobile.rawValue:getString(anything: self.mobile),
                                   Keys.desc.rawValue:getString(anything: self.desc)].encryptDic()
        let commonDict = WShandler.commonDict()
        for key in commonDict.keys.sorted() {
            dict[key] = commonDict[key] as! String
        }
        return dict
    }
    

    
    func getFeedbackResponse(block: @escaping ((Dictionary) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            let param = self.feedbackDict()
            print(param,"param")
            WShandler.shared.postWebRequest(urlStr: self.getFeedbackUrl, param: param) { (json, flag) in
                var responseModel = Dictionary()
                if (flag == 200) {
                        if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                            if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == 2 {
                                UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                                UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                                UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                                UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                                UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                                UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                                Global.shared.changeInitialVC(animated: true, direction: .toLeft)
                                Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                            } else {
                                responseModel = json
                            }
                            
                        } else {
                            Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        }
                    
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel)
            }
        }  else {
            block([:])
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
}



