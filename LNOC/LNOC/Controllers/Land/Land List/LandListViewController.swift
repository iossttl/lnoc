//
//  LandListViewController.swift
//  LNOC
//
//  Created by Dhruv on 30/12/21.
//

import UIKit

enum LandListType {
    case landFavourite
    case landSearch
    case landBank
    case linkLand
}

class LandListViewController: HeaderViewController {
    
    //MARK: - Outlet
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var viewSearch: UIView!
    @IBOutlet weak var txtCSNumberSearch: UITextField!
    @IBOutlet weak var tblList: UITableView!
    @IBOutlet weak var lblSearchTitle: LocalizedLabel!
    @IBOutlet weak var imgSearch: UIImageView!
    @IBOutlet weak var btnAddLinkLand: UIButton!
    @IBOutlet weak var btnSearch: UIButton!
    @IBOutlet weak var viewSearchTopConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var viewMainTaxLimit: UIView!
    @IBOutlet weak var viewTaxLimit: UIView!
    @IBOutlet weak var lblTaxLimitTitle: UILabel!
    @IBOutlet weak var lblTaxLimitDescription: UILabel!
    @IBOutlet weak var btnOk: UIButton!
    
    // variables
    var landListType : LandListType = .landFavourite
    var arrList = [LandListModel]()
    
    var filter_crr = ""
    var filter_cs = ""
    var filter_division = ""
    var filter_tenure = ""
    var filter_holder = ""
    var filter_locationOfProperty = ""
    var filter_area = ""
    var filter_division_id = ""
    var filter_tenure_id = ""
    
    ///value default is 0 if only LandListType type is linkLand value get
    fileprivate(set) var viewSearchTotalHeight: CGFloat = 0
    fileprivate(set) var lastContentOffset: CGFloat = 0
    
    fileprivate var webServiceModel = LandListWebServiceModel()
    var isPullToRefresh = false
    var refreshControl = UIRefreshControl()
    var totalCount = 0
    var pageNo = 1
    var isDataLoadingOn = false

    
    //MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupConstraintIfIsIPad()
        self.setupHeaderView()
        self.setupUpView()
        self.getListData()
        self.setUpPullToRefresh()
        addObserver()
                
        if landListType == .linkLand {
            self.view.layoutIfNeeded()
            DispatchQueue.main.async {
                self.viewSearchTotalHeight = self.viewSearch.superview!.frame.height
                self.viewSearch.superview?.setHeightWidth(setHeight: self.viewSearchTotalHeight, setWidth: nil)
            }
        }
    }
    
    private func addObserver() {
        K_NC.addObserver(self, selector: #selector(UpdateList(_:)), name: .addLand, object: nil)
    }
    
    private func removeObserver() {
        K_NC.removeObserver(self, name: .addLand, object: nil)
    }
    
    
    @objc private func UpdateList(_ noti: Notification) {
        self.pageNo = 1
        totalCount = 0
        self.getListData()    }
    
    //MARK: - Private Methods
    private func setupConstraintIfIsIPad() {
        changeLeadingTrailingForiPad(view: self.view)
        changeHeightForiPad(view: self.btnAddLinkLand, constant: 16)
        changeHeightForiPad(view: self.txtCSNumberSearch)
    }
    
    private func setupHeaderView() {
        self.setHeaderView_BackImage()
        switch self.landListType {
        case .landFavourite:
            self.setUpHeaderTitle(strHeaderTitle: Headers.favouriteLands)
        case .landSearch:
            self.setUpHeaderTitle(strHeaderTitle: Headers.searchLands)
            self.showFilter()
        case .landBank:
            self.setUpHeaderTitle(strHeaderTitle: Headers.landBank)
            self.showFilter()
        case .linkLand:
            self.setUpHeaderTitle(strHeaderTitle: Headers.linkLand)
            self.addBtn()
        }
    }
    
    fileprivate func setUpPullToRefresh() {
        refreshControl = self.tblList.addRefreshControl(target: self, action: #selector(pullToRefreshCalled(_:)))
        refreshControl.attributedTitle = NSAttributedString(string: CommonLabels.pull_to_refresh, attributes: [NSAttributedString.Key.font : UIFont.boldValueFont, NSAttributedString.Key.foregroundColor : UIColor.customSubtitle])
    }
    
    //MARK:- Selector Methods
    @objc fileprivate func pullToRefreshCalled(_ sender: UIRefreshControl) {
        isPullToRefresh = true
        self.pageNo = 1
        totalCount = 0
        self.getListData()
    }
    
    func addBtn() {
        let btnAddLand = UIButton()
        if IS_IPAD {
            btnAddLand.frame = CGRect(x: viewHeader.frame.size.width - 155, y: 10, width: 120, height: 32)
            btnAddLand.layer.cornerRadius = 15
        } else {
            btnAddLand.frame = CGRect(x: viewHeader.frame.size.width - 100, y: 10, width: 75, height: 24)
            btnAddLand.layer.cornerRadius = 12
        }
        btnAddLand.setTitle(Headers.addLand, for: .normal)
        btnAddLand.setTitleColor(.customWhite, for: .normal)
        btnAddLand.backgroundColor = .customRedBtnClr
        btnAddLand.titleLabel?.font = .mediumSmallFont
        btnAddLand.clipsToBounds = true
        viewHeader.addSubview(btnAddLand)
        btnAddLand.addTarget(self, action: #selector(self.btnAddLandTapped(_:)), for: .touchUpInside)
    }
    
    //MARK: - Navigate post comment
    @objc func btnAddLandTapped(_ sender: UIButton) {
        let dvc = AddLandDetailViewController.instantiate(appStoryboard: .addLandDetail)
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    private func setupUpView() {
        
        self.viewMainTaxLimit.backgroundColor = .black.withAlphaComponent(0.6)
        self.viewTaxLimit.layer.cornerRadius = 4
        self.viewTaxLimit.addShadow()
        self.lblTaxLimitTitle.font = .semiboldExtraLargeFont
        self.lblTaxLimitDescription.font = .regularMediumFont
        self.btnOk.titleLabel?.font = .mediumValueFont
        
        let mutableString = NSMutableAttributedString()
        mutableString.append(NSAttributedString(string: "+ ", attributes: [.font : UIFont.boldLargePlusFont, .foregroundColor: UIColor.customWhite]))
        mutableString.append(NSAttributedString(string: CommonButtons.btn_AddLinkLand, attributes: [.font : UIFont.boldValueFont, .foregroundColor: UIColor.customWhite]))
        
        self.btnAddLinkLand.setAttributedTitle(mutableString, for: .normal)
        self.btnAddLinkLand.backgroundColor = .customgreenTheamClr
        self.btnAddLinkLand.setFullCornerRadius()
        
        self.lblSearchTitle.font = .regularTitleFont
        self.lblSearchTitle.textColor = .customBlue
        
        self.txtCSNumberSearch.textColor = .black
        self.txtCSNumberSearch.font = UIFont.regularValueFont
        self.txtCSNumberSearch.setPlaceholder(FilterPlaceHolders.enter_CS_Number, color: .lightGray, setFont: UIFont.regularValueFont)
        self.txtCSNumberSearch.delegate = self
        
        self.imgSearch.image = .ic_header_search
        self.imgSearch.tintColor = .darkGray
        
        self.viewSearch.setCustomCornerRadius(radius: 12)
        
        self.tblList.tableFooterView = UIView()
        self.tblList.tableHeaderView = UIView()
        self.tblList.register(cellType: CellLandList.self)
        self.tblList.delegate = self
        self.tblList.dataSource = self
        
        switch self.landListType {
        case .landBank, .linkLand :
            self.tblList.contentInset = .init(top: 0, left: 0, bottom: 0, right: 0)
            self.btnAddLinkLand.superview?.isHidden = self.landListType != .landBank
            self.viewSearch.superview?.superview?.isHidden = self.landListType != .linkLand
            
        case .landSearch, .landFavourite:
            self.tblList.contentInset = .init(top: 16, left: 0, bottom: 0, right: 0)
            self.btnAddLinkLand.superview?.isHidden = true
            self.viewSearch.superview?.superview?.isHidden = true
        }
    }
    
    fileprivate func setUpBackgroundView() {
        if (self.arrList.count > 0) {
            self.tblList.backgroundView = nil
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                let frame = CGRect(x: 0, y: 0, width: self.tblList.frame.size.width, height: self.tblList.frame.size.height / 1.35)
                self.tblList.backgroundView = UIView.makeNoRecordFoundView(frame: frame, msg: CommonLabels.no_record_found)
            }
        }
        self.tblList.reloadData()
    }
    
    //MARK: - Button Action Methods
    @IBAction func btnAddLinkLandClicked(_ sender: UIButton) {
        let dvc = LandListViewController.instantiate(appStoryboard: .LandList)
        dvc.landListType = .linkLand
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    @IBAction func btnSearchClicked(_ sender: Any) {
        self.filterSearchData()
    }
    
    @IBAction func TapOnTaxLimitOKBtn(_ sender: UIButton) {
        self.viewMainTaxLimit.isHidden = true
    }
    
    func filterSearchData() {
        let searchString = (self.txtCSNumberSearch.text ?? "").trimming_WS
        self.filter_cs = searchString
        self.pageNo = 1
        totalCount = 0
        self.getListData()

//        if !searchString.isEmptyString {
//            self.arrList = self.arrListAll.filter({ $0.csnumber.lowercased().contains(searchString.lowercased()) })
//        } else {
//            self.arrList = self.arrListAll
//        }
//
//        self.setUpBackgroundView()
        
       
    }
    
    override func btnSyncTapped(_ sender: UIButton) {
        self.viewMainTaxLimit.isHidden = true
        let vc = FilterViewController.instantiate(appStoryboard: .Filter)
        vc.landListType = self.landListType
        vc.delegate = self
        vc.filter_crr = self.filter_crr
        vc.filter_cs = self.filter_cs
        vc.filter_division = self.filter_division
        vc.filter_tenure = self.filter_tenure
        vc.filter_holder = self.filter_holder
        vc.filter_locationOfProperty = self.filter_locationOfProperty
        vc.filter_area = self.filter_area
        vc.filter_division_id = self.filter_division_id
        vc.filter_tenure_id = self.filter_tenure_id
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc fileprivate func btnWarrantyListClicked(_ sender:UIButton) {
        if self.landListType == .landBank {
            let model = self.arrList[sender.tag]
            let dvc = NoticeWarrantListViewController.instantiate(appStoryboard: .noticeWarrant)
            dvc.land_id = model.id
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    @objc fileprivate func btnPayNowClicked(_ sender:UIButton) {
        var strUrl = ""
        let lanCode = UserPreferences.string(forKey: UserPreferencesKeys.General.languageCode)
        
        if self.arrList[sender.tag].payment_done {
            strUrl = "http://lris.php-staging.com/landpayment/\(self.arrList[sender.tag].land_id)?type=receipt&&mobile=yes"
            
        } else {
            strUrl = "http://lris.php-staging.com/landpayment/\(self.arrList[sender.tag].land_id)?mobile=yes"
        }
        
        let dvc = WebViewViewController.instantiate(appStoryboard: .webView)
        dvc.urlStr = strUrl + "&language=" + lanCode
        dvc.customTitle = "Land Payment Receipt"
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    @objc fileprivate func btnPlusClicked(_ sender: UIButton) {
        if self.landListType == .linkLand { //for add link land
            CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
            self.webServiceModel.landListType = self.landListType
            self.webServiceModel.land_id = self.arrList[sender.tag].id
            self.webServiceModel.deleteRecordAndLinkLandApi { isSucess in
                CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
                if isSucess {
                    self.arrList[sender.tag].is_linked = true
                    sender.isHidden = true
                    self.view.makeToast(LandListMessages.msg_Land_linked_successfully)
                }
            }
            
        } else if self.landListType == .landBank { //for delete
            UIAlertController.showWith(title: LandListMessages.msg_delete, msg: "", style: .alert, onVC: self, actionTitles: [CommonButtons.no,CommonButtons.yes], actionStyles: [.default,.destructive], actionHandlers: [nil, { (yesAction) in

                CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
                self.webServiceModel.landListType = self.landListType
                self.webServiceModel.land_id = self.arrList[sender.tag].id
                self.webServiceModel.deleteRecordAndLinkLandApi { isSucess in
                    CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)

                    if isSucess {
                        self.arrList.remove(at: sender.tag)
                        self.totalCount -= 1
                        self.setUpBackgroundView()
                        self.view.makeToast(LandListMessages.msg_deleted)
                    }
                }
                
            }])
        }
        print(#function)
    }
}

//MARK: - ------ UITextFieldDelegate Delegate Methods -----------
extension LandListViewController {
    override func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        btnSearchClicked(0)
        return true
    }
}

//MARK: - ------ Filter Screen Delegate Methods -----------
extension LandListViewController : ProtocolForLandFilter {
    
    func implimant(crr: String, cs: String, division: String, landTenure: String, landHolder: String, locationOfProperty: String, area: String, division_id: String, landTenure_id: String) {
        
        self.filter_crr = crr
        self.filter_cs = cs
        self.filter_division = division
        self.filter_tenure = landTenure
        self.filter_holder = landHolder
        self.filter_locationOfProperty = locationOfProperty
        self.filter_area = area
        self.filter_division_id = division_id
        self.filter_tenure_id = landTenure_id
        self.pageNo = 1
        totalCount = 0
        self.getListData()
    }
}

//MARK: - ------ UITableView DataSource, Delegate Methods -----------
extension LandListViewController : UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(with: CellLandList.self, for: indexPath)
        
        cell.cellConfigure(item: self.arrList[indexPath.row], landListType: self.landListType)
        cell.btnPlus.tag = indexPath.row
        cell.btnPlus.addTarget(self, action: #selector(btnPlusClicked), for: .touchUpInside)
        cell.btnWarrantyList.tag = indexPath.row
        cell.btnWarrantyList.addTarget(self, action: #selector(btnWarrantyListClicked(_:)), for: .touchUpInside)
        cell.btnPayNow.tag = indexPath.row
        cell.btnPayNow.addTarget(self, action: #selector(btnPayNowClicked(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let dvc = LandDetailsViewController.instantiate(appStoryboard: .LandDetails)
        let dvc = LandDetailsViewControllerMultiple.instantiate(appStoryboard: .LandDetailsMultiple)
        dvc.land_id = self.arrList[indexPath.row].id
        dvc.total_area_sum = self.arrList[indexPath.row].total_area_sum
        dvc.landListType = self.landListType
        dvc.delegate = self
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if self.isDataLoadingOn == false && (indexPath.row == arrList.count - 1) && (arrList.count < totalCount) {
            self.pageNo += 1
            self.getListData()
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == self.tblList {
            if self.landListType == .linkLand {
                self.txtCSNumberSearch.resignFirstResponder()
                
                let bottomOffset = scrollView.contentSize.height - scrollView.bounds.height
                
                guard scrollView.contentOffset.y < bottomOffset else {
                    return
                }
                
                guard scrollView.contentOffset.y > 0 else {
                    self.animateHeader(constraintConstant: 0)
                    return
                }
                
                let offsetDiff = scrollView.contentOffset.y - lastContentOffset
                
                let unsafeNewConstant = self.viewSearchTopConstraint.constant + (offsetDiff > 0 ? -abs(offsetDiff) : abs(offsetDiff))
                let minConstant:CGFloat = -self.viewSearchTotalHeight
                let maxConstant:CGFloat = 0
                
                self.animateHeader(constraintConstant: max(minConstant, min(maxConstant, unsafeNewConstant)))
                
                lastContentOffset = scrollView.contentOffset.y
            }
        }
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        if scrollView == self.tblList {
            if self.landListType == .linkLand {
                if self.viewSearchTopConstraint.constant < 0 || self.viewSearchTopConstraint.constant == -self.viewSearchTotalHeight {
                    if -self.viewSearchTopConstraint.constant < (self.viewSearchTotalHeight / 2) {
                        self.animateHeader(constraintConstant: 0)
                    } else {
                        self.animateHeader(constraintConstant: -self.viewSearchTotalHeight)
                    }
                }
            }
        }
    }
    
    private func animateHeader(constraintConstant: CGFloat) {
        self.viewSearchTopConstraint.constant = constraintConstant
        UIView.animate(withDuration: 0.2, animations: {
            //Manipulate UI elements within the header here
            self.view.layoutIfNeeded()
        })
    }
}

extension LandListViewController : ProtocolForFavouriteDetails {
    func implimant(id: String) {
        if self.landListType == .landFavourite, let index = self.arrList.firstIndex(where: { $0.id == id }) {
            self.arrList.remove(at: index)
            self.totalCount -= 1
            self.setUpBackgroundView()
        }
    }
}

//MARK:- Webservice Functions
extension LandListViewController {
    func getListData() {
        
        self.webServiceModel.landListType = self.landListType
        self.isDataLoadingOn = true
        self.webServiceModel.divison_id = self.filter_division_id
        self.webServiceModel.land_holder = self.filter_holder
        self.webServiceModel.crr_number = self.filter_crr
        self.webServiceModel.cs_number = self.filter_cs
        self.webServiceModel.location_of_property = self.filter_locationOfProperty
        self.webServiceModel.page = self.pageNo
        self.webServiceModel.land_tenure = self.filter_tenure_id
        self.webServiceModel.area = self.filter_area

        if !(isPullToRefresh) {
            CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        }

        webServiceModel.getLandListApi { list, isSucess, totalRecords, txtmessage  in

            if (self.isPullToRefresh) {
                self.isPullToRefresh = false
                self.refreshControl.endRefreshing()
            } else {
                CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            }

            if isSucess {
            self.totalCount = totalRecords
            if (self.pageNo == 1) {
                self.arrList = list
                if self.landListType == .landBank && !txtmessage.isEmptyString {
                    self.lblTaxLimitDescription.text = txtmessage
                    self.viewMainTaxLimit.isHidden = false
                }
            } else {
                self.arrList += list
            }
                
//                if self.landListType == .linkLand {
//                    self.filterSearchData()
//                } else {
                    self.setUpBackgroundView()
//                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2, execute: {
                self.isDataLoadingOn = false
            })
        }
    }
}
 
