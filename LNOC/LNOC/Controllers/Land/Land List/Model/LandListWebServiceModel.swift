//
//  LandListWebServiceModel.swift
//  LNOC
//
//  Created by STTL Mac on 07/01/22.
//

import Foundation

class LandListWebServiceModel : NSObject {

    var divison_id = ""
    var land_holder = ""
    var crr_number = ""
    var cs_number = ""
    var location_of_property = ""
    var page = 1
    var land_tenure = ""
    var favorite = ""
    var user_id = ""
    var area = ""
    var landListType : LandListType = .landSearch
    var land_id = ""
    
    //MARK:- URL
    fileprivate var LandListURL:String {
        BASEURL + API.getLandList
    }
    
    fileprivate var LinkLandURL:String {
        BASEURL + API.linkland
    }
    
    //MARK:- URL
    fileprivate var LandDeleteURL:String {
        BASEURL + API.deleteland
    }
    
    //MARK:- Enum
    enum Keys : String {
        case divison_id = "divison_id"
        case land_holder = "land_holder"
        case crr_number = "crr_number"
        case cs_number = "cs_number"
        case location_of_property = "location_of_property"
        case landList = "landList"
        case page = "page"
        case ms_tenure_of_properties_id = "ms_tenure_of_properties_id"
        case favorite = "favorite"
        case user_id = "user_id"
        case mylist = "mylist"
        case area = "area"
        case land_id = "land_id"
        case txtmessage = "txtmessage"
    }
    
    fileprivate var landListDictParam : [String : Any] {
        var dict: Dictionary = [
            Keys.divison_id.rawValue : self.divison_id.encryptStr,
            Keys.crr_number.rawValue : self.crr_number.encryptStr,
            Keys.cs_number.rawValue : self.cs_number.encryptStr,
            Keys.page.rawValue : self.page,
            Keys.ms_tenure_of_properties_id.rawValue : self.land_tenure.encryptStr,
            Keys.location_of_property.rawValue : self.location_of_property.encryptStr,
            Keys.area.rawValue : self.area.encryptStr,
            Keys.land_holder.rawValue: self.land_holder.encryptStr]
        
        dict[Keys.user_id.rawValue] = UserPreferences.string(forKey: UserPreferencesKeys.UserInfo.userID)

        if self.landListType == .landFavourite {
            dict[Keys.favorite.rawValue] = "1".encryptStr
        } else if self.landListType == .landBank {
            dict[Keys.mylist.rawValue] = "Y".encryptStr
        }
        
        return dict
    }
    
    //MARK:- Public Methods
    func getLandListApi(block:@escaping (([LandListModel], _ isSucess : Bool, _ totalRecords : Int, _ txtmessage : String) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {

            var param = WShandler.commonDict()
            for key in landListDictParam.keys {
                param[key] = landListDictParam[key]
            }
            
            WShandler.shared.postWebRequest(urlStr: LandListURL, param: param) { (json, flag) in
                
                if (flag == 200) {
                    
                    //Invalid Token
                    if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                        
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                        UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                        Global.shared.changeInitialVC(animated: true, direction: .toLeft)
                        block([], false, 0, "")
                        
                    } else if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        let responseModel = (json[Keys.landList.rawValue] as? [Dictionary] ?? []).map({ LandListModel(dict: $0) })
                        let totallist = getInteger(anything: getString(anything: json[CommonAPIConstant.key_totalRecords]).decryptStr)
                        let txtmessage = getString(anything: json[Keys.txtmessage.rawValue]).decryptStr
                        block(responseModel, true, totallist, txtmessage)
                        
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        block([], false, 0, "")
                    }
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block([], false, 0, "")
                }
            }
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block([], false, 0, "")
        }
    }
    
    //MARK:- Public Methods
    func deleteRecordAndLinkLandApi(block:@escaping ((_ isSucess : Bool) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {

            var param = WShandler.commonDict()
            param[Keys.land_id.rawValue] = self.land_id.encryptStr
            param[Keys.user_id.rawValue] = UserPreferences.string(forKey: UserPreferencesKeys.UserInfo.userID)

            var strurl = ""
            if self.landListType == .landBank {
                strurl = LandDeleteURL
            } else if self.landListType == .linkLand {
                strurl = LinkLandURL
            }
            
            WShandler.shared.postWebRequest(urlStr: strurl, param: param) { (json, flag) in
                
                if (flag == 200) {
                    
                    //Invalid Token
                    if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                        
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                        UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                        Global.shared.changeInitialVC(animated: true, direction: .toLeft)
                        block(false)
                        
                    } else if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        block(true)

                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        block(false)
                    }
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block(false)
                }
            }
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block(false)
        }
    }
}
