//
//  LandDetailsModel.swift
//  LNOC
//
//  Created by STTL Mac on 04/01/22.
//

import Foundation
import SwiftUI

class LandListModel : NSObject {
    
    let id: String
    let crrnumber: String
    let csnumber: String
    let division : String
    let landtenure : String
    let landholder: String
    let locaionOfproperty : String
    let amount : String
    let area: String
    let address_of_owner : String
    var is_linked : Bool
    var payment_done : Bool
    let land_id : String
    let type_org : String
    let type : String
    let total_area_sum : String
    
    enum Keys: String {
        case id = "id"
        case crrnumber = "crr_number"
        case csnumber = "cs_number"
        case division = "division"
        case landtenure = "land_tenure"
        case landholder = "land_holder"
        case locaionOfproperty = "location_of_property"
        case amount = "land_amount"
        case area = "area"
        case address_of_owner = "address_of_owner"
        case is_linked = "is_linked"
        case payment_done = "payment_done"
        case land_id = "land_id"
        case type_org = "type_org"
        case type = "type"
        case total_area_sum = "total_area_sum"
    }
    
    init(dict: Dictionary) {
        area = getString(anything: dict[Keys.area.rawValue]).isEmptyString ? "-" : getString(anything: dict[Keys.area.rawValue]).decryptStr
        crrnumber = getString(anything: dict[Keys.crrnumber.rawValue]).isEmptyString ? "-" : getString(anything: dict[Keys.crrnumber.rawValue]).decryptStr
        csnumber = getString(anything: dict[Keys.csnumber.rawValue]).isEmptyString ? "-" : getString(anything: dict[Keys.csnumber.rawValue]).decryptStr
        division = getString(anything: dict[Keys.division.rawValue]).isEmptyString ? "-" : getString(anything: dict[Keys.division.rawValue]).decryptStr
        id = getString(anything: dict[Keys.id.rawValue]).decryptStr
        amount = getString(anything: dict[Keys.amount.rawValue]).isEmptyString ? "-" : getString(anything: dict[Keys.amount.rawValue]).decryptStr
        landholder = getString(anything: dict[Keys.landholder.rawValue]).isEmptyString ? "-" : getString(anything: dict[Keys.landholder.rawValue]).decryptStr
        landtenure = getString(anything: dict[Keys.landtenure.rawValue]).isEmptyString ? "-" : getString(anything: dict[Keys.landtenure.rawValue]).decryptStr
        locaionOfproperty = getString(anything: dict[Keys.locaionOfproperty.rawValue]).isEmptyString ? "-" : getString(anything: dict[Keys.locaionOfproperty.rawValue]).decryptStr
        address_of_owner = getString(anything: dict[Keys.address_of_owner.rawValue]).isEmptyString ? "-" : getString(anything: dict[Keys.address_of_owner.rawValue]).decryptStr
        is_linked = (getString(anything: dict[Keys.is_linked.rawValue]).decryptStr == "Yes") ? true : false
        payment_done = (getString(anything: dict[Keys.payment_done.rawValue]).decryptStr == "Yes") ? true : false

        land_id = getString(anything: dict[Keys.land_id.rawValue]).decryptStr
        type = getString(anything: dict[Keys.type.rawValue]).decryptStr
        type_org = getString(anything: dict[Keys.type_org.rawValue])
        total_area_sum = getString(anything: dict[Keys.total_area_sum.rawValue]).decryptStr

        super.init()
    }
}

class LandDetailsModel : NSObject {
    
    let crr_number: String
    let cs_number: String
    let division: String
    let name_of_owner : String
    let address_of_owner : String
    let location_of_property: String
    let land_tenure : String
    let assessed_properties : String
    let interest_rate: String
    let area: String
    let pending_tax: String
    let interest: String
    let rent: String
    let due_date1: String
    let due_date2: String
    let due_date3: String
    let due_date4: String
    let comm_date: String
    let deed_date: String
    let exp_date: String
    let lease_start_date: String
    let lease_end_date: String
    let lease_period: String
    let pr_yr: String
    var favorite : Bool
    let land_amount : String
    let deed_no : String
    let fee : String
    let due_date : String
    
    enum Keys: String {
        case crr_number = "crr_number"
        case cs_number = "cs_number"
        case division = "division"
        case name_of_owner = "name_of_owner"
        case address_of_owner = "address_of_owner"
        case location_of_property = "location_of_property"
        case land_tenure = "land_tenure"
        case assessed_properties = "assessed_properties"
        case interest_rate = "interest_rate"
        case area = "area"
        case pending_tax = "pending_tax"
        case interest = "interest"
        case rent = "rent"
        case due_date1 = "due_date1"
        case due_date2 = "due_date2"
        case due_date3 = "due_date3"
        case due_date4 = "due_date4"
        case comm_date = "comm_date"
        case deed_date = "deed_date"
        case exp_date = "exp_date"
        case pr_yr = "pr_yr"
        case lease_start_date = "lease_start_date"
        case lease_end_date = "lease_end_date"
        case lease_period = "lease_period"
        case favorite = "favorite"
        case land_amount = "land_amount"
        case deed_no = "deed_no"
        case fee = "fee"
        case due_date = "due_date"
    }
    
    init(dict: Dictionary) {
        crr_number = getString(anything: dict[Keys.crr_number.rawValue]).decryptStr
        cs_number = getString(anything: dict[Keys.cs_number.rawValue]).decryptStr
        division = getString(anything: dict[Keys.division.rawValue]).decryptStr
        name_of_owner = getString(anything: dict[Keys.name_of_owner.rawValue]).decryptStr
        address_of_owner = getString(anything: dict[Keys.address_of_owner.rawValue]).decryptStr
        location_of_property = getString(anything: dict[Keys.location_of_property.rawValue]).decryptStr
        land_tenure = getString(anything: dict[Keys.land_tenure.rawValue]).decryptStr
        assessed_properties = getString(anything: dict[Keys.assessed_properties.rawValue]).decryptStr
        interest_rate = getString(anything: dict[Keys.interest_rate.rawValue]).decryptStr
        area = getString(anything: dict[Keys.area.rawValue]).decryptStr
        pending_tax = getString(anything: dict[Keys.pending_tax.rawValue]).decryptStr
        interest = getString(anything: dict[Keys.interest.rawValue]).decryptStr
        rent = getString(anything: dict[Keys.rent.rawValue]).decryptStr
        due_date1 = getString(anything: dict[Keys.due_date1.rawValue]).decryptStr
        due_date2 = getString(anything: dict[Keys.due_date2.rawValue]).decryptStr
        due_date3 = getString(anything: dict[Keys.due_date3.rawValue]).decryptStr
        due_date4 = getString(anything: dict[Keys.due_date4.rawValue]).decryptStr
        comm_date = getString(anything: dict[Keys.comm_date.rawValue]).decryptStr
        deed_date = getString(anything: dict[Keys.deed_date.rawValue]).decryptStr
        exp_date = getString(anything: dict[Keys.exp_date.rawValue]).decryptStr
        pr_yr = getString(anything: dict[Keys.pr_yr.rawValue]).decryptStr
        lease_start_date = getString(anything: dict[Keys.lease_start_date.rawValue]).decryptStr
        lease_end_date = getString(anything: dict[Keys.lease_end_date.rawValue]).decryptStr
        lease_period = getString(anything: dict[Keys.lease_period.rawValue]).decryptStr
        favorite = (getString(anything: dict[Keys.favorite.rawValue]).decryptStr == "Yes") ? true : false
        land_amount = getString(anything: dict[Keys.land_amount.rawValue]).decryptStr
        deed_no = getString(anything: dict[Keys.deed_no.rawValue]).decryptStr
        due_date = getString(anything: dict[Keys.due_date.rawValue]).decryptStr
        fee = getString(anything: dict[Keys.fee.rawValue]).decryptStr

        super.init()
    }
}

class TaxDetailsModel : NSObject {
    
    let remaining_amount : String
    let tax: String
    let total_assesment_amount: String
    let total_payable_amount : String
    
    enum Keys: String {
        case remaining_amount = "remaining_amount"
        case tax = "tax"
        case total_assesment_amount = "total_assesment_amount"
        case total_payable_amount = "total_payable_amount"
    }
    
    init(dict: Dictionary) {
        remaining_amount = getString(anything: dict[Keys.remaining_amount.rawValue])
        tax = getString(anything: dict[Keys.tax.rawValue])
        total_assesment_amount = getString(anything: dict[Keys.total_assesment_amount.rawValue])
        total_payable_amount = getString(anything: dict[Keys.total_payable_amount.rawValue])

        super.init()
    }
}
