//
//  CellLandList.swift
//  RajyaSabha
//
//  Created by Mobile Mac on 21/10/21.
//

import UIKit

class CellLandList : UITableViewCell {
   //MARK: - Interface builder
    
    //UIView
    @IBOutlet weak var vwbg: UIView!
    @IBOutlet weak var vwSeparator: UIView!
    
    //UILabel
    @IBOutlet weak var lblAppNo: UILabel!
    @IBOutlet weak var lblCRRNumber: UILabel!
    @IBOutlet weak var lblPayNow: UILabel!
    @IBOutlet weak var lblCSNumber: UILabel!
    @IBOutlet weak var lblDivison: UILabel!
    @IBOutlet weak var lblLandTenure: UILabel!
    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var lblLandHolder: UILabel!
    @IBOutlet weak var lblAddressOfOwner: UILabel!
    
    @IBOutlet weak var btnPlus: UIButton!
    @IBOutlet weak var btnWarrantyList: UIButton!
    @IBOutlet weak var btnPayNow: UIButton!
    
    @IBOutlet var lblTitles: [LocalizedLabel]!
    @IBOutlet var lblValues: [UILabel]!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        setupvw()
        setuplbl()
    }
    
    func setupvw(){
        self.vwSeparator.backgroundColor = .orange
        selectionStyle = .none
        vwbg.setCustomCornerRadius(radius: 8.0)
    }
    
    func setuplbl() {
        
        self.lblTitles.forEach({
            $0.font = IS_IPAD ? .semiboldMediumFont : .semiboldSmallTitleFont
            $0.textColor = .customDarkBlueGradient
        })
        
        self.lblValues.forEach({
            $0.font = IS_IPAD ? .regularLargeFont : .regularTitleFont
            $0.textColor = .black
        })
        
        self.lblAppNo.font = IS_IPAD ? .regularLargeFont : .regularTitleFont
        self.lblAppNo.textColor = .orange
        self.btnPlus.tintAdjustmentMode = .normal
//        self.lblTitle.font = .boldValueFont
//        self.lblTitle.textColor = .black
        
        lblPayNow.font = IS_IPAD ? .semiboldMediumFont : .semiboldSmallTitleFont
        lblPayNow.textColor = .customWhite
        
        
        lblPayNow.superview?.setCustomCornerRadius(radius: 8.0)
    }
    
    func cellConfigure(item: LandListModel, landListType: LandListType) {
        self.lblAppNo.text = item.id
        self.lblCRRNumber.text = item.crrnumber
        self.lblCSNumber.text = (item.csnumber != "") ? item.csnumber : "-"
        self.lblDivison.text = item.division
        self.lblLandTenure.text = item.landtenure
        self.lblLocation.text = item.locaionOfproperty
        self.lblAmount.text = item.amount
        self.lblLandHolder.text = item.landholder
        self.lblArea.text = item.area
        self.lblAddressOfOwner.text = item.address_of_owner

        self.lblAddressOfOwner.superview?.isHidden = landListType != .landFavourite
        self.lblArea.superview?.isHidden = landListType != .landBank
        self.lblLandTenure.superview?.isHidden = (landListType == .linkLand || landListType == .landBank)

        if item.payment_done {
            lblPayNow.superview?.backgroundColor = UIColor(88, 107, 196)
            lblPayNow.text = CommonLabels.Receipt
        } else {
            lblPayNow.superview?.backgroundColor = .orange
            lblPayNow.text = CommonLabels.PayNow
        }
        
        if landListType == .linkLand && !item.is_linked {
            self.btnPlus.setTitleColor(.customWhite, for: .normal)
            self.btnPlus.setTitle("+", for: .normal)
            self.btnPlus.titleLabel?.font = IS_IPAD ? .boldExtraLargeFont : .boldLargeFont
            self.btnPlus.backgroundColor = .customgreenTheamClr
            self.btnPlus.setCustomCornerRadius(radius: 8)
            self.btnPlus.setImage(nil, for: .normal)
            self.btnPlus.isHidden =  false
            self.btnWarrantyList.isHidden = true
            self.btnPayNow.superview?.superview?.isHidden = true
            
            
        } else if landListType == .landBank {
            self.btnPlus.setTitle("", for: .normal)
            self.btnPlus.setImage(.ic_header_delete, for: .normal)
            self.btnPlus.backgroundColor = .clear
            self.btnPlus.isHidden =  false
            self.btnPlus.imageEdgeInsets = UIEdgeInsets(top: 4, left: 4, bottom: 4, right: 4)
            self.btnPlus.tintColor = .themeDarkOrangeClr
            
            self.btnWarrantyList.setImage(.ic_list, for: .normal)
            self.btnWarrantyList.tintColor = .themeDarkOrangeClr
            self.btnWarrantyList.isHidden = false
            self.btnPayNow.superview?.superview?.isHidden = false
            
        } else {
            self.btnPlus.isHidden =  true
            self.btnWarrantyList.isHidden = true
            self.btnPayNow.superview?.superview?.isHidden = true
        }
    }
}
