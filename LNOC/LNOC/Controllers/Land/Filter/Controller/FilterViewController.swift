//
//  RegisterViewController.swift
//  NWM
//
//  Created by Gaurang Patel on 14/04/21.
//

import UIKit

protocol ProtocolForLandFilter: AnyObject {
    func implimant(crr: String, cs: String, division: String, landTenure: String, landHolder: String, locationOfProperty: String, area: String, division_id : String, landTenure_id : String)
}

class FilterViewController : HeaderViewController {

    //View
    @IBOutlet weak var viewBtnSubmitGradient: GradientView!
    @IBOutlet weak var viewForScroll: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewForShadow: UIView!
    
    @IBOutlet var lblTitles: [LocalizedLabel]!
    @IBOutlet var txtValues: [UITextField]!
    @IBOutlet var lblSeprator: [UILabel]!
    
    @IBOutlet weak var lblTenure: UILabel!
    @IBOutlet weak var lblDivison: UILabel!

    @IBOutlet weak var tfCrrNumber: UITextField!
    @IBOutlet weak var tfCsNumber: UITextField!
    @IBOutlet weak var tfLandHolder: UITextField!
    @IBOutlet weak var tfLocationOfProperty: UITextField!
    @IBOutlet weak var tfArea: UITextField!
    
    @IBOutlet weak var imgLandTenureArrow: UIImageView!
    @IBOutlet weak var imgDivisonArrow: UIImageView!

    @IBOutlet weak var btnSearch : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    
    private var arrLandTenure = [DropDownModel]()
    private var arrDivisions = [DropDownModel]()
    private var dictData:Dictionary = [:]
    
    weak var delegate: ProtocolForLandFilter?
    var filter_crr = ""
    var filter_cs = ""
    var filter_division = ""
    var filter_tenure = ""
    var filter_holder = ""
    var filter_locationOfProperty = ""
    var filter_area = ""
    var filter_division_id = ""
    var filter_tenure_id = ""

    var landListType: LandListType = .landSearch
    
    var isFromAppList = false
    fileprivate var webServiceModel = FilterLandWebServiceModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpIPadConstraints()
        self.setUpView()
        self.setupLbl()
        self.setUpHeader()
        self.fontSetUp()
        
        self.tfCrrNumber.text = self.filter_crr
        self.tfCsNumber.text = self.filter_cs
        self.tfLandHolder.text = self.filter_holder
        self.tfLocationOfProperty.text = filter_locationOfProperty
        self.tfArea.text = filter_area
        self.lblDivison.text = (self.filter_division != "") ? self.filter_division : CommonLabels.select
        self.lblTenure.text = (self.filter_tenure != "") ? self.filter_tenure : CommonLabels.select
        
        self.getDataFromWebServices()
    }

    fileprivate func setUpView() {
        self.viewMain.setCustomCornerRadius(radius: 12)
        self.viewForShadow.setCustomCornerRadius(radius: 12)
        self.viewForShadow.addShadow()
        self.viewBtnSubmitGradient.setFullCornerRadius()
        self.viewBtnSubmitGradient.addGradient(ofColors: [UIColor.themeDarkOrangeClr.cgColor, UIColor.themeLightOrangeClr.cgColor], direction: .leftRight)
        self.btnCancel.setFullCornerRadius()

        self.lblSeprator.forEach({
            $0.backgroundColor = .customSeparator
        })
        
        self.imgLandTenureArrow.image = .ic_down_arrow
        self.imgLandTenureArrow.tintColor = .themeColor
        self.imgDivisonArrow.image = .ic_down_arrow
        self.imgDivisonArrow.tintColor = .themeColor
        
        if self.landListType == .landBank {
            self.lblTenure.superview?.isHidden = true
        } else {
            self.tfLocationOfProperty.superview?.isHidden = true
            self.tfArea.superview?.isHidden = true
        }
    }
    
    fileprivate func setupLbl() {
        self.lblTenure.text = CommonLabels.select
        self.lblDivison.text = CommonLabels.select

        let font = UIFont.regularValueFont
        self.tfCrrNumber.setPlaceholder(FilterPlaceHolders.enter_CRR_number, color: .lightGray, setFont: font)
        self.tfCsNumber.setPlaceholder(FilterPlaceHolders.enter_Cs_number, color: .lightGray, setFont: font)
        self.tfLandHolder.setPlaceholder(FilterPlaceHolders.enter_Land_Holder, color: .lightGray, setFont: font)
        self.tfLocationOfProperty.setPlaceholder(FilterPlaceHolders.enter_Location_Of_Property, color: .lightGray, setFont: font)
        self.tfArea.setPlaceholder(FilterPlaceHolders.enter_Area, color: .lightGray, setFont: font)
        
        
        if isFromAppList {
            self.tfCrrNumber.text = getString(anything: dictData[NOCAppListWebServicesModel.Keys.crr_number.rawValue])
            self.tfCsNumber.text = getString(anything: dictData[NOCAppListWebServicesModel.Keys.cs_number.rawValue])
            self.lblDivison.text = getString(anything: dictData[NOCAppListWebServicesModel.Keys.divison_id.rawValue])
            self.tfLandHolder.text = getString(anything: dictData[NOCAppListWebServicesModel.Keys.land_holder.rawValue])
            self.tfLocationOfProperty.text = getString(anything: dictData[NOCAppListWebServicesModel.Keys.location_of_property.rawValue])
        }
    }
    
    fileprivate func fontSetUp() {
        self.lblTitles.forEach({
            $0.font = .regularTitleFont
            $0.textColor = .customBlackTitle
        })

        self.txtValues.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
        })
        
        self.lblTenure.font = .regularValueFont
        self.lblTenure.textColor = .customBlack
        self.lblDivison.font = .regularValueFont
        self.lblDivison.textColor = .customBlack
        
        btnCancel.titleLabel?.font = UIFont.semiboldLargeFont
        btnSearch.titleLabel?.font = UIFont.semiboldLargeFont
    }
    
    fileprivate func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.Filter)
        setHeaderView_BackImage()
    }
    
    fileprivate func setUpIPadConstraints() {
        changeLeadingTrailingForiPad(view: viewForScroll)
        changeHeightForiPad(view: btnCancel)
        changeHeightForiPad(view: btnSearch)
        changeHeightForiPad(view: self.tfCsNumber)
        changeHeightForiPad(view: self.tfCrrNumber)
        changeHeightForiPad(view: self.tfLandHolder)
        changeHeightForiPad(view: self.tfLocationOfProperty)
        changeHeightForiPad(view: self.tfArea)
    }

    //MARK:- Selector Methods
    @IBAction func TapOnSubmitBtn(_ sender: LocalizedButton) {
        let crr = getString(anything: self.tfCrrNumber.text)
        let cs = getString(anything: self.tfCsNumber.text)
        let holder = getString(anything: self.tfLandHolder.text)
        let locationOfProperty = getString(anything: self.tfLocationOfProperty.text)
        let area = getString(anything: self.tfArea.text)
        self.delegate?.implimant(crr: crr, cs: cs, division: self.filter_division, landTenure: self.filter_tenure, landHolder: holder, locationOfProperty: locationOfProperty, area: area, division_id: self.filter_division_id, landTenure_id: self.filter_tenure_id)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func TapOnSelectLandTenureBtn(_ sender: UIButton) {
        var list = self.arrLandTenure.map({ $0.title })
        list.insert(CommonLabels.select, at: 0)
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblTenure.text, arrSource: [list], doneAction: { (rows, value) in
            self.lblTenure.text = getString(anything: value)
            if getString(anything: value) == CommonLabels.select {
                self.filter_tenure = ""
                self.filter_tenure_id = ""
            } else {
                self.filter_tenure = getString(anything: value)
                self.filter_tenure_id = self.arrLandTenure[rows[0] - 1].id
            }
        }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func TapOnSelectDivisionBtn(_ sender: UIButton) {
        var list = self.arrDivisions.map({ $0.title })
        list.insert(CommonLabels.select, at: 0)
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblDivison.text, arrSource: [list], doneAction: { (rows, value) in
            self.lblDivison.text = getString(anything: value)
            if getString(anything: value) == CommonLabels.select {
                self.filter_division = ""
                self.filter_division_id = ""
            } else {
                self.filter_division = getString(anything: value)
                self.filter_division_id = self.arrDivisions[rows[0] - 1].id
            }
        }, animated: true, origin: sender, onVC: self)
    }

    @IBAction func TapOnReset(_ sender: UIButton) {
        self.txtValues.forEach({
            $0.text = ""
        })
        self.lblTenure.text = CommonLabels.select
        self.lblDivison.text = CommonLabels.select
        self.delegate?.implimant(crr: "", cs: "", division: "", landTenure: "", landHolder: "", locationOfProperty: "", area: "", division_id: "", landTenure_id: "")
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:-  Web Service model
extension FilterViewController {
    func getDataFromWebServices() {
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.webServiceModel.getFilterDropDownListApi { arr_Divisions, isSucess, tenures in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            self.arrDivisions = arr_Divisions
            self.arrLandTenure = tenures
        }
    }
}


