//
//  FilterLandWebServiceModel.swift
//  LNOC
//
//  Created by STTL Mac on 07/01/22.
//

import Foundation

class FilterLandWebServiceModel : NSObject {

    //MARK:- URL
    fileprivate var filterURL:String {
        BASEURL + API.landfilterdropdown
    }
    
    //MARK:- Enum
    enum Keys : String {
        case divisions = "divisions"
        case tenures = "tenures"
    }
    
    //MARK:- Public Methods
    func getFilterDropDownListApi(block:@escaping (([DropDownModel], _ isSucess : Bool, _ tenures : [DropDownModel]) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {

            let param = WShandler.commonDict()
 
            WShandler.shared.getWebRequest(urlStr: filterURL, param: param) { (json, flag) in
                
                if (flag == 200) {
                    
                    //Invalid Token
                    if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                        
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                        UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                        Global.shared.changeInitialVC(animated: true, direction: .toLeft)
                        block([], false, [])
                        
                    } else if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        let divisions = (json[Keys.divisions.rawValue] as? [Dictionary] ?? []).map({ DropDownModel(dict: $0) })
                        let tenures = (json[Keys.tenures.rawValue] as? [Dictionary] ?? []).map({ DropDownModel(dictTenures: $0) })
                        block(divisions, true, tenures)

                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        block([], false, [])
                    }
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block([], false, [])
                }
            }
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block([], false, [])
        }
    }
}
