//
//  LandDetailsViewController.swift
//  LNOC
//
//  Created by Dhruv on 30/12/21.
//

import UIKit
import Toast_Swift

protocol ProtocolForFavouriteDetails : AnyObject {
    func implimant(id: String)
}

class LandDetailsViewController: HeaderViewController {

    //UILabel
    @IBOutlet var lblCRRNo: UILabel!
    @IBOutlet var lblCSNo: UILabel!
    @IBOutlet var lblDivision: UILabel!
    @IBOutlet var lblLandHolder: UILabel!
    @IBOutlet var lblAddOfOwner: UILabel!
    @IBOutlet var lblLocOfProperty: UILabel!
    @IBOutlet var lblLandTenure: UILabel!
    @IBOutlet var lblUseofLand: UILabel!
    @IBOutlet var lblAssessAmount: UILabel!
    @IBOutlet var lblInterestRate: UILabel!
    @IBOutlet var lblArea: UILabel!
    @IBOutlet var lblPendingTax: UILabel!
    @IBOutlet var lblInterest: UILabel!
    @IBOutlet weak var lblRent: UILabel!
    @IBOutlet weak var lblDueDate1: UILabel!
    @IBOutlet weak var lblDueDate2: UILabel!
    @IBOutlet weak var lblDueDate3: UILabel!
    @IBOutlet weak var lblDueDate4: UILabel!
    @IBOutlet weak var lblCommDate: UILabel!
    @IBOutlet weak var lblDeedDate: UILabel!
    @IBOutlet weak var lblExpDate: UILabel!
    @IBOutlet weak var lblPrYear: UILabel!
    @IBOutlet weak var lblLeaseStartDate: UILabel!
    @IBOutlet weak var lblLeaseEndDate: UILabel!
    @IBOutlet weak var lblLeasePeriod: UILabel!
    @IBOutlet weak var lblFee: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblDeedNo: UILabel!
    @IBOutlet weak var lblTitleAssessmentAmount: LocalizedLabel!
    
    // Collections
    @IBOutlet var lblsTitles: [UILabel]!
    @IBOutlet var lblsValues: [UILabel]!
    @IBOutlet var viewsDetailsShadow : [UIView]!
    
    var dataModel = [LandDetailsModel]()
    var land_id = ""
    fileprivate var webServiceModel = LandDetailsWebServiceModel()
    var landListType : LandListType = .landSearch
    weak var delegate: ProtocolForFavouriteDetails?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupConstraintIfIsIPad()
        setupHeader()
        setupView()
        setupLabel()
        self.getDataFromWebServices()
    }
    
    //MARK: - Private Methods
    private func setupConstraintIfIsIPad() {
        changeLeadingTrailingForiPad(view: self.view)
    }
    
    private func setupHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.LandDetails)
        setHeaderView_BackImage()
        if self.landListType != .landBank {
            showFavourite()
            self.viewHeader.btnFavourite.imageEdgeInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
            self.viewHeader.btnFavourite.tintColor = .white
        }
    }
    
    private func setupView() {
        self.view.backgroundColor = UIColor(232, 238, 243)
        self.viewsDetailsShadow.forEach({
            $0.isHidden = true
            $0.addShadow()
            $0.subviews.first?.viewWith(radius: 8)
            $0.backgroundColor = .clear
            $0.subviews.first?.backgroundColor = .white
        })
    }
    
    private func setupLabel() {
        lblsTitles.forEach({
            $0.font = .regularTitleFont
            $0.textColor = .customGray
            $0.numberOfLines = 0
        })
        
        lblsValues.forEach({
            $0.font = .boldValueFont
            $0.textColor = .black
            $0.numberOfLines = 0
        })
    }

    fileprivate func setupData() {
//        self.lblCRRNo.text = self.dataModel.crr_number
//        self.lblCRRNo.superview?.isHidden = self.dataModel.crr_number.isEmptyString
//
//        self.lblCSNo.text = self.dataModel.cs_number
//        self.lblCSNo.superview?.isHidden = self.dataModel.cs_number.isEmptyString
//
//        self.lblDivision.text = self.dataModel.division
//        self.lblDivision.superview?.isHidden = self.dataModel.division.isEmptyString
//
//        self.lblLandHolder.text = self.dataModel.name_of_owner
//        self.lblLandHolder.superview?.isHidden = self.dataModel.name_of_owner.isEmptyString
//
//        self.lblAddOfOwner.text = self.dataModel.address_of_owner
//        self.lblAddOfOwner.superview?.isHidden = self.dataModel.address_of_owner.isEmptyString
//
//        self.lblLocOfProperty.text = self.dataModel.location_of_property
//        self.lblLocOfProperty.superview?.isHidden = self.dataModel.location_of_property.isEmptyString
//
//        self.lblLandTenure.text = self.dataModel.land_tenure
//        self.lblLandTenure.superview?.isHidden = self.dataModel.land_tenure.isEmptyString
//
//        self.lblUseofLand.text = self.dataModel.assessed_properties
//        self.lblUseofLand.superview?.isHidden = self.dataModel.assessed_properties.isEmptyString
//
//        self.lblInterestRate.text = self.dataModel.interest_rate
//        self.lblInterestRate.superview?.isHidden = self.dataModel.interest_rate.isEmptyString
//
//        self.lblArea.text = self.dataModel.area
//        self.lblArea.superview?.isHidden = self.dataModel.area.isEmptyString
//
//        self.lblPendingTax.text = self.dataModel.pending_tax
//        self.lblPendingTax.superview?.isHidden = self.dataModel.pending_tax.isEmptyString
//
//        self.lblInterest.text = self.dataModel.interest
//        self.lblInterest.superview?.isHidden = self.dataModel.interest.isEmptyString
//
//        self.lblDueDate.text = self.dataModel.due_date
//        self.lblDueDate.superview?.isHidden = self.dataModel.due_date.isEmptyString
//
//        self.lblDueDate1.text = self.dataModel.due_date1
//        self.lblDueDate1.superview?.isHidden = self.dataModel.due_date1.isEmptyString
//
//        self.lblDueDate2.text = self.dataModel.due_date2
//        self.lblDueDate2.superview?.isHidden = self.dataModel.due_date2.isEmptyString
//
//        self.lblDueDate3.text = self.dataModel.due_date3
//        self.lblDueDate3.superview?.isHidden = self.dataModel.due_date3.isEmptyString
//
//        self.lblDueDate4.text = self.dataModel.due_date4
//        self.lblDueDate4.superview?.isHidden = self.dataModel.due_date4.isEmptyString
//
//        self.lblCommDate.text = self.dataModel.comm_date
//        self.lblCommDate.superview?.isHidden = self.dataModel.comm_date.isEmptyString
//
//        self.lblDeedDate.text = self.dataModel.deed_date
//        self.lblDeedDate.superview?.isHidden = self.dataModel.deed_date.isEmptyString
//
//        self.lblExpDate.text = self.dataModel.exp_date
//        self.lblExpDate.superview?.isHidden = self.dataModel.exp_date.isEmptyString
//
//        self.lblPrYear.text = self.dataModel.pr_yr
//        self.lblPrYear.superview?.isHidden = self.dataModel.pr_yr.isEmptyString
//
//        self.lblLeaseStartDate.text = self.dataModel.lease_start_date
//        self.lblLeaseStartDate.superview?.isHidden = self.dataModel.lease_start_date.isEmptyString
//
//        self.lblLeaseEndDate.text = self.dataModel.lease_end_date
//        self.lblLeaseEndDate.superview?.isHidden = self.dataModel.lease_end_date.isEmptyString
//
//        self.lblLeasePeriod.text = self.dataModel.lease_period
//        self.lblLeasePeriod.superview?.isHidden = self.dataModel.lease_period.isEmptyString
//
//        self.lblDeedNo.text = self.dataModel.deed_no
//        self.lblDeedNo.superview?.isHidden = self.dataModel.deed_no.isEmptyString
//
//        if !self.dataModel.rent.isEmptyString && self.dataModel.rent != "0.00" {
//            self.lblRent.text = self.dataModel.rent
//            self.lblFee.superview?.isHidden = true
//            self.lblAssessAmount.superview?.isHidden = true
//            self.lblRent.superview?.isHidden = false
//
//        } else if !self.dataModel.fee.isEmptyString && self.dataModel.fee != "0.00" {
//            self.lblFee.text = self.dataModel.fee
//            self.lblRent.superview?.isHidden = true
//            self.lblAssessAmount.superview?.isHidden = true
//            self.lblFee.superview?.isHidden = false
//
//        } else if !self.dataModel.land_amount.isEmptyString && self.dataModel.land_amount != "0.00" {
//            self.lblAssessAmount.text = self.dataModel.land_amount
//            self.lblFee.superview?.isHidden = true
//            self.lblRent.superview?.isHidden = true
//            self.lblAssessAmount.superview?.isHidden = false
//
//        } else {
//            self.lblTitleAssessmentAmount.text = CommonLabels.Assessment_Amount_Rent_Fee
//            self.lblAssessAmount.text = self.dataModel.land_amount
//            self.lblFee.superview?.isHidden = true
//            self.lblRent.superview?.isHidden = true
//        }
//
//        self.viewsDetailsShadow.forEach({
//            $0.isHidden = false
//        })
//
//        if self.dataModel.favorite {
//            self.viewHeader.btnFavourite.setImage(.favorite_fill, for: .normal)
//        } else {
//            self.viewHeader.btnFavourite.setImage(.favorite_empty, for: .normal)
//        }
    }
    
    override func btnFavourite(_ sender: UIButton) {
//        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
//        self.webServiceModel.land_id = self.land_id
//        self.webServiceModel.AddRemoveFavouriteApi { isSucess in
//            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
//            
//            if isSucess && self.dataModel.favorite {
//                self.viewHeader.btnFavourite.setImage(.favorite_empty, for: .normal)
//                self.dataModel.favorite = false
//                self.view.makeToast(LandDetailsMessages.msg_Removed_from_Land_Favourites)
//
//            } else if isSucess {
//                self.viewHeader.btnFavourite.setImage(.favorite_fill, for: .normal)
//                self.dataModel.favorite = true
//                self.view.makeToast(LandDetailsMessages.msg_Added_to_Land_Favourites)
//            }
//        }
    }
    
    override func btnBackTapped(_ sender: UIButton) {
//        if self.landListType == .landFavourite && self.dataModel.favorite == false && self.delegate != nil {
//            self.delegate?.implimant(id: self.land_id)
//        }
//        self.navigationController?.popViewController(animated: true)
    }
}
  
//MARK:-  Web Service model
extension LandDetailsViewController {
    func getDataFromWebServices() {
//        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
//        self.webServiceModel.land_id = self.land_id
//        self.webServiceModel.getLandDetailsApi { details, isSucess in
//            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
//            if isSucess {
//                self.dataModel = details
//                self.setupData()
//            }
//        }
    }
}
