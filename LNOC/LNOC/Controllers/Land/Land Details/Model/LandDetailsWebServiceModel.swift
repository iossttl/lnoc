//
//  LandDetailsWebServiceModel.swift
//  LNOC
//
//  Created by STTL Mac on 07/01/22.
//

import Foundation

class LandDetailsWebServiceModel : NSObject {

    var land_id = ""
    var total_area_sum = ""
    
    //MARK:- URL
    fileprivate var LandDetailsURL:String {
        BASEURL + API.landdetail
    }
    
    fileprivate var LandFavouriteURL:String {
        BASEURL + API.land_favourite
    }
    
    //MARK:- Enum
    enum Keys : String {
        case land_id = "land_id"
        case landDetail = "landDetail"
        case user_id = "user_id"
        case total_area_sum = "total_area_sum"
        case tax_detail = "tax_detail"
    }
    
    //MARK:- Public Methods
    func getLandDetailsApi(block:@escaping (([LandDetailsModel], _ isSucess : Bool, _ taxdetailDic : Dictionary) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {

            var param = WShandler.commonDict()
            param[Keys.land_id.rawValue] = self.land_id.encryptStr
            param[Keys.total_area_sum.rawValue] = self.total_area_sum.encryptStr
            param[Keys.user_id.rawValue] = UserPreferences.string(forKey: UserPreferencesKeys.UserInfo.userID)

            WShandler.shared.postWebRequest(urlStr: LandDetailsURL, param: param) { (json, flag) in
                
                if (flag == 200) {
                    
                    //Invalid Token
                    if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                        
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                        UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                        Global.shared.changeInitialVC(animated: true, direction: .toLeft)
                        block([LandDetailsModel(dict: [:])], false, [:])
                        
                    } else if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        let responseModel = (json[Keys.landDetail.rawValue] as? [Dictionary] ?? []).map({ LandDetailsModel(dict: $0) })
                        let tax_detailDic = (json[Keys.tax_detail.rawValue] as? Dictionary ?? [:]).decryptDic()
                        block(responseModel, true, tax_detailDic)

                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        block([LandDetailsModel(dict: [:])], false, [:])
                    }
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block([LandDetailsModel(dict: [:])], false, [:])
                }
            }
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block([LandDetailsModel(dict: [:])], false, [:])
        }
    }
    
    //MARK:- Public Methods
    func AddRemoveFavouriteApi(block:@escaping ((_ isSucess : Bool) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {

            var param = WShandler.commonDict()
            param[Keys.land_id.rawValue] = self.land_id.encryptStr
            param[Keys.user_id.rawValue] = UserPreferences.string(forKey: UserPreferencesKeys.UserInfo.userID)

            WShandler.shared.postWebRequest(urlStr: LandFavouriteURL, param: param) { (json, flag) in
                
                if (flag == 200) {
                    
                    //Invalid Token
                    if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                        
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                        UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                        UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                        UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                        Global.shared.changeInitialVC(animated: true, direction: .toLeft)
                        block(false)
                        
                    } else if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        block(true)

                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        block(false)
                    }
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block(false)
                }
            }
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block(false)
        }
    }
}
