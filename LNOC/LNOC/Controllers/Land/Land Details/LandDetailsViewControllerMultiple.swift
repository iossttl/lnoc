//
//  LandDetailsViewController.swift
//  LNOC
//
//  Created by Dhruv on 30/12/21.
//

import UIKit
import Toast_Swift

class LandDetailsViewControllerMultiple : HeaderViewController {
 
    @IBOutlet weak var stackViewForList: UIStackView!
    
    @IBOutlet weak var lblPayTaxDetails: UILabel!
    @IBOutlet weak var stackViewForPayTaxDetails: UIStackView!
    @IBOutlet weak var viewPayTaxDetailsMain: UIView!
    @IBOutlet weak var viewPayTaxDetails: UIView!

    var dataModel = [LandDetailsModel]()
    var land_id = ""
    var total_area_sum = ""
    fileprivate var webServiceModel = LandDetailsWebServiceModel()
    var landListType : LandListType = .landSearch
    weak var delegate: ProtocolForFavouriteDetails?
    var dicTaxDetail = Dictionary()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupConstraintIfIsIPad()
        setupHeader()
        setupView()
        self.getDataFromWebServices()
    }
    
    //MARK: - Private Methods
    private func setupConstraintIfIsIPad() {
        changeLeadingTrailingForiPad(view: self.view)
    }
    
    private func setupHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.LandDetails)
        setHeaderView_BackImage()
        if self.landListType != .landBank {
            showFavourite()
            self.viewHeader.btnFavourite.imageEdgeInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
            self.viewHeader.btnFavourite.tintColor = .white
        }
    }
    
    private func setupView() {
        self.view.backgroundColor = UIColor(232, 238, 243)
        self.stackViewForList.subviews.forEach { subV in
            subV.removeFromSuperview()
        }
        
        self.viewPayTaxDetails.addShadow()
        self.viewPayTaxDetails.viewWith(radius: 8)
        self.lblPayTaxDetails.font = .boldLargeFont
        self.lblPayTaxDetails.textColor = .customgreenTheamClr
    }

    fileprivate func setupData() {
        
        self.dataModel.forEach { landData in
            let ldView = LandDetailsCustomView.loadNib()
            
            ldView.lblCRRNo.text = landData.crr_number
            ldView.lblCRRNo.superview?.isHidden = landData.crr_number.isEmptyString
    
            ldView.lblCSNo.text = landData.cs_number
            ldView.lblCSNo.superview?.isHidden = landData.cs_number.isEmptyString
    
            ldView.lblDivision.text = landData.division
            ldView.lblDivision.superview?.isHidden = landData.division.isEmptyString
    
            ldView.lblLandHolder.text = landData.name_of_owner
            ldView.lblLandHolder.superview?.isHidden = landData.name_of_owner.isEmptyString
    
            ldView.lblAddOfOwner.text = landData.address_of_owner
            ldView.lblAddOfOwner.superview?.isHidden = landData.address_of_owner.isEmptyString
    
            ldView.lblLocOfProperty.text = landData.location_of_property
            ldView.lblLocOfProperty.superview?.isHidden = landData.location_of_property.isEmptyString
    
            ldView.lblLandTenure.text = landData.land_tenure
            ldView.lblLandTenure.superview?.isHidden = landData.land_tenure.isEmptyString
    
            ldView.lblUseofLand.text = landData.assessed_properties
            ldView.lblUseofLand.superview?.isHidden = landData.assessed_properties.isEmptyString
    
            ldView.lblInterestRate.text = landData.interest_rate
            ldView.lblInterestRate.superview?.isHidden = landData.interest_rate.isEmptyString
    
            ldView.lblArea.text = landData.area
            ldView.lblArea.superview?.isHidden = landData.area.isEmptyString
    
            ldView.lblPendingTax.text = landData.pending_tax
            ldView.lblPendingTax.superview?.isHidden = landData.pending_tax.isEmptyString
    
            ldView.lblInterest.text = landData.interest
            ldView.lblInterest.superview?.isHidden = landData.interest.isEmptyString
    
            ldView.lblDueDate.text = landData.due_date
            ldView.lblDueDate.superview?.isHidden = landData.due_date.isEmptyString
    
            ldView.lblDueDate1.text = landData.due_date1
            ldView.lblDueDate1.superview?.isHidden = landData.due_date1.isEmptyString
    
            ldView.lblDueDate2.text = landData.due_date2
            ldView.lblDueDate2.superview?.isHidden = landData.due_date2.isEmptyString
    
            ldView.lblDueDate3.text = landData.due_date3
            ldView.lblDueDate3.superview?.isHidden = landData.due_date3.isEmptyString
    
            ldView.lblDueDate4.text = landData.due_date4
            ldView.lblDueDate4.superview?.isHidden = landData.due_date4.isEmptyString
    
            ldView.lblCommDate.text = landData.comm_date
            ldView.lblCommDate.superview?.isHidden = landData.comm_date.isEmptyString
    
            ldView.lblDeedDate.text = landData.deed_date
            ldView.lblDeedDate.superview?.isHidden = landData.deed_date.isEmptyString
    
            ldView.lblExpDate.text = landData.exp_date
            ldView.lblExpDate.superview?.isHidden = landData.exp_date.isEmptyString
    
            ldView.lblPrYear.text = landData.pr_yr
            ldView.lblPrYear.superview?.isHidden = landData.pr_yr.isEmptyString
    
            ldView.lblLeaseStartDate.text = landData.lease_start_date
            ldView.lblLeaseStartDate.superview?.isHidden = landData.lease_start_date.isEmptyString
    
            ldView.lblLeaseEndDate.text = landData.lease_end_date
            ldView.lblLeaseEndDate.superview?.isHidden = landData.lease_end_date.isEmptyString
    
            ldView.lblLeasePeriod.text = landData.lease_period
            ldView.lblLeasePeriod.superview?.isHidden = landData.lease_period.isEmptyString
    
            ldView.lblDeedNo.text = landData.deed_no
            ldView.lblDeedNo.superview?.isHidden = landData.deed_no.isEmptyString
    
            if !landData.rent.isEmptyString && landData.rent != "0.00" {
                ldView.lblRent.text = landData.rent
                ldView.lblFee.superview?.isHidden = true
                ldView.lblAssessAmount.superview?.isHidden = true
                ldView.lblRent.superview?.isHidden = false
    
            } else if !landData.fee.isEmptyString && landData.fee != "0.00" {
                ldView.lblFee.text = landData.fee
                ldView.lblRent.superview?.isHidden = true
                ldView.lblAssessAmount.superview?.isHidden = true
                ldView.lblFee.superview?.isHidden = false
    
            } else if !landData.land_amount.isEmptyString && landData.land_amount != "0.00" {
                ldView.lblAssessAmount.text = landData.land_amount
                ldView.lblFee.superview?.isHidden = true
                ldView.lblRent.superview?.isHidden = true
                ldView.lblAssessAmount.superview?.isHidden = false
    
            } else {
                ldView.lblTitleAssessmentAmount.text = CommonLabels.Assessment_Amount_Rent_Fee
                ldView.lblAssessAmount.text = landData.land_amount
                ldView.lblFee.superview?.isHidden = true
                ldView.lblRent.superview?.isHidden = true
            }
            
            self.stackViewForList.addArrangedSubview(ldView)
        }

        if self.dataModel.count > 1 {
            self.viewHeader.btnFavourite.isHidden = true
            
        } else if (self.dataModel.first?.favorite ?? false) {
            self.viewHeader.btnFavourite.setImage(.favorite_fill, for: .normal)
        } else {
            self.viewHeader.btnFavourite.setImage(.favorite_empty, for: .normal)
        }
        
        if self.dicTaxDetail.count == 0  {
            self.viewPayTaxDetailsMain.isHidden = true
        } else {
            self.viewPayTaxDetailsMain.isHidden = false
            self.stackViewForPayTaxDetails.subviews.forEach({ $0.removeFromSuperview() })
            
            var listData = [(listDataTitle:String, listDataValue: String)]()
            let model = TaxDetailsModel(dict: self.dicTaxDetail)
            
            if !model.total_assesment_amount.isEmptyString {
                listData.append(("Total Assesment Amount".localizedString, model.total_assesment_amount))
            } else {
                listData.append(("Total Assesment Amount".localizedString, "-"))
            }
            
            if !model.tax.isEmptyString {
                listData.append(("Tax".localizedString, model.tax))
            } else {
                listData.append(("Tax".localizedString, "-"))
            }
            
            if !model.total_payable_amount.isEmptyString {
                listData.append(("Total Payable Amount".localizedString, model.total_payable_amount))
            } else {
                listData.append(("Total Payable Amount".localizedString, "-"))
            }

            if !model.remaining_amount.isEmptyString {
                listData.append(("Remaining Amount".localizedString, model.remaining_amount))
            } else {
                listData.append(("Remaining Amount".localizedString, "-"))
            }
            
            for item in listData {
                let clearView = UIView()
                clearView.backgroundColor = .clear
                
                let labelTitle = UILabel()
                labelTitle.font = .regularTitleFont
                labelTitle.textColor = .customGray
                labelTitle.numberOfLines = 0
                labelTitle.text = item.listDataTitle
                clearView.addSubview(labelTitle)
                
                let labelValue = UILabel()
                labelValue.font = .boldValueFont
                labelValue.textColor = .black
                labelValue.numberOfLines = 0
                labelValue.text = item.listDataValue
                clearView.addSubview(labelValue)
                
                labelTitle.addConstraint(topAnchr: clearView.topAnchor,
                                         leftAnchr: clearView.leftAnchor,
                                         rightAnchr: clearView.rightAnchor,
                                         bottomAnchr: nil)
                
                labelValue.addConstraint(topAnchr: labelTitle.bottomAnchor,
                                         leftAnchr: clearView.leftAnchor,
                                         rightAnchr: clearView.rightAnchor,
                                         bottomAnchr: clearView.bottomAnchor,
                                         padding: .init(top: 8, left: 0, bottom: 0, right: 0))
                
                stackViewForPayTaxDetails.addArrangedSubview(clearView)
            }
        }
    }
    
    override func btnFavourite(_ sender: UIButton) {
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.webServiceModel.land_id = self.land_id
        self.webServiceModel.AddRemoveFavouriteApi { isSucess in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            
            if isSucess && (self.dataModel.first?.favorite ?? false) {
                self.viewHeader.btnFavourite.setImage(.favorite_empty, for: .normal)
                self.dataModel.first?.favorite = false
                self.view.makeToast(LandDetailsMessages.msg_Removed_from_Land_Favourites)

            } else if isSucess {
                self.viewHeader.btnFavourite.setImage(.favorite_fill, for: .normal)
                self.dataModel.first?.favorite = true
                self.view.makeToast(LandDetailsMessages.msg_Added_to_Land_Favourites)
            }
        }
    }
    
    override func btnBackTapped(_ sender: UIButton) {
        if self.landListType == .landFavourite && self.dataModel.first?.favorite == false {
            self.delegate?.implimant(id: self.land_id)
        }
        self.navigationController?.popViewController(animated: true)
    }
}
  
//MARK:-  Web Service model
extension LandDetailsViewControllerMultiple {
    func getDataFromWebServices() {
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.webServiceModel.land_id = self.land_id
        self.webServiceModel.total_area_sum = self.total_area_sum
        self.webServiceModel.getLandDetailsApi { details, isSucess, tax_detail  in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if isSucess {
                self.dataModel = details
                self.dicTaxDetail = tax_detail
                self.setupData()
            }
        }
    }
}
