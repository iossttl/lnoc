//
//  LandDetailsCustomView.swift
//  LNOC
//
//  Created by Savan Ankola on 13/04/22.
//

import UIKit

class LandDetailsCustomView : UIView {
    
    @IBOutlet var viewsDetailsShadow : UIView!

    //UILabel
    @IBOutlet var lblCRRNo: UILabel!
    @IBOutlet var lblCSNo: UILabel!
    @IBOutlet var lblDivision: UILabel!
    @IBOutlet var lblLandHolder: UILabel!
    @IBOutlet var lblAddOfOwner: UILabel!
    @IBOutlet var lblLocOfProperty: UILabel!
    @IBOutlet var lblLandTenure: UILabel!
    @IBOutlet var lblUseofLand: UILabel!
    @IBOutlet var lblAssessAmount: UILabel!
    @IBOutlet var lblInterestRate: UILabel!
    @IBOutlet var lblArea: UILabel!
    @IBOutlet var lblPendingTax: UILabel!
    @IBOutlet var lblInterest: UILabel!
    @IBOutlet weak var lblRent: UILabel!
    @IBOutlet weak var lblDueDate1: UILabel!
    @IBOutlet weak var lblDueDate2: UILabel!
    @IBOutlet weak var lblDueDate3: UILabel!
    @IBOutlet weak var lblDueDate4: UILabel!
    @IBOutlet weak var lblCommDate: UILabel!
    @IBOutlet weak var lblDeedDate: UILabel!
    @IBOutlet weak var lblExpDate: UILabel!
    @IBOutlet weak var lblPrYear: UILabel!
    @IBOutlet weak var lblLeaseStartDate: UILabel!
    @IBOutlet weak var lblLeaseEndDate: UILabel!
    @IBOutlet weak var lblLeasePeriod: UILabel!
    @IBOutlet weak var lblFee: UILabel!
    @IBOutlet weak var lblDueDate: UILabel!
    @IBOutlet weak var lblDeedNo: UILabel!
    @IBOutlet weak var lblTitleAssessmentAmount: LocalizedLabel!
    
    // Collections
    @IBOutlet var lblsTitles: [UILabel]!
    @IBOutlet var lblsValues: [UILabel]!
    
    override class func loadNib() -> Self {
        return Bundle.main.loadNibNamed("LandDetailsCustomView", owner: nil, options: nil)![0] as! Self
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
        setupLabel()
    }
    
    private func setUpView() {
        self.viewsDetailsShadow.addShadow()
        self.viewsDetailsShadow.subviews.first?.viewWith(radius: 8)
        self.viewsDetailsShadow.backgroundColor = .clear
        self.viewsDetailsShadow.subviews.first?.backgroundColor = .white
    }
    
    private func setupLabel() {
        lblsTitles.forEach({
            $0.font = .regularTitleFont
            $0.textColor = .customGray
            $0.numberOfLines = 0
        })
        
        lblsValues.forEach({
            $0.font = .boldValueFont
            $0.textColor = .black
            $0.numberOfLines = 0
        })
    }
}
