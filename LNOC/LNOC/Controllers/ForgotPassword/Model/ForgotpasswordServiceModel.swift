//
//  ForgotpasswordServiceModel.swift
//  ASIDEMO
//
//  Created by Dhruv Patel on 13/01/21.
//

import UIKit

class ForgotpasswordServiceModel: NSObject {
    
    var email:String = ""
    
    fileprivate var getForgotpassword:String {
        return BASEURL + API.forgetpassword
    }

    enum keys:String {
        case email = "email"
    }
    
    //MARK:- Initializer
    override init() {
        super.init()
    }
    
     func sendForgotPassword(block:@escaping ((Dictionary?) -> Swift.Void)) {
         if (WShandler.shared.CheckInternetConnectivity()) {
           // let uuid = UUID().uuidString
            var param = WShandler.commonDict()//(uuid: "uuid")
             param[keys.email.rawValue] = self.email.encryptStr
            print(param,"param")
             
             WShandler.shared.postWebRequest(urlStr: getForgotpassword, param: param) { (json, flag) in
                var responseModel:Dictionary?
                 if flag == 200 {
                     if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                        
                        print(json,"json")
                        responseModel = json
                         
                     } else {
                         Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                     }
                 } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                 }
                 block(responseModel)
             }
         } else {
             block(nil)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
         }
     }
}
