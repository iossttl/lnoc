//
//  ForgotpasswordViewController.swift
//  MyGovMaharashtra
//
//  Created by Gunjan Patel on 14/06/19.
//  Copyright © 2019 Vikram Jagad. All rights reserved.
//

import UIKit

class ForgotpasswordViewController: UIViewController {
    
    //MARK:- Interface Builder
    //UIView
    @IBOutlet weak var viewForgetPassword: UIView!
    @IBOutlet weak var viewScrlViewMain: UIView!
    @IBOutlet weak var viewbtnSubmit: GradientView!
    
    //UILabel
    @IBOutlet weak var lblForgetPassword: UILabel!
    @IBOutlet weak var lblReference: UILabel!
    
    //UIScrollView
    @IBOutlet weak var scrlViewForgetPassword: UIScrollView!
    
    //UITextField
    @IBOutlet weak var tfForgetPassword: UITextField!

    //UIButton
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnCancel: UIButton!
    
    fileprivate var forgotPasswordResponseModel = ForgotpasswordServiceModel()
    
    //MARK:- View Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViewController()
    }

    //ViewController set up
    fileprivate func setUpViewController() {
        changeLeadingTrailingForiPad(view: viewScrlViewMain)
        changeHeightForiPad(view: btnCancel)
        changeHeightForiPad(view: btnSubmit)
        changeHeightForiPad(view: tfForgetPassword)

        setupUI()
        setUpLbls()
        setupBtns()
        setUptxtField()
    }
    
    //UI set up
    func setupUI() {
        self.viewForgetPassword.setCustomCornerRadius(radius: 8)
        self.viewForgetPassword.backgroundColor = .white
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.65)
        
        self.viewbtnSubmit.setFullCornerRadius()
        self.viewbtnSubmit.addGradient(ofColors: [UIColor.themeDarkOrangeClr.cgColor, UIColor.themeLightOrangeClr.cgColor], direction: .leftRight)
    }
    
    //Label set up
    func setUpLbls() {
        self.lblForgetPassword.text = LoginButtons.btn_forgot_password
        self.lblForgetPassword.textColor = .black
        self.lblForgetPassword.font = .boldTitleFont
 
        lblReference.text = Login.Email.uppercased()
        lblReference.font = .regularTitleFont
    }
    
    //Button set up
    func setupBtns() {
        self.btnSubmit.setTitle(CommonButtons.btn_submit.uppercased(), for: .normal)
        self.btnSubmit.titleLabel?.font = .regularValueFont
        self.btnCancel.setFullCornerRadius()
        self.btnCancel.titleLabel?.font = .boldTitleFont
        self.btnCancel.setTitleColor(.white, for: .normal)
        self.btnSubmit.setTitleColor(.white, for: .normal)
    }
    
    //textField set up
    func setUptxtField() {
        tfForgetPassword.font = UIFont.mediumValueFont
        tfForgetPassword.textColor = .customBlack
        tfForgetPassword.placeholder = LoginPlaceHolders.email_address
    }
    
    //MARK:- IBActions
    @IBAction func btnClickedCancel(_ sender: Any) {
        UIView.hideKeyBoard()
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnClickedSubmit(_ sender: Any) {
        if self.validateForgotPassword() {
            UIView.hideKeyBoard()
            fillModel()
            getDataFromWebServices()
        }
    }
}

//MARK:- Validation
extension ForgotpasswordViewController {
    private func validateForgotPassword() -> Bool {
         var isValid = false
        if (self.tfForgetPassword.text ?? "").isStringEmpty {
            Global.shared.showBanner(message: LoginMessages.msg_enter_email)
            tfForgetPassword.becomeFirstResponder()
        } else if !(self.tfForgetPassword.text ?? "").isValidEmail {
            Global.shared.showBanner(message: LoginMessages.msg_please_enter_valid_email)
            tfForgetPassword.becomeFirstResponder()
        }
        else {
            isValid = true
        }
        return isValid
    }
}


extension ForgotpasswordViewController{
    
    func getDataFromWebServices() {
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        forgotPasswordResponseModel.sendForgotPassword{ (json) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            
            if let response = json {

                Global.shared.showBanner(message: getString(anything: response[CommonAPIConstant.key_message]).decryptStr)
                self.tfForgetPassword.text = ""
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    fileprivate func fillModel() {
        forgotPasswordResponseModel.email = getString(anything: tfForgetPassword.text).trimming_WS
    }
    
}
