//
//  LoginServiceModel.swift
//  ASIDEMO
//
//  Created by Dhruv Patel on 13/01/21.
//

import UIKit

class ProfileWebServiceModel: NSObject {
    
    var email:String = ""
    var mobile : String = ""
    var gender : String = ""
    var first_name : String = ""
    var middle_name : String = ""
    var last_name : String = ""
    var mobile_otp : String = ""
    var date_of_birth : String = ""
    var state_id : String = ""
    var districts_id : String = ""
    var city_id : String = ""
    var pan_no : String = ""
    var adhaar_card : String = ""
    var bank_name : String = ""
    var bank_account_number : String = ""
    var adhaar_card_file : String = ""
    var pan_card_file : String = ""
    var picture : String = ""
    var address : String = ""


    var scriptPanDic : JSONObject = [:]
    var scriptAdharCardDic : JSONObject = [:]
    
    var documentModels : [DocumentModel] = []
 
    
    
    enum keys:String {
        case first_name = "first_name"
        case middle_name = "middle_name"
        case last_name = "last_name"
        case email = "email"
        case mobile = "mobile"
        case gender = "gender"
        case mobile_otp = "mobile_otp"
        case pan = "pan"
        case addhar = "addhar"
        case district_code = "district_code"
        case state_id = "state_id"
        case districts_id = "districts_id"
        case city_id = "city_id"
        case date_of_birth = "date_of_birth"
        case pan_no = "pan_no"
        case adhaar_card = "adhaar_card"
        case bank_name = "bank_name"
        case bank_account_number = "bank_account_number"
        case adhaar_card_file = "adhaar_card_file"
        case pan_card_file = "pan_card_file"
        case picture = "picture"
        case address = "address"
    }


    
    fileprivate var getProfileUrl :String {
         return BASEURL + API.getProfile
     }
    
    fileprivate var editProfileUrl :String {
         return BASEURL + API.editprofile
     }
    
    fileprivate var getOTPUrl : String {
         return BASEURL //+ API.getOTP
     }
    
    fileprivate var verifyOTPUrl : String {
         return BASEURL //+ API.verifyOTP
     }
    
    
    fileprivate var getDistrictUrl : String {
         return BASEURL + API.getdistricts
     }
    
    fileprivate var getCityUrl : String {
         return BASEURL + API.getCity
     }
   
    
    //MARK:- Initializer
    override init() {
        super.init()
    }
    
    private func updateProfileDict() -> Dictionary {
        return [keys.first_name.rawValue: first_name, //EncryptionModel.default.encrypt(str: first_name, key: uuid),
                keys.middle_name.rawValue: middle_name, //EncryptionModel.default.encrypt(str: first_name, key: uuid),
                keys.last_name.rawValue: last_name, //EncryptionModel.default.encrypt(str: last_name, key: uuid),
                keys.email.rawValue: email, //EncryptionModel.default.encrypt(str: email, key: uuid),
                keys.mobile.rawValue: mobile, //EncryptionModel.default.encrypt(str: mobile, key: uuid),
                keys.address.rawValue : address,
                keys.gender.rawValue: gender,
                keys.date_of_birth.rawValue: date_of_birth,
                keys.state_id.rawValue: state_id,
                keys.districts_id.rawValue: districts_id,
                keys.city_id.rawValue: city_id,
                keys.pan_no.rawValue: pan_no,
                keys.adhaar_card.rawValue: adhaar_card,
                keys.bank_name.rawValue: bank_name,
                keys.bank_account_number.rawValue: bank_account_number]
    }
    
     
    fileprivate var getOtpDict : [String : Any] {
        return [keys.mobile.rawValue: self.mobile]
    }
 
    fileprivate var verifyOtpDict : [String : Any] {
        return [keys.mobile.rawValue: self.mobile,
                keys.mobile_otp.rawValue: self.mobile_otp]
    }
    
    func GetProfileDetails(invalidTokenMsg : @escaping ((String) -> Swift.Void), block:@escaping ((UserModel?) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            let uuid = UUID().uuidString
           let param = WShandler.commonDict()//(uuid: uuid)
//            param["token"] = UserPreferences.string(forKey: UserPreferencesKeys.General.token)
            print(param)
            WShandler.shared.postWebRequest(urlStr: self.getProfileUrl, param: param) { (json, flag) in
                var responseModel:UserModel?
                if flag == 200 {
                    
                    //Invalid Token
                    if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                        invalidTokenMsg(getString(anything:json[CommonAPIConstant.key_message]).decryptStr)

                    } else if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                       if let dict = json[CommonAPIConstant.key_userDetails] as? Dictionary {
                        responseModel = UserModel(dict: dict, uuid: uuid)
                       }
                       
                    } else {
                       Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                    }
                } else {
                   Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel)
            }
        } else {
            block(nil)
           Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
    
    //Edit Profile
     func updateProfile(invalidTokenMsg : @escaping ((String) -> Swift.Void), block:@escaping ((UserModel?) -> Swift.Void)) {
         if (WShandler.shared.CheckInternetConnectivity()) {
            let uuid = UUID().uuidString
            var param = WShandler.commonDict()//(uuid: uuid)
            let dict = updateProfileDict().encryptDic()
            for key in dict.keys {
                param[key] = dict[key]
            }
            print(param)
            
           /* WShandler.shared.postWebRequest(urlStr: self.editProfileUrl, param: param) { (json, flag) in
                 var responseModel:UserModel?
                 if flag == 200 {
                    
                    //Invalid Token
                    if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                        invalidTokenMsg(getString(anything:json[CommonAPIConstant.key_message]))

                    } else if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message])) 

                        if let dict = json[CommonAPIConstant.key_userDetails] as? Dictionary {
                            responseModel = UserModel(dict: dict, uuid: uuid)
                        }
                        
                     } else {
                        Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]))
                     }
                 } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]))
                 }
                 block(responseModel)
             }
         } else {
             block(nil)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
         }*/
        
            WShandler.shared.multipartWebRequest(urlStr: editProfileUrl, dictParams: param, documents: documentModels) { (json, flag) in
                var responseModel:UserModel?
                if flag == 200 {
                   
                   //Invalid Token
                   if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                       invalidTokenMsg(getString(anything:json[CommonAPIConstant.key_message]).decryptStr)

                   } else if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                       Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)

                       if let dict = json[CommonAPIConstant.key_userDetails] as? Dictionary {
                           responseModel = UserModel(dict: dict, uuid: uuid)
                       }
                       
                    } else {
                       Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                    }
                } else {
                   Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel)
            }
        } else {
            block(nil)
           Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
        
        
     }
    
    
    //Send/Resend Otp
    func getOTPreq(block:@escaping ((Bool,String) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            var param = WShandler.commonDict()//(uuid: uuid)
            let dict = getOtpDict.encryptDic()
            for key in dict.keys {
                param[key] = dict[key]
            }
            
         /*   WShandler.shared.postWebRequest(urlStr: getOTPUrl, param: param) { (json, flag) in

                if flag == 200 {
                    if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                        
                        let otp = EncryptionModel.default.decrypt(str: getString(anything: json[CommonAPIConstant.key_otp]), key: uuid)
                        block(true,getString(anything: otp))
                        
                    } else {
                       Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]))
                        block(false,"")
                    }
                } else {
                   Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]))
                    block(false,"")
                }
            }
        } else {
           Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block(false,"")
        }*/
        }
    }
    
    //Verify Otp
    func verifyOTPreq(block:@escaping ((Bool) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            var param = WShandler.commonDict()//(uuid: uuid)
            let dict = verifyOtpDict.encryptDic()
            for key in dict.keys {
                param[key] = dict[key]
            }
            
            WShandler.shared.postWebRequest(urlStr: verifyOTPUrl, param: param) { (json, flag) in

                if flag == 200 {
                    if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                        block(true)
                        
                    } else {
                       Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                        block(false)
                    }
                } else {
                   Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block(false)
                }
            }
        } else {
           Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block(false)
        }
    }
    
    
    func getDistrictData(block: @escaping ((DropDownResponseModel?) -> Swift.Void)) {
         
         if (WShandler.shared.CheckInternetConnectivity()) {
             let param = WShandler.commonDict()
             WShandler.shared.getWebRequest(urlStr:getDistrictUrl,param: param) { (json, flag) in
                 var responseModel:DropDownResponseModel?
                 if (flag == 200) {
                     if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                         responseModel = DropDownResponseModel(dictDistrict: json)
                     } else {
                         Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                     }
                 } else {
                     Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                 }
                 block(responseModel)
             }
             }else{
             block(nil)
             Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
         }
     }
    
    func getCityData(block: @escaping ((DropDownResponseModel?) -> Swift.Void)) {
         
         if (WShandler.shared.CheckInternetConnectivity()) {
            var param = WShandler.commonDict()
             param[keys.district_code.rawValue] = districts_id.encryptStr
             WShandler.shared.getWebRequest(urlStr:getCityUrl,param: param) { (json, flag) in
                 var responseModel:DropDownResponseModel?
                 if (flag == 200) {
                     if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                         responseModel = DropDownResponseModel(dictCity: json)
                     } else {
                         Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                     }
                 } else {
                     Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                 }
                 block(responseModel)
             }
             }else{
             block(nil)
             Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
         }
     }
    
    
    
    func saveOrDraftGrivienceRegistrationURL(block: @escaping ((Dictionary?) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            var param = WShandler.commonDict()//(uuid: uuid)
            let dict = updateProfileDict().encryptDic()
            for key in dict.keys {
                param[key] = dict[key]
            }
            print(param)
            
            WShandler.shared.multipartWebRequest(urlStr: editProfileUrl, dictParams: param, documents: documentModels) { (json, flag) in
                var responseModel:Dictionary?
                if (flag == 200) {
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        //responseModel = DropDownResponseModel(dictState: json)
                        responseModel = json
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                    }
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel)
            }
        } else {
            block(nil)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
        
    }

}

/*func saveOrDraftGrivienceRegistrationURL(block: @escaping ((Dictionary?) -> Swift.Void)) {
    if (WShandler.shared.CheckInternetConnectivity()) {
        let uuid = UUID().uuidString
        
        let documentModels = grievanceDetailsModel?.documentModels.filter({ !$0.isEditModeOpen })
        
        WShandler.shared.multipartWebRequest(urlStr: grivienceRegistrationURL, dictParams: grivienceRegistrationURLDict(uuid: uuid), documents: documentModels) { (json, flag) in
            var responseModel:Dictionary?
            if (flag == 200) {
                if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                    //responseModel = DropDownResponseModel(dictState: json)
                    responseModel = json
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]))
                }
            } else {
                Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]))
            }
            block(responseModel)
        }
    } else {
        block(nil)
        Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
    }
    
}*/
