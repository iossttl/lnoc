//
//  RegisterViewController.swift
//  NWM
//
//  Created by Gaurang Patel on 14/04/21.
//

import UIKit
import SDWebImage
import Photos

class ProfileViewController: HeaderViewController {
    
    //MARK: - Outlet
    
    //View
    @IBOutlet weak var viewSubmit: UIView!
    @IBOutlet weak var viewBtnSubmitGradient: GradientView!
    @IBOutlet weak var viewForScroll: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewForShadow: UIView!
    @IBOutlet weak var viewGetResendOTP: UIView!
    @IBOutlet weak var viewOTP: UIView!
    @IBOutlet weak var viewProfileImage: UIView!
    @IBOutlet weak var viewImgSize: UIView!
    
    @IBOutlet weak var vieBgImg: UIView!
    
    
    @IBOutlet weak var txtFullName: UITextField!
    @IBOutlet weak var txtMiddleName: UITextField!
    @IBOutlet weak var txtLastName: UITextField!
    @IBOutlet weak var txtMobileNo: UITextField!
    @IBOutlet weak var txtOTP: UITextField!
    
    @IBOutlet weak var txtAddress: UITextField!
    
    @IBOutlet weak var txtPanNo: UITextField!
    @IBOutlet weak var txtAadhaarNo: UITextField!
    @IBOutlet weak var txtBankName: UITextField!
    @IBOutlet weak var txtBankAccountNo: UITextField!
    
    @IBOutlet weak var viewDocumentForPan: UIStackView!
    @IBOutlet weak var viewDocuymentForAddharCard: UIStackView!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var lblOTPTime: UILabel!
    @IBOutlet weak var lblGetResendOTP: LocalizedLabel!
    @IBOutlet weak var lblOTPSent: UILabel!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblMaxImgSize: LocalizedLabel!
    
    @IBOutlet weak var lblDateOfBirth: UILabel!
    @IBOutlet weak var lblState: UILabel!
    @IBOutlet weak var lblDistrict: UILabel!
    @IBOutlet weak var lblCity: UILabel!
    
    @IBOutlet weak var imgGenderDownArrow : UIImageView!
    
    @IBOutlet weak var imgViewCalnder: UIImageView!
    
    @IBOutlet weak var btnSelectGender : UIButton!
    
    @IBOutlet weak var btnEditProfile: UIButton!
    
    @IBOutlet weak var imgProfile: UIImageView!
    
    @IBOutlet weak var imgEditProfile: UIImageView!
    
    @IBOutlet var lblTitles: [LocalizedLabel]!
    @IBOutlet var txtValues: [UITextField]!
    @IBOutlet var lblAsterisks: [LocalizedLabel]!
    @IBOutlet var lblUnderLine: [UILabel]!
    @IBOutlet var lblValueDropDown: [UILabel]!
    @IBOutlet var imgDropDown: [UIImageView]!
    @IBOutlet var btnDropdown: [UIButton]!
    
    @IBOutlet weak var topConstraintStackView: NSLayoutConstraint!
    
    
    fileprivate var webServiceModel = ProfileWebServiceModel()
    fileprivate var documentPickerViewControllerDelegate: DocumentPickerViewControllerDelegate!
    
    fileprivate var strOTP = "-"
    fileprivate var timer: Timer?
    fileprivate var totalSecond = 120
    fileprivate var strMobileNumber = ""
    private var selectedGenderType : Gender = .male
    private var arrGenderOptions = [String]()
    private var arrStateOptions = [String]()
    var arrDistrictOptions: [DropDownModel] = []
    var arrCityOptions: [DropDownModel] = []
    
    var documentTag = 0
    
    let documentForPan = DocumentView2.loadNib()
    let documentForAddharCard = DocumentView2.loadNib()
    
    
    var imagePicker = UIImagePickerController()
    ///////////
    var typeId = ""
    private var pickerControllerDelegate: PickerDelegateController!
    private var actionSheetTableviewControllerDelegate: ActionSheetTableViewControllerDelegate!
    //    private var qlPreviewVC: QLPreviewController!
    private var itemUrls: [URL?] = []
    
    private var docForPhotos: DocumentModel = DocumentModel()
    private var docForPan: DocumentModel = DocumentModel()
    private var docForAadhar: DocumentModel = DocumentModel()
    
    
    //MARK: - Override methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpDocumentView()
        self.setUpIPadConstraints()
        self.setUpView()
        self.setUpHeader()
        self.fontSetUp()
        self.getProfileDataFromWebServices()
        
        
        self.arrGenderOptions.append(CommonLabels.select)
        self.arrGenderOptions += self.selectedGenderType.arrayTitle
        self.arrStateOptions.append(RegistrtionLabels.selectState)
        self.arrStateOptions.append("MAHARASHRTA")
        
        arrDistrictOptions = [DropDownModel(dict: [DropDownModel.Keys.title.rawValue: RegistrtionLabels.selectDistrict,
                                                   DropDownModel.Keys.id.rawValue: ""])]
        arrCityOptions = [DropDownModel(dict: [DropDownModel.Keys.title.rawValue: RegistrtionLabels.selectCity,
                                               DropDownModel.Keys.id.rawValue: ""])]
        
        self.view.layoutIfNeeded()
//        self.scrollView.setContentOffset(CGPoint(x: 0, y: self.scrollView.contentSize.height - self.scrollView.frame.height), animated: true)
    }
    
    fileprivate func setUpView() {
        //        self.heightForDocumentPan.isActive = false
        //        self.heightForDocumentAddharCard.isActive = false
        
        
        
        self.viewForScroll.backgroundColor = UIColor.darkBlue.withAlphaComponent(0.2)
        self.viewMain.setCustomCornerRadius(radius: 12)
        self.viewForShadow.setCustomCornerRadius(radius: 12)
        self.viewForShadow.addShadow()
        self.viewBtnSubmitGradient.setCustomCornerRadius(radius: 4)
        self.viewBtnSubmitGradient.backgroundColor = .customOrange
        self.viewOTP.isHidden = true
        self.txtOTP.text = ""
        self.viewGetResendOTP.isHidden = true
        self.viewGetResendOTP.setCustomCornerRadius(radius: self.viewGetResendOTP.frame.size.height / 2)
        self.viewBtnSubmitGradient.setCustomCornerRadius(radius: self.viewBtnSubmitGradient.frame.size.height / 2)
        self.viewBtnSubmitGradient.addGradient(ofColors: [UIColor.themeDarkOrangeClr.cgColor, UIColor.themeLightOrangeClr.cgColor], direction: .leftRight)
        
        self.topConstraintStackView.constant = IS_IPAD ? 110 : 75

        //       r.imgProfile.layer.cornerRadius = IS_iPad ? 70 : 40
        
        self.vieBgImg.setFullCornerRadius()
        self.vieBgImg.addShadow()
        self.imgProfile.setFullCornerRadius()
        
        btnEditProfile.isHidden = true
        imgEditProfile.isHidden = true
        
        
        self.viewImgSize.isHidden = true
        
        
        self.viewSubmit.isHidden = true
        
        self.txtMobileNo.delegate = self
        self.txtOTP.delegate = self
        self.txtAadhaarNo.delegate = self
        self.txtBankAccountNo.delegate = self
        self.txtPanNo.delegate = self
        
        self.lblUnderLine.forEach { (lbl) in
            lbl.isHidden = true
            lbl.backgroundColor = .customSeparator
        }
        
        self.lblAsterisks.forEach { (lbl) in
            lbl.isHidden = true
        }
        
        self.txtValues.forEach({
            $0.isUserInteractionEnabled = false
        })
        
        self.imgDropDown.forEach({
            $0.isHidden = true
            $0.tintColor = .customgreenTheamClr
            $0.image = .ic_down_arrow
        })
        
        self.btnDropdown.forEach({
            $0.isUserInteractionEnabled = false
        })
        
        self.lblValueDropDown.forEach({
            $0.text = CommonLabels.select
        })
        lblState.text = RegistrtionLabels.selectState
        lblDistrict.text = RegistrtionLabels.selectDistrict
        lblCity.text = RegistrtionLabels.selectCity
        
        self.imgViewCalnder.isHidden = true
        self.imgViewCalnder.image = .calender
        self.imgViewCalnder.tintColor = .customgreenTheamClr
        
    }
    
    fileprivate func fontSetUp() {
        self.lblTitles.forEach({
            $0.font = .regularTitleFont
            $0.textColor = .customBlackTitle
        })
        
        for lbl in lblAsterisks {
            lbl.textColor = .customRed
            lbl.font = .regularTitleFont
            lbl.text = "*"
        }
        
        self.txtValues.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
            //            $0.textAlignment = isRightToLeftLocalization ? .right : .left
        })
        
        self.lblValueDropDown.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
        })
        
        lblGetResendOTP.text = SignUp_EditProfileLabels.get_Otp
        lblGetResendOTP.textColor = .white
        lblGetResendOTP.font = .regularTitleFont
        lblOTPTime.font = .regularTitleFont
        lblOTPTime.textColor = .customBlack
        lblOTPSent.font = .regularTitleFont
        
        self.btnSubmit.titleLabel?.font = UIFont.semiboldLargeFont
        
        self.lblMaxImgSize.text = CommonLabels.maxImgMBAllowed.replacingOccurrences(of: "abc", with: getString(anything: imageSizeLimit))
    }
    
    fileprivate func setUpDocumentView() {
        
        self.viewDocumentForPan.subviews.forEach({
            $0.removeFromSuperview()
        })
        
        self.viewDocuymentForAddharCard.subviews.forEach({
            $0.removeFromSuperview()
        })
        
        self.documentForPan.configureTitle(str: ProfileLabels.AttachPan,
                                           btnTag: 0,
                                           showFormatBtn: false,
                                           hideAsterisk: true,
                                           isFileNameShow: true)
        self.documentForPan.btnDocument.isUserInteractionEnabled = false
        self.documentForPan.btnViewDoc.isUserInteractionEnabled = true
        self.documentForPan.btnDocument.addTarget(self, action: #selector(btnDocumentTapped), for: .touchUpInside)
        self.documentForPan.btnViewDoc.addTarget(self, action: #selector(btnViewDocumentClicked), for: .touchUpInside)
        self.documentForPan.lblMaxSize.text = CommonLabels.maxDocMBAllowed.replacingOccurrences(of: "abc", with: getString(anything: imageSizeLimit))// "Max. \(documentSizeLimit)MB (Allowed pdf/image)"
        self.viewDocumentForPan.addArrangedSubview(self.documentForPan)
        
        self.documentForAddharCard.configureTitle(str: ProfileLabels.AttachAdharCard,
                                                   btnTag: 1,
                                                   showFormatBtn: false,
                                                   hideAsterisk: true,
                                                   isFileNameShow: true)
        self.documentForAddharCard.btnDocument.isUserInteractionEnabled = false
        self.documentForAddharCard.btnViewDoc.isUserInteractionEnabled = true
        
        self.documentForAddharCard.btnDocument.addTarget(self, action: #selector(btnDocumentTapped), for: .touchUpInside)
        self.documentForAddharCard.btnViewDoc.addTarget(self, action: #selector(btnViewDocumentClicked), for: .touchUpInside)
        self.documentForAddharCard.lblMaxSize.text = CommonLabels.maxDocMBAllowed.replacingOccurrences(of: "abc", with: getString(anything: imageSizeLimit))// "Max. \(documentSizeLimit)MB (Allowed pdf/image)"
        self.viewDocuymentForAddharCard.addArrangedSubview(self.documentForAddharCard)
    }

    
    fileprivate func setDistrictData() {
//        arrDistrictOptions = [DropDownModel(dict: [DropDownModel.Keys.title.rawValue: CommonLabels.select,
//                                                   DropDownModel.Keys.id.rawValue: ""]),
//                              DropDownModel(dict: [DropDownModel.Keys.title.rawValue: "MUMBAI",
//                                                   DropDownModel.Keys.id.rawValue: "1"]),
//                              DropDownModel(dict: [DropDownModel.Keys.title.rawValue: "NASHIK",
//                                                   DropDownModel.Keys.id.rawValue: "2"]),
//                              DropDownModel(dict: [DropDownModel.Keys.title.rawValue: "PUNE",
//                                                   DropDownModel.Keys.id.rawValue: "3"])]
//
//        arrCityOptions = [DropDownModel(dict: [DropDownModel.Keys.title.rawValue: CommonLabels.select,
//                                               DropDownModel.Keys.id.rawValue: ""]),
//                          DropDownModel(dict: [DropDownModel.Keys.title.rawValue: "AMBEGAON",
//                                               DropDownModel.Keys.id.rawValue: "1"]),
//                          DropDownModel(dict: [DropDownModel.Keys.title.rawValue: "HAVELI",
//                                               DropDownModel.Keys.id.rawValue: "2"])]
    }
    
    fileprivate func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.profile)
        setHeaderView_BackImage()
        showSearch()
        self.viewHeader.btnSearch.setImage(.ic_edit, for: .normal)
        self.viewHeader.btnSearch.removeTarget(self, action: #selector(btnSearchTapped(_:)), for: .touchUpInside)
        self.viewHeader.btnSearch.addTarget(self, action: #selector(self.TapOnEditProfile), for: .touchUpInside)
    }
    
    fileprivate func setUpIPadConstraints() {
        changeLeadingTrailingForiPad(view: viewForScroll)
        changeHeightForiPad(view: txtFullName)
        changeHeightForiPad(view: btnSubmit)
    }
    
    fileprivate func invalidateTimer() {
        totalSecond = 120
        viewOTP.isUserInteractionEnabled = true
        self.txtOTP.text = ""
        lblOTPTime.text = ""
        timer?.invalidate()
        timer?.fire()
    }
    
    fileprivate func setupData() {
        /* self.txtEmail.text = "dhruv000v@gmail.com"
         self.txtMobileNo.text = "9409408541"
         self.strMobileNumber = "9409408541"
         self.txtFullName.text = "Dhruv"
         self.txtMiddleName.text = "Dhiren"
         self.txtLastName.text = "Patel"
         self.txtAddress.text = "AHMEDABAD"
         self.txtPanNo.text = "ADROPP8088k"
         self.txtAadhaarNo.text = "1234567899876"
         self.txtBankName.text = "IDBI"
         self.txtBankAccountNo.text = "12345678987456321"
         self.selectedGenderType = .male
         self.lblGender.text = self.selectedGenderType.displayString
         self.lblOTPSent.text = SignUp_EditProfileLabels.otp_has_been_sent_on_registered_mobile_number + " \("9409408541".substring(to: 2))xxxxxx\("9409408541".substring(from: 8))"*/
        
        
        if getBoolean(anything: UserPreferences.bool(forKey: UserPreferencesKeys.General.isUserLoggedIn)) {
            
            self.txtEmail.text = UserModel.currentUser.email.decryptStr.is_trimming_WS_NL_to_String ?? "-"
            self.txtMobileNo.text = UserModel.currentUser.mobileno.decryptStr.is_trimming_WS_NL_to_String ?? "-"
            self.strMobileNumber = UserModel.currentUser.mobileno.decryptStr.is_trimming_WS_NL_to_String ?? "-"
            self.txtFullName.text = UserModel.currentUser.first_name.decryptStr.is_trimming_WS_NL_to_String ?? "-"
            self.txtMiddleName.text = UserModel.currentUser.middle_name.decryptStr.is_trimming_WS_NL_to_String ?? "-"
            self.txtLastName.text = UserModel.currentUser.last_name.decryptStr.is_trimming_WS_NL_to_String ?? "-"
            self.txtAddress.text = UserModel.currentUser.address.decryptStr.is_trimming_WS_NL_to_String ?? "-"
            self.txtPanNo.text = UserModel.currentUser.pan_no.decryptStr.is_trimming_WS_NL_to_String ?? "-"
            self.txtAadhaarNo.text = UserModel.currentUser.adhaar_card.decryptStr.is_trimming_WS_NL_to_String ?? "-"
            self.txtBankName.text = UserModel.currentUser.bank_name.decryptStr.is_trimming_WS_NL_to_String ?? "-"
                self.txtBankAccountNo.text = UserModel.currentUser.bank_account_number.decryptStr.is_trimming_WS_NL_to_String ?? "-"
            self.imgProfile.downloadImage(with: UserModel.currentUser.imageUrl.decryptStr, placeholderImage: .ic_user_placeholder, options: .continueInBackground, progess: nil, completed: nil)
            
            
            if !UserModel.currentUser.pan_card_file.decryptStr.isStringEmpty,
               let getURL = URL(string: UserModel.currentUser.pan_card_file.decryptStr) ?? URL(string: UserModel.currentUser.pan_card_file.decryptStr.urlQueryAllowed) {
                var dict: JSONObject = [:]
                dict[MultipartRequestKeys.key_documentKey] = ProfileWebServiceModel.keys.pan_card_file.rawValue
                dict[MultipartRequestKeys.key_documentURL] = getURL
                dict[MultipartRequestKeys.key_documentType] = getURL.pathExtension
                dict[MultipartRequestKeys.key_documentTitle] = getURL.lastPathComponent
                self.documentForPan.setViewMode(fileName: getURL.lastPathComponent)
                self.webServiceModel.scriptPanDic = dict
            } else {
                self.documentForPan.setViewMode(fileName: "")
            }
            
            if !UserModel.currentUser.adhaar_card_file.decryptStr.isStringEmpty,
               let getURL = URL(string: UserModel.currentUser.adhaar_card_file.decryptStr) ?? URL(string: UserModel.currentUser.adhaar_card_file.decryptStr.urlQueryAllowed) {
                var dict: JSONObject = [:]
                dict[MultipartRequestKeys.key_documentKey] = ProfileWebServiceModel.keys.adhaar_card_file.rawValue
                dict[MultipartRequestKeys.key_documentURL] = getURL
                dict[MultipartRequestKeys.key_documentType] = getURL.pathExtension
                dict[MultipartRequestKeys.key_documentTitle] = getURL.lastPathComponent
                self.documentForAddharCard.setViewMode(fileName: getURL.lastPathComponent)
                self.webServiceModel.scriptAdharCardDic = dict
            } else {
                self.documentForAddharCard.setViewMode(fileName: "")
            }
            
            self.lblDateOfBirth.text = UserModel.currentUser.date_of_birth.decryptStr.is_trimming_WS_NL_to_String ?? "-"
            
            let gender = getInteger(anything: UserModel.currentUser.gender.decryptStr)
            if gender == GenderTypeInt.male.rawValue {
                self.selectedGenderType = .male
                self.lblGender.text = self.selectedGenderType.displayString
                
            } else if gender == GenderTypeInt.female.rawValue {
                self.selectedGenderType = .female
                self.lblGender.text = self.selectedGenderType.displayString
                
            } else if gender == GenderTypeInt.Transgender.rawValue {
                self.selectedGenderType = .transgender
                self.lblGender.text = self.selectedGenderType.displayString
                
            } else {
                self.lblGender.text = CommonLabels.select
            }
            
            
            if UserModel.currentUser.ms_states_id.decryptStr != "" {
                //                if UserModel.currentUser.ms_states_id == "24"{
                self.webServiceModel.state_id = UserModel.currentUser.ms_states_id.decryptStr
                self.lblState.text = "MAHARASHRTA"
                getWebserviceforDistrict(isSetData: true)
                //                }
            }
            
            self.webServiceModel.documentModels.removeAll()
            self.docForPhotos = DocumentModel()
            self.docForPan = DocumentModel()
            self.docForAadhar = DocumentModel()

            
            //            if UserModel.currentUser.ms_districts_id != "" {
            //                if arrDistrictOptions.count >= 0{
            //                    let districtData = arrDistrictOptions.filter({$0.id == UserModel.currentUser.ms_districts_id})
            //                    lblDistrict.text = districtData.first?.title
            //                }
            //                webServiceModel.district_code = UserModel.currentUser.ms_districts_id
            //                getWebserviceCity(isSetData: true)
            //            }
            
            //            if UserModel.currentUser.ms_cities_id != "" {
            //                if arrCityOptions.count >= 0{
            //                    let districtData = arrCityOptions.filter({$0.id == UserModel.currentUser.ms_cities_id})
            //                    lblCity.text = districtData.first?.title
            //                }
            //            }
            
            
            
            
            
        }
    }
    
    //MARK:- Selector Methods
    @objc fileprivate func timerCalled(_ t: Timer) {
        if (totalSecond == 0) {
            invalidateTimer()
            viewGetResendOTP.isUserInteractionEnabled = true
            viewGetResendOTP.backgroundColor = .customOrange
            return
        }
        totalSecond = totalSecond - 1
        lblOTPTime.text = String(format: "%02d:%02d", (totalSecond % 3600) / 60, (totalSecond % 3600) % 60)
    }
    
    @objc fileprivate func TapOnEditProfile() {
        self.viewHeader.btnSearch.isHidden = true
        setUpHeaderTitle(strHeaderTitle: Headers.edit_profile)
        self.viewSubmit.isHidden = false
        
        self.lblUnderLine.forEach { (lbl) in
            lbl.isHidden = false
        }
        self.lblAsterisks.forEach { (lbl) in
            lbl.isHidden = false
        }
        
        self.txtValues.forEach({
            $0.isUserInteractionEnabled = true
        })
        
        self.imgDropDown.forEach({
            $0.isHidden = false
        })
        
        self.btnDropdown.forEach({
            $0.isUserInteractionEnabled = true
        })
        self.imgViewCalnder.isHidden = false
        
        self.viewImgSize.isHidden = false
        self.btnEditProfile.isHidden = false
        self.imgEditProfile.isHidden = false
        
        self.documentForAddharCard.setEditMode(fileName: UserModel.currentUser.adhaar_card_file.decryptStr.nsString.lastPathComponent, hideAsterisk: false)
        self.documentForPan.setEditMode(fileName: UserModel.currentUser.pan_card_file.decryptStr.nsString.lastPathComponent, hideAsterisk: false)
        
        self.documentForAddharCard.btnDocument.isUserInteractionEnabled = true
        self.documentForAddharCard.btnViewDoc.isUserInteractionEnabled = true
        self.documentForPan.btnDocument.isUserInteractionEnabled = true
        self.documentForPan.btnViewDoc.isUserInteractionEnabled = true
        
        
        self.viewDocumentForPan.superview?.constraints.forEach({ $0.constant = 10 })
        self.viewDocuymentForAddharCard.superview?.constraints.forEach({ $0.constant = 10 })
        
        self.viewDocumentForPan.superview?.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        self.viewDocuymentForAddharCard.superview?.backgroundColor = UIColor.lightGray.withAlphaComponent(0.2)
        self.viewDocumentForPan.superview?.setCustomCornerRadius(radius: 8)
        self.viewDocuymentForAddharCard.superview?.setCustomCornerRadius(radius: 8)
        
        self.txtEmail.text = UserModel.currentUser.email.decryptStr.is_trimming_WS_NL_to_String ?? ""
        self.txtMobileNo.text = UserModel.currentUser.mobileno.decryptStr.is_trimming_WS_NL_to_String ?? ""
        self.strMobileNumber = UserModel.currentUser.mobileno.decryptStr.is_trimming_WS_NL_to_String ?? ""
        self.txtFullName.text = UserModel.currentUser.first_name.decryptStr.is_trimming_WS_NL_to_String ?? ""
        self.txtMiddleName.text = UserModel.currentUser.middle_name.decryptStr.is_trimming_WS_NL_to_String ?? ""
        self.txtLastName.text = UserModel.currentUser.last_name.decryptStr.is_trimming_WS_NL_to_String ?? ""
        self.txtAddress.text = UserModel.currentUser.address.decryptStr.is_trimming_WS_NL_to_String ?? ""
        self.txtPanNo.text = UserModel.currentUser.pan_no.decryptStr.is_trimming_WS_NL_to_String ?? ""
        self.txtAadhaarNo.text = UserModel.currentUser.adhaar_card.decryptStr.is_trimming_WS_NL_to_String ?? ""
        self.txtBankName.text = UserModel.currentUser.bank_name.decryptStr.is_trimming_WS_NL_to_String ?? ""
        self.txtBankAccountNo.text = UserModel.currentUser.bank_account_number.decryptStr.is_trimming_WS_NL_to_String ?? ""
        self.lblDateOfBirth.text = UserModel.currentUser.date_of_birth.decryptStr.is_trimming_WS_NL_to_String ?? "-"
        
    }
    
    @objc fileprivate func btnDocumentTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.documentTag = sender.tag
        let tableViewController = ActionSheetTableViewController()
        tableViewController.tableView.delegate = tableViewController
        tableViewController.tableView.dataSource = tableViewController
        let alertController = UIAlertController(title: nil, message: CommonLabels.select, preferredStyle: .actionSheet)
        
        tableViewController.alertController = alertController
        tableViewController.labelTexts = [CommonButtons.camera, CommonButtons.photo_library, CommonButtons.document]
        tableViewController.imgNames = [ImageNameConstants.ic_camera, ImageNameConstants.ic_photo_library,ImageNameConstants.ic_document]
        tableViewController.imgTintColor = .customBlue
        tableViewController.setUpView()
        actionSheetTableviewControllerDelegate = ActionSheetTableViewControllerDelegate(tbl: tableViewController, selectedIndex: { (selectedIndex) in
            let imagePicker = UIImagePickerController()
            if (selectedIndex == 0) { //for Camera/Capture Photo
                if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
                    self.pickerControllerDelegate = PickerDelegateController(imagePicker: imagePicker, fileSizeLimit: imageSizeLimit, delegate: self)
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .camera
                    imagePicker.modalPresentationStyle = .overFullScreen
                    self.present(imagePicker, animated: true, completion: nil)
                } else {
                    UIAlertController.showWith(title: ProfileLabels.Camera_unavailable, msg: CommonMessages.msg_This_Device_Has_No_Camera, style: .alert, onVC: self, actionTitles: [CommonLabels.OK], actionStyles: [.default], actionHandlers: [nil])
                }
                
            } else if (selectedIndex == 1){ //For Photo Gallary
                self.pickerControllerDelegate = PickerDelegateController(imagePicker: imagePicker, fileSizeLimit: imageSizeLimit, delegate: self)
                imagePicker.sourceType = .photoLibrary
                imagePicker.modalPresentationStyle = .overFullScreen
                self.present(imagePicker, animated: true, completion: nil)
                
            }else if (selectedIndex == 2) { //document
                let aDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: arrdocTypesForProfile, in: .import)
                self.documentPickerViewControllerDelegate = DocumentPickerViewControllerDelegate(VC: aDocumentPickerViewController, delegate: self, sender: sender, file_Size: imageSizeLimit)
                aDocumentPickerViewController.modalPresentationStyle = .overFullScreen
                self.present(aDocumentPickerViewController, animated: true, completion: nil)
            }
        })
        tableViewController.preferredContentSize.height = 70 * CGFloat(tableViewController.labelTexts.count)
        alertController.preferredContentSize.height = 70 * CGFloat(tableViewController.labelTexts.count)
        alertController.set(vc: tableViewController)
        alertController.addAction(withTitle: CommonButtons.cancel, style: .destructive) { (cancelAction) in
        }
        alertController.modalPresentationStyle = .overFullScreen
        alertController.popoverPresentationController?.sourceView = sender
        present(alertController, animated: true, completion: nil)
       
        /*
        UIView.hideKeyBoard()
        let aDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: arrdocTypesForProfile, in: .import)
        documentPickerViewControllerDelegate = DocumentPickerViewControllerDelegate(VC: aDocumentPickerViewController, delegate: self, sender: sender)
        self.present(aDocumentPickerViewController, animated: true, completion: nil)*/
//
//        if sender.tag == 0{
//            let dvc = PDFDocumentViewerViewController.instantiate(appStoryboard: .pdfDocumentViewer)
//            dvc.documentURLStr = UserModel.currentUser.pan_card_file
//            self.navigationController?.pushViewController(dvc, animated: true)
//        }else{
//            let dvc = PDFDocumentViewerViewController.instantiate(appStoryboard: .pdfDocumentViewer)
//            dvc.documentURLStr = UserModel.currentUser.adhaar_card_file
//            self.navigationController?.pushViewController(dvc, animated: true)
//        }
        
    }
    
    @objc fileprivate func btnViewDocumentClicked(_ sender: UIButton) {
        
        var urlString = ""
        if sender.tag == 0 {
            urlString = UserModel.currentUser.pan_card_file.decryptStr
        } else {
            urlString = UserModel.currentUser.adhaar_card_file.decryptStr
        }
        
        if !urlString.isStringEmpty {
//            let dvc = PDFDocumentViewerViewController.instantiate(appStoryboard: .pdfDocumentViewer)
//            dvc.documentURLStr = urlString
//            self.navigationController?.pushViewController(dvc, animated: true)
            openURL(urlString:  urlString)
        }
    }
    
    @IBAction func TapOnGetResendOTP(_ sender: UIButton) {
        if self.isValidMobileNumber() {
            self.getOTP()
        }
    }
    
    @IBAction func TapOnEditProfileImage(_ sender: UIButton) {
        UIView.hideKeyBoard()
        self.documentTag = 100
        let tableViewController = ActionSheetTableViewController()
        tableViewController.tableView.delegate = tableViewController
        tableViewController.tableView.dataSource = tableViewController
        let alertController = UIAlertController(title: nil, message: CommonLabels.select, preferredStyle: .actionSheet)
        tableViewController.alertController = alertController
        
        tableViewController.labelTexts = [CommonButtons.camera,CommonButtons.photo_library]
        tableViewController.imgNames = [ImageNameConstants.ic_camera, ImageNameConstants.ic_photo_library]
        
        
        
        tableViewController.imgTintColor = .black
        tableViewController.setUpView()
        actionSheetTableviewControllerDelegate = ActionSheetTableViewControllerDelegate(tbl: tableViewController, selectedIndex: { (selectedIndex) in
            
            let imagePicker = UIImagePickerController()
            
            if (selectedIndex == 0) && sender == self.btnEditProfile { //for Camera/Capture Photo
                if (UIImagePickerController.isSourceTypeAvailable(.camera)) {
                    self.pickerControllerDelegate = PickerDelegateController(imagePicker: imagePicker, fileSizeLimit: imageSizeLimit, delegate: self)
                    imagePicker.allowsEditing = false
                    imagePicker.sourceType = .camera
                    imagePicker.modalPresentationStyle = .overFullScreen
                    self.present(imagePicker, animated: true, completion: nil)
                } else {
                    UIAlertController.showWith(title: ProfileLabels.Camera_unavailable, msg: CommonMessages.msg_This_Device_Has_No_Camera, style: .alert, onVC: self, actionTitles: [CommonLabels.OK], actionStyles: [.default], actionHandlers: [nil])
                }
                
            } else if (selectedIndex == 1) && sender == self.btnEditProfile { //For Photo Gallary
                self.pickerControllerDelegate = PickerDelegateController(imagePicker: imagePicker, fileSizeLimit: imageSizeLimit, delegate: self)
                imagePicker.sourceType = .photoLibrary
                imagePicker.modalPresentationStyle = .overFullScreen
                self.present(imagePicker, animated: true, completion: nil)
                
            }
        })
        tableViewController.preferredContentSize.height = 70 * CGFloat(tableViewController.labelTexts.count)
        alertController.preferredContentSize.height = 70 * CGFloat(tableViewController.labelTexts.count)
        alertController.set(vc: tableViewController)
        alertController.addAction(withTitle: CommonLabels.cancel, style: .destructive) { (cancelAction) in
            
        }
        alertController.modalPresentationStyle = .overFullScreen
        alertController.popoverPresentationController?.sourceView = sender
        present(alertController, animated: true, completion: nil)
        
        
        /* //Code for popup for new document to be selected.
         let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
         alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
         self.openCamera()
         }))
         
         alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
         self.openGallary()
         }))
         
         alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
         
         /*If you want work actionsheet on ipad
          then you have to use popoverPresentationController to present the actionsheet,
          otherwise app will crash on iPad */
         switch UIDevice.current.userInterfaceIdiom {
         case .pad:
         alert.popoverPresentationController?.sourceView = sender
         alert.popoverPresentationController?.sourceRect = sender.bounds
         alert.popoverPresentationController?.permittedArrowDirections = .up
         default:
         break
         }
         
         self.present(alert, animated: true, completion: nil)*/
        
    }
    
    @IBAction func TapOnSubmitBtn(_ sender: LocalizedButton) {
        if self.isValid() {
            resignFirstResponder()
            self.editProfileDataWebServices()
            
            //            if (self.viewGetResendOTP.isHidden == false) {
            //                self.verifyOtp()
            //            } else {
            //                self.editProfileDataWebServices()
            //            }
        }
    }
    
    @IBAction func TapOnGenderBtn(_ sender: UIButton) {
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblGender.text, arrSource: [self.arrGenderOptions], doneAction: { (rows, value) in
            
            self.lblGender.text = getString(anything: value)
            if self.arrGenderOptions[1] == self.lblGender.text {
                self.selectedGenderType = .male
            } else if self.arrGenderOptions[2] == self.lblGender.text {
                self.selectedGenderType = .female
            } else if self.arrGenderOptions[3] == self.lblGender.text {
                self.selectedGenderType = .transgender
            }
            
        }, animated: true, origin: sender)
    }
    
    @IBAction func TapOnDateOfBirthBtn(_ sender: UIButton) {
        let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblDateOfBirth.text), inputFormat: DateFormats.dd_MM_yyyy), maximumDate: nil, minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dobString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.yyyy_MM_dd_dashed)
            self.lblDateOfBirth.text = dobString
            
        }, animated: true, origin: sender)
        
        datePickerVC.lblTitleText = CommonLabels.select
        datePickerVC.lblTitleColor = .customDarkBlueGradient
        datePickerVC.lblTitleFont = .boldMediumFont
        datePickerVC.btnCancelTitle = CommonButtons.cancel
        datePickerVC.btnCancelTitleColor = .customSubtitle
        datePickerVC.btnCancelTitleFont = .regularValueFont
        datePickerVC.btnDoneTitle = CommonButtons.done
        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)
    }
    
    @IBAction func TapOnSelectStateBtn(_ sender: UIButton) {
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblState.text, arrSource: [self.arrStateOptions], doneAction: { (rows, value) in
            self.lblState.text = getString(anything: value)
            if getString(anything: value) == "MAHARASHRTA" {
                self.getWebserviceforDistrict()
            }else{
                self.lblDistrict.text = RegistrtionLabels.selectDistrict
                self.arrDistrictOptions.removeAll()
                self.arrDistrictOptions.append(DropDownModel(dict: [DropDownModel.Keys.title.rawValue: RegistrtionLabels.selectDistrict,
                                                                    DropDownModel.Keys.id.rawValue: ""]))
                self.lblCity.text = RegistrtionLabels.selectCity
                self.arrCityOptions.removeAll()
                self.arrCityOptions.append(DropDownModel(dict: [DropDownModel.Keys.title.rawValue: RegistrtionLabels.selectCity,
                                                                DropDownModel.Keys.id.rawValue: ""]))
            }
            
        }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func TapOnSslectDistrictBtn(_ sender: UIButton) {
        let list = arrDistrictOptions.map({ $0.title })
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblDistrict.text, arrSource: [list], doneAction: { (rows, value) in
            self.lblDistrict.text = getString(anything: value)
            self.dismiss(animated: true, completion: nil)
            if let index = rows.first{
                self.webServiceModel.districts_id = self.arrDistrictOptions[index].id
                self.lblCity.text = RegistrtionLabels.selectCity
                if !self.arrDistrictOptions[index].id.isEmptyString{
                    self.getWebserviceCity()
                }
            }
        }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func TapOnSelectCityBtn(_ sender: UIButton) {
        let list = arrCityOptions.map({ $0.title })
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblCity.text, arrSource: [list], doneAction: { (rows, value) in
            
            self.lblCity.text = getString(anything: value)
            self.webServiceModel.city_id = self.arrCityOptions[rows.first ?? 0].id
            
        }, animated: true, origin: sender, onVC: self)
    }
    
    func ViewProfileMode() {
        self.viewHeader.btnSearch.isHidden = false
        setUpHeaderTitle(strHeaderTitle: Headers.profile)
        self.viewSubmit.isHidden = true
        
        self.viewImgSize.isHidden = true
        self.btnEditProfile.isHidden = true
        self.imgEditProfile.isHidden = true

        
        self.lblUnderLine.forEach { (lbl) in
            lbl.isHidden = true
        }
        self.lblAsterisks.forEach { (lbl) in
            lbl.isHidden = true
        }
        
        self.txtValues.forEach({
            $0.isUserInteractionEnabled = false
        })
        
        self.imgDropDown.forEach({
            $0.isHidden = true
        })
        
        self.btnDropdown.forEach({
            $0.isUserInteractionEnabled = false
        })
        
        self.imgViewCalnder.isHidden = true
        
        self.viewDocumentForPan.superview?.constraints.forEach({ $0.constant = 0})
        self.viewDocuymentForAddharCard.superview?.constraints.forEach({ $0.constant = 0 })
        
        self.viewDocumentForPan.superview?.backgroundColor = .clear
        self.viewDocuymentForAddharCard.superview?.backgroundColor = .clear
        self.viewDocumentForPan.superview?.setCustomCornerRadius(radius: 0)
        self.viewDocuymentForAddharCard.superview?.setCustomCornerRadius(radius: 0)
        
        self.documentForAddharCard.btnDocument.isUserInteractionEnabled = false
        self.documentForAddharCard.btnViewDoc.isUserInteractionEnabled = true
        self.documentForPan.btnDocument.isUserInteractionEnabled = false
        self.documentForPan.btnViewDoc.isUserInteractionEnabled = true
        
        //        self.imgGenderDownArrow.isHidden = true
        //        self.btnSelectGender.isUserInteractionEnabled = false
    }
}

extension ProfileViewController : PickerDelegate {
    
    func imageViewUrl(url: URL?, data: Data?, errorMsg: String) {
        
        switch self.documentTag {
        case 0:
            if let imgdata = data{
                if let img = UIImage(data: imgdata){
                    if let imgUrl = url {
    //                    self.documentsForPhotos.url = imgUrl
                        self.docForPan.key = ProfileWebServiceModel.keys.pan_card_file.rawValue
                        self.docForPan.title = imgUrl.lastPathComponent
                        self.docForPan.type = imgUrl.pathExtension
                        self.documentForPan.setFileName(fileName: imgUrl.lastPathComponent)

                    }
                    self.docForPan.document = img
                }
            }
        case 1:
            if let imgdata = data{
            if let img = UIImage(data: imgdata){
                if let imgUrl = url {
//                    self.documentsForPhotos.url = imgUrl
                    self.docForAadhar.key = ProfileWebServiceModel.keys.adhaar_card_file.rawValue
                    self.docForAadhar.title = imgUrl.lastPathComponent
                    self.docForAadhar.type = imgUrl.pathExtension
                    self.documentForAddharCard.setFileName(fileName: imgUrl.lastPathComponent)
                }
                self.docForAadhar.document = img
            }
        }
        case 100:
        
            if let imgdata = data{
            if let img = UIImage(data: imgdata){
                if let imgUrl = url {
                    //                    self.documentsForPhotos.url = imgUrl
                    self.docForPhotos.key = ProfileWebServiceModel.keys.picture.rawValue
                    self.docForPhotos.title = imgUrl.lastPathComponent
                    self.docForPhotos.type = imgUrl.pathExtension
                }
                self.docForPhotos.document = img
                self.imgProfile.image = img
            }
            }
        default:
            break;
        }
    }
}

//MARK: - UIImagePickerControllerDelegate

extension ProfileViewController:  UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            imagePicker.delegate = self
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: ProfileLabels.Camera_unavailable, message: CommonMessages.msg_This_Device_Has_No_Camera, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: CommonLabels.OK, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        imagePicker.delegate = self
        
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let editedImage = info[UIImagePickerController.InfoKey(rawValue: UIImagePickerController.InfoKey.editedImage.rawValue)] as? UIImage{
            self.imgProfile.image = editedImage
            
            let model = DocumentModel()
            model.document = editedImage
            model.title = getString(anything: Int(Date().timeIntervalSince1970))
            
            if let asset = info[UIImagePickerController.InfoKey(rawValue: "UIImagePickerControllerPHAsset")] as? PHAsset{
                if let fileName = asset.value(forKey: "filename") as? String{
                    print(fileName)
                }
            }
            //            model.type = imgType
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.isNavigationBarHidden = false
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK:- Validations
extension ProfileViewController {
    
    func isValid() -> Bool {
        var isValid : Bool = false
        if getString(anything: self.txtFullName.text).isStringEmpty {
            Global.shared.showBanner(message: ProfileMessages.firstNameIsRequired)
            self.txtFullName.becomeFirstResponder()
            
        } else if getString(anything: self.txtMiddleName.text).isStringEmpty {
            Global.shared.showBanner(message: ProfileMessages.middleNameIsRequired)
            self.txtMiddleName.becomeFirstResponder()
            
        }else if getString(anything: self.txtLastName.text).isStringEmpty {
            Global.shared.showBanner(message: ProfileMessages.lastNameIsRequired)
            self.txtLastName.becomeFirstResponder()
            
        }else if isValidMobileNumber() == false {
            
        } else if getString(anything: self.txtAddress.text).isStringEmpty {
            Global.shared.showBanner(message: ProfileMessages.addressIsRequired)
            self.txtAddress.becomeFirstResponder()
            
        } else if getString(anything: self.txtEmail.text).isStringEmpty {
            Global.shared.showBanner(message: ProfileMessages.emailIsRequired)
            self.txtEmail.becomeFirstResponder()
            
        } else if !getString(anything: self.txtEmail.text).isValidEmail {
            Global.shared.showBanner(message: ProfileMessages.pleaseEnteraValidEmailAddress)
            self.txtEmail.becomeFirstResponder()
            
        }   else if (self.lblGender.text ?? "").isStringEmpty || self.lblGender.text == CommonLabels.select {
            Global.shared.showBanner(message: ProfileMessages.pleaseSelectGender)
            
        } else if (self.lblDateOfBirth.text ?? "").isStringEmpty || self.lblDateOfBirth.text == CommonLabels.select {
            Global.shared.showBanner(message: ProfileMessages.dateOfBirthIsRequired)
            
        }else if (self.lblState.text ?? "").isStringEmpty || self.lblState.text == RegistrtionLabels.selectState {
            Global.shared.showBanner(message: ProfileMessages.pleaseSelectYourState)
            
        } else if (self.lblDistrict.text ?? "").isStringEmpty || self.lblDistrict.text == RegistrtionLabels.selectDistrict {
            Global.shared.showBanner(message: ProfileMessages.pleaseSelectYourDistrict)
            
        }/*  else if getString(anything: self.txtOTP.text).isStringEmpty && (self.viewGetResendOTP.isHidden == false) {
          Global.shared.showBanner(message: ProfileMessages.msg_please_enter_OTP)
          self.txtOTP.becomeFirstResponder()
          
          } else if getString(anything: self.txtPanNo.text).isStringEmpty {
              Global.shared.showBanner(message: ProfileMessages.panNoIsRequired)
              self.txtPanNo.becomeFirstResponder()
              
         }*/else if isValidPanNo() == false {
            
         } else if getString(anything: self.txtAadhaarNo.text).isStringEmpty {
              Global.shared.showBanner(message: ProfileMessages.aadhaarNoIsRequired)
              self.txtAadhaarNo.becomeFirstResponder()
              
          }/* else if getString(anything: self.txtBankName.text).isStringEmpty {
              Global.shared.showBanner(message: ProfileMessages.bankNameIsRequired)
              self.txtBankName.becomeFirstResponder()
              
          }*/ else if !getString(anything: self.txtBankAccountNo.text).isStringEmpty && getString(anything: self.txtBankAccountNo.text).count < 12 {
              Global.shared.showBanner(message: ProfileMessages.valueBtw1220chrs)
              self.txtBankAccountNo.becomeFirstResponder()
              
          }/* else if getString(anything: self.txtBankAccountNo.text).count < 12 {
              Global.shared.showBanner(message: ProfileMessages.valueBtw1220chrs)
              self.txtBankAccountNo.becomeFirstResponder()
              
          }*/ /*else if self.webServiceModel.scriptPanDic.count == 0 {
             Global.shared.showBanner(message: ProfileMessages.attachPanCard)
             
             } else if self.webServiceModel.scriptAdharCardDic.count == 0 {
             Global.shared.showBanner(message: ProfileMessages.attachAadharCard)
             }*/
        //        } else if getString(anything: txtOTP.text).trimming_WS != self.strOTP && (self.viewGetResendOTP.isHidden == false) {
        //            Global.shared.showBanner(message: ProfileMessages.msg_please_enter_valid_OTP)
        //            self.txtOTP.becomeFirstResponder()
        
        //        } else if (self.lblGender.text ?? "").isStringEmpty || self.lblGender.text == CommonLabels.select {
        //            Global.shared.showBanner(message: ProfileMessages.selectGender)
        
        else {
            isValid = true
        }
        return isValid
    }
    
    func isValidMobileNumber() -> Bool {
        if getString(anything: self.txtMobileNo.text).isStringEmpty {
            Global.shared.showBanner(message: ProfileMessages.mobileNumberIsRequired)
            self.txtMobileNo.becomeFirstResponder()
            return false
            
        } else if (getString(anything: self.txtMobileNo.text)).count < 10 {
            Global.shared.showBanner(message: ProfileMessages.mobileNumberIsNotValid)
            self.txtMobileNo.becomeFirstResponder()
            return false
            
        } else {
            return true
        }
    }
    
    func isValidPanNo() -> Bool {
        if !(getString(anything: self.txtPanNo.text).isStringEmpty) && !(getString(anything: self.txtPanNo.text).isValidPanCard) {
            Global.shared.showBanner(message: ProfileMessages.pleaseEnterPANNumberInValidFormat)
            self.txtPanNo.becomeFirstResponder()
            return false
        }/*  else if !(getString(anything: self.txtPanNo.text).isValidPanCard) {
            Global.shared.showBanner(message: ProfileMessages.pleaseEnterPANNumberInValidFormat)
            self.txtPanNo.becomeFirstResponder()
            return false
        }*/
        
        return true
    }
    
}

//MARK:- UITextField Delegate
extension ProfileViewController {
    
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let fullString = NSString(string: textField.text ?? "").replacingCharacters(in: range, with: string)
        
        if (textField == txtMobileNo) {
            
            /*if self.strMobileNumber == fullString {
             self.viewGetResendOTP.isHidden = true
             self.viewOTP.isHidden = true
             self.txtOTP.text = ""
             
             } else if (!(viewOTP.isHidden) || self.viewGetResendOTP.isHidden) && (fullString.count <= MOBILE_LENGTH_VALIDATION) {
             txtOTP.text = ""
             viewOTP.isHidden = true
             invalidateTimer()
             lblGetResendOTP.text = SignUp_EditProfileLabels.get_Otp
             viewGetResendOTP.backgroundColor = .customOrange
             viewGetResendOTP.isUserInteractionEnabled = true
             viewGetResendOTP.isHidden = false
             }*/
            
            let numberSet = NSCharacterSet(charactersIn: NUMERIC).inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if (string == numberFiltered) && (fullString.count < (MOBILE_LENGTH_VALIDATION + 1)) {
                if (fullString.hasPrefix("6") || fullString.hasPrefix("7") || fullString.hasPrefix("8") || fullString.hasPrefix("9")) || fullString.isEmpty {
                    return true
                }else{
                    return false
                }
            } else {
                return false
            }
            
        } else if (textField == txtOTP) {
            if string.isEmptyString {
                return true
            }
            if getString(anything: textField.text).count >= OTP_LENGTH {
                return false
            }
            let set = CharacterSet(charactersIn: NUMERIC)
            if let temp = string.rangeOfCharacter(from: set.inverted), !temp.isEmpty {
                return false
            } else {
                return true
            }
        }else if (textField == txtAadhaarNo){
            let numberSet = NSCharacterSet(charactersIn: NUMERIC).inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            return (string == numberFiltered) && (fullString.count < (AADHAR_CARD_LENGTH + 1))
        } else if (textField == txtBankAccountNo) {
            if fullString.count < BANK_ACC_VALIDATION_LENGTH + 1 {
                return true
            } else {
                return false
            }
        } else if (textField == txtPanNo) {
            
            let numberSet = NSCharacterSet(charactersIn: ALPHABETS_NUMERIC).inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if  (string == numberFiltered) && (fullString.count < PAN_CARD_LENGTH + 1) {
                return true
            } else {
                return false
            }
        }
        return true
    }
    
}

//MARK:-  Web Service model
extension ProfileViewController {
    func getProfileDataFromWebServices() {
        view.endEditing(true)
        self.viewMain.isHidden = true
        self.viewForShadow.isHidden = true
        self.viewProfileImage.isHidden = true
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.webServiceModel.GetProfileDetails { (invalidTokenMsg) in
            
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            UIAlertController.invalidToken(msg: invalidTokenMsg, onVC: self) {
                self.getProfileDataFromWebServices()
            }
            
        } block: { (responseModel) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if let dictUser = responseModel {
                dictUser.setUserModel()
                self.setupData()
                self.viewMain.isHidden = false
                self.viewForShadow.isHidden = false
                self.viewProfileImage.isHidden = false
                
                UserPreferences.set(value: getString(anything: dictUser.email), forKey: UserPreferencesKeys.General.email)
                UserPreferences.set(value: getString(anything: dictUser.id), forKey: UserPreferencesKeys.UserInfo.userID)
                UserPreferences.set(value: getString(anything: dictUser.token), forKey: UserPreferencesKeys.General.token)
                UserPreferences.set(value: getString(anything: dictUser.user_roles_id) , forKey: UserPreferencesKeys.General.user_roles_id)
                UserPreferences.set(value: getString(anything: dictUser.first_name), forKey: UserPreferencesKeys.General.first_name)
                UserPreferences.set(value: getString(anything: dictUser.last_name), forKey: UserPreferencesKeys.General.last_name)
                
            }
        }
    }
    
    fileprivate func fillModel() {
        self.webServiceModel.first_name = getString(anything: txtFullName.text).trimming_WS
        self.webServiceModel.middle_name = getString(anything: txtMiddleName.text).trimming_WS
        self.webServiceModel.last_name = getString(anything: txtLastName.text).trimming_WS
        self.webServiceModel.email = getString(anything: txtEmail.text).trimming_WS
        self.webServiceModel.mobile = getString(anything: txtMobileNo.text).trimming_WS
        self.webServiceModel.state_id = "24"
        self.webServiceModel.date_of_birth = getString(anything: lblDateOfBirth.text).trimming_WS
        self.webServiceModel.pan_no = getString(anything: txtPanNo.text).trimming_WS
        self.webServiceModel.adhaar_card = getString(anything: txtAadhaarNo.text).trimming_WS
        self.webServiceModel.bank_name = getString(anything: txtBankName.text).trimming_WS
        self.webServiceModel.bank_account_number = getString(anything: txtBankAccountNo.text).trimming_WS
        self.webServiceModel.address = getString(anything: txtAddress.text).trimming_WS

        self.webServiceModel.documentModels.append(docForPhotos)
        self.webServiceModel.documentModels.append(docForPan)
        self.webServiceModel.documentModels.append(docForAadhar)

        if self.lblGender.text != CommonLabels.select {
            self.webServiceModel.gender = getString(anything: self.selectedGenderType.id)
        } else {
            self.webServiceModel.gender = ""
        }
    }
    
    func editProfileDataWebServices() {
        self.fillModel()
        self.viewGetResendOTP.isHidden = true
        self.viewOTP.isHidden = true
        self.txtOTP.text = ""
        
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        //        self.viewMain.isHidden = true
        
        self.webServiceModel.updateProfile { (invalidTokenMsg) in
            
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            //            self.viewMain.isHidden = false
            UIAlertController.invalidToken(msg: invalidTokenMsg, onVC: self) {
                self.ViewProfileMode()
                self.getProfileDataFromWebServices()
            }
            
        } block: { (responseModel) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if let dictUser = responseModel {
                dictUser.setUserModel()
                self.setupData()
                self.ViewProfileMode()
                
                UserPreferences.set(value: getString(anything: dictUser.email), forKey: UserPreferencesKeys.General.email)
                UserPreferences.set(value: getString(anything: dictUser.id), forKey: UserPreferencesKeys.UserInfo.userID)
                UserPreferences.set(value: getString(anything: dictUser.token), forKey: UserPreferencesKeys.General.token)
                UserPreferences.set(value: getString(anything: dictUser.user_roles_id) , forKey: UserPreferencesKeys.General.user_roles_id)
                UserPreferences.set(value: getString(anything: dictUser.first_name), forKey: UserPreferencesKeys.General.first_name)
                UserPreferences.set(value: getString(anything: dictUser.last_name), forKey: UserPreferencesKeys.General.last_name)
            }
        }
    }
    
    func getOTP() {
        view.endEditing(true)
        CustomActivityIndicator.sharedInstance.showIndicator(view: view)
        self.webServiceModel.mobile = getString(anything: txtMobileNo.text).trimming_WS
        self.webServiceModel.getOTPreq { (result,otp) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if result {
                self.invalidateTimer()
                self.viewGetResendOTP.backgroundColor = .customSubtitle
                self.lblGetResendOTP.text = SignUp_EditProfileLabels.resend_Otp
                self.viewGetResendOTP.isUserInteractionEnabled = false
                self.lblOTPTime.text = String(format: "%02d:%02d", (self.totalSecond % 3600) / 60, (self.totalSecond % 3600) % 60)
                self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.timerCalled(_:)), userInfo: nil, repeats: true)
                self.viewOTP.isHidden = false
                //self.txtOTP.text = otp
                self.lblOTPSent.text = SignUp_EditProfileLabels.otp_has_been_sent_on_registered_mobile_number + " \(self.webServiceModel.mobile.substring(to: 2))xxxxxx\(self.webServiceModel.mobile.substring(from: 8))"
                
            }
        }
    }
    
    func verifyOtp() {
        CustomActivityIndicator.sharedInstance.showIndicator(view: view)
        self.webServiceModel.mobile = getString(anything: txtMobileNo.text).trimming_WS
        self.webServiceModel.mobile_otp = getString(anything: txtOTP.text).trimming_WS
        self.webServiceModel.verifyOTPreq { (result) in
            if result {
                self.editProfileDataWebServices()
            } else {
                CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
                self.viewGetResendOTP.backgroundColor = .customOrange
                self.viewGetResendOTP.isHidden = false
                self.viewGetResendOTP.isUserInteractionEnabled = true
                self.lblGetResendOTP.text = SignUp_EditProfileLabels.get_Otp
            }
        }
    }
    
    func getWebserviceforDistrict(isSetData:Bool = false) {
        arrDistrictOptions.removeAll()
        arrDistrictOptions.append(DropDownModel(dict: [DropDownModel.Keys.title.rawValue: RegistrtionLabels.selectDistrict,
                                                       DropDownModel.Keys.id.rawValue: ""]))
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        webServiceModel.getDistrictData { (responseModel) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if let response = responseModel {
                print(response)
                self.arrDistrictOptions.append(contentsOf: response.list)
                
                if isSetData{
                    if UserModel.currentUser.ms_districts_id.decryptStr != "" {
                        if self.arrDistrictOptions.count >= 0{
                            let districtData = self.arrDistrictOptions.filter({$0.id == UserModel.currentUser.ms_districts_id.decryptStr })
                            self.lblDistrict.text = districtData.first?.title
                        }
                        self.webServiceModel.districts_id = UserModel.currentUser.ms_districts_id.decryptStr
                        self.getWebserviceCity(isSetData: true)
                    }
                }
            }
        }
    }
    
    
    func getWebserviceCity(isSetData:Bool = false) {
        arrCityOptions.removeAll()
        arrCityOptions.append(DropDownModel(dict: [DropDownModel.Keys.title.rawValue: RegistrtionLabels.selectCity,
                                                   DropDownModel.Keys.id.rawValue: ""]))
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        webServiceModel.getCityData { (responseModel) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if let response = responseModel {
                print(response)
                self.arrCityOptions.append(contentsOf: response.list)
                
                if isSetData{
                    if UserModel.currentUser.ms_cities_id.decryptStr != "" {
                        if self.arrCityOptions.count >= 0 {
                            let districtData = self.arrCityOptions.filter({$0.id == UserModel.currentUser.ms_cities_id.decryptStr })
                            self.lblCity.text = districtData.first?.title
                            self.webServiceModel.city_id = UserModel.currentUser.ms_cities_id.decryptStr
                        }
                    }
                }
            }
        }
    }
}

extension ProfileViewController : DocumentPickerDelegate {
    
    func DocumentPickerData(url: URL?, errorMsg: String, sender: UIView) {
        if let getURL = url {
            //            if let change_URL = changeURL(url: getURL, name: ProfileWebServiceModel.keys.pan.rawValue) {
            
            switch self.documentTag {
            case 0:
                if let change_URL = changeURL(url: getURL, name: ProfileWebServiceModel.keys.pan.rawValue) {
                    self.documentForPan.imgViewDocument.image = .correct
                    self.documentForPan.imgViewDocument.tintColor = .themeColor
                    var dict: JSONObject = [:]
                    dict[MultipartRequestKeys.key_documentKey] = ProfileWebServiceModel.keys.pan_card_file.rawValue
                    dict[MultipartRequestKeys.key_documentURL] = change_URL
                    dict[MultipartRequestKeys.key_documentType] = getURL.pathExtension
                    dict[MultipartRequestKeys.key_documentTitle] = getURL.lastPathComponent
                    self.documentForPan.setFileName(fileName: getURL.lastPathComponent)
                    self.webServiceModel.scriptPanDic = dict
                    
                    self.docForPan.key = ProfileWebServiceModel.keys.pan_card_file.rawValue
                    self.docForPan.title = getURL.lastPathComponent
                    self.docForPan.type = getURL.pathExtension

                    /*
                     //                    if let url = NSURL(string: myURLstring) {
                     //                        let imageDataFromURL = NSData(contentsOfURL: url)
                     //                    }
                     do {
                     //                        let DataFromFile = try Data(contentsOf: getURL)
                     //                        print(DataFromFile)
                     //
                     //
                     
                     
                     let doc = try FileManager.default.attributesOfItem(atPath: getURL.path)
                     
                     } catch {
                     print(error)
                     }*/
                    
                    
                    //                    self.documentsForPhotos.document = img

                    self.docForPan.document = change_URL
                }
                
            default:
                if let change_URL = changeURL(url: getURL, name: ProfileWebServiceModel.keys.addhar.rawValue) {
                    self.documentForAddharCard.imgViewDocument.image = .correct
                    self.documentForAddharCard.imgViewDocument.tintColor = .themeColor
                    var dict: JSONObject = [:]
                    dict[MultipartRequestKeys.key_documentKey] = ProfileWebServiceModel.keys.adhaar_card_file.rawValue
                    dict[MultipartRequestKeys.key_documentURL] = change_URL
                    dict[MultipartRequestKeys.key_documentType] = getURL.pathExtension
                    dict[MultipartRequestKeys.key_documentTitle] = getURL.lastPathComponent
                    self.documentForAddharCard.setFileName(fileName: getURL.lastPathComponent)
                    self.webServiceModel.scriptAdharCardDic = dict
                    //                    self.docForAadhar.url = getURL
                    self.docForAadhar.key = ProfileWebServiceModel.keys.adhaar_card_file.rawValue
                    self.docForAadhar.title = getURL.lastPathComponent
                    self.docForAadhar.type = getURL.pathExtension
                    self.docForAadhar.document = change_URL
                }
            }
        } else {
            Global.shared.showBanner(message: errorMsg)
        }
    }
}
