//
//  RegisterViewController.swift
//  NWM
//
//  Created by Gaurang Patel on 14/04/21.
//

import UIKit
import Toast_Swift

class AddDetailsViewController : HeaderViewController {

    //View
    @IBOutlet weak var viewBtnSubmitGradient: GradientView!
    @IBOutlet weak var viewForScroll: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewForShadow: UIView!
    @IBOutlet weak var stackViewForDocument: UIStackView!
    
    @IBOutlet weak var viewDocumentShadow: UIView!
    @IBOutlet weak var viewDocument: UIView!
    
    @IBOutlet weak var txtBankName: UITextField!
    @IBOutlet weak var txtChalalnNumber: UITextField!
    @IBOutlet weak var txtAmount: UITextField!
    @IBOutlet weak var txtAccountHead: UITextField!
    @IBOutlet weak var btnSubmit: UIButton!
    
    @IBOutlet weak var lblApplicationNo: UILabel!
    @IBOutlet weak var lblChallanDate: UILabel!
    
    @IBOutlet var lblTitles: [LocalizedLabel]!
    @IBOutlet var txtValues: [UITextField]!
    @IBOutlet var lblAsterisks: [LocalizedLabel]!
    @IBOutlet var lblSeprator: [UILabel]!
    @IBOutlet weak var lblAccHead: UILabel!
    
    @IBOutlet var imgDropDown: [UIImageView]!

    let uploadAffidavit = DocumentView2.loadNib()
    let uploadChallan = DocumentView2.loadNib()
    let uploadOtherDocs = DocumentView2.loadNib()
    
    private var docForAffidavit : DocumentModel = DocumentModel()
    private var docForChallan : DocumentModel = DocumentModel()
    private var docForOtherDocs : DocumentModel = DocumentModel()
    
    fileprivate var privacyPolicy: Bool = false
    fileprivate var addDetailsWebServiceModel = AddDetailsWebServiceModel()
    fileprivate var documentPickerViewControllerDelegate: DocumentPickerViewControllerDelegate!

    var documentTag = 0
    var applications_id = ""
    var applications_no = ""
    var previousVc = UIView()
    var recovery_amount : Double = 0
    var arrList = [ChallanListModel]()
    
    fileprivate var key_privacy_policy = "key_privacy_policy"
    fileprivate var key_check_privacy_policy = "key_check_privacy_policy"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpIPadConstraints()
        self.setUpView()
        self.setupLbl()
        self.setUpHeader()
        self.fontSetUp()
        self.setUpDocumentViews()
        
        self.txtAmount.text = String(self.recovery_amount)
        self.lblApplicationNo.text = applications_no
        self.lblChallanDate.text = CommonLabels.select
        self.lblAccHead.text = CommonLabels.select
        self.txtChalalnNumber.delegate = self
    }
    
    fileprivate func setUpView() {
        self.viewMain.setCustomCornerRadius(radius: 12)
        self.viewForShadow.setCustomCornerRadius(radius: 12)
        self.viewDocument.setCustomCornerRadius(radius: 12)
        self.viewDocumentShadow.setCustomCornerRadius(radius: 12)
        self.viewForShadow.addShadow()
        self.viewDocumentShadow.addShadow()
        
        self.lblSeprator.forEach({
            $0.backgroundColor = .customSeparator
        })

        self.viewBtnSubmitGradient.setFullCornerRadius()


        self.viewBtnSubmitGradient.addGradient(ofColors: [UIColor.themeDarkOrangeClr.cgColor, UIColor.themeLightOrangeClr.cgColor], direction: .rightLeft)
    }
    
    fileprivate func setupLbl() {
        
        let privicyPolicy = NSMutableAttributedString(string: "CommonLabels.IhavereadprivacyPolicy", attributes: [NSAttributedString.Key.font:UIFont.regularTitleFont])
        
        privicyPolicy.addAttributes([NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue, NSAttributedString.Key.link : key_privacy_policy], range: "CommonLabels.IhavereadprivacyPolicy".nsString.range(of: "CommonLabels.privacyPolicy"))
    }
    
    fileprivate func fontSetUp() {
        self.lblTitles.forEach({
            $0.font = .regularTitleFont
            $0.textColor = .customBlackTitle
        })
        
        for lbl in lblAsterisks{
            lbl.textColor = .customRed
            lbl.font = .regularTitleFont
            lbl.text = "*"
        }
        
        self.txtValues.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
        })
        
        self.imgDropDown.forEach({
            $0.tintColor = .customgreenTheamClr
            $0.image = .ic_down_arrow
        })
        
        self.lblChallanDate.textColor = .customBlack
        self.lblChallanDate.font = .regularValueFont
        self.lblAccHead.textColor = .customBlack
        self.lblAccHead.font = .regularValueFont
        
        self.lblApplicationNo.textColor = .customBlack
        self.lblApplicationNo.font = .regularValueFont
        
        self.btnSubmit.titleLabel?.font = UIFont.semiboldLargeFont
        self.btnSubmit.setTitle(CommonButtons.btn_submit, for: .normal)
    }
    
    fileprivate func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.AddChallanDetails)
        setHeaderView_BackImage()
        
        if self.arrList.count > 0 {
            showFavourite()
            self.viewHeader.btnFavourite.imageEdgeInsets = UIEdgeInsets(top: 7, left: 7, bottom: 7, right: 7)
            self.viewHeader.btnFavourite.tintColor = .white
            self.viewHeader.btnFavourite.setImage(.ic_list, for: .normal)
        }
    }
    
    override func btnFavourite(_ sender: UIButton) {
        let dvc = ChallanViewController.instantiate(appStoryboard: .challanList)
        dvc.arrList = self.arrList
        dvc.applicationId = self.applications_id
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    fileprivate func setUpIPadConstraints() {
        changeLeadingTrailingForiPad(view: viewForScroll)
        changeHeightForiPad(view: btnSubmit)
    }
    
    private func setUpDocumentViews() {

        self.stackViewForDocument.subviews.forEach({
            $0.removeFromSuperview()
        })
        
        self.uploadAffidavit.configureTitle(str: AddDeatilsLabels.Affidavit,
                                       btnTag: 0,
                                       showFormatBtn: false,
                                       hideAsterisk: true,
                                       isFileNameShow: true)
        self.uploadAffidavit.btnDocument.addTarget(self, action: #selector(btnAffidavitTapped), for: .touchUpInside)
        self.stackViewForDocument.addArrangedSubview(self.uploadAffidavit)
        
        self.uploadChallan.configureTitle(str: AddDeatilsLabels.Challan,
                                                  btnTag: 1,
                                                  showFormatBtn: false,
                                                  hideAsterisk: false,
                                                  isFileNameShow: true)
        
        self.uploadChallan.btnDocument.addTarget(self, action: #selector(btnAffidavitTapped), for: .touchUpInside)
        self.stackViewForDocument.addArrangedSubview(self.uploadChallan)
        
        self.uploadOtherDocs.configureTitle(str: AddDeatilsLabels.OtherDoc,
                                                  btnTag: 2,
                                                  showFormatBtn: false,
                                                  hideAsterisk: true,
                                                  isFileNameShow: true)
        self.uploadOtherDocs.btnDocument.addTarget(self, action: #selector(btnAffidavitTapped), for: .touchUpInside)
        self.stackViewForDocument.addArrangedSubview(self.uploadOtherDocs)
    }
    
    @objc fileprivate func btnAffidavitTapped(_ sender: UIButton) {
        self.documentTag = sender.tag
        UIView.hideKeyBoard()
        let aDocumentPickerViewController = UIDocumentPickerViewController(documentTypes: arrdocTypes, in: .import)
        documentPickerViewControllerDelegate = DocumentPickerViewControllerDelegate(VC: aDocumentPickerViewController, delegate: self, sender: sender)
        self.present(aDocumentPickerViewController, animated: true, completion: nil)
    }
    
    @IBAction func TapOnSubmitBtn(_ sender: LocalizedButton) {
        if self.isValid() {
            resignFirstResponder()
            self.fillModel()
            self.getAddChalanDetails_req()
        }
    }
    
    @IBAction func TapOnAccountHeadBtn(_ sender: UIButton) {
        var list = ["002911101"]
        list.insert(CommonLabels.select, at: 0)
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblAccHead.text, arrSource: [list], doneAction: { (rows, value) in
            self.lblAccHead.text = getString(anything: value)
        }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func TapOnChallanDateBtn(_ sender: UIButton) {
        let datePickerVC = DatePickerViewController(initialSelection: DateConverter.getDateFromDateString(aDateString: getString(anything: self.lblChallanDate.text), inputFormat: DateFormats.dd_MM_yyyy), maximumDate: Date(), minimumDate: nil, datePickerMode: .date, doneAction: { (selectedDate) in
            
            let dobString = DateConverter.getDateStringFromDate(aDate: selectedDate, outputFormat: DateFormats.dd_MM_yyyy_dashed)
            self.lblChallanDate.text = dobString
            
        }, animated: true, origin: sender)
        
        datePickerVC.lblTitleText = CommonLabels.select
        datePickerVC.lblTitleColor = .customDarkBlueGradient
        datePickerVC.lblTitleFont = .boldMediumFont
        datePickerVC.btnCancelTitle = CommonButtons.cancel
        datePickerVC.btnCancelTitleColor = .customSubtitle
        datePickerVC.btnCancelTitleFont = .regularValueFont
        datePickerVC.btnDoneTitle = CommonButtons.done
        datePickerVC.btnDoneTitleColor = .customDarkBlueGradient
        datePickerVC.btnDoneTitleFont = .regularValueFont
        datePickerVC.show(onVC: self)
    }
}

//MARK:- Validations
extension AddDetailsViewController {
    func isValid() -> Bool {
        var isValid : Bool = false
        if getString(anything: self.txtChalalnNumber.text).isStringEmpty {
            Global.shared.showBanner(message: AddDetailsMessages.enterChallanNumber)
            self.txtChalalnNumber.becomeFirstResponder()

        } else if getString(anything: self.txtAmount.text).isStringEmpty {
            Global.shared.showBanner(message: AddDetailsMessages.enterAmount)
            self.txtAmount.becomeFirstResponder()

        }  else if getString(anything: self.lblAccHead.text).isStringEmpty || getString(anything: self.lblAccHead.text) == CommonLabels.select {
            Global.shared.showBanner(message: AddDetailsMessages.enter_AccountHead)
//            self.txtAccountHead.becomeFirstResponder()

        } else if getString(anything: self.txtBankName.text).isStringEmpty {
            Global.shared.showBanner(message: AddDetailsMessages.enter_BankName)
            self.txtBankName.becomeFirstResponder()

        } else if self.lblChallanDate.text == CommonLabels.select {
            Global.shared.showBanner(message: AddDetailsMessages.selectChallanDate)

        }/* else if self.docForAffidavit.document == nil {
            Global.shared.showBanner(message: AddDetailsMessages.attachAffidavit)

        }*/ else if self.docForChallan.document == nil {
            Global.shared.showBanner(message: AddDetailsMessages.attachChallan)
            
        } else {
            isValid = true
        }

        return isValid
    }
}

////MARK:-  Web Service model
extension AddDetailsViewController {
    fileprivate func fillModel() {
        self.addDetailsWebServiceModel.applicationId = self.applications_id //getString(anything: lblApplicationNo.text).trimming_WS
        self.addDetailsWebServiceModel.challanNumber = getString(anything: txtChalalnNumber.text).trimming_WS
        self.addDetailsWebServiceModel.amount = getString(anything: txtAmount.text)
        self.addDetailsWebServiceModel.accountHead = getString(anything: lblAccHead.text)
        self.addDetailsWebServiceModel.bankName = getString(anything: txtBankName.text)
        self.addDetailsWebServiceModel.challandate = (getString(anything: lblChallanDate.text).trimming_WS !=  CommonLabels.select) ? getString(anything: lblChallanDate.text).trimming_WS : ""
        
        self.addDetailsWebServiceModel.documentModels.removeAll()
        if self.docForAffidavit.document != nil {
            self.addDetailsWebServiceModel.documentModels.append(docForAffidavit)
        }
        if self.docForOtherDocs.document != nil {
            self.addDetailsWebServiceModel.documentModels.append(docForOtherDocs)
        }
        self.addDetailsWebServiceModel.documentModels.append(docForChallan)
    }
    
    func getAddChalanDetails_req() {

        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)

        self.addDetailsWebServiceModel.addChalanDetailsReq(block: { isSuccess, sucessMsg  in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if isSuccess {
                self.previousVc.makeToast(sucessMsg)
                self.navigationController?.popViewController(animated: true)
            }
        })
    }
}

extension AddDetailsViewController : DocumentPickerDelegate {
    
    func DocumentPickerData(url: URL?,errorMsg: String, sender: UIView) {

        if let getURL = url {
            
            switch self.documentTag {
            case 0:
                if let change_URL = changeURL(url: getURL, name: AddDetailsWebServiceModel.keys.affidavitDoc.rawValue) {
                    self.uploadAffidavit.imgViewDocument.image = .correct
                    self.uploadAffidavit.imgViewDocument.tintColor = .themeColor
                    self.uploadAffidavit.setFileName(fileName: getURL.lastPathComponent)

                    self.docForAffidavit.key = AddDetailsWebServiceModel.keys.affidavitDoc.rawValue
                    self.docForAffidavit.title = getURL.lastPathComponent
                    self.docForAffidavit.type = getURL.pathExtension
                    self.docForAffidavit.document = change_URL
                    
//                    var dict: JSONObject = [:]
//                    dict[MultipartRequestKeys.key_documentKey] = AddDetailsWebServiceModel.keys.affidavitDoc.rawValue
//                    dict[MultipartRequestKeys.key_documentURL] = change_URL
//                    dict[MultipartRequestKeys.key_documentType] = getURL.pathExtension
//                    dict[MultipartRequestKeys.key_documentTitle] = getURL.lastPathComponent
//                    self.addDetailsWebServiceModel.scriptAffidavit = dict
                }
            case 1:
                if let change_URL = changeURL(url: getURL, name: AddDetailsWebServiceModel.keys.challanDoc.rawValue) {
                    self.uploadChallan.imgViewDocument.image = .correct
                    self.uploadChallan.setFileName(fileName: getURL.lastPathComponent)

                    self.docForChallan.key = AddDetailsWebServiceModel.keys.challanDoc.rawValue
                    self.docForChallan.title = getURL.lastPathComponent
                    self.docForChallan.type = getURL.pathExtension
                    self.docForChallan.document = change_URL
                    
//                    var dict: JSONObject = [:]
//                    dict[MultipartRequestKeys.key_documentKey] = AddDetailsWebServiceModel.keys.challanDoc.rawValue
//                    dict[MultipartRequestKeys.key_documentURL] = change_URL
//                    dict[MultipartRequestKeys.key_documentType] = getURL.pathExtension
//                    dict[MultipartRequestKeys.key_documentTitle] = getURL.lastPathComponent
//                    self.addDetailsWebServiceModel.scriptChallan = dict
                }
                
            case 2:
                if let change_URL = changeURL(url: getURL, name: AddDetailsWebServiceModel.keys.otherDoc.rawValue) {
                    self.uploadOtherDocs.imgViewDocument.image = .correct
                    self.uploadOtherDocs.setFileName(fileName: getURL.lastPathComponent)

                    self.docForOtherDocs.key = AddDetailsWebServiceModel.keys.otherDoc.rawValue
                    self.docForOtherDocs.title = getURL.lastPathComponent
                    self.docForOtherDocs.type = getURL.pathExtension
                    self.docForOtherDocs.document = change_URL
                    
//                    var dict: JSONObject = [:]
//                    dict[MultipartRequestKeys.key_documentKey] = AddDetailsWebServiceModel.keys.otherDoc.rawValue
//                    dict[MultipartRequestKeys.key_documentURL] = change_URL
//                    dict[MultipartRequestKeys.key_documentType] = getURL.pathExtension
//                    dict[MultipartRequestKeys.key_documentTitle] = getURL.lastPathComponent
//                    self.addDetailsWebServiceModel.scriptOtherDocs = dict
                }
            default:
                print("default")
            }
            
        } else {
            Global.shared.showBanner(message: errorMsg)
        }
    }
}


//MARK: - UITextField Delegate
extension AddDetailsViewController {
    override func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == self.txtChalalnNumber { // only allowed ALPHABETS & NUMERIC
            let numberSet = NSCharacterSet(charactersIn: ALPHABETS_NUMERIC).inverted
            let compSepByCharInSet = string.components(separatedBy: numberSet)
            let numberFiltered = compSepByCharInSet.joined(separator: "")
            if (string == numberFiltered) {
                return true
            }
            return false
        }
        return true
    }
}
