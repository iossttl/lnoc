//
//  LoginServiceModel.swift
//  ASIDEMO
//
//  Created by Dhruv Patel on 13/01/21.
//

import UIKit

class AddDetailsWebServiceModel : NSObject {
    
    var applicationId :String = ""
    var challanNumber : String = ""
    var amount : String = ""
    var accountHead:String = ""
    var bankName :String = ""
    var challandate : String = ""

    var documentModels : [DocumentModel] = []
    
    fileprivate var getAddChalanDetailsUrl : String {
         return BASEURL + API.add_challan
     }
   
    enum keys : String {
        case citizen_users_id = "citizen_users_id"
        case applications_id = "applications_id"
        case challen_number = "challen_number"
        case challen_date = "challen_date"
        case amount = "amount"
        case account_head = "account_head"
        case bank_name = "bank_name"

        case affidavitDoc = "affidavit"
        case challanDoc = "challan"
        case otherDoc = "other_doc"
    }
    
    //MARK:- Initializer
    override init() {
        super.init()
    }
    
    fileprivate var addDetailsDict: [String : Any] {
        let dict: Dictionary = [keys.citizen_users_id.rawValue: UserPreferences.string(forKey: UserPreferencesKeys.UserInfo.userID).decryptStr,
                                keys.applications_id.rawValue: self.applicationId,
                                keys.challen_number.rawValue: self.challanNumber,
                                keys.challen_date.rawValue: self.challandate,
                                keys.amount.rawValue: self.amount,
                                keys.account_head.rawValue:self.accountHead,
                                keys.bank_name.rawValue:self.bankName]
        return dict
    }
     
    func addChalanDetailsReq(block:@escaping ((Bool, _ strSucessMsg : String) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {

           var param = WShandler.commonDict()
            let dic = addDetailsDict.encryptDic()
            for key in dic.keys {
               param[key] = dic[key]
           }
           print(param)
            print("documents - ", documentModels)

           WShandler.shared.multipartWebRequest(urlStr: getAddChalanDetailsUrl, dictParams: param, documents: documentModels) { (json, flag) in
               
               print(json)
               
               if flag == 200 {
                  
                  //Invalid Token
                  if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                      
                      UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                      UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                      UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                      UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                      UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                      UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                      block(false, "")
                      Global.shared.changeInitialVC(animated: true, direction: .toLeft)

                  } else if getBoolean(anything: json[CommonAPIConstant.key_resultFlag]) {
                      let msg = getString(anything: json[CommonAPIConstant.key_message]).decryptStr
                      block(true, msg)
                      
                  } else {
                      block(false, "")
                      Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                      
                 }
                   
               } else {
                  block(false, "")
                   Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
               }
           }
       } else {
          block(false, "")
          Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
       }
    }
}
