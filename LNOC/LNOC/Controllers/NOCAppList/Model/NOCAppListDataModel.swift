//
//  PlayerListDataModel.swift
//  Khel Mahakumbh
//
//  Created by Dhruv Patel on 27/12/21.
//

import UIKit

class NOCAppListDataModel : NSObject {
    
    let app_no: String
    let id: String
    let status: String
    let desc: String
    let date : String
    let address : String
    let title: String
    let fees: String
    let transferee:String
    let plot_no:String
    let noc_type:String
    let division:String
    let percentage:String
    let add_challan : Bool
    let add_clarification : Bool
    let noc_id:String
    let doc : [docModel]
    
    enum Keys: String {
        case app_no = "appno"
        case id = "id"
        case status = "app_status"
        case desc = "desc"
        case date = "date"
        case address = "address"
        case fees = "fees"
        case transferee = "transferee"
        case plot_no = "plot_no"
        case noc_type = "noc_type"
        case division = "division"
        case percentage = "percentage"
        case title = "title"
        case add_challan = "add_challan"
        case add_clarification = "add_clarification"
        case noc_id = "noc_id"
        case doc = "doc"
        case csno_plot_no = "csno_plot_no"
    }
    
    init(dict: Dictionary) {
        
        app_no = getString(anything: dict[Keys.app_no.rawValue]).decryptStr
        id = getString(anything: dict[Keys.id.rawValue]).decryptStr
        status = getString(anything: dict[Keys.status.rawValue]).decryptStr
        desc = getString(anything: dict[Keys.desc.rawValue]).decryptStr
        date = getString(anything: dict[Keys.date.rawValue]).decryptStr
        address = getString(anything: dict[Keys.address.rawValue]).decryptStr
        fees = getString(anything: dict[Keys.fees.rawValue]).decryptStr
        transferee = getString(anything: dict[Keys.transferee.rawValue]).decryptStr
        plot_no = (getString(anything: dict[Keys.plot_no.rawValue]).isStringEmpty ? getString(anything: dict[Keys.csno_plot_no.rawValue]) : getString(anything: dict[Keys.plot_no.rawValue])).decryptStr
        noc_type = getString(anything: dict[Keys.noc_type.rawValue]).decryptStr
        division = getString(anything: dict[Keys.division.rawValue]).decryptStr
        percentage = getString(anything: dict[Keys.percentage.rawValue]).decryptStr
        title = getString(anything: dict[Keys.title.rawValue]).decryptStr
        add_challan = (getString(anything: dict[Keys.add_challan.rawValue]).decryptStr == "Y")
        add_clarification = (getString(anything: dict[Keys.add_clarification.rawValue]).decryptStr == "Y")
        noc_id = getString(anything: dict[Keys.noc_id.rawValue]).decryptStr
        doc = (dict[Keys.doc.rawValue] as? [Dictionary] ?? [[:]]).map({ docModel.init(dict: $0) })
        
        super.init()
    }
}

class docModel : NSObject {
    
    let title: String
    let url: String
    
    enum Keys: String {
        case title = "title"
        case url = "url"
    }
    
    init(dict: Dictionary) {
        title = getString(anything: dict[Keys.title.rawValue]).decryptStr
        url = getString(anything: dict[Keys.url.rawValue]).decryptStr
        super.init()
    }
}
