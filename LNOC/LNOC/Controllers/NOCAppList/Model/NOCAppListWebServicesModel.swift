//
//  PlayerListWebServiceModel.swift
//  Khel Mahakumbh
//
//  Created by Dhruv Patel on 27/12/21.
//
import UIKit

class NOCAppListWebServicesModel : NSObject {
    
    //MARK:- Variables
    //Public
    var search = ""
    var filterDict = Dictionary()
    var Pageno = 1

    //MARK:- Enum
    enum Keys: String {
        case divison_id = "divison_id"
        case land_holder = "land_holder"
        case crr_number = "crr_number"
        case cs_number = "cs_number"
        case location_of_property = "location_of_property"
        case favorite = "favorite"
        case user_id = "user_id"
        case page = "page"
    }
    
    fileprivate var getNOCAppListUrl:String {
        BASEURL + API.applicationlist
    }
    
    private func nocAppListDict() -> Dictionary {
        var dict : [String:Any] = [:]
        let commonDict = WShandler.commonDict()//(uuid: uuid)
        
        /*for key in filterDict.keys {
            dict[key] = filterDict[key]
        }*/
        
        for key in commonDict.keys.sorted() {
            dict[key] = commonDict[key] as! String
        }
        dict[Keys.user_id.rawValue] = UserPreferences.string(forKey: UserPreferencesKeys.UserInfo.userID)
        dict[Keys.page.rawValue] = getString(anything: Pageno)
        return dict
    }
    

    
    func getPlayerList(block: @escaping (([NOCAppListDataModel], _ totalCount: Int) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            let param = self.nocAppListDict()
            print(param,"param")
            WShandler.shared.postWebRequest(urlStr: self.getNOCAppListUrl, param: param) { (json, flag) in

                var responseModel = [NOCAppListDataModel]()
                var totallist = 0

                if (flag == 200) {
                        if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                                if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == 2 {
                                    UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
                                    UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
                                    UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
                                    UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
                                    UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
                                    UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
                                    Global.shared.changeInitialVC(animated: true, direction: .toLeft)
                                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                                } else {
                                    responseModel = (json["applications"] as? [Dictionary] ?? []).map({ NOCAppListDataModel(dict: $0) })
                                    totallist = getInteger(anything: getString(anything: json[CommonAPIConstant.key_totalRecords]).decryptStr)
                                }
                            
                        } else {
                            Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        }
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                }
                block(responseModel, totallist)
            }
        }  else {
            block([], 0)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
}



