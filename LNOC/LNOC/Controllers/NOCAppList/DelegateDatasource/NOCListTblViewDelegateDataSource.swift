//
//  PlayerListDelegateDataSource.swift
//  Khel Mahakumbh
//
//  Created by Dhruv Patel on 27/12/21.
//

import UIKit

class NOCListTblViewDelegateDataSource : NSObject {
    //MARK:- Variables
    //Private
    fileprivate var arrSource: [NOCAppListDataModel]
    fileprivate let tblView: UITableView
    fileprivate weak var delegate: TblViewDelegate?
    fileprivate var showHideProgress:Bool
    //MARK:- Initializer
    init(arrData: [NOCAppListDataModel], tbl: UITableView, delegate: TblViewDelegate,showHideProgress:Bool) {
        arrSource = arrData
        tblView = tbl
        self.delegate = delegate
        self.showHideProgress = showHideProgress
        super.init()
        setUp()
    }
    
    //MARK:- Private Methods
    fileprivate func setUp() {
        setUpTblView()
    }
    
    fileprivate func setUpTblView() {
        registerCell()
        tblView.delegate = self
        tblView.dataSource = self
        tblView.rowHeight = UITableView.automaticDimension
        tblView.estimatedRowHeight = 200
        setUpBackgroundView()
        
        self.tblView.contentInset = UIEdgeInsets(top: 16, left: 0, bottom: 0, right: 0)
    }
    
    fileprivate func registerCell() {
        tblView.register(cellType: cellNOCApp.self)
    }
    
    private func setUpBackgroundView() {
        if (self.arrSource.count > 0) {
            self.tblView.backgroundView = nil
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                let frame = CGRect(x: 0, y: 0, width: self.tblView.frame.size.width, height: self.tblView.frame.size.height / 1.35)
                self.tblView.backgroundView = UIView.makeNoRecordFoundView(frame: frame, msg: CommonLabels.no_record_found)
            }
        }
    }
    
    //MARK:- Public Methods
    func reloadData(arrData: [NOCAppListDataModel]) {
        arrSource = arrData
        tblView.reloadData()
        setUpBackgroundView()
    }
}

//MARK:- UITableViewDelegate Methods
extension NOCListTblViewDelegateDataSource : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelect?(tbl: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        delegate?.willDisplay?(tbl: tableView, indexPath: indexPath)
    }
}

//MARK:- UITableViewDataSource Methods
extension NOCListTblViewDelegateDataSource : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(with: cellNOCApp.self, for: indexPath)
        cell.cellConfigure(data: arrSource[indexPath.row], showHideProgress: showHideProgress)
        return cell
    }
}

extension NOCListTblViewDelegateDataSource:UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        delegate?.tbldidScroll?(scrollView: scrollView)
    }
}
