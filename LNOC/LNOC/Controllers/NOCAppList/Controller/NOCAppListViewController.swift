//
//  PlayerListViewController.swift
//  Khel Mahakumbh
//
//  Created by Dhruv Patel on 27/12/21.
//

import UIKit

class NOCAppListViewController: HeaderViewController {
    
    @IBOutlet weak var tblList: UITableView!
    
    fileprivate var nocListTblViewDelegateDataSource : NOCListTblViewDelegateDataSource!
    fileprivate var nocAppListWebServicesModel = NOCAppListWebServicesModel()
    
    var isPullToRefresh = false
    var refreshControl = UIRefreshControl()
    var arrNOCList : [NOCAppListDataModel] = []
    var pageNo = 1
    var totalCount = 0
    //top scroll
    var filterDictData:Dictionary = [:]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        changeLeadingTrailingForiPad(view: self.view)
        self.setUpHeader()
        self.getListData()
        self.setUpPullToRefresh()
    }
    
    deinit {
        DebugLog("Deinit LNOC")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if isMovingFromParent {
            nocListTblViewDelegateDataSource = nil
        }
    }
    
    fileprivate func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.NOCApp)
        setHeaderView_BackImage()
        //showFilter()
    }
    
    fileprivate func setUpPullToRefresh() {
        refreshControl = self.tblList.addRefreshControl(target: self, action: #selector(pullToRefreshCalled(_:)))
        refreshControl.attributedTitle = NSAttributedString(string: CommonLabels.pull_to_refresh, attributes: [NSAttributedString.Key.font : UIFont.boldValueFont, NSAttributedString.Key.foregroundColor : UIColor.customSubtitle])
    }
    
    //MARK:- Selector Methods
    @objc fileprivate func pullToRefreshCalled(_ sender: UIRefreshControl) {
        self.arrNOCList = []
        isPullToRefresh = true
        self.pageNo = 1
        totalCount = 0
        getListData()
    }
    
    override func btnSyncTapped(_ sender: UIButton) {
        let vc = FilterViewController.instantiate(appStoryboard: .Filter)
        //vc.delegate = self
        //vc.isFromAppList = true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    fileprivate func setUpTblView() {
        if (self.nocListTblViewDelegateDataSource == nil) {
            self.nocListTblViewDelegateDataSource = NOCListTblViewDelegateDataSource(arrData: self.arrNOCList, tbl: self.tblList, delegate: self, showHideProgress: false)
        } else {
            self.nocListTblViewDelegateDataSource.reloadData(arrData: self.arrNOCList)
        }

    }
}

/*extension NOCAppListViewController:ProtocolForLandFilter {
    func implimant(crr: String, cs: String, division: String, landTenure: String, landHolder: String, locationOfProperty: String, area: String, division_id: String, landTenure_id: String) {
        
        nocAppListWebServicesModel.filterDict = [NOCAppListWebServicesModel.Keys.divison_id.rawValue:getString(anything: division_id),
                                                 NOCAppListWebServicesModel.Keys.land_holder.rawValue:landHolder,
                                                 NOCAppListWebServicesModel.Keys.crr_number.rawValue:crr,
                                                 NOCAppListWebServicesModel.Keys.cs_number.rawValue:cs,
                                                 NOCAppListWebServicesModel.Keys.location_of_property.rawValue:locationOfProperty
        ]
        
        self.filterDictData = nocAppListWebServicesModel.filterDict
        
    }
}*/

//MARK:- ------ Tbl View Delegate ---------
extension NOCAppListViewController : TblViewDelegate {
    func didSelect(tbl: UITableView, indexPath: IndexPath) {
        let dvc = ApplicationDetailsViewController.instantiate(appStoryboard: .applicationDetails)
        dvc.noc_id = arrNOCList[indexPath.row].id
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    func willDisplay(tbl: UITableView, indexPath: IndexPath) {
        if (indexPath.row == arrNOCList.count - 1) && (arrNOCList.count < totalCount) {
            self.pageNo += 1
            getListData()
        }
    }
    
    func tbldidScroll(scrollView: UIScrollView) {
        
    }
}

//MARK:- Webservice Functions
extension NOCAppListViewController {
    func getListData() {
        
        if !(isPullToRefresh) {
            CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        }
        nocAppListWebServicesModel.Pageno = self.pageNo
        nocAppListWebServicesModel.getPlayerList { (arr, totalCount) in
            if (self.isPullToRefresh) {
                self.isPullToRefresh = false
                self.refreshControl.endRefreshing()
            } else {
                CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            }
            
            if (self.pageNo == 1) {
                self.arrNOCList = arr
                self.setUpTblView()
            } else {
                self.arrNOCList += arr
                self.setUpTblView()
            }
            
            self.totalCount = totalCount
        }
    }
}







