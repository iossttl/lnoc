//
//  SelectLanguageViewController.swift
//  Khel Mahakumbh
//
//  Created by Dhruv Patel on 21/12/21.
//

import UIKit
import IQKeyboardManagerSwift


class SelectLanguageViewController: UIViewController {
    
    //MARK:- Interface Builder
    //UIView
    @IBOutlet weak var viewBGLanguage: UIView!
    @IBOutlet weak var viewBgBtnContinue: GradientView!
    
    //UILabel
    @IBOutlet weak var lblStaticSelectlanguage: LocalizedLabel!
    @IBOutlet weak var lblLanguage: UILabel!
    
    //UIImageView
    @IBOutlet weak var imgDropDown: UIImageView!
    @IBOutlet weak var imgLanguage: UIImageView!
    @IBOutlet weak var imgBg: UIImageView!
    
    //UIButton
    @IBOutlet weak var btnContinue: LocalizedButton!
    
    //MARK:- Variables
    private var languageTitleData: [LanguageListModel] = []
    fileprivate var titleLanguage : String = ""

    //MARK: - Life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    
    //MARK:- Private Methods
    //ViewController set up
    private func setUp(){
        setupData()
        setUpIPadConstraint()
        setUpLabel()
        setUpImage()
        setUpBtn()
        setupView()
    }
    
    private func setupData() {
        let dictJson = Global.shared.readJsonFile(ofName: "Settings")
        if let arrlanguage = dictJson["lang_list"] as? [Dictionary] {
            languageTitleData = arrlanguage.map({LanguageListModel(dict: $0)})
            
            if titleLanguage == "" {
//                titleLanguage = languageTitleData[0].title
                let code = self.languageTitleData.filter({$0.culture == selectedLanguage.rawValue })
                titleLanguage = code[0].title

            }
        }
    }
    
    private func setUpIPadConstraint() {
        changeLeadingTrailingForiPad(view: viewBGLanguage)
        changeHeightForiPad(view: btnContinue)
        changeHeightForiPad(view: imgLanguage)
        changeHeightForiPad(view: imgDropDown)
        changeHeightForiPad(view: lblLanguage)
    }
    
    private func setupView() {
        viewBgBtnContinue.setFullCornerRadius()
        viewBgBtnContinue.addGradient(ofColors: [UIColor.themeDarkOrangeClr.cgColor, UIColor.themeLightOrangeClr.cgColor], direction: .leftRight)
    }
    
    private func setUpLabel(){
        lblLanguage.text = titleLanguage
        lblStaticSelectlanguage.font = .boldTitleFont
        lblLanguage.font = .boldValueFont
        lblLanguage.textColor = .white
        lblStaticSelectlanguage.textColor = .white
        lblStaticSelectlanguage.text = HomeLabels.selectLanguage

//        lblLanguage.textAlignment = isRightToLeftLocalization ? .right : .left
    }
    
    private func setUpImage(){
        imgLanguage.image = .ico_language_globewithrenderingmode
        imgDropDown.image = .ic_down_arrow
        imgDropDown.tintColor = .white
        imgLanguage.tintColor = .white
    }
    
    private func setUpBtn() {
        btnContinue.setTitle(HomeButtons.continueStr.uppercased(), for: .normal)
        btnContinue.titleLabel?.font = UIFont.semiboldValueFont
        btnContinue.setTitleColor(.customWhite, for: .normal)
    }
    
    private func languageChanged() {
        btnContinue.setTitle(HomeButtons.continueStr.uppercased(), for: .normal)
        lblStaticSelectlanguage.text = HomeLabels.selectLanguage
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            setLocalization(language: LocalizationParam.getInstance.localizationCode)
            UserPreferences.set(value: LocalizationParam.getInstance.localizationCode.rawValue, forKey: UserPreferencesKeys.General.languageCode)
            UserPreferences.set(value: true, forKey: UserPreferencesKeys.General.isLanguageSelected)
//            Global.shared.changeInitialVC(animated: true, direction: .toRight)
            
            let dvc = LoginViewController.instantiate(appStoryboard: .Login)
            self.navigationController?.pushViewController(dvc, animated: true)
            
        }else{
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
    
    //MARK:- IBActions
    @IBAction func btnLanguageTapped(_ sender: UIButton) {
        let language = self.languageTitleData.map({$0.title})
        StringPickerViewController.show(pickerType: .single, initialSelection: getString(anything: lblLanguage.text), arrSource: [language], doneAction: { (rows, value) in
            self.lblLanguage.text = value as? String
            let index = rows[0]
            let lang_culture = self.languageTitleData[index].culture
            
            LocalizationParam.getInstance.localizationCode = LanguageCodes(rawValue: lang_culture) ?? .english
            UserPreferences.set(value: lang_culture, forKey: UserPreferencesKeys.General.languageCode)

            IQKeyboardManager.shared.toolbarDoneBarButtonItemText = CommonButtons.done
            IQKeyboardManager.shared.enable = true
            
//            let isChange = isRightToLeftLocalization
            
            setLocalization(language: LocalizationParam.getInstance.localizationCode)
            
//            if isChange != isRightToLeftLocalization {
//                let dVC = SelectLanguageViewController.instantiate(appStoryboard: .selectLanguage)
//                let navCon = UICustomNavigationController(rootViewController: dVC)
//                dVC.titleLanguage = getString(anything: value)
//                Global.shared.window?.rootViewController = navCon
//            } else {
//                self.languageChanged()
//            }
            
        }, animated: true, origin: sender)
    }
    
    @IBAction func btnContinueClickHandler(_ sender: UIButton) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            setLocalization(language: LocalizationParam.getInstance.localizationCode)
            UserPreferences.set(value: LocalizationParam.getInstance.localizationCode.rawValue, forKey: UserPreferencesKeys.General.languageCode)
            UserPreferences.set(value: true, forKey: UserPreferencesKeys.General.isLanguageSelected)
//            Global.shared.changeInitialVC(animated: true, direction: .toRight)
            
            let dvc = LoginViewController.instantiate(appStoryboard: .Login)
            self.navigationController?.pushViewController(dvc, animated: true)
            
        }else{
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
}
