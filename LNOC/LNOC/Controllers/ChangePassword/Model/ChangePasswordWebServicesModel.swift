//
//  ChangePasswordWebServicesModel.swift
//  NWM
//
//  Created by Gunjan on 07/04/21.
//

import UIKit

class ChangePasswordWebServicesModel: NSObject {

    //MARK:- URL
    fileprivate var changePasswordURL:String {
        BASEURL + API.changePassword
    }
    
    //MARK:- Enum
    enum Keys:String {
        case oldPassword = "oldpassword"
        case newPassword = "password"
    }
    
    //MARK:- Public Methods
    func changePassword(dictParam:Dictionary, invalidTokenMsg : @escaping ((String) -> Swift.Void), block: @escaping ((Dictionary?) -> Swift.Void)) {
        if (WShandler.shared.CheckInternetConnectivity()) {

            var param = WShandler.commonDict()
            
            for key in dictParam.keys {
                param[key] = dictParam[key]
            }
            
            WShandler.shared.postWebRequest(urlStr: changePasswordURL, param: param) { (json, flag) in
                var responseModel:Dictionary?
                if (flag == 200) {
                    
                    //Invalid Token
                    if getInteger(anything: json[CommonAPIConstant.key_resultFlag]) == CommonAPIConstant.key_InvalidToken_Flag {
                        invalidTokenMsg(getString(anything:json[CommonAPIConstant.key_message]).decryptStr)

                    } else if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        responseModel = json
                        block(responseModel)

                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        block(responseModel)
                    }
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block(responseModel)
                }
            }
        } else {
            block(nil)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
}
