//
//  ChangePasswordViewController.swift
//  AapleSarkar
//
//  Created by Vikram Jagad on 10/01/20.
//  Copyright © 2020 Vikram Jagad. All rights reserved.
//

import UIKit

class ChangePasswordViewController: UIViewController {
    //MARK:- Interface Builder
    //UIView
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewCenterShadow: UIView!
    @IBOutlet weak var viewCenterRound: UIView!
    @IBOutlet weak var viewChangepasswordTitle: UIView!
    
    @IBOutlet weak var viewBgSubmit: GradientView!
    @IBOutlet weak var viewBgCancel: GradientView!
    
    //UILabel
    @IBOutlet weak var lblChangePassword: LocalizedLabel!
    
    //UITextField
    @IBOutlet weak var tfCurrentPassword: UITextField!
    @IBOutlet weak var tfNewPassword: UITextField!
    @IBOutlet weak var tfRepeatPassword: UITextField!
    
    //UIButton
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnSubmit: UIButton!
    @IBOutlet weak var btnCurrentPasswrd: UIButton!
    
    @IBOutlet weak var btnNewPassword: UIButton!
    
    @IBOutlet weak var btnconfirmpassword: UIButton!
    
    
    //Collection
    @IBOutlet var lblTitles: [LocalizedLabel]!
    @IBOutlet var lblAsterisks: [UILabel]!
    @IBOutlet var viewSeparators: [UIView]!
    
    @IBOutlet var textField: [UITextField]!
    
    
    //MARK:- Variables
    //Private
    var changePasswordWebServicesModel = ChangePasswordWebServicesModel()
    
    //MARK:- Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpViewController()
    }
    
    fileprivate func setUpViewController() {
        setUpView()
        setUpIPadConstraint()
        setUpLbls()
        setUpBtns()
        setUpTfs()
    }
    
    
    fileprivate func setUpIPadConstraint() {
        changeLeadingTrailingForiPad(view: view)
        changeHeightForiPad(view: tfCurrentPassword)
        changeHeightForiPad(view: tfNewPassword)
        changeHeightForiPad(view: tfRepeatPassword)
        changeHeightForiPad(view: btnSubmit,constant: 40)
        changeHeightForiPad(view: btnCancel)

    }
    
    
    fileprivate func setUpView() {
        viewChangepasswordTitle.backgroundColor = .customgreenTheamClr
        view.backgroundColor =  UIColor.black.withAlphaComponent(0.65)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureTapped(_:)))
        tapGesture.numberOfTapsRequired = 1
        viewMain.addGestureRecognizer(tapGesture)
        for viewSeparator in viewSeparators {
            viewSeparator.backgroundColor = .customSeparator
        }
        viewCenterShadow.addShadow()//addShadow(shadowColor: .customShadow, shadowOpacity: 0.3)
        viewCenterRound.viewWith(radius: 12)
        self.viewCenterRound.backgroundColor = .white
    }
    
    fileprivate func setUpLbls() {
        lblChangePassword.font = .regularExtraLargeFont
        lblChangePassword.textColor = .customWhite
        for lbl in lblTitles {
            lbl.font = .regularTitleFont
            lbl.textColor = .customSubtitle
        }
        lblAsterisks.forEach { (lbl) in
            lbl.text = "*"
            lbl.textColor = .customRed
            lbl.font = .regularTitleFont
        }
    }
    
    fileprivate func setUpBtns() {
        btnSubmit.setTitle(CommonButtons.btn_submit, for: .normal)
        btnSubmit.titleLabel?.font = .regularValueFont
//        btnSubmit.viewWith(radius: btnSubmit.frame.size.height/2)
        btnSubmit.setTitleColor(.customWhite, for: .normal)
        btnSubmit.backgroundColor = .clear

        btnCancel.setTitle(CommonButtons.cancel, for: .normal)
        btnCancel.titleLabel?.font = .regularValueFont
//        btnCancel.viewWith(radius: btnCancel.frame.size.height/2)
        btnCancel.setTitleColor(.customWhite, for: .normal)
        btnCancel.backgroundColor = .clear

        viewBgSubmit.setFullCornerRadius()
        viewBgSubmit.addGradient(ofColors: [UIColor.themeDarkOrangeClr.cgColor, UIColor.themeLightOrangeClr.cgColor], direction: .leftRight)
        
        viewBgCancel.setFullCornerRadius()
        viewBgCancel.addGradient(ofColors: [UIColor.customCancel.cgColor, UIColor.lightGray.cgColor], direction: .leftRight)

        
        
        btnCurrentPasswrd.setImage(.ic_password_show, for: .normal)
        btnCurrentPasswrd.tintColor = .customGray
        
        btnNewPassword.setImage(.ic_password_show, for: .normal)
        btnNewPassword.tintColor = .customGray
        
        btnconfirmpassword.setImage(.ic_password_show, for: .normal)
        btnconfirmpassword.tintColor = .customGray
    }
    
    fileprivate func setUpTfs() {
        self.textField.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
            $0.isSecureTextEntry = true
//            $0.textAlignment = isRightToLeftLocalization ? .right : .left
        })
    }
    
    @IBAction func btnClickPasswordVisible(_ sender: UIButton) {
      
        if (sender.isSelected) {
            sender.setImage(.ic_password_show, for: .normal)
            sender.tintColor = .customGray}
        else{
            sender.setImage(.ic_password_not_show, for: .normal)
            sender.tintColor = .customGray
        }
        
        if sender.tag == 0{
        if (sender.isSelected) {
         tfCurrentPassword.isSecureTextEntry = true
        } else {
            tfCurrentPassword.isSecureTextEntry = false
        }
        }
       else if sender.tag == 1{
        if (sender.isSelected) {
         tfNewPassword.isSecureTextEntry = true
        } else {
            tfNewPassword.isSecureTextEntry = false
        }
        }
       else if sender.tag == 2{
        if (sender.isSelected) {
         tfRepeatPassword.isSecureTextEntry = true
        } else {
            tfRepeatPassword.isSecureTextEntry = false
        }
        }
        sender.isSelected = !sender.isSelected
    }
    
    
    @IBAction func btnCancelTapped(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
    @IBAction func btnSubmitTapped(_ sender: UIButton) {
        if getString(anything: tfCurrentPassword.text).count == 0 {
            Global.shared.showBanner(message: SettingsMessages.msg_please_enter_current_password)
            tfCurrentPassword.becomeFirstResponder()
            
        } else if getString(anything: tfNewPassword.text).count == 0 {
            Global.shared.showBanner(message: SettingsMessages.msg_please_new_password)
            tfNewPassword.becomeFirstResponder()
            
        } else if !getString(anything: self.tfNewPassword.text).trimming_WS.isValidPassword() {
            UIAlertController.showWith(title: CommonLabels.alert, msg: SettingsMessages.password_not_in_valid_formate, style: .alert, onVC: self, actionTitles: [CommonButtons.ok], actionStyles: [.default], actionHandlers: [{ (yesAction) in
                self.tfNewPassword.becomeFirstResponder()
            }])
        } else if getString(anything: tfRepeatPassword.text).count == 0 {
            Global.shared.showBanner(message: SettingsMessages.msg_please_repeat_new_password)
            tfRepeatPassword.becomeFirstResponder()
            
        } else if (getString(anything: tfNewPassword.text) != getString(anything: tfRepeatPassword.text)) {
            Global.shared.showBanner(message: SettingsMessages.msg_please_enter_same_password)
            tfRepeatPassword.becomeFirstResponder()
            
        } else {
            self.apiCall()
        }
    }
    
    fileprivate func apiCall() {
        let passDict = [ChangePasswordWebServicesModel.Keys.oldPassword.rawValue: getString(anything:self.tfCurrentPassword.text).encryptStr,
                        ChangePasswordWebServicesModel.Keys.newPassword.rawValue: getString(anything:self.tfRepeatPassword.text).encryptStr, CommonAPIConstant.key_token : UserPreferences.string(forKey: UserPreferencesKeys.General.token)]
        self.changePassword(param: passDict)
    }
    
    @objc func tapGestureTapped(_ gesture: UIGestureRecognizer) {
//        dismiss(animated: false, completion: nil)
    }
    
    func getLogout() {
        UserPreferences.remove(forKey:UserPreferencesKeys.General.email)
        UserPreferences.remove(forKey:UserPreferencesKeys.General.first_name)
        UserPreferences.remove(forKey:UserPreferencesKeys.General.last_name)
        UserPreferences.remove(forKey:UserPreferencesKeys.UserInfo.userID)
        UserPreferences.remove(forKey:UserPreferencesKeys.General.token)
        UserPreferences.remove(forKey: UserPreferencesKeys.General.isUserLoggedIn)
        Global.shared.changeInitialVC(animated: true, direction: .toLeft)
    }
}

//MARK:- Webservice Functions
extension ChangePasswordViewController {
    func changePassword(param: Dictionary) {
        CustomActivityIndicator.sharedInstance.showIndicator(view: view)
        changePasswordWebServicesModel.changePassword(dictParam: param) { (invalidTokenMsg) in
            
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            UIAlertController.showWith(title: "", msg: invalidTokenMsg, style: .alert, onVC: self, actionTitles: [CommonButtons.btn_signin], actionStyles: [.destructive], actionHandlers: [{ (yesAction) in
                self.getLogout()
            }])
            
        } block: { (json) in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if let response = json {
                self.tfNewPassword.text = ""
                self.tfRepeatPassword.text = ""
                self.tfCurrentPassword.text = ""
                Global.shared.showBanner(message: getString(anything:response[CommonAPIConstant.key_message]).decryptStr)
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
}
