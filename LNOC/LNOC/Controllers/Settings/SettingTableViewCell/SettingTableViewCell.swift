//
//  SettingTableViewCell.swift
//  LSAttendance
//
//  Created by Vikram Jagad on 30/06/20.
//  Copyright © 2020 Hitesh. All rights reserved.
//

import UIKit

class SettingTableViewCell: UITableViewCell {
    //MARK:- Outlets
    
    //UIView
    @IBOutlet var viewMain : UIView!
    @IBOutlet var viewShadow : UIView!
    @IBOutlet var viewSelectLanguage : UIView!
    @IBOutlet var viewSeperator : UIView!
    @IBOutlet weak var viewSwitch: UIView!
    @IBOutlet weak var viewMainTitle: UIView!
    @IBOutlet weak var viewImgMainTitle: UIView!
    @IBOutlet weak var viewImgMainTitleArrow: UIView!
    
    //UILabel
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet weak var lblLanguage: UILabel!
    @IBOutlet weak var lblSwitchTitle: UILabel!
    @IBOutlet weak var lblSwitchDescription: UILabel!
    
    //UIImageView
    @IBOutlet weak var imgDropDown: UIImageView!
    @IBOutlet weak var imgSelectLang: UIImageView!
    @IBOutlet weak var imgMainTitle: UIImageView!
    @IBOutlet weak var imgSwitchSection: UIImageView!
    @IBOutlet var images: [UIImageView]!
    @IBOutlet weak var imgMainTitleArrow: UIImageView!
    
    //UIButton
    @IBOutlet var btnLanguage : UIButton!
    
    //MARK:- Variable
    
    //MARK:- Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        setupView()
        setUpIPadConstraints()
        setupLabel()
        setupImg()
    }
    
    //MARK:- Privates Methods
    private func setupView() {
        //BackgroundColor
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        viewMain.backgroundColor = .customWhite
        viewShadow.backgroundColor = .customWhite
        self.viewMain.viewWith(radius: 12)
        self.viewShadow.viewWith(radius: 12)
        self.viewShadow.addShadow()
        self.viewSeperator.backgroundColor = .customSeparator
    }
    
    fileprivate func setUpIPadConstraints() {
//        changeHeightForiPad(view: imgSelectLang)
//        changeHeightForiPad(view: self.customSwitchNew)
        self.images.forEach { (img) in
            changeHeightForiPad(view: img)
        }
    }

    private func setupImg() {
        self.images.forEach { (img) in
            img.tintColor = .customOrange
        }
        imgDropDown.image  = .ic_down_arrow
        imgSelectLang.image = .ico_language_globewithrenderingmode
        imgMainTitleArrow.image = .img_rightArrow
        self.imgDropDown.tintColor = .customgreenTheamClr
        self.imgMainTitle.tintColor = .customgreenTheamClr
        self.imgSelectLang.tintColor = .customgreenTheamClr

    }
    
    private func setupLabel() {
        lblTitle.textColor = .customBlack
        lblTitle.font = .regularTitleFont
        
        lblSwitchTitle.textColor = .customBlack
        lblSwitchTitle.font = .semiboldValueFont
        
        lblSwitchDescription.textColor = .customBlackTitle
        lblSwitchDescription.font = .regularSmallFont
        
        lblLanguage.textColor = .customBlack
        lblLanguage.font = .semiboldValueFont
        
        /*if isRightToLeftLocalization {
            lblTitle.textAlignment = .right
            lblLanguage.textAlignment = .right
            lblSwitchTitle.textAlignment = .right
            lblSwitchDescription.textAlignment = .right
        }else{*/
            lblTitle.textAlignment = .left
            lblLanguage.textAlignment = .left
            lblSwitchTitle.textAlignment = .left
            lblSwitchDescription.textAlignment = .left
//        }
    }
    
    //MARK:- Public Method
    func configValue(cellData: SettingListModel) {
        self.lblTitle.text = cellData.title.localizedString
        self.lblSwitchTitle.text = cellData.switchTitle.localizedString
        self.lblSwitchDescription.text = cellData.switchDescription.localizedString
        
        if SettingType(rawValue: cellData.type) == .password {
            imgMainTitle.image = .ico_password

        }
    }
}
