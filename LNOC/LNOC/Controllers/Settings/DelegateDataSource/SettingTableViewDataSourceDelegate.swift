//
//  SettingTableViewDataSourceDelegate.swift
//  LSAttendance
//
//  Created by Vikram Jagad on 30/06/20.
//  Copyright © 2020 Hitesh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
class SettingTableViewDataSourceDelegate : NSObject {

    typealias T = SettingListModel
    typealias ScrlView = UITableView
    typealias Delegate = TblViewDelegate
    private var arrLanguageSelection:[LanguageListModel] = []
    //MARK:- Variables
    internal var arrSource: [SettingListModel]
    internal weak var delegate: TblViewDelegate?
    internal var scrlView: UITableView

    var changeLanguage : (() -> Void)?
    
    //MARK:- Initializer
    required init(arrData: [T], delegate: Delegate, scrl: ScrlView, arrLanguage:[LanguageListModel]) {
        arrSource = arrData
        self.delegate = delegate
        self.arrLanguageSelection = arrLanguage
        scrlView = scrl
        super.init()
        self.setUpScrlView()
    }
    
    // set up
    func setUpScrlView() {
        registerCell()
        
        scrlView.estimatedRowHeight = 77
        scrlView.rowHeight = UITableView.automaticDimension
        scrlView.separatorStyle = .none
        scrlView.contentInset = .init(top: 20, left: 0, bottom: 0, right: 0)
        scrlView.delegate = self
        scrlView.dataSource = self
        setupTblBackground()
        scrlView.reloadData()
    }
    
    func registerCell() {
        scrlView.register(cellType: SettingTableViewCell.self)
    }
    
    func reloadScrlView(arr: [T]) {
        arrSource = arr
        scrlView.reloadData()
        setupTblBackground()
    }
    
    func setupTblBackground() {
        
        if (arrSource.count > 0) {
            scrlView.backgroundView = nil
        } else {
            scrlView.backgroundView = UIView.makeNoRecordFoundView(frame: scrlView.bounds, msg: CommonLabels.no_record_found)
        }
    }
    
    /*@objc func SwitchValueUpdates(switchn : CustomSwitchNew) {
        if switchn.tag == 1 { //Auto Play
            Constants.isAudioAutoPlay = switchn.isOn
            UserPreferences.set(value: switchn.isOn, forKey: UserPreferencesKeys.General.isAutoPlayAudioClips)
        } else { //Find Near By
            Constants.isFindNearBy = switchn.isOn
            UserPreferences.set(value: switchn.isOn, forKey: UserPreferencesKeys.General.isFindNearBy)
            
            
            
            if Constants.isFindNearBy{
                Constants.macAddress = ""
                Constants.virtualTourGuideId = ""
                Constants.virtualTourPlaceId = ""
                beaconGroupId = []
                Constants.currentBeaconIds = []
            }else{
                
            }
        }
    }*/
}

//MARK:- UITableViewDelegate Methods
extension SettingTableViewDataSourceDelegate:UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.didSelect?(tbl: tableView, indexPath: indexPath)
    }
}

//MARK:- UITableViewDataSource Methods
extension SettingTableViewDataSourceDelegate:UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(with: SettingTableViewCell.self, for: indexPath)
        cell.configValue(cellData: arrSource[indexPath.row])
        let type = arrSource[indexPath.row].type
        cell.btnLanguage.addTarget(self, action: #selector(btnLanguageTapped), for: .touchUpInside)
        cell.btnLanguage.tag = indexPath.row
        
        if SettingType(rawValue: type) == .language {
            cell.viewSelectLanguage.isHidden = false
            cell.viewSeperator.isHidden = false
            cell.viewSwitch.isHidden = true
            cell.viewImgMainTitle.isHidden = true
            let code = self.arrLanguageSelection.filter({$0.culture == selectedLanguage.rawValue })
            cell.lblLanguage.text = code[0].title
            cell.viewImgMainTitleArrow.isHidden = true

        } else if SettingType(rawValue: type) == .password /*|| SettingType(rawValue: type) == .user_guide*/ {
            cell.viewSelectLanguage.isHidden = true
            cell.viewSeperator.isHidden = true
            cell.viewSwitch.isHidden = true
            cell.lblTitle.font = .semiboldValueFont
            cell.viewImgMainTitleArrow.isHidden = false
            
        } /*else if SettingType(rawValue: type) == .customSwitch {
            cell.viewSelectLanguage.isHidden = true
            cell.viewSeperator.isHidden = true
            cell.viewSwitch.isHidden = false
            cell.viewMainTitle.isHidden = true
            cell.customSwitchNew.addTarget(self, action: #selector(self.SwitchValueUpdates(switchn:)), for: .valueChanged)
            cell.customSwitchNew.tag = indexPath.row
            
            if indexPath.row == 1 {
                cell.customSwitchNew.isOn = Constants.isAudioAutoPlay
                cell.imgSwitchSection.image = .ico_autoplay

            } else {
                cell.customSwitchNew.isOn = Constants.isFindNearBy
                cell.imgSwitchSection.image = .ico_find_nearby
            }
        }*/
        
        return cell
    }
}

extension SettingTableViewDataSourceDelegate {
    
    @objc func btnLanguageTapped(sender:UIButton) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            
            let cell = scrlView.cellForRow(at: IndexPath(row: sender.tag, section: 0)) as? SettingTableViewCell
            let language = self.arrLanguageSelection.map({$0.title})
            StringPickerViewController.show(pickerType: .single, initialSelection: cell?.lblLanguage.text, arrSource: [language], doneAction: { (rows, value) in
                
                
                cell?.lblLanguage.text = getString(anything: value)
                let index = rows[0]
                let lang_culture = self.arrLanguageSelection[index].culture

                
                UserPreferences.set(value: lang_culture, forKey: UserPreferencesKeys.General.languageCode)
                LocalizationParam.getInstance.localizationCode = LanguageCodes(rawValue: lang_culture) ?? LanguageCodes.english
                setLocalization(language : LanguageCodes(rawValue: lang_culture) ?? .english)
                self.changeLanguage?()
                IQKeyboardManager.shared.toolbarDoneBarButtonItemText = CommonButtons.done
                IQKeyboardManager.shared.enable = true
            }, animated: true, origin: sender)
        }else{
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
}
