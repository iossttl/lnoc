//
//  SettingListModel.swift
//  LSAttendance
//
//  Created by Vikram Jagad on 30/06/20.
//  Copyright © 2020 Hitesh. All rights reserved.
//

import UIKit

class SettingListModel : NSObject {
    
    var id = "id"
    var title = "title"
    var type = "type"
    var switchTitle = "switchTitle"
    var switchDescription = "switchDescription"

    enum Keys: String {
        case id = "id"
        case title = "title"
        case type = "type"
        case switchTitle = "switchTitle"
        case switchDescription = "switchDescription"
    }
    
    init(dict:Dictionary) {
        self.id = getString(anything: dict[Keys.id.rawValue])
        self.title = getString(anything: dict[Keys.title.rawValue])
        self.type = getString(anything: dict[Keys.type.rawValue])
        self.switchTitle = getString(anything: dict[Keys.switchTitle.rawValue])
        self.switchDescription = getString(anything: dict[Keys.switchDescription.rawValue])
    }
}

