//
//  SettingViewController.swift
//  LSAttendance
//
//  Created by Vikram Jagad on 30/06/20.
//  Copyright © 2020 Hitesh. All rights reserved.
//

import UIKit

class SettingViewController : HeaderViewController {
    
    //MARK:- Outlet
    @IBOutlet var settingTblView : UITableView!
    
    fileprivate var settingTableViewDataSourceDelegate : SettingTableViewDataSourceDelegate!
    fileprivate var settingListModel : [SettingListModel]!
    fileprivate var languageListModel : [LanguageListModel]!
    
    //MARK:- Life Cycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpVC()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    //MARK:- Privates Methods
    fileprivate func setUpVC() {
        setUpHeader()
        setUpIPadConstraint()
        setupView()
        setupData()
    }
    
    private func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle:Headers.Setting)
    }
    
    
    fileprivate func setUpIPadConstraint() {
        changeLeadingTrailingForiPad(view: view)
    }
    
    private func setupView() {
        //        self.view.backgroundColor = .customLightSkyBlue
        self.settingTblView.backgroundColor = .clear
        setHeaderView_BackImage()
    }
    
    private func setupData() {
        
        let dictJson = Global.shared.readJsonFile(ofName: "Settings")
        if let settingData = dictJson["list"] as? [Dictionary] {
            settingListModel = settingData.map({ SettingListModel(dict: $0) })
        }
        if let arrlanguage = dictJson["lang_list"] as? [Dictionary] {
            languageListModel = arrlanguage.map({ LanguageListModel(dict: $0) })
        }
     
        // change password hide if user not login
    /*    if !UserModel.signedIn, settingListModel.count > 1 {
            settingListModel.remove(at: settingListModel.count - 2)
        }*/
        
        settingTableViewDataSourceDelegate = SettingTableViewDataSourceDelegate(arrData: settingListModel, delegate: self, scrl: settingTblView,arrLanguage:self.languageListModel)
        settingTableViewDataSourceDelegate.changeLanguage = changeLanguageCallBack
    }
    
    private func changeLanguageCallBack() {
        
    
        
//        var contentOffset : CGPoint = .zero
//        (self.navigationController?.viewControllers.filter({ $0.isKind(of: DashboardViewController.self) }) as? [DashboardViewController] ?? []).forEach({ contentOffset = (($0.scrlViewMain != nil) ? $0.scrlViewMain.contentOffset : .zero) })
        
        let dVC = DashboardViewController.instantiate(appStoryboard: .dashboard)
        
        let nVC = UICustomNavigationController(rootViewController: dVC)
        
        let settingVc = SettingViewController.instantiate(appStoryboard: .settings)
        
//        dVC.scrlViewContentOffset = contentOffset
        
        nVC.viewControllers = [dVC, settingVc]
        
        Global.shared.window?.rootViewController = nVC
    }
}

extension SettingViewController : TblViewDelegate {
    
    func didSelect(tbl: UITableView, indexPath: IndexPath) {
        
        let type = getString(anything: settingListModel[indexPath.row].type)
        
        switch SettingType(rawValue: type) {
        case .language:
            break
        /* let VC = ChangeLanguageViewController.instantiate(appStoryboard: .changeLanguage)
         //self.navigationController?.pushViewController(VC, animated: true)
         pushViewControllerWithAnimation(VC: VC)*/
        case .password:
            let dvc = ChangePasswordViewController.instantiate(appStoryboard: .changePassword)
            dvc.modalPresentationStyle = .overFullScreen
            dvc.modalTransitionStyle = .coverVertical
            present(dvc, animated: false, completion: nil)
            
        default:
            break
        }
        
        
    }
}
