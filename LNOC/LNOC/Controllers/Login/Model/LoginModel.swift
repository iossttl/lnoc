//
//  languageListModel.swift
//  NWM
//
//  Created by Vikram Jagad on 07/05/21.
//

import UIKit

class LogInWebServiceModel : NSObject {
    
    var email : String = ""
    var pwd : String = ""
    
    fileprivate var getLogin:String {
         return BASEURL + API.login
     }
    
    enum keys:String {
        case email = "email"
        case pwd = "pwd"
    }
    
    //MARK:- Initializer
    override init() {
        super.init()
    }
    
    fileprivate var LoginDetailsDict: [String : Any] {
        let dict: Dictionary = [keys.email.rawValue: self.email, keys.pwd.rawValue: self.pwd]
        return dict
    }
        
    func getLogin(block:@escaping ((Dictionary?, _ isSucess : Bool) -> Swift.Void)) {
         if (WShandler.shared.CheckInternetConnectivity()) {
            var param = WShandler.commonDict()
             let dict = LoginDetailsDict.encryptDic()
            for key in dict.keys {
                param[key] = dict[key]
            }
             print("Login Url", getLogin)
            print(param,"param")
             WShandler.shared.postWebRequest(urlStr: getLogin, param: param) { (json, flag) in
                var responseModel:Dictionary?
                 if flag == 200 {
                     if getBoolean(anything: json[CommonAPIConstant.key__resultFlag]) {
                        
                        print(json,"json")
                        responseModel = json
                        block(responseModel, true)

                     } else {
                         Global.shared.showBanner(message: getString(anything:json[CommonAPIConstant.key_message]).decryptStr)
                         block(responseModel, false)
                     }
                 } else {
                     Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                     block(responseModel, false)
                 }
             }
         } else {
             block(nil, false)
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
         }
     }
}
