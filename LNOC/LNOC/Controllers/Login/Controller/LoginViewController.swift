//
//  SelectLanguageViewController.swift
//  Khel Mahakumbh
//
//  Created by Dhruv Patel on 21/12/21.
//

import UIKit
import IQKeyboardManagerSwift


class LoginViewController : UIViewController {
    
    //MARK:- Interface Builder
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblPassword: UILabel!
    @IBOutlet weak var lblDontHaveAnAccount: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    //UITextfields
    @IBOutlet weak var tfEmail: UITextField!
    @IBOutlet weak var tfPassword: UITextField!
    
    @IBOutlet var txtfld: [UITextField]!
    @IBOutlet var lbl: [UILabel]!
    @IBOutlet var viewSeparators: [UIView]!
    
    @IBOutlet weak var viewSignInBtn: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewScrlViewMain: UIView!
    
    //UIButtons
    @IBOutlet weak var btnForgotPassword: UIButton!
    @IBOutlet weak var btnSignIn: UIButton!
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnPassword: UIButton!
    
    @IBOutlet weak var imgRememberMe: UIImageView!
    //UIImageView
    @IBOutlet weak var imgViewLogo: UIImageView!
    
    @IBOutlet weak var lblRememberMe: UILabel!
    fileprivate var loginResponseModel = LogInWebServiceModel()

    
    //MARK: - Life cycle method
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        
//        #if targetEnvironment(simulator)
//        self.tfEmail.text = "lnoc_front@mailinator.com"
//        self.tfPassword.text = "User@1234"
//        #endif
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
    
    //MARK:- Private Methods
    //ViewController set up
    private func setUp(){
        setUpIPadConstraint()
        setUpLabel()
        setUpBtn()
        
        self.viewSignInBtn.addGradient(ofColors: [UIColor.themeDarkOrangeClr.cgColor, UIColor.themeLightOrangeClr.cgColor], direction: .leftRight)
        
        if UserPreferences.bool(forKey: UserPreferencesKeys.General.isRemember) {
            self.imgRememberMe.image = UIImage.checkboxSelected?.alwaysTemplate
            self.imgRememberMe.tag = 1
            self.tfEmail.text = UserPreferences.string(forKey: UserPreferencesKeys.General.rememberEmail)
            self.tfPassword.text = UserPreferences.string(forKey: UserPreferencesKeys.General.rememberPass)
        } else {
            self.imgRememberMe.image = UIImage.checkbox?.alwaysTemplate
            self.imgRememberMe.tag = 0
        }
    }
    
    private func setUpIPadConstraint() {
        changeLeadingTrailingForiPad(view: viewScrlViewMain)
        changeHeightForiPad(view: btnSignIn)
        changeHeightForiPad(view: btnPassword)
        changeHeightForiPad(view: tfEmail)
        changeHeightForiPad(view: tfPassword)
    }
    
    private func setUpLabel() {
        self.lbl.forEach { label in
            label.textColor = .placeholderTextClr
        }
        
        self.txtfld.forEach { label in
            label.font = .mediumValueFont
        }
        
        lblTitle.font = .boldExtraLargeFont
        lblDescription.font = .regularTitleFont
      
        lblEmail.text = Login.Email.uppercased()
        lblPassword.text = Login.Password.uppercased()

        let mutableString = NSMutableAttributedString(string: "", attributes: [:])
        mutableString.append(NSAttributedString(string: Login.dontHaveAccount, attributes: [.foregroundColor : self.lblDontHaveAnAccount.textColor ?? .white, .font: UIFont.regularMediumFont]))
        
        mutableString.append(NSAttributedString(string: " " + Login.Register, attributes: [.foregroundColor : UIColor.white, .font: UIFont.heavyExtraLargeFont]))
        
        lblDontHaveAnAccount.attributedText = mutableString
        lblDontHaveAnAccount.font = .regularValueFont
        self.lblRememberMe.font = .regularValueFont
        
        self.tfEmail.setPlaceholder(LoginPlaceHolders.email_address, color: UIColor.white.withAlphaComponent(0.5), setFont: self.tfEmail.font)
        self.tfPassword.setPlaceholder(LoginPlaceHolders.password, color: UIColor.white.withAlphaComponent(0.5), setFont: self.tfEmail.font)
        self.tfPassword.isSecureTextEntry = true
        self.tfPassword.isSelected = false
    }
    
    private func setUpBtn() {
        btnForgotPassword.titleLabel?.font = .mediumValueFont
        btnSignIn.titleLabel?.font = UIFont.semiboldLargeFont
        viewSignInBtn.setFullCornerRadius()
        btnSignIn.setTitle(LoginButtons.btn_login.uppercased(), for: .normal)
        btnForgotPassword.setTitle(Login.forgotPassword, for: .normal)

        /*btnContinue.setCustomCornerRadius(radius: 5)
        btnContinue.setTitle(HomeButtons.continueStr.uppercased(), for: .normal)
        btnContinue.titleLabel?.font = UIFont.semiboldValueFont
        btnContinue.setTitleColor(.themeLightBlueClr, for: .normal)
        btnContinue.backgroundColor = .clear
        btnContinue.superview?.backgroundColor = .clear
        
        self.btnContinue.superview?.layer.borderWidth = 1
        self.btnContinue.superview?.layer.borderColor = UIColor.lightGray.cgColor */
    }
    
    //MARK:- IBActions
    @IBAction func btnForgotPwd(_ sender: UIButton) {
        let dvc =  ForgotpasswordViewController.instantiate(appStoryboard: .ForgotpasswordScreen)
        dvc.modalPresentationStyle = .overFullScreen
        dvc.modalTransitionStyle = .coverVertical
        self.present(dvc, animated: false, completion: nil)
    }
    
    @IBAction func btnRegister(_ sender: Any) {
        let dvc = RegisterViewController.instantiate(appStoryboard: .register)
//            dvc.isPopedUp = isPopedUp
//            dvc.signUpDoneBlock = {
//                self.navigationController?.dismiss(animated: false) {
//                    self.loginDoneBlock?()
//                }
//            }
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    
    
    @IBAction func btnShowHidePassWord(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        sender.setImage(sender.isSelected ? .ic_password_show : .ic_password_not_show, for: .normal)
        tfPassword.isSecureTextEntry = !sender.isSelected
    }
    
    @IBAction func btnSignIn(_ sender: UIButton) {
        if isValidEmailId && isValidPassword {
            self.resignFirstResponder()
            UIView.hideKeyBoard()
            loginResponseModel.email = getString(anything: tfEmail.text).trimming_WS
            loginResponseModel.pwd = getString(anything: self.tfPassword.text)
            getDataFromWebServices()
        }
    } 
    
    @IBAction func btnrememberMeClicked(_ sender: UIButton) {
        if self.imgRememberMe.tag == 0 {
            self.imgRememberMe.image = UIImage.checkboxSelected?.alwaysTemplate
            self.imgRememberMe.tag = 1
        } else {
            self.imgRememberMe.tag = 0
            self.imgRememberMe.image = UIImage.checkbox?.alwaysTemplate
        }
    }
    
    fileprivate var isValidEmailId: Bool {
        if (getString(anything: tfEmail.text).isEmptyString) {
            Global.shared.showBanner(message: LoginMessages.msg_enter_email)
            tfEmail.becomeFirstResponder()
            return false
        } else if !(getString(anything: tfEmail.text).isValidEmail) {
            Global.shared.showBanner(message: LoginMessages.msg_please_enter_valid_email)
            tfEmail.becomeFirstResponder()
            return false
        }
        return true
    }
    
    fileprivate var isValidPassword: Bool {
        if getString(anything: tfPassword.text).count == 0 {
            Global.shared.showBanner(message: LoginMessages.msg_enter_password)
            tfPassword.becomeFirstResponder()
            return false
        }
        return true
    }
    
    func getDataFromWebServices() {
        
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        loginResponseModel.getLogin { responseModel, isSucess in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if isSucess, let dictUser = responseModel {

                if  let userDetails = dictUser["userDetails"] as? Dictionary {

                    let email = getString(anything: userDetails["email"])
                                        
                    print("token - ", getString(anything: userDetails["token"]))
                    UserPreferences.set(value: email, forKey: UserPreferencesKeys.General.email)
                    UserPreferences.set(value: getString(anything: userDetails["id"]), forKey: UserPreferencesKeys.UserInfo.userID)
                    UserPreferences.set(value: getString(anything: userDetails["token"]), forKey: UserPreferencesKeys.General.token)
                    UserPreferences.set(value: getString(anything: userDetails["user_roles_id"]), forKey: UserPreferencesKeys.General.user_roles_id)
                    UserPreferences.set(value: getString(anything: userDetails["first_name"]), forKey: UserPreferencesKeys.General.first_name)
                    UserPreferences.set(value: getString(anything: userDetails["last_name"]), forKey: UserPreferencesKeys.General.last_name)

                    UserPreferences.set(value: true, forKey: UserPreferencesKeys.General.isUserLoggedIn)
                    UserPreferences.set(value: false, forKey: UserPreferencesKeys.General.isSkip)
                    
                    if self.imgRememberMe.tag == 1 {
                        UserPreferences.set(value: true, forKey: UserPreferencesKeys.General.isRemember)
                        UserPreferences.set(value: self.tfEmail.text, forKey: UserPreferencesKeys.General.rememberEmail)
                        UserPreferences.set(value: self.tfPassword.text, forKey: UserPreferencesKeys.General.rememberPass)
                    } else {
                        UserPreferences.set(value: false, forKey: UserPreferencesKeys.General.isRemember)
                        UserPreferences.set(value: "", forKey: UserPreferencesKeys.General.rememberEmail)
                        UserPreferences.set(value: "", forKey: UserPreferencesKeys.General.rememberPass)
                    }
                    
                    let dvc = DashboardViewController.instantiate(appStoryboard: .dashboard)
                    self.navigationController?.pushViewController(dvc, animated: true)
                }
        }
    }
}
}
