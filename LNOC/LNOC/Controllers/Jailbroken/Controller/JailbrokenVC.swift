//
//  JailbrokenVC.swift
//  LNOC
//
//  Created by MacOS on 25/04/22.
//

import UIKit
import IOSSecuritySuite

class JailbrokenVC: UIViewController {

    @IBOutlet var lblTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()


        lblTitle.text = "This Device is JailBroken/Rooted\n\nYou can't access to this `LNOC` application for JailBroken/Rooted DEVICE"
        
        if IOSSecuritySuite.amIRunInEmulator() {
            lblTitle.text = "`LNOC` Application cannot be used in Simulator."
        }
        
        lblTitle.font = UIFont.boldLargeFont
        
    }

}
