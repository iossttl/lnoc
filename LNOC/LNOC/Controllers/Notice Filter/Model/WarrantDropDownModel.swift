//
//  WarrantDropDownModel.swift
//  LNOC
//
//  Created by Dhruv Patel on 11/02/22.
//

import Foundation

class TemplateDropDownModel: NSObject {
    let title: String
    let id: String
    var type: String
    
    enum Keys: String {
        case title = "template_name"
        case id = "id"
        case type = "template_type"
    }
    
    init(dict: [String : Any]) {
        title = getString(anything: dict[Keys.title.rawValue]).decryptStr
        id = getString(anything: dict[Keys.id.rawValue]).decryptStr
        type = getString(anything: dict[Keys.type.rawValue]).decryptStr
    }
}


class LandOwnerDropDownModel: NSObject {
    let title: String
  
    enum Keys: String {
        case title = "name_of_owner"
      
    }
    
    init(dict: [String : Any]) {
        title = getString(anything: dict[Keys.title.rawValue]).decryptStr
     
    }
}

class StatusDropDownModel: NSObject {
    let title: String
    let id: String
    
    enum Keys: String {
        case title = "title"
        case id = "val"
    }
    
    init(dict: [String : Any]) {
        title = getString(anything: dict[Keys.title.rawValue]).decryptStr
        id = getString(anything: dict[Keys.id.rawValue]).decryptStr
    }
}
