//
//  NoticeFilterWebServiceModel.swift
//  LNOC
//
//  Created by Dhruv Patel on 08/02/22.
//

import Foundation

class NoticeFilterWebServiceModel : NSObject {

    //MARK:- URL
    fileprivate var filterURL:String {
        BASEURL + API.getnotice_filters
    }
    
    //MARK:- Enum
    enum Keys : String {
        case template = "noticeTemplates"
        case status = "status"
        case land_Owner = "LandOwnerDetails"
    }
    
    //MARK:- Public Methods
    func getFilterDropDownListApi(block:@escaping (([TemplateDropDownModel], _ isSucess : Bool, [LandOwnerDropDownModel], [StatusDropDownModel]) -> Swift.Void)) {
        
        if (WShandler.shared.CheckInternetConnectivity()) {
            
            let param = WShandler.commonDict()
            
            WShandler.shared.getWebRequest(urlStr: filterURL, param: param) { (json, flag) in
                
                if (flag == 200) {
                    
                    if (getBoolean(anything: json[CommonAPIConstant.key_resultFlag])) {
                        let template = (json[Keys.template.rawValue] as? [Dictionary] ?? []).map({ TemplateDropDownModel(dict: $0) })
                        let LandOwner = (json[Keys.land_Owner.rawValue] as? [Dictionary] ?? []).map({ LandOwnerDropDownModel(dict: $0) })
                        let status = (json[Keys.status.rawValue] as? [Dictionary] ?? []).map({ StatusDropDownModel(dict: $0) })
                        block(template, true,LandOwner, status)
                        
                    } else {
                        Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_message]).decryptStr)
                        block([], false, [], [])
                    }
                    
                } else {
                    Global.shared.showBanner(message: getString(anything: json[CommonAPIConstant.key_errormsg]).decryptStr)
                    block([], false, [], [])
                }
            }
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
            block([], false, [],[])
        }
    }
}
