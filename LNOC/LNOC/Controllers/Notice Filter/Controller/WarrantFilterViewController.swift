//
//  WarrantFilterViewController.swift
//  LNOC
//
//  Created by Dhruv Patel on 08/02/22.
//

import UIKit

protocol ProtocolForNoticeFilter: AnyObject {
    func implimant(NoticeType: String, TemplateType: String, Template: String, Template_Id: String, Status: String, status_Id: String,landOwner:String)
}

class WarrantFilterViewController: HeaderViewController {
    
    
    //View
    @IBOutlet weak var viewBtnSubmitGradient: GradientView!
    @IBOutlet weak var viewForScroll: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var viewForShadow: UIView!
    
    @IBOutlet var lblTitles: [LocalizedLabel]!
    @IBOutlet var txtValues: [UITextField]!
    @IBOutlet var lblSeprator: [UILabel]!
    
    @IBOutlet var lblDropDownValue: [UILabel]!
    
    @IBOutlet var imgDropDown: [UIImageView]!
    
    @IBOutlet weak var lblTemplate: UILabel!
    @IBOutlet weak var lblLandOwner: UILabel!
    @IBOutlet weak var lblStatus: UILabel!

    
    
    @IBOutlet weak var tfNoticeType: UITextField!
    @IBOutlet weak var tfTemplateType: UITextField!
    
    @IBOutlet weak var btnSearch : UIButton!
    @IBOutlet weak var btnCancel : UIButton!
    
    private var arrTemplate = [TemplateDropDownModel]()
    private var arrLandOwner = [LandOwnerDropDownModel]()
    private var arrStatus = [StatusDropDownModel]()
    
    var filter_Notice_Type = ""
    var filter_template_Type = ""
    var filter_template = ""
    var filter_status = ""
    var filter_LandOwner = ""
    var filter_template_id = ""
    var filter_status_id = ""
    
    weak var delegate: ProtocolForNoticeFilter?
    fileprivate var webServiceModel = NoticeFilterWebServiceModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpIPadConstraints()
        self.setUpView()
        self.setupLbl()
        self.setUpHeader()
        self.fontSetUp()
        self.getDataFromWebServices()
        
        self.tfNoticeType.text = filter_Notice_Type
        self.tfTemplateType.text = filter_template_Type
        self.lblTemplate.text = filter_template.isEmpty ? CommonLabels.select : filter_template
        self.lblLandOwner.text = filter_LandOwner.isEmpty ? CommonLabels.select : filter_LandOwner
        self.lblStatus.text = filter_status.isEmpty ? CommonLabels.select : filter_status

    }
    
    
    fileprivate func setUpView() {
        self.viewMain.setCustomCornerRadius(radius: 12)
        self.viewForShadow.setCustomCornerRadius(radius: 12)
        self.viewForShadow.addShadow()
        self.viewBtnSubmitGradient.setFullCornerRadius()
        self.viewBtnSubmitGradient.addGradient(ofColors: [UIColor.themeDarkOrangeClr.cgColor, UIColor.themeLightOrangeClr.cgColor], direction: .leftRight)
        self.btnCancel.setFullCornerRadius()
        
        self.lblSeprator.forEach({
            $0.backgroundColor = .customSeparator
        })
        
        
        self.imgDropDown.forEach({
            $0.image = .ic_down_arrow
            $0.tintColor = .themeColor
        })
        
    }
    
    fileprivate func setupLbl() {
        self.lblTemplate.text = CommonLabels.select
         self.lblLandOwner.text = CommonLabels.select
        self.lblStatus.text = CommonLabels.select
        
        let font = UIFont.regularValueFont
        self.tfNoticeType.setPlaceholder(NoticeFilterPlaceHolders.enter_Notice_Type, color: .lightGray, setFont: font)
        self.tfTemplateType.setPlaceholder(NoticeFilterPlaceHolders.enter_Template_Type, color: .lightGray, setFont: font)
        //
        
        
        /* if isFromAppList {
         self.tfNoticeType.text = getString(anything: dictData[NOCAppListWebServicesModel.Keys.crr_number.rawValue])
         self.tfTemplateType.text = getString(anything: dictData[NOCAppListWebServicesModel.Keys.cs_number.rawValue])
         self.lblTemplate.text = getString(anything: dictData[NOCAppListWebServicesModel.Keys.divison_id.rawValue])
         self.lblLandOwner.text = getString(anything: dictData[NOCAppListWebServicesModel.Keys.land_holder.rawValue])
         self.lblStatus.text = getString(anything: dictData[NOCAppListWebServicesModel.Keys.location_of_property.rawValue])
         }*/
    }
    
    fileprivate func fontSetUp() {
        self.lblTitles.forEach({
            $0.font = .regularTitleFont
            $0.textColor = .customBlackTitle
        })
        
        self.txtValues.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
        })
        
        self.lblDropDownValue.forEach({
            $0.font = .regularValueFont
            $0.textColor = .customBlack
        })
        
        btnCancel.titleLabel?.font = UIFont.semiboldLargeFont
        btnSearch.titleLabel?.font = UIFont.semiboldLargeFont
        
        btnSearch.isUserInteractionEnabled = true
        btnCancel.isUserInteractionEnabled = true
        tfNoticeType.isUserInteractionEnabled = true
        tfTemplateType.isUserInteractionEnabled = true
    }
    
    fileprivate func setUpHeader() {
        setUpHeaderTitle(strHeaderTitle: Headers.Filter)
        setHeaderView_BackImage()
    }
    
    fileprivate func setUpIPadConstraints() {
        changeLeadingTrailingForiPad(view: viewForScroll)
        changeHeightForiPad(view: btnCancel)
        changeHeightForiPad(view: btnSearch)
        changeHeightForiPad(view: self.tfNoticeType)
        changeHeightForiPad(view: self.tfTemplateType)
    }
    
    //MARK:- Selector Methods
    @IBAction func TapOnSEarchBtn(_ sender: LocalizedButton) {
        let notice_type = getString(anything: self.tfNoticeType.text)
        let template_type = getString(anything: self.tfTemplateType.text)
        
        self.delegate?.implimant(NoticeType: notice_type, TemplateType: template_type, Template: self.filter_template, Template_Id: self.filter_template_id, Status: self.filter_status, status_Id: self.filter_status_id,landOwner:self.filter_LandOwner)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func TapOnSelectTemplateBtn(_ sender: UIButton) {
        //        self.arrTemplate.append(DropDownModel()
        var list = self.arrTemplate.map({ $0.title })
//        list.append("Warrant")
//        list.append("Notice")
        list.insert(CommonLabels.select, at: 0)
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblTemplate.text, arrSource: [list], doneAction: { (rows, value) in
            self.lblTemplate.text = getString(anything: value)
            if getString(anything: value) == CommonLabels.select {
                self.filter_template = ""
                self.filter_template_id = ""
            } else {
                self.filter_template = getString(anything: value)
                self.filter_template_id = self.arrTemplate[rows[0] - 1].id
            }
            
        }, animated: true, origin: sender, onVC: self)
    }
    
    
    @IBAction func TapOnSelectLandOwnerBtn(_ sender: UIButton) {
        
        //        self.arrTemplate.append(DropDownModel()
        var list = self.arrLandOwner.map({ $0.title })
//        list.append("Warrant")
//        list.append("Notice")
        list.insert(CommonLabels.select, at: 0)
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblLandOwner.text, arrSource: [list], doneAction: { (rows, value) in
            self.lblLandOwner.text = getString(anything: value)
            if getString(anything: value) == CommonLabels.select {
                self.filter_LandOwner = ""
            } else {
                self.filter_LandOwner = getString(anything: value)
            }
            
        }, animated: true, origin: sender, onVC: self)
    }
    
    
    
    @IBAction func TapOnSelectStatusBtn(_ sender: UIButton) {
        var list = self.arrStatus.map({ $0.title })
        list.insert(CommonLabels.select, at: 0)
//        list.append("Actiove")
//        list.append("Inactive")
        StringPickerViewController.show(pickerType: .single, initialSelection: self.lblStatus.text, arrSource: [list], doneAction: { (rows, value) in
            self.lblStatus.text = getString(anything: value)
            if getString(anything: value) == CommonLabels.select {
                self.filter_status = ""
                self.filter_status_id = ""
            } else {
                self.filter_status = getString(anything: value)
                self.filter_status_id = self.arrStatus[rows[0] - 1].id
            }
        }, animated: true, origin: sender, onVC: self)
    }
    
    @IBAction func TapOnReset(_ sender: UIButton) {
        self.txtValues.forEach({
            $0.text = ""
        })
        self.lblTemplate.text = CommonLabels.select
        self.lblStatus.text = CommonLabels.select
        self.lblLandOwner.text = CommonLabels.select
        self.delegate?.implimant(NoticeType: "", TemplateType: "", Template: "", Template_Id: "", Status: "", status_Id: "",landOwner: "")
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK:-  Web Service model
extension WarrantFilterViewController {
    func getDataFromWebServices() {
        CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        self.webServiceModel.getFilterDropDownListApi { arr_Template, isSucess, arr_LandOwner,arr_status in
            CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
            if isSucess{
                self.arrTemplate = arr_Template
                self.arrLandOwner = arr_LandOwner
                self.arrStatus = arr_status
            }
        }
    }
}
