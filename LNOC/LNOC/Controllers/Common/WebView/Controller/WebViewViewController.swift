//
//  WebViewViewController.swift
//  Vidyanjali
//
//  Created by Vikram Jagad on 01/12/20.
//  Copyright © 2020 Vikram Jagad. All rights reserved.
//

import UIKit
import WebKit

class WebViewViewController: HeaderViewController {
    //MARK:- Interface Builder
    //UIView Outlets
    @IBOutlet weak var viewForWeb: UIView!
    
    //MARK:- Variables
    //Public
    var customTitle = ""
    var pageName = ""
    var urlStr = ""
    var typeOfWebView : webViewType = .aboutUS
    var id = ""
    //Private9
    fileprivate var webView: WKWebView!
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpVC()
    }
    
    fileprivate func setUpVC() {
        setUpIPadConstraint()
        setupHeader()
        setUpWebView()
    }
    
    fileprivate func setUpIPadConstraint() {
        changeLeadingTrailingForiPad(view: view)
    }
    
    fileprivate func setupHeader() {                                                    
        if urlStr.isStringEmpty {
            setUpHeaderTitle(strHeaderTitle: typeOfWebView.displayStr)
        } else {
            setUpHeaderTitle(strHeaderTitle: customTitle)
        }
        setHeaderView_BackImage()
    }
    
    fileprivate func setUpWebView() {
        let preferences = WKPreferences()
        preferences.javaScriptEnabled = true
        let configuration = WKWebViewConfiguration()
        configuration.preferences = preferences
        webView = WKWebView(frame: CGRect(x: 0, y: 0, width: viewForWeb.frame.size.width, height: viewForWeb.frame.size.height), configuration: configuration)
        webView.configuration.preferences.javaScriptEnabled = true
        webView.scrollView.isScrollEnabled = true
        webView.navigationDelegate = self
        viewForWeb.addSubview(webView)
        webView.translatesAutoresizingMaskIntoConstraints = false
        webView.topAnchor.constraint(equalTo: viewForWeb.topAnchor, constant: 0).isActive = true
        webView.bottomAnchor.constraint(equalTo: viewForWeb.bottomAnchor, constant: 0).isActive = true
        webView.leadingAnchor.constraint(equalTo: viewForWeb.leadingAnchor, constant: 0).isActive = true
        viewForWeb.trailingAnchor.constraint(equalTo: webView.trailingAnchor, constant: 0).isActive = true
        
        if urlStr.isStringEmpty {
            if let url = URL(string: typeOfWebView.apiValue) {
                webView.load(URLRequest(url: url))
            }
        } else {
            if let url = URL(string: urlStr) {
            webView.load(URLRequest(url:url))
            }
        }
    }
}

//MARK:- WKNavigationDelegate Methods
extension WebViewViewController: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        if (WShandler.shared.CheckInternetConnectivity()) {
            CustomActivityIndicator.sharedInstance.showIndicator(view: self.view)
        } else {
            Global.shared.showBanner(message: CommonMessages.msg_Internet_Issue)
        }
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        CustomActivityIndicator.sharedInstance.hideIndicator(view: self.view)
        print(error)
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        //Open in Safari for Paytm Status Html only
        if navigationAction.navigationType == .linkActivated {

            if let newURL = navigationAction.request.url, UIApplication.shared.canOpenURL(newURL), newURL.absoluteString.contains("registration") {
                self.webView.load(navigationAction.request)
                decisionHandler(.allow)
                
            } else if let newURL = navigationAction.request.url, UIApplication.shared.canOpenURL(newURL) {
                UIApplication.shared.open(newURL, options: [:], completionHandler: nil)
                decisionHandler(.cancel)
                
            } else {
                self.webView.load(navigationAction.request)
                decisionHandler(.allow)
            }
            
        } else {
            decisionHandler(.allow)
        }
    }
}
