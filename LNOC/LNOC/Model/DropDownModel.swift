//
//  DropDownModel.swift
//  Vidyanjali
//
//  Created by Nimitt Nemade on 12/11/20.
//  Copyright © 2020 Vikram Jagad. All rights reserved.
//

import UIKit

class DropDownModel: NSObject {
    let title: String
    let id: String
    var isSelected: Bool
    
    enum Keys: String {
        case title = "title"
        case id = "id"
        case isSelected = "isSelected"
        case interestId = "InterestID"
        case interestName = "InterestName"
        case classId = "ClassId"
        case className = "ClassName"
        case stateId = "StateID"
        case stateName = "StateName"
        case districtId = "DistrictID"
        case districtName = "district_name"
        case districtCode = "district_code"
        case locationId = "LocationID"
        case locationName = "LocationName"
        case ID = "ID"
        case name = "name"
        case cityCode = "city_code"
        case cityName = "city_name"
    }
    
    init(dict: [String : Any]) {
        title = getString(anything: dict[Keys.title.rawValue]).decryptStr
        id = getString(anything: dict[Keys.id.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictTenures : [String : Any]) {
        title = getString(anything: dictTenures[Keys.name.rawValue]).decryptStr
        id = getString(anything: dictTenures[Keys.id.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictSubcategory: [String : Any]) {
        title = getString(anything: dictSubcategory[Keys.name.rawValue]).decryptStr
        id = getString(anything: dictSubcategory[Keys.ID.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictMaterialList: [String : Any]) {
        title = getString(anything: dictMaterialList[Keys.name.rawValue]).decryptStr
        id = getString(anything: dictMaterialList[Keys.ID.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictContributionFilterStatus: [String : Any]) {
        title = getString(anything: dictContributionFilterStatus[Keys.title.rawValue]).decryptStr
        id = getString(anything: dictContributionFilterStatus[Keys.id.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictGender: [String : Any]) {
        title = getString(anything: dictGender[Keys.title.rawValue]).decryptStr
        id = getString(anything: dictGender[Keys.id.rawValue]).decryptStr
        isSelected = false
    }

    init(dictActivityCategory: [String : Any]) {
        title = getString(anything: dictActivityCategory[Keys.interestName.rawValue]).decryptStr
        id = getString(anything: dictActivityCategory[Keys.interestId.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictActivityName: [String : Any]) {
        title = getString(anything: dictActivityName[Keys.interestName.rawValue]).decryptStr
        id = getString(anything: dictActivityName[Keys.interestId.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictActivityStatus: [String : Any]) {
        title = getString(anything: dictActivityStatus[Keys.title.rawValue]).decryptStr
        id = getString(anything: dictActivityStatus[Keys.id.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictClassGrade: [String : Any]) {
        title = getString(anything: dictClassGrade[Keys.className.rawValue]).decryptStr
        id = getString(anything: dictClassGrade[Keys.classId.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictRequiredSpecializaion: [String : Any]) {
        title = getString(anything: dictRequiredSpecializaion[Keys.interestName.rawValue]).decryptStr
        id = getString(anything: dictRequiredSpecializaion[Keys.interestId.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictQualification: [String : Any]) {
        title = getString(anything: dictQualification[Keys.name.rawValue]).decryptStr
        id = getString(anything: dictQualification[Keys.ID.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictEmployeeStatus: [String : Any]) {
        title = getString(anything: dictEmployeeStatus[Keys.name.rawValue]).decryptStr
        id = getString(anything: dictEmployeeStatus[Keys.ID.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictState: [String : Any]) {
        title = getString(anything: dictState[Keys.stateName.rawValue]).decryptStr
        id = getString(anything: dictState[Keys.stateId.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictDistrict: [String : Any]) {
        title = getString(anything: dictDistrict[Keys.districtName.rawValue]).decryptStr
        id = getString(anything: dictDistrict[Keys.districtCode.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictCity: [String : Any]) {
        title = getString(anything: dictCity[Keys.cityName.rawValue]).decryptStr
        id = getString(anything: dictCity[Keys.cityCode.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictNOCType: [String : Any]) {
        title = getString(anything: dictNOCType[Keys.title.rawValue]).decryptStr
        id = getString(anything: dictNOCType[Keys.id.rawValue]).decryptStr
        isSelected = false
    }
    
    init(dictUseOfLand: [String : Any]) {
        title = getString(anything: dictUseOfLand[Keys.name.rawValue]).decryptStr
        id = getString(anything: dictUseOfLand[Keys.id.rawValue]).decryptStr
        isSelected = false
    }
    
    
    init(dictLocation: [String : Any]) {
        title = getString(anything: dictLocation[Keys.locationName.rawValue]).decryptStr
        id = getString(anything: dictLocation[Keys.locationId.rawValue]).decryptStr
        isSelected = false
    }
}

class DropDownResponseModel: NSObject {
    let resultFlag: Bool
    let message: String
    let list: [DropDownModel]
    
    enum Keys: String {
        case list = "list"
        case contributionmaterialList = "ContributionList"
        case contributionFilterstatus = "ContributionStatus"


        case activityCategory = "activityCategoryList"
        case activityName = "InterestAreasList"
        case activityStatus = "ActivityStatus"
        case classGrade = "class"
        case requiredSpecializaion = "RequiredSpecializaionAreasList"

        case gender = "gender"
        case qualification = "QualificationList"
        case emplyeeStatus = "EmploymentList"
        case areaofInterest = "areaofInterest"
        case getState = "StateList"
        case getDistrict = "DistrictList"
        case getLocation = "LocationList"
        case blockList = "BlockList"
        case typeList = "TypeList"
        case uploadDocument = "uploadDocument"
        case message
        case landList = "landList"
    }
    
    init(dict: [String : Any]) {
        resultFlag = getBoolean(anything: dict[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dict[CommonAPIConstant.key_message]).decryptStr
        if let arr = dict[Keys.list.rawValue] as? [[String : Any]] {
            list = arr.map({ DropDownModel(dict: $0) })
        } else {
            list = []
        }
        super.init()
    }
    
    init(dictSubcategory: [String : Any]) {
        resultFlag = getBoolean(anything: dictSubcategory[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictSubcategory[Keys.message.rawValue]).decryptStr
        if let arr = dictSubcategory[Keys.activityName.rawValue] as? [[String : Any]] {
            list = arr.map({ DropDownModel(dictSubcategory: $0) })
        } else {
            list = []
        }
        super.init()
    }
    
    init(dictMaterialList: [String : Any]) {
        resultFlag = getBoolean(anything: dictMaterialList[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictMaterialList[Keys.message.rawValue]).decryptStr
        if let arr = dictMaterialList[Keys.contributionmaterialList.rawValue] as? [[String : Any]] {
            list = arr.map({ DropDownModel(dictMaterialList: $0) })
        } else {
            list = []
        }
        super.init()
    }
    
    init(dictContributionFilterStatus: [String : Any]) {
        resultFlag = getBoolean(anything: dictContributionFilterStatus[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictContributionFilterStatus[CommonAPIConstant.key_message]).decryptStr
        if let arr = dictContributionFilterStatus[Keys.contributionFilterstatus.rawValue] as? [[String : Any]] {
            list = arr.map({ DropDownModel(dictContributionFilterStatus: $0) })
        } else {
            list = []
        }
        super.init()
    }
    
    init(dictGender: [String : Any]) {
        resultFlag = getBoolean(anything: dictGender[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictGender[CommonAPIConstant.key_message]).decryptStr
        if let arr = dictGender[Keys.gender.rawValue] as? [[String : Any]] {
            list = arr.map({ DropDownModel(dictGender: $0) })
        } else {
            list = []
        }
        super.init()
    }

    init(dictActivityCategory: [String : Any]) {
        resultFlag = getBoolean(anything: dictActivityCategory[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictActivityCategory[Keys.message.rawValue]).decryptStr
        if let arr = dictActivityCategory[Keys.activityCategory.rawValue] as? [[String : Any]] {
            list = arr.map({ DropDownModel(dictActivityCategory: $0) })
        } else {
            list = []
        }
        super.init()
    }
    
    init(dictActivityName: [String : Any]) {
        resultFlag = getBoolean(anything: dictActivityName[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictActivityName[Keys.message.rawValue]).decryptStr
        if let arr = dictActivityName[Keys.activityName.rawValue] as? [[String : Any]] {
            list = arr.map({ DropDownModel(dictActivityName: $0) })
        } else {
            list = []
        }
        super.init()
    }
    
    init(dictActivityStatus: [String : Any]) {
        resultFlag = getBoolean(anything: dictActivityStatus[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictActivityStatus[Keys.message.rawValue]).decryptStr
        if let arr = dictActivityStatus[Keys.activityStatus.rawValue] as? [[String : Any]] {
            list = arr.map({ DropDownModel(dictActivityStatus: $0) })
        } else {
            list = []
        }
        super.init()
    }
    
    init(dictClassGrade: [String : Any]) {
        resultFlag = getBoolean(anything: dictClassGrade[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictClassGrade[Keys.message.rawValue]).decryptStr
        if let arr = dictClassGrade[Keys.classGrade.rawValue] as? [[String : Any]] {
            list = arr.map({ DropDownModel(dictClassGrade: $0) })
        } else {
            list = []
        }
        super.init()
    }
    
    init(dictRequiredSpecializaion: [String : Any]) {
        resultFlag = getBoolean(anything: dictRequiredSpecializaion[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictRequiredSpecializaion[Keys.message.rawValue]).decryptStr
        if let arr = dictRequiredSpecializaion[Keys.requiredSpecializaion.rawValue] as? [[String : Any]] {
            list = arr.map({ DropDownModel(dictRequiredSpecializaion: $0) })
        } else {
            list = []
        }
        super.init()
    }
    
    init(dictQualification: [String : Any]) {
        resultFlag = getBoolean(anything: dictQualification[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictQualification[Keys.message.rawValue]).decryptStr
        if let arr = dictQualification[Keys.qualification.rawValue] as? [[String:Any]] {
            list = arr.map({DropDownModel(dictQualification: $0)})
        } else {
            list = []
        }
    }
    
    init(dictEmployeeStatus:[String:Any]) {
        resultFlag = getBoolean(anything: dictEmployeeStatus[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictEmployeeStatus[Keys.message.rawValue]).decryptStr
        if let arr = dictEmployeeStatus[Keys.emplyeeStatus.rawValue] as? [[String:Any]] {
            list = arr.map({DropDownModel(dictEmployeeStatus: $0)})
        } else {
            list = []
        }
    }
    
    init(dictState:[String:Any]) {
        resultFlag = getBoolean(anything: dictState[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictState[Keys.message.rawValue]).decryptStr
        if let arr = dictState[Keys.getState.rawValue] as? [[String:Any]] {
            list = arr.map({DropDownModel(dictState: $0)})
        } else {
            list = []
        }
    }
    
    init(dictDistrict:[String:Any]) {
        resultFlag = getBoolean(anything: dictDistrict[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictDistrict[Keys.message.rawValue]).decryptStr
        if let arr = dictDistrict[Keys.landList.rawValue] as? [[String:Any]] {
            list = arr.map({DropDownModel(dictDistrict: $0)})
        } else {
            list = []
        }
    }
    
    
    init(dictCity:[String:Any]) {
        resultFlag = getBoolean(anything: dictCity[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictCity[Keys.message.rawValue]).decryptStr
        if let arr = dictCity[Keys.landList.rawValue] as? [[String:Any]] {
            list = arr.map({DropDownModel(dictCity: $0)})
        } else {
            list = []
        }
    }
    
    
    
    init(dictNOCType:[String:Any]) {
        resultFlag = getBoolean(anything: dictNOCType[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictNOCType[Keys.message.rawValue]).decryptStr
        if let arr = dictNOCType[Keys.landList.rawValue] as? [[String:Any]] {
            list = arr.map({DropDownModel(dictNOCType: $0)})
        } else {
            list = []
        }
    }
    
    init(dictUseOfLand:[String:Any]) {
        resultFlag = getBoolean(anything: dictUseOfLand[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictUseOfLand[Keys.message.rawValue]).decryptStr
        if let arr = dictUseOfLand[Keys.landList.rawValue] as? [[String:Any]] {
            list = arr.map({DropDownModel(dictUseOfLand: $0)})
        } else {
            list = []
        }
    }
    
    init(dictLocation: [String : Any]) {
        resultFlag = getBoolean(anything: dictLocation[CommonAPIConstant.key_resultFlag])
        message = getString(anything: dictLocation[Keys.message.rawValue]).decryptStr
        if let arr = dictLocation[Keys.getLocation.rawValue] as? [[String:Any]] {
            list = arr.map({DropDownModel(dictLocation: $0)})
        } else {
            list = []
        }
    }
}
