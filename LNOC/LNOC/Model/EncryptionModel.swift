////
////  EncryptionModel.swift
////
////  Created by Vikram Jagad on 17/01/21.
////
//
import UIKit
import CryptoSwift

final class EncryptionModel: NSObject {
    //MARK:- Variables
    //Private
//    private let iv: String = "nwmaaetrmi0oo0rn0ia0al0l"
    private let iv: String = "lristeamlnoco0rn0ia0al0l" // Global.shared.getIVKey()
    static let `default` = EncryptionModel()
    
    private override init() {
        super.init()
    }
    
    //MARK:- Public Methods
    func encrypt(str: String, key: String = DeviceID) -> String {
        if str.isEmptyString {
            return ""
        }
        let newKey = key.sha256()[0..<32]
        let newIv = iv.sha256()[0..<16]
        let data: Array<UInt8> = Array(str.utf8)
        var encryptedStr = ""
        do {
            let enc = try AES(key: newKey, iv: newIv).encrypt(data)
            let encData = Data(bytes: enc, count: enc.count)
            let base64String = encData.base64EncodedString(options: .init(rawValue: 0))
            encryptedStr = base64String
        } catch let error {
            DebugLog("Error encrypting - \(error.localizedDescription)")
        }
        encryptedStr = getString(anything: encryptedStr.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed))
        return encryptedStr
    }
    
    func encryptDic(dic : [String : Any]) -> [String : Any] {
        var encryptedDic = [String : Any]()
        for i in dic {
            encryptedDic[i.key] = self.encrypt(str: "\(i.value)")
        }
        return encryptedDic
    }
    
    func decrypt(str: String, key: String = DeviceID) -> String {
        return EncryptionOtherKeys.decryptWithKeyIV(str: str, key: key, newiv: iv)
    }
}

final class EncryptionOtherKeys {
    
    class func decryptWithKeyIV(str: String, key: String, newiv: String) -> String {
        let lastTwoChars = str.substring(from: str.count - 1)
        var text = str
        if (lastTwoChars == "\n") {
            text = text.substring(to: text.count - 1)
        }
        let newKey = key.sha256()[0..<32]
        let newIv = newiv.sha256()[0..<16]
        var decryptedStr = ""
        do {
            if (text.isEmptyString) {
                decryptedStr = text
            } else if let base64Data = Data(base64Encoded: text) {
                let data: Array<UInt8> = Array(base64Data)
                let dec = try AES(key: newKey, iv: newIv).decrypt(data)
                let decData = Data(bytes: dec, count: dec.count)
                decryptedStr = getString(anything: String(data: decData, encoding: .utf8))
            } else {
                decryptedStr = text
            }
        } catch let error {
            DebugLog("Error decrypting - \(error.localizedDescription)")
        }
        return decryptedStr
    }
}

extension Dictionary {
    func encryptDic() -> [String : Any] {
        var encryptedDic = [String : Any]()
        for i in self {
            encryptedDic[i.key] = EncryptionModel.default.encrypt(str: "\(i.value)")
        }
        return encryptedDic
    }
    
    func decryptDic() -> [String : Any] {
        var decryptedDic = [String : Any]()
        self.forEach { dic in
            decryptedDic[dic.key] = "\(dic.value)".decryptStr
        }
        return decryptedDic
    }
}

extension String {
    var encryptStr : String {
        return EncryptionModel.default.encrypt(str: self)
    }
    var decryptStr : String {
        return EncryptionModel.default.decrypt(str: self)
    }
}
