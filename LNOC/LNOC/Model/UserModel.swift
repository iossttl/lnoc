//
//  UserModel.swift
//  Vidyanjali
//
//  Created by Vikram Jagad on 12/11/20.
//  Copyright © 2020 Vikram Jagad. All rights reserved.
//

import UIKit
import UIKit

class UserModel: NSObject, NSCoding {
    
    let first_name : String
    let middle_name : String
    let last_name : String
    var email: String
    let mobileno: String
    let gender: String
    let imageUrl: String
    let password:String
    let token: String
    let access_token: String
    let date_of_birth: String
    var id: String
    let device_id :String
    let address : String
    let adhaar_card: String
    let adhaar_card_file: String
    let bank_account_number:String
    let bank_name:String
    let ms_cities_id:String
    let ms_countries_id :String
    let ms_districts_id : String
    let ms_states_id: String
    let pan_card_file: String
    let pan_no:String
    let user_roles_id:String
    
    class var currentUser: UserModel {
        if let decoded = UserPreferences.data(forKey: UserPreferencesKeys.UserInfo.userDetail){
            return NSKeyedUnarchiver.unarchiveObject(with: decoded) as? UserModel ?? UserModel(dict: [:], uuid: "")
        }
        return UserModel(dict: [:], uuid: "")
        
    }
    
    class var signedIn: Bool {
        get {
            return UserPreferences.bool(forKey: UserPreferencesKeys.General.isUserLoggedIn)
        } set {
            UserPreferences.set(value: newValue, forKey: UserPreferencesKeys.General.isUserLoggedIn)
        }
    }
    
    
    class var email: String {
        get {
            return UserPreferences.string(forKey: UserPreferencesKeys.General.email)
        } set {
            UserPreferences.set(value: newValue, forKey: UserPreferencesKeys.General.email)
        }
    }
    
    
    class var userId: String {
        get {
            return UserModel.currentUser.id
        } set(newString) {
            let userModel = UserModel.currentUser
            userModel.id = newString
            userModel.setUserModel()
        }
    }
    
    enum Keys: String {
        case first_name = "first_name"
        case middle_name = "middle_name"
        case last_name = "last_name"
        case email = "email"
        case mobileNo = "mobile_no"
        case gender = "gender"
        case imageUrl = "picture"
        case token
        case access_token
        case date_of_birth = "date_of_birth"
        case id
        case password = "pwd"
        case device_id = "device_id"
        case user_roles_id = "user_roles_id"
        case address = "address"
        case pan_no = "pan_no"
        case pan_card_file = "pan_card_file"
        case adhaar_card = "adhaar_card"
        case adhaar_card_file = "adhaar_card_file"
        case bank_name = "bank_name"
        case bank_account_number = "bank_account_number"
        case ms_states_id = "ms_states_id"
        case ms_districts_id = "ms_districts_id"
        case ms_countries_id = "ms_countries_id"
        case ms_cities_id = "ms_cities_id"
        
        
    }
/* init(dict: [String : Any]) {
        first_name = getString(anything: dict[Keys.first_name.rawValue])
        last_name = getString(anything: dict[Keys.last_name.rawValue])
        email = getString(anything: dict[Keys.email.rawValue])
        mobileno = getString(anything: dict[Keys.mobileNo.rawValue])
        gender = getString(anything: dict[Keys.gender.rawValue])
        imageUrl = getString(anything: dict[Keys.imageUrl.rawValue])
        token = getString(anything: dict[Keys.token.rawValue])
        access_token = getString(anything: dict[Keys.access_token.rawValue])
        date_of_birth = getString(anything: dict[Keys.date_of_birth.rawValue])
        id = getString(anything: dict[Keys.id.rawValue])
        password = getString(anything: dict[Keys.password.rawValue])
        device_id = getString(anything: dict[Keys.device_id.rawValue])
        super.init()
    }*/
    
    
    init(dict: [String : Any],uuid:String) {
        first_name = getString(anything: dict[Keys.first_name.rawValue]) //EncryptionModel.default.decrypt(str: getString(anything: dict[Keys.first_name.rawValue]), key: uuid)//getString(anything: dict[Keys.first_name.rawValue])
        middle_name = getString(anything: dict[Keys.middle_name.rawValue])
        last_name = getString(anything: dict[Keys.last_name.rawValue]) // EncryptionModel.default.decrypt(str: getString(anything: dict[Keys.last_name.rawValue]), key: uuid)//getString(anything: dict[Keys.last_name.rawValue])
        email = getString(anything: dict[Keys.email.rawValue]) // EncryptionModel.default.decrypt(str: getString(anything: dict[Keys.email.rawValue]), key: uuid)//getString(anything: dict[Keys.email.rawValue])
        mobileno = getString(anything: dict[Keys.mobileNo.rawValue]) //EncryptionModel.default.decrypt(str: getString(anything: dict[Keys.mobileNo.rawValue]), key: uuid)//getString(anything: dict[Keys.mobileNo.rawValue])
        gender = getString(anything: dict[Keys.gender.rawValue]) // EncryptionModel.default.decrypt(str: getString(anything: dict[Keys.gender.rawValue]), key: uuid)
        imageUrl = getString(anything: dict[Keys.imageUrl.rawValue]) // EncryptionModel.default.decrypt(str: getString(anything: dict[Keys.imageUrl.rawValue]), key: uuid)
        token = getString(anything: dict[Keys.token.rawValue]) // EncryptionModel.default.decrypt(str: getString(anything: dict[Keys.token.rawValue]), key: uuid)//getString(anything: dict[Keys.token.rawValue])
        access_token = getString(anything: dict[Keys.access_token.rawValue]) // EncryptionModel.default.decrypt(str: getString(anything: dict[Keys.access_token.rawValue]), key: uuid)//getString(anything: dict[Keys.access_token.rawValue])
        date_of_birth = getString(anything: dict[Keys.date_of_birth.rawValue]) // EncryptionModel.default.decrypt(str: getString(anything: dict[Keys.date_of_birth.rawValue]), key: uuid)
        id = getString(anything: dict[Keys.id.rawValue]) // EncryptionModel.default.decrypt(str: getString(anything: dict[Keys.id.rawValue]), key: uuid)//getString(anything: dict[Keys.id.rawValue])
        password = getString(anything: dict[Keys.password.rawValue]) // EncryptionModel.default.decrypt(str: getString(anything: dict[Keys.password.rawValue]), key: uuid)//getString(anything: dict[Keys.password.rawValue])
        device_id = getString(anything: dict[Keys.device_id.rawValue])//EncryptionModel.default.decrypt(str: getString(anything: dict[Keys.device_id.rawValue]), key: uuid)//getString(anything: dict[Keys.device_id.rawValue])
        
        address = getString(anything: dict[Keys.address.rawValue])
        adhaar_card = getString(anything: dict[Keys.adhaar_card.rawValue])
        adhaar_card_file = getString(anything: dict[Keys.adhaar_card_file.rawValue])
        bank_name = getString(anything: dict[Keys.bank_name.rawValue])
        bank_account_number = getString(anything: dict[Keys.bank_account_number.rawValue])
        ms_countries_id = getString(anything: dict[Keys.ms_countries_id.rawValue])
        ms_states_id = getString(anything: dict[Keys.ms_states_id.rawValue])
        ms_districts_id = getString(anything: dict[Keys.ms_districts_id.rawValue])
        ms_cities_id = getString(anything: dict[Keys.ms_cities_id.rawValue])
        pan_no = getString(anything: dict[Keys.pan_no.rawValue])
        pan_card_file = getString(anything: dict[Keys.pan_card_file.rawValue])
        user_roles_id = getString(anything: dict[Keys.user_roles_id.rawValue])
        
        super.init()
    }
    
    
    
    
    /**
     Generates description of the object in the form of a NSDictionary.
     - returns: A Key value pair containing all valid values in the object.
     */
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        dictionary[Keys.first_name.rawValue] = first_name
        dictionary[Keys.middle_name.rawValue] = middle_name
        dictionary[Keys.last_name.rawValue] = last_name
        dictionary[Keys.email.rawValue] = email
        dictionary[Keys.mobileNo.rawValue] = mobileno
        dictionary[Keys.gender.rawValue] = gender
        dictionary[Keys.imageUrl.rawValue] = imageUrl
        dictionary[Keys.token.rawValue] = token
        dictionary[Keys.access_token.rawValue] = access_token
        dictionary[Keys.date_of_birth.rawValue] = date_of_birth
        dictionary[Keys.id.rawValue] = id
        dictionary[Keys.device_id.rawValue] = device_id
        dictionary[Keys.address.rawValue] = address
        dictionary[Keys.adhaar_card.rawValue] = adhaar_card
        dictionary[Keys.adhaar_card_file.rawValue] = adhaar_card_file
        dictionary[Keys.bank_name.rawValue] = bank_name
        dictionary[Keys.bank_account_number.rawValue] = bank_account_number
        dictionary[Keys.ms_countries_id.rawValue] = ms_countries_id
        dictionary[Keys.ms_states_id.rawValue] = ms_states_id
        dictionary[Keys.ms_districts_id.rawValue] = ms_districts_id
        dictionary[Keys.ms_cities_id.rawValue] = ms_cities_id
        dictionary[Keys.pan_no.rawValue] = pan_no
        dictionary[Keys.pan_card_file.rawValue] = pan_card_file
        dictionary[Keys.user_roles_id.rawValue] = user_roles_id
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        first_name = getString(anything: aDecoder.decodeObject(forKey: Keys.first_name.rawValue))
        middle_name = getString(anything: aDecoder.decodeObject(forKey: Keys.middle_name.rawValue))
        last_name = getString(anything: aDecoder.decodeObject(forKey: Keys.last_name.rawValue))
        email = getString(anything: aDecoder.decodeObject(forKey: Keys.email.rawValue))
        mobileno = getString(anything: aDecoder.decodeObject(forKey: Keys.mobileNo.rawValue))
        gender = getString(anything: aDecoder.decodeObject(forKey: Keys.gender.rawValue))
        imageUrl = getString(anything: aDecoder.decodeObject(forKey: Keys.imageUrl.rawValue))
        token = getString(anything: aDecoder.decodeObject(forKey:Keys.token.rawValue))
        access_token = getString(anything: aDecoder.decodeObject(forKey:Keys.access_token.rawValue))
        date_of_birth = getString(anything: aDecoder.decodeObject(forKey: Keys.date_of_birth.rawValue))
        id = getString(anything: aDecoder.decodeObject(forKey: Keys.id.rawValue))
        password = getString(anything: aDecoder.decodeObject(forKey: Keys.password.rawValue))
        device_id = getString(anything: aDecoder.decodeObject(forKey: Keys.device_id.rawValue))
        
        address = getString(anything: aDecoder.decodeObject(forKey:Keys.address.rawValue))
        adhaar_card = getString(anything: aDecoder.decodeObject(forKey:Keys.adhaar_card.rawValue))
        adhaar_card_file = getString(anything: aDecoder.decodeObject(forKey:Keys.adhaar_card_file.rawValue))
        bank_name = getString(anything: aDecoder.decodeObject(forKey:Keys.bank_name.rawValue))
        bank_account_number = getString(anything:aDecoder.decodeObject(forKey:Keys.bank_account_number.rawValue))
        ms_countries_id = getString(anything: aDecoder.decodeObject(forKey:Keys.ms_countries_id.rawValue))
        ms_states_id = getString(anything: aDecoder.decodeObject(forKey:Keys.ms_states_id.rawValue))
        ms_districts_id = getString(anything: aDecoder.decodeObject(forKey:Keys.ms_districts_id.rawValue))
        ms_cities_id = getString(anything: aDecoder.decodeObject(forKey:Keys.ms_cities_id.rawValue))
        pan_no = getString(anything: aDecoder.decodeObject(forKey:Keys.pan_no.rawValue))
        pan_card_file = getString(anything: aDecoder.decodeObject(forKey:Keys.pan_card_file.rawValue))
        user_roles_id = getString(anything: aDecoder.decodeObject(forKey:Keys.user_roles_id.rawValue))
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(first_name, forKey: Keys.first_name.rawValue)
        aCoder.encode(middle_name, forKey: Keys.middle_name.rawValue)
        aCoder.encode(last_name, forKey: Keys.last_name.rawValue)
        aCoder.encode(email, forKey: Keys.email.rawValue)
        aCoder.encode(mobileno, forKey: Keys.mobileNo.rawValue)
        aCoder.encode(gender, forKey: Keys.gender.rawValue)
        aCoder.encode(imageUrl, forKey: Keys.imageUrl.rawValue)
        aCoder.encode(token, forKey: Keys.token.rawValue)
        aCoder.encode(access_token, forKey: Keys.access_token.rawValue)
        aCoder.encode(date_of_birth, forKey: Keys.date_of_birth.rawValue)
        aCoder.encode(id, forKey: Keys.id.rawValue)
        aCoder.encode(device_id, forKey: Keys.device_id.rawValue)
        
        aCoder.encode(address, forKey: Keys.address.rawValue)
        aCoder.encode(adhaar_card, forKey: Keys.adhaar_card.rawValue)
        aCoder.encode(adhaar_card_file, forKey: Keys.adhaar_card_file.rawValue)
        aCoder.encode(bank_name, forKey: Keys.bank_name.rawValue)
        aCoder.encode(bank_account_number, forKey: Keys.bank_account_number.rawValue)
        aCoder.encode(ms_countries_id, forKey: Keys.ms_countries_id.rawValue)
        aCoder.encode(ms_states_id, forKey: Keys.ms_states_id.rawValue)
        aCoder.encode(ms_districts_id, forKey: Keys.ms_districts_id.rawValue)
        aCoder.encode(ms_cities_id, forKey: Keys.ms_cities_id.rawValue)
        aCoder.encode(pan_no, forKey: Keys.pan_no.rawValue)
        aCoder.encode(pan_card_file, forKey: Keys.pan_card_file.rawValue)
        aCoder.encode(user_roles_id, forKey: Keys.user_roles_id.rawValue)
    }
    
    func setUserModel() {
        var encodedData: Data?
        if #available(iOS 12, *) {
            do {
                encodedData = try NSKeyedArchiver.archivedData(withRootObject: self, requiringSecureCoding: false)
            } catch let error {
                DebugLog("Error archiving User Model - \(error.localizedDescription)")
            }
        } else {
            encodedData = NSKeyedArchiver.archivedData(withRootObject: self)
        }
        UserPreferences.set(value: encodedData, forKey: UserPreferencesKeys.UserInfo.userDetail)
    }
    
    class func remove() {
        UserPreferences.remove(forKey: UserPreferencesKeys.UserInfo.userDetail)
    }
}
