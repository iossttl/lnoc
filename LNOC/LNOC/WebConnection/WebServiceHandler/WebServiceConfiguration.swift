//
//  WebServiceConfiguration.swift
//  SharkID
//
//  Created by Vikram Jagad on 25/02/19.
//  Copyright © 2019 sttl. All rights reserved.
//

import UIKit

let isBeta = true


//MARK:- Live
let liveBaseURL = "http://seva.mumbaicity.gov.in/apiv3/"
let liveWebPageBaseURL = "http://seva.mumbaicity.gov.in/"

//MARK:- Staging
let stageBaseURL = "http://lris.php-staging.com/apiv3/"
let stageWebPageBaseURL = "http://lris.php-staging.com/"

let BASEURL = isBeta ? stageBaseURL : liveBaseURL
let BASEURL_WebPage = isBeta ? stageWebPageBaseURL : liveWebPageBaseURL

var staticKey : String { return "1424513D65C-BD43-4031-9EE4-5CD98D9C3A6FJ12" }
var staticIv : String { return "afhbadaslkdf1123slkdkasknndfkls" }
var STATIC_IV_VALUE : String { return "2eqpgawYDPe5bJbrYxQVY7mqgpDspDOdRdhgzWpGFKQ=" }

struct WebViewAPI {
    /*static var Introduction:String { "http://sag.php-staging.com/introduction?mobile=yes" }
    static var Games:String { "http://sag.php-staging.com/games?mobile=yes" }
//    "https://nationalwarmemorial.gov.in/plan-your-visit/?mobile=Y" + "&lang=" + selectedLanguage.rawValue
    static var eventDetails:String{ "http://sag.php-staging.com/events/view?Mw==&mobile=yes" }
    static var newDetails :String {"http://sag.php-staging.com/news/view?Ng==&mobile=yes" }*/
    
    
    static var aboutUs:String { BASEURL_WebPage + "about-us?mobile=yes" }
    static var FAQ:String { BASEURL_WebPage + "faqs?mobile=yes" }
    static var contactUs:String{ BASEURL_WebPage + "contact-us?mobile=yes" }
}

struct API {
    static let login = "login"
    static let logout = "logout"
    static let changePassword = "changepassword"
    static let getLandList = "landlist"
    static let landdetail = "landdetail"
    static let land_favourite = "land_favourite"
    static let deleteland = "deleteland"
    static let linkland = "linkland"
    static let landfilterdropdown = "landfilterdropdown"
    static let getDashboard = "dashboard"
    static let getphotogallery = "photoalbums"
    static let getvideogallery = "videoalbums"
    static let getphotogalleryDetails = "photos"
    static let getvideogalleryDetails = "videos"
    static let getUpComingEventList = "upcomingEventList"
    static let getwhatsnewList = "whatsnew"
    static let forgetpassword = "forgetpassword"
    static let getplayerlist = "playerlist"
    
    static let getdistricts = "districts"
    static let getCity = "cities"
    static let getNOCType = "noctypes"
    static let getUseOfLand = "land_used_types"
    static let feedback = "feedback"
    static let applicationlist = "applicationlist"
    static let getSendMobileOTP = "sendmobileotp"
    static let register = "register"
    static let mobileOTPVerify = "mobileotpverify"
    static let getProfile = "getprofile"
    static let sendotp = "sendotp"
    static let verifyotp = "verifyotp"
    static let editprofile = "editprofile"
    static let notifications = "notifications"
    static let addLand = "rest/add_land"
    static let add_challan = "rest/add_challan"
    static let add_clearification = "add_clearification"
    static let applicationdetail = "applicationdetail"
    static let addNOCApplication = "add_application"
    static let getNOC_documents = "noc_documents"
    static let upload_nocdocument = "upload_nocdocument"
    static let getnotice_filters = "getnotice_filters"
    static let noticewarrent_list = "noticewarrent_list"
    static let deletewarrent = "deletewarrent"
    static let challan_list = "challan-list"
 }

struct WebUrlLink {
    static var aboutUs = "http://lris.php-staging.com/about-us?mobile=yes"
    static var FAQ = "http://lris.php-staging.com/faqs?mobile=yes"
    static var contactUs = "http://lris.php-staging.com/contact-us?mobile=yes"
    
}
//    http://lnoc.silvertouch-staging.com/about-us?mobile=yes&language=en
//
//    http://lnoc.silvertouch-staging.com/faqs?mobile=yes&language=en
//
//    http://lnoc.silvertouch-staging.com/contact-us?mobile=yes&language=en



//http://lris.php-staging.com/about-us?mobile=yes&language=en
